// series.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "series%03d";

import graph;



// ================= block stacking ===============
real block_width = 2;
real block_hgt = 1/4*block_width;
path block = (0,0)--(block_width,0)--(block_width,block_hgt)--(0,block_hgt)--cycle;

// Return an array of blocks, as they should be stacked, starting at origin
path[] blocks(int num_blocks) {
  real cent_mass[];  // cent_mass[i] is x value of center of mass of i-tall system
  cent_mass.push(0);
  for (int i=1; i<=num_blocks; ++i){
    cent_mass.push(cent_mass[i-1]+(1/(i)));
  }
  // write("cent_mass: ",cent_mass);

  real slide_over[];  // the overhang of the i-th block down from top
  for (int i=0; i<=num_blocks; ++i){
    slide_over.push(0);
  }
  for (int i=1; i<=num_blocks; ++i){
    for (int j=i; j <= num_blocks; ++j){
      slide_over[i]=slide_over[i]+1/(j);
    }
  }
  real slide_back = slide_over[num_blocks]; // slide them back to the origin
  for (int i=0; i<=num_blocks; ++i){
    slide_over[i]=slide_over[i]-slide_back;
  }
  // write("slide_over: ",slide_over);

  path[] T;
  for (int i=1; i<=num_blocks; ++i){
    T.append(shift(slide_over[i],(num_blocks-i)*block_hgt)*block);
  }
  return T;
}

// ........... blocks on x-axis .............

for (int num_blocks=2; num_blocks<=4; ++num_blocks) {
  picture pic;
  int picnum = 0+(num_blocks-2);
  size(pic,4cm,keepAspect=true);

  real xleft, xright, ybot, ytop; // limits of graph
  xleft=0; xright=4;
  ybot=0; ytop=2.5;

  for (path b : blocks(num_blocks)){
    filldraw(pic,b,FILLCOLOR, black);  
  };

  // x and y axes
  Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
  // The x axis
  xaxis(pic, L="",  
	// axis=YZero,
	xmin=xleft-0.2, xmax=xright+0.5,
	p=currentpen,
	ticks=RightTicks("%", Step=1, Size=2pt),
	arrow=EndArrow(TeXHead));
  labelx(pic, "$0$", 0);
  // for center of mass of num_block-sized system
  int numerator  = 2*num_blocks-1;
  int denominator = num_blocks;
  real T4[] = {numerator/denominator};
  xaxis(pic, L="",  
	// axis=YZero,
	xmin=xleft-0.2, xmax=xright+0.5,
	p=currentpen,
	ticks=RightTicks("%", T4, Size=3pt, highlight_color),
	arrow=EndArrow(TeXHead));
  labelx(pic, format("$%d", numerator)+format("/%d$", denominator), numerator/denominator, highlight_color);

  shipout(format(OUTPUT_FN,picnum),pic); 
}


// ........... blocks on table .............

for (int num_blocks=2; num_blocks<=4; ++num_blocks) {
  picture pic;
  int picnum = 3+(num_blocks-2);
  unitsize(pic,1cm);

  real xleft, xright, ybot, ytop; // limits of graph
  xleft=0; xright=4;
  ybot=0; ytop=2.5;

  // make the table as a separate picture so I can clip, then add
  picture tmp_pic;
  unitsize(tmp_pic, 1cm);
  
  path table = (-1,0)--(2,0)--(2,-1.0){W}..(1,-0.75)..{N}(-1,0)--cycle;
  for(int i=0; i<=20; ++i) {
    draw(tmp_pic, (-1+(i/5),-1)--(-1+(i+1)/5,0) , GRAPHPAPERPEN);
  }
  clip(tmp_pic, table);
  draw(tmp_pic, table);
  label(tmp_pic, Label("table",filltype=Fill(white)), (0.5,-0.3));
  add(pic, tmp_pic.fit(), (0,0));

  // for center of mass of num_block-sized system
  int numerator  = 2*num_blocks-1;
  int denominator = num_blocks;
  real overhang = 2-(numerator/denominator);
  for (path b : blocks(num_blocks)){
    filldraw(pic, shift(overhang,0)*b, FILLCOLOR, black);  
  };
  pair label_pt = (4.25,-0.5);
  draw(pic, label_pt{W}..{NW}(2.3,-0.15), arrow=EndArrow(ARROW_SIZE), highlight_color);
  label(pic, Label(format("overhang $\frac{1}{%d}$", num_blocks),filltype=Fill(white)), label_pt);

  shipout(format(OUTPUT_FN,picnum),pic); 
}




