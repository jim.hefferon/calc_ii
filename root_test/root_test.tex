\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 11.6\hspace{1em} Root and ratio tests}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Root and ratio tests}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage stub_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
% \input asy/arc_length_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



% \section{}


\begin{frame}
  \frametitle{Recall: geometric series}

Recall that a geometric series is one where the terms are in constant ratio,
\begin{equation*}
  S=\sum_{0\leq j}a\cdot r^j=a+a\cdot r+a\cdot r^2+a\cdot r^3+\cdots
\end{equation*}
so that $s_{n+1}=r\cdot s_n$. 
This series converges if $|r|<1$,
\begin{equation*}
  \sum_n ar^n= a+a\cdot r+a\cdot r^2+a\cdot r^3+\cdots
  =a\cdot \frac{1}{1-r}
\end{equation*}
while it diverges if $|r|\geq 1$. 
\end{frame}



\begin{frame}
\Tm (Ratio Test) Let $\sum_n a_n$ be a series such that this limit exists.
\begin{equation*}
  L=\lim_{n\to\infty}\biggl| \frac{a_{n+1}}{a_n} \biggr|
\end{equation*}
If $L$ is less than one then the series converges absolutely.
If $L$ is greater than one or is infinite then the series diverges.
If $L=1$ then this test in inconclusive; some such series converge
while others diverge.

\pause\medskip
\Pf If $L<1$ then choose $r$ with $L<r<1$.
By the definition of the limit os a sequence, 
there is an index $N$ large enough so that if $n>N$ then $|a_{n+1}/a_n|<r$.
That is, $|a_{N+1}|<r\cdot |a_N|$ and  $|a_{N+2}|<r\cdot |a_{N+1}|<r^2\cdot|a_N|$, 
\ldots\sentencespace
Therefore
the series $\sum_i a_n$ converges absolutely, by the Comparison Test
with the geometric series for $r$
\begin{equation*}
  \sum_{N\leq n}|a_n|\leq \sum_{0\leq n}r^n\cdot|a_N|=|a_N|\cdot\sum_{0\leq n}r^n
\end{equation*}
(recall that convergence or divergence depends only on the long-term
behavior of the terms, so we can just look at what happens past term~$N$).
\end{frame}

\begin{frame}
\Ex Use the Ratio Test to show that this converges.
\begin{equation*}
  \sum_{1\leq i} \frac{1}{i!}
\end{equation*}

\pause
Take the limit of the ratio of absolute values.
\begin{align*}
  \lim_{i\to\infty}\biggl| \frac{a_{i+1}}{a_i} \biggr|
  &=\lim_{i\to\infty}\biggl| \frac{1/(i+1)!}{1/i!} \biggr|  \\
  &=\lim_{i\to\infty} \frac{i!}{(i+1)!}   \\
  &=\lim_{i\to\infty} \frac{1}{i+1}  
  =0
\end{align*}
Note that we must write ``$\lim_{i\to\infty}$" on each line.
\end{frame}

\begin{frame}
\noindent\Ex Use the Ratio Test to decide convergence.
\begin{equation*}
  \sum_{1\leq k} \frac{5^k}{k!}
\end{equation*}
\pause
\begin{equation*}
  \lim_{k\to\infty}\biggl| \frac{a_{k+1}}{a_k} \biggr|
  =\lim_{k\to\infty}\biggl| \frac{5^{k+1}/(k+1)!}{5^k/k!} \biggr|  
  =\lim_{k\to\infty} \frac{5^{k+1}}{5^k}\cdot\frac{k!}{(k+1)!}   
  =\lim_{k\to\infty} \frac{5}{k+1}  
  =0
\end{equation*}
Described informally, the factorial function grows very quickly.
It even outgrows the powers of five, so that the 
terms shrink to zero fast enough that the series converges.

\pause
\Ex Try to apply the Ratio Test to $\sum_{j}j^2$.
\pause
\begin{equation*}
  \lim_{j\to\infty}\biggl| \frac{a_{j+1}}{a_j} \biggr|
  =\lim_{j\to\infty} \frac{(j+1)^2}{j^2} 
  =\lim_{j\to\infty} \frac{j^2+2j+1}{j^2}
  =1
\end{equation*}
The test does not apply. 
Using other things that we know, 
this is a series where the terms do not go to zero 
so this series diverges.

\pause
\Ex Try to apply the Ratio Test to $\sum_{1\leq j}j^{-2}$.
\pause
\begin{equation*}
  \lim_{j\to\infty}\biggl| \frac{a_{j+1}}{a_j} \biggr|
  =\lim_{j\to\infty} \frac{(j+1)^{-2}}{j^{-2}} 
  =\lim_{j\to\infty} \frac{j^2}{j^2+2j+1}
  =1
\end{equation*}
The test does not apply. 
Using other knowledge, this is a $p$-series with $p=2$ so it converges.
\end{frame}


\begin{frame}{Practice}
\begin{enumerate}
\item $\displaystyle \sum_n\frac{n}{3^n}$

\pause\smallskip
It converges: $\displaystyle \lim_{n\to\infty} \biggl| \frac{a_{n+1}}{a_n} \biggr|
  =\lim_{n\to\infty}\frac{(n+1)/3^{n+1}}{n/3^n}
  =\lim_{n\to\infty}\frac{n+1}{n\cdot 3}
  =\frac{1}{3}
  $.

\pause\item $\displaystyle \sum_n\frac{n^2}{2^n}$

\pause\smallskip
It converges: $\displaystyle \lim_{n\to\infty} \biggl| \frac{a_{n+1}}{a_n} \biggr|
  =\lim_{n\to\infty}\frac{(n+1)^2/2^{n+1}}{n^2/2^n}
  =\lim_{n\to\infty}\frac{n^2+2n+1}{n^2\cdot 2}
  =\frac{1}{2}
  $.


\pause\item $\displaystyle \sum_n\frac{4n-1}{n^2+1}$

\pause\smallskip
We have 
\begin{equation*}
  \lim_{n\to\infty}\frac{(4(n+1)-1)/((n+1)^2+1)}{(4n-1)/(n^2+1)}
  =\lim_{n\to\infty}\frac{(4n+3)(n^2+1)}{(4n-1)(n^2+2n+2)}
  =1  
\end{equation*}
so the Ratio Test provides no information.
(This series diverges via a Limit Comparison Test with the harmonic series.)
\end{enumerate}
\end{frame}


\begin{frame}
\begin{enumerate}\setcounter{enumi}{3}
\item $\displaystyle \sum_n (-1)^n\cdot\frac{n!}{5^n}$

\pause\smallskip
We have
\begin{equation*}
  \lim_{n\to\infty} \biggl| \frac{a_{n+1}}{a_n} \biggr|
  =\lim_{n\to\infty}\frac{(n+1)!/5^{n+1}}{n!/5^n}
  =\lim_{n\to\infty}\frac{n+1}{5}
  =\infty
\end{equation*}
so the Ratio Test gives that it diverges.

\pause
\item $\displaystyle \sum_n\frac{n^n}{n!}$

\pause\smallskip
Here is a start.
\begin{equation*}
  \lim_{n\to\infty} \biggl| \frac{a_{n+1}}{a_n} \biggr|
  =\lim_{n\to\infty}\frac{(n+1)^{n+1}/(n+1)!}{n^n/n!}
  =\lim_{n\to\infty}\frac{(n+1)^{n+1}}{n^n}\cdot\frac{1}{n+1}
\end{equation*}
Cancel the $n+1$'s.
\begin{equation*}
  =\lim_{n\to\infty}\frac{(n+1)^n}{n^n}
  =\lim_{n\to\infty}\biggl(\frac{n+1}{n}\biggr)^n
  =\lim_{n\to\infty}\biggl(1+\frac{1}{n}\biggr)^n
  =e
\end{equation*}
Because $e$ is greater than~$1$,
the Ratio Test gives divergence.
\end{enumerate}
\end{frame}



\section{Root Test}

\begin{frame}
\Tm (Root Test) Let $\sum_n a_n$ be a series such that this limit exists.
\begin{equation*}
  L=\lim_{n\to\infty} \sqrt[n]{ |a_n| }
\end{equation*}
If $L$ is less than one then the series converges absolutely.
If $L$ is greater than one or is infinite then the series diverges.
If $L=1$ then this test in inconclusive; some such series converge
while others diverge.

\Ex Use the Root Test to decide whether this is absolutely convergent, 
conditionally convergent, or divergent.
\begin{equation*}
  \sum_n(-1)^n\frac{2^{n+1}}{(n+3)^n}
\end{equation*}

\pause
Find the limit as here.
\begin{equation*}
  \lim_{n\to\infty} \sqrt[n]{ |a_n| }
  =\lim_{n\to\infty} \sqrt[n]{ \frac{2^{n+1}}{(n+3)^n} }
  =\lim_{n\to\infty} \frac{2^{(n+1)/n}}{n+3} 
  =\lim_{n\to\infty} \frac{2^{1+(1/n)}}{n+3} 
  =\lim_{n\to\infty} \frac{2\cdot 2^{1/n}}{n+3} 
\end{equation*}
Because $2^{1/n}=\sqrt[n]{2}$ goes to one, and the denominator goes to 
infinity, this limit is zero.
So the series is absolutely convergent.
\end{frame}


\begin{frame}{Practice}
Try to apply the Root Test to each.
\begin{enumerate}
\item $\displaystyle \sum_{1\leq k}\frac{5^k}{k^k}$

\pause\smallskip
This series converges absolutely.
\begin{equation*}
  \lim_{k\to\infty} \sqrt[k]{ |a_k| }
  =\lim_{k\to\infty} \sqrt[k]{ \frac{5^k}{k^k} }  
  =\lim_{k\to\infty} \frac{5}{k}
  =0  
\end{equation*}


\item $\displaystyle \sum_n \frac{(3n^2+4)^n}{(2n^2+n)^n}$

\pause\smallskip
The series diverges.
\begin{equation*}
  \lim_{n\to\infty} \sqrt[n]{ |a_n| }
  =\lim_{n\to\infty} \sqrt[n]{ \frac{(3n^2+4)^n}{(2n^2+n)^n} }  
  =\lim_{n\to\infty} \frac{3n^2+4}{2n^2+n}
  =\frac{3}{2}
\end{equation*}

\item $\displaystyle \sum_{1\leq i} (-1)^{i+1}(1/i)^i$

\pause\smallskip
It converges absolutely.
\begin{equation*}
  \lim_{i\to\infty} \sqrt[i]{ |a_i| }
  =\lim_{i\to\infty} \sqrt[i]{ (1/i)^i }  
  =\lim_{i\to\infty} \frac{1}{i}
  =0  
\end{equation*}

\end{enumerate}
\end{frame}



\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
