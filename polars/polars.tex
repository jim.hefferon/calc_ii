\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 10.3\hspace{1em} Polar coordinates}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Polar coordinates}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage arc_length_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
% \input asy/arc_length_3d.pre
% \usepackage[bigfiles]{media9}
% \RequirePackage{asymptote}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



% \section{}


\begin{frame}
  \frametitle{Polar coordinates}
In place of the usual rectangular coordinate system giving $(x,y)$,
sometimes polar coordinates are convenient.
\begin{center}
  \vcenteredhbox{\includegraphics{polars000.pdf}}
\end{center}
The conversions are straightforward.
\begin{center}
  \begin{tabular}[t]{l}
    $x=r\cos\theta$  \\
    $y=r\sin\theta$
  \end{tabular}
  \qquad
  \begin{tabular}[t]{l}
    $r=\sqrt{x^2+y^2}$  \\
    $\theta=\arctan(y/x)$
  \end{tabular}
\end{center}
Notes:
\begin{itemize}
\item Angles are not uniquely specified:
  $\theta$ and $\theta+n\cdot 2\pi$ ($n$ an integer) 
  describe the same angle.
  (Sometimes we restrict to $\theta$ in $\leftclosed{0}{2\pi}$.)
\item We allow negative $r$.
  (Sometimes we restrict to $r\geq 0$.)
\item No angle is associated with $r=0$. 
\end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Polar functions}
We may specify a function as $r=f(\theta)$.
\begin{center}
  \vcenteredhbox{\includegraphics{polars001.pdf}}
\end{center}
Some curves are easiest to understand in polars.
A circle of radius~$r=2$ is $f(\theta)=2$.
A line through the origin is $\theta=k$ for some constant~$k$.

\pause
\end{frame}


\begin{frame}
\Ex Here is the function $r=f(\theta)=1.5\cdot\theta$.  
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$\theta$} 
      &\multicolumn{1}{c}{$r=1.5\cdot \theta$} \\ \hline
    \uncover<1->{$0$} &\uncover<1->{$0$} \\
    \uncover<2->{$\pi/6$} &\uncover<2->{$0.785$} \\
    \uncover<3->{$\pi/4$} &\uncover<3->{$1.178$} \\
    \uncover<4->{$\pi/3$} &\uncover<4->{$1.571$} \\
    \uncover<5->{$\pi/2$} &\uncover<5->{$2.356$} \\
    \uncover<6->{$\pi/2+\pi/6$} &\uncover<6->{$3.142$} \\
    \uncover<7->{$\pi/2+\pi/4$} &\uncover<7->{$3.534$} \\
    \uncover<8->{$\pi/2+\pi/3$} &\uncover<8->{$3.927$} \\
    \uncover<9->{$\pi/2+\pi/2$} &\uncover<9->{$4.712$} \\
  \end{tabular}
  \qquad
  % \vcenteredhbox{\includegraphics{polars003.pdf}}
  \only<1>{\vcenteredhbox{\includegraphics{polars021.pdf}}}%
  \only<2>{\vcenteredhbox{\includegraphics{polars022.pdf}}}%
  \only<3>{\vcenteredhbox{\includegraphics{polars023.pdf}}}%
  \only<4>{\vcenteredhbox{\includegraphics{polars024.pdf}}}%
  \only<5>{\vcenteredhbox{\includegraphics{polars025.pdf}}}%
  \only<6>{\vcenteredhbox{\includegraphics{polars026.pdf}}}%
  \only<7>{\vcenteredhbox{\includegraphics{polars027.pdf}}}%
  \only<8>{\vcenteredhbox{\includegraphics{polars028.pdf}}}%
  \only<9>{\vcenteredhbox{\includegraphics{polars029.pdf}}}%
\end{center}
The dotted line has length about $r=8.247$.
\end{frame}
% sage: A = [ 0, pi/6, pi/4, pi/3, pi/2, 7*pi/4 ]
% sage: for t in A:
% ....:     n(1.5*t)
% ....: 
% 0.000000000000000
% 0.785398163397448
% 1.17809724509617
% 1.57079632679490
% 2.35619449019234
% 8.24668071567321
% sage: A = [pi/6, pi/4, pi/3, pi/2]
% sage: for t in A:
% ....:     round(1.5*(t+pi/2), ndigits=3)
% ....: 
% 3.142
% 3.534
% 3.927
% 4.712


\begin{frame}
\Ex Here is the function $r=2\cos\theta$.  
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$\theta$} 
      &\multicolumn{1}{c}{$r=f(\theta)$} \\ \hline
    $0$ &$2$ \\
    $\pi/6$ &$\sqrt{3}\approx 1.732$ \\
    $\pi/4$ &$\sqrt{2}\approx 1.414$ \\
    $\pi/3$ &$1$ \\
    $\pi/2$ &$0$ \\
    $\pi$ &$-2$ \\
  \end{tabular}
  \qquad
  \vcenteredhbox{\includegraphics{polars002.pdf}}
\end{center}
The dotted line has length about $1.732$.
Notice that at $\theta=\pi$~radians the radius is negative.
\end{frame}
% sage: n(sqrt(3))
% 1.73205080756888
% sage: n(sqrt(2))
% 1.41421356237310




\begin{frame}{Roses}
\begin{center}
  \begin{tabular}{c}
    \vcenteredhbox{\includegraphics{polars004.pdf}}  \\
    $r=\cos 2\theta$, $0\leq\theta<2\pi$
  \end{tabular}
  \qquad
  \begin{tabular}{c}
    \vcenteredhbox{\includegraphics{polars005.pdf}}  \\
    $r=\cos 3\theta$, $0\leq\theta<\pi$
  \end{tabular}
\end{center}

This shows how the four petal rose is traversed.
\begin{center}
  \only<1>{\vcenteredhbox{\includegraphics{polars013.pdf}}}%
  \only<2>{\vcenteredhbox{\includegraphics{polars014.pdf}}}%
  \only<3>{\vcenteredhbox{\includegraphics{polars015.pdf}}}%
  \only<4>{\vcenteredhbox{\includegraphics{polars016.pdf}}}%
  \only<5>{\vcenteredhbox{\includegraphics{polars017.pdf}}}%
  \only<6>{\vcenteredhbox{\includegraphics{polars018.pdf}}}%
  \only<7>{\vcenteredhbox{\includegraphics{polars019.pdf}}}%
  \only<8>{\vcenteredhbox{\includegraphics{polars020.pdf}}}%
\end{center}
\end{frame}




\section{Area}

\begin{frame}{Area in polar coordinates}
We want the area of this region.
\begin{center}
  \vcenteredhbox{\includegraphics{polars006.pdf}}%  
\end{center}
\end{frame}

\begin{frame}
The formula that we derive depends on two things.
First, we break into subregions that are each $\Delta\theta$~wide.
These are not rectangles,
instead they are wedge-shaped.
\begin{center}
    \vcenteredhbox{\includegraphics{polars007.pdf}} 
\end{center}
We will find a formula for the area of each subregion, and for the sum of all
of these areas, and then take the limit as $\Delta\theta$ goes to zero.

\pause
Second, as we have seen several times, to get an integral expression we need
to put the expression from the prior paragraph 
in a form that matches the definition of the integral.
\begin{equation*}
  \integral{\theta=a}{b}{f(\theta)}{\theta}
  =\lim_{\Delta\theta\to 0}\,\Bigl[ \sum_{i} f(\theta_i^*)\,\Delta \theta\Bigr]
  \qquad\text{where $\theta_i^*$ in interval $i$}
\end{equation*}
\end{frame}


\begin{frame}
The area of a $\Delta\theta$-wide wedge 
of a circle $(1/2) r^2\cdot \Delta \theta$.
(There are $2\pi$ radians in a circle, which has a total area of $\pi r^2\!$.
So the area of a wedge is 
$(\Delta\theta/2\pi)\cdot\pi r^2$, which is $(1/2)r^2\Delta\theta$.)

But the gold region that we are measuring has wiggles at the end:~what
$r$ to use?
\begin{center}
    \vcenteredhbox{\includegraphics{polars008.pdf}}%  
\end{center}
\pause 
The Mean Value Theorem says that there
is an angle $\theta_i^*$ between the ends such that the associated radius 
$r_i^*=f(\theta_i^*)$ gives a circular 
wedge, shown in burgundy, whose area
equals the gold region.

Consequently we define the total area with this limit.
\begin{equation*}
  \text{Area}
  % =\lim_{\Delta\theta\to 0}\biggl( \sum_i \frac{1}{2} (r_i^*)^2\Delta\theta\biggr)
  =\lim_{\Delta\theta\to 0}\Biggl[ \sum_i \frac{1}{2} f(\theta_i^*)^2\Delta\theta\Biggr]
\end{equation*}%
\end{frame}




\begin{frame}{Definition of area}
\Df Let $f$ be continuous.
The region bounded by the curve $r=f(\theta)$ 
that is swept out between the rays $\theta=a$ and
$\theta=b$ has this area.
\begin{equation*}
  \integral{\theta=a}{b}{ \frac{1}{2}r^2 }{\theta}
  =
  \integral{a}{b}{ \frac{1}{2}f(\theta)^2 }{\theta}
\end{equation*}
\begin{center}
    \vcenteredhbox{\includegraphics{polars006.pdf}}%  
\end{center}

\pause
Contrast this with the 
$\integral{a}{b}{ f(x) }{x}$ 
from rectangular coordinates. 
Here, the $1/2$ and the square in $f(\theta_i^*)^2$ appear because we have
decomposed the region into
subregions that are wedges, not rectangles.
\end{frame}


\begin{frame}
\Ex Find the area of the \alert{cartioid} $r=1+\cos\theta$ for $\theta$
in $\leftclosed{0}{2\pi}$.
\begin{center}
    \vcenteredhbox{\includegraphics{polars009.pdf}}%  
\end{center}
\pause
\begin{align*}
  \integral{\theta=0}{2\pi}{ \frac{1}{2}(1+\cos\theta)^2 }{\theta}
  &=\frac{1}{2}\integral{0}{2\pi}{ 1+2\cos\theta+\cos^2\theta }{\theta} \\
  &=\frac{1}{2}\integral{0}{2\pi}{ 1+2\cos\theta+\frac{1+\cos 2\theta}{2} }{\theta} \\
  &=\frac{1}{2}\integral{0}{2\pi}{ \frac{3}{2}+2\cos\theta+\frac{1}{2}\cos 2\theta }{\theta}  \\
  &=\frac{1}{2}\cdot\Bigl[ \frac{3}{2}\theta+2\sin\theta+\frac{1}{4}\sin 2\theta \Bigr]_{0}^{2\pi}
   =\frac{3}{2}\pi
\end{align*}
\end{frame}

\begin{frame}{Practice}
\Ex Find the area enclosed by one petal of the rose given by 
$f(\theta)=3\sin(2\theta)$.
\begin{center}
    \vcenteredhbox{\includegraphics{polars010.pdf}}%  
\end{center}

\pause
The relevant angles are $\theta=0$ to $\theta=\pi/2$.
\begin{align*}
  \frac{1}{2}\cdot\integral{\theta=0}{\pi/2}{ (3\sin 2\theta)^2 }{\theta}
  =\frac{9}{2}\cdot\integral{\theta=0}{\pi/2}{ \sin^2 2\theta }{\theta}  
  &=\frac{9}{2}\cdot\integral{\theta=0}{\pi/2}{ \frac{1-\cos 4\theta}{2} }{\theta}  \\
  &=\frac{9}{4}\cdot\integral{\theta=0}{\pi/2}{ 1-\cos 4\theta }{\theta}  
\end{align*}
Antidifferentiating gives this.
\begin{equation*}
  \frac{9}{4}\netchange{0}{\pi/2}{\theta-\frac{\sin 4\theta}{4}}
  =\frac{9\pi}{8}
\end{equation*}
\end{frame}

\begin{frame}
The area between two curves is this.
\begin{equation*}
  \frac{1}{2}\integral{\theta=a}{b}{ g(\theta)^2-f(\theta)^2 }{\theta}
\end{equation*}

\Ex Set up but do not solve 
the integral for the area enclosed by $r=2+2\sin\theta$
and $r=6\sin\theta$ where $0\leq\theta\leq 2\pi$.
\begin{center}
    \vcenteredhbox{\includegraphics{polars011.pdf}}%  
\end{center}

\pause
Setting $2+2\sin\theta=6\sin\theta$ gives $\sin\theta =1/2$, so
the limits of integration are from $\theta=\pi/6$ to $\theta=5\pi/6$.
\begin{equation*}
  \text{Area}
  =\frac{1}{2}\Biggl[\integral{\theta=\pi/6}{5\pi/6}{ (6\sin\theta)^2 }{\theta}
  -
  \integral{\theta=\pi/6}{5\pi/6}{ (2+2\sin\theta)^2 }{\theta}\biggr]
\end{equation*}
Antidifferentiating gives $4\pi$.
\end{frame}

\begin{frame}
\textit{Caution:}\hspace{0.4em}
To get the area right make sure the curve is traced out exactly once 
as $\theta$ sweeps from $a$ to~$b$.

\Ex Find the area inside the circle $r=\cos\theta$.
\begin{center}
  \includegraphics{polars012.pdf}
\end{center}

\pause 
The answer here is not right;~what did we do wrong?
\begin{equation*}
  \integral{\theta=0}{2\pi}{ \frac{1}{2}\cos^2\theta }{\theta}
  =\frac{1}{4}\cdot\integral{0}{2\pi}{ 1+\cos 2\theta }{\theta}
  =\frac{1}{4}\cdot\netchange{0}{2\pi}{ \theta+\frac{\sin 2\theta}{2} }
  =\frac{2\pi}{4}
\end{equation*}
\pause
The mistake is that $r\cos\theta$ goes around the circle once when 
$\theta$ goes from~$0$ to~$\pi$.
Since this integral went twice around the circle, it double-counts the area. 
\end{frame}



\section{Arc length}
\begin{frame}{Arc length in polars}
Recall that for a curve parametrized with a variable~$t$ the
arc length is $\integral{t=a}{b}{ \sqrt{x'(t)^2+y'(t)^2} }{t}$. 
For polars, parametrize the curve as here.
\begin{equation*}
  x=x(\theta)=r\cos\theta=f(\theta)\cdot\cos\theta
  \qquad
  y=y(\theta)=r\sin\theta=f(\theta)\cdot\sin\theta
\end{equation*}
Take the derivatives with the Product Rule.
\begin{equation*}
  x'(\theta)=-f(\theta)\sin\theta+f'(\theta)\cos\theta
  \qquad
  y'(\theta)=f(\theta)\cos\theta+f'(\theta)\sin\theta
\end{equation*}
\pause
Some algebra
\begin{align*}
  x'(\theta)^2+y'(\theta)^2
  &=f(\theta)^2\sin^2\theta-2f(\theta)f'(\theta)\sin\theta\cos\theta+f'(\theta)^2\cos^2\theta \\
  &\qquad
  +f(\theta)^2\cos^2\theta+2f(\theta)f'(\theta)\cos\theta\sin\theta+f'(\theta)^2\sin^2\theta \\
  &=f(\theta)^2\cdot(\sin^2\theta+\cos^2\theta)
    +f'(\theta)^2\cdot(\cos^2\theta+\sin^2\theta)
\end{align*}
gives that 
$x'(\theta)^2+y'(\theta)^2=f(\theta)^2+f'(\theta)^2\!$.

\Tm In polar coordinates, the formula for arc length is this.
\begin{equation*}
  s=\integral{\theta=a}{b}{ \sqrt{f(\theta)^2+f'(\theta)^2} }{\theta}
   =\integral{a}{b}{ \sqrt{r^2+\biggl(\frac{d\,r}{d\theta}\biggr)^2} }{\theta}
\end{equation*}
\end{frame}



\begin{frame}
\Ex Find the length of the curve of the cartiod $r=1+\cos\theta$.  
\begin{center}
    \vcenteredhbox{\includegraphics{polars009.pdf}}%  
\end{center}

\pause We find the length above the $x$~axis and double 
(so we can take the square root of the square).
The derivative is $f'(\theta)=-\sin\theta$.
\begin{align*}
  2\integral{\theta=0}{\pi}{ \sqrt{(1+\cos\theta)^2+(-\sin\theta)^2} }{\theta}
  &=2\integral{0}{\pi}{ \sqrt{1+2\cos\theta+\cos^2\theta+\sin^2\theta} }{\theta} \\
  &=2\integral{0}{\pi}{ \sqrt{2+2\cos\theta} }{\theta}
  =2\sqrt{2}\integral{0}{\pi}{ \sqrt{1+\cos\theta} }{\theta} \\
  &=2\sqrt{2}\integral{0}{\pi}{ \sqrt{2\cos^2(\theta/2)} }{\theta} \\
  &=4\integral{0}{\pi}{ \left|\cos(\theta/2)\right| }{\theta} 
  =4\integral{0}{\pi}{ \cos(\theta/2) }{\theta} \\
  &=4\Bigl[ 2\cdot\sin(\theta/2)\Bigr]_0^{\pi}=8
\end{align*}
\end{frame}

\begin{frame}
Set up a definite integral to give the arc length.
\begin{enumerate}
\item $r=1+\sin\theta$ for $0\leq\theta\leq \pi/2$

\pause
$\displaystyle \integral{\theta=0}{\pi/2}{ \sqrt{(1+\sin\theta)^2+\cos^2\theta} }{\theta}$

\pause\item $r=4\cos\theta$ for $0\leq\theta\leq \pi/2$

\pause
$\displaystyle \integral{\theta=0}{\pi/2}{ \sqrt{ 16\cos^2\theta+16\sin^2\theta} }{\theta}$
\end{enumerate}

\pause\Ex
Find the arc length of the curve $r=f(\theta)=3$ from $\theta=-\pi/2$
to $\theta=3\pi/2$.

\pause
\begin{equation*}
  \integral{\theta=-\pi/2}{3\pi/2}{ \sqrt{(3)^2+(0)^2} }{\theta}
  =3\cdot\integral{\theta=-\pi/2}{3\pi/2}{ 1 }{\theta}
  =3\cdot\netchange{\theta=-\pi/2}{3\pi/2}{\theta}
  =3\cdot 2\pi
\end{equation*}
\end{frame}



\begin{frame}
\textit{Caution}\hspace{0.4em}As earlier, to get the length of the curve 
right make sure that the curve is traced out exactly once 
as $\theta$ sweeps from $a$ to~$b$.

\pause
\Ex If we parametrize a radius $r=3$ circle so as to go around twice
\begin{equation*}
  F(t)=(3\cos\theta,3\sin\theta)
  \qquad
  0\leq t<4\pi
\end{equation*}
then the arc length will be twice the length of the curve, the circumference.
\begin{equation*}
  \integral{\theta=0}{4\pi}{ \sqrt{f(\theta)^2+f'(\theta)^2} }{\theta}
  =\integral{0}{4\pi}{ \sqrt{3^2+0^2} }{\theta}
  =\integral{0}{4\pi}{ 3 }{\theta}
  =\Bigl[ 3\theta \Bigr]_{0}^{4\pi}
  =12\pi
\end{equation*}
\end{frame}


\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
