// polars.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "polars%03d";

import graph;

// These conversions work in radians
pair polar_to_rect(pair p) {
  return (p.x*cos(p.y), p.x*sin(p.y));
}
pair rect_to_polar(pair p) {
  return (sqrt( p.x^2+p.y^2 ), atan(p.y/p.x));
}

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);
path general_polar_fcn = (-0.25,2)..(1,2.1)..(2,1.2)..(3,0.75)..(4,-0.25);

// =========== Defn of polar coords ===============

picture pic;
int picnum = 0;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2.5;
ybot=0; ytop=1.5;

real r = 2;
real theta = 30;  // degrees

pair p_in_radians = (r,radians(theta));
pair p_in_rect = polar_to_rect( (r,radians(theta)) );

path line_to_p = (0,0)--p_in_rect;
draw(pic, Label("$\theta$",MidPoint,highlight_color),  arc((0,0), 0.4, 0, theta), arrow=EndArrow);
draw(pic, Label("$r$",MidPoint,highlight_color), reverse(line_to_p));

dotfactor = 4;
dot(pic, Label("$(r,\theta)$", N, highlight_color),  p_in_rect);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=NoTicks,  // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ======= general polar function ==========
picture pic;
int picnum = 1;
size(pic,0,3cm,keepAspect=true);
path f = general_polar_fcn;

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=2;

draw(pic, f, FCNPEN);

real tm = 2.1; // time on the curve
pair p = point(general_polar_fcn, tm);
path line_to_p = (0,0)--p;
draw(pic, line_to_p);
draw(pic, Label("$\theta$",MidPoint,highlight_color),  arc((0,0), 0.4, 0, theta), arrow=EndArrow);
dotfactor = 5;
dot(pic, "$(r,\theta)=(f(\theta),\theta)$", p, NE, highlight_color);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);





// ======= some polar functions ==========
pair F2(real theta) {
  return polar_to_rect( (2*cos(theta), theta) );
}

picture pic;
int picnum = 2;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=-1; ytop=1;

path f = graph(pic, F2, 0, pi);
draw(pic, f, FCNPEN);
draw(pic, (0,0)--F2(pi/6), dotted);

dotfactor = 4;
dot(pic, Label("$\theta=0$ or $\pi$",filltype=Fill(white)), F2(0), highlight_color);
dot(pic, Label("$\theta=\pi/6$",filltype=Fill(white)), F2(pi/6), NE, highlight_color);
dot(pic, Label("$\theta=\pi/4$",filltype=Fill(white)), F2(pi/4), N, highlight_color);
dot(pic, Label("$\theta=\pi/3$",filltype=Fill(white)), F2(pi/3), NW, highlight_color);
dot(pic, Label("$\theta=\pi/2$",filltype=Fill(white)), F2(pi/2), W, highlight_color);
// dot(pic, Label("$\theta=\pi$, $\theta=\pi$",filltype=Fill(white)), F2(pi), highlight_color);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-1.25, xmax=xright+1.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ......... spiral ......................
pair F3(real theta) {
  return polar_to_rect( (1.5*theta, theta) );
}

picture pic;
int picnum = 3;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1.5*3*pi; xright=1.5*3*pi-5;
ybot=-8; ytop=14;

path f = graph(pic, F3, 0, 3*pi+0.1);
draw(pic, f, FCNPEN);

draw(pic, (0,0)--F3(7*pi/4), dotted);
dotfactor = 4;
real[] angles = {0, pi/6, pi/4, pi/3, pi/2};
for (real t : angles) {
    dot(pic, F3(t), highlight_color);
}
dot(pic, "$\theta=7\pi/4$", F3(7*pi/4), SE, highlight_color);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-1.25, xmax=xright+1.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ......... roses, n=2 ......................
pair F4(real theta) {
  return polar_to_rect( (cos(2*theta), theta) );
}

picture pic;
int picnum = 4;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-1; ytop=1;

path f = graph(pic, F4, 0, 2*pi);
draw(pic, f, FCNPEN);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ......... roses, n=3 ......................
pair F5(real theta) {
  return polar_to_rect( (cos(3*theta), theta) );
}

picture pic;
int picnum = 5;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-1; ytop=1;

path f = graph(pic, F5, 0, pi);
draw(pic, f, FCNPEN);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ======= Integration ==========
picture pic;
int picnum = 6;
size(pic,0,3cm,keepAspect=true);
path f = general_polar_fcn;

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=2;

draw(pic, f, FCNPEN);

// the region to be integrated
real th_start = 20; // starting theta
real th_end = 50; // ending theta
real tm_start = intersections(f, (0,0), (cos(radians(th_start)),sin(radians(th_start))))[0];
real tm_end = intersections(f, (0,0), (cos(radians(th_end)),sin(radians(th_end))))[0];
path region = ( (0,0)--point(f,tm_start)
		&subpath(f, tm_start, tm_end)
		&point(f, tm_end)--(0,0) )--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, point(f,tm_start)--(0,0)--point(f,tm_end));

// real th_i; 
// real tm;  // curve time
// for (int i = 0; i<10; ++i ) {
//   th_i = radians(th+2*i);
//   tm = intersections(f, (0,0), (cos(th_i),sin(th_i)))[0];
//   draw(pic, (0,0)--point(f,tm), highlight_color);
// }
// mark starting and ending thetas
dotfactor = 4;
dot(pic, "$\theta=a$", point(f,tm_start), NE, highlight_color);
dot(pic, "$\theta=b$", point(f,tm_end), NE, highlight_color);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ..... lots of subthetas ........
picture pic;
int picnum = 7;
size(pic,0,3cm,keepAspect=true);
path f = general_polar_fcn;

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=2;

draw(pic, f, FCNPEN);

// the region to be integrated
real th_start = 20; // starting theta
real th_end = 50; // ending theta
real tm_start = intersections(f, (0,0), (cos(radians(th_start)),sin(radians(th_start))))[0];
real tm_end = intersections(f, (0,0), (cos(radians(th_end)),sin(radians(th_end))))[0];
path region = ( (0,0)--point(f,tm_start)
		&subpath(f, tm_start, tm_end)
		&point(f, tm_end)--(0,0) )--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, point(f,tm_start)--(0,0)--point(f,tm_end));

// draw the subintervals
int num_intervals = 10; 
for (int i = 0; i<num_intervals; ++i ) {
  real rads = radians( th_start+i*(th_end-th_start)/num_intervals );
  real tm = intersections(f, (0,0), (cos(rads),sin(rads)))[0];
  draw(pic, (0,0)--point(f,tm), highlight_color);
}

// mark starting and ending thetas
dotfactor = 4;
dot(pic, "$\theta=a$", point(f,tm_start), NE, highlight_color);
dot(pic, "$\theta=b$", point(f,tm_end), NE, highlight_color);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ..... one subtheta ........
picture pic;
int picnum = 8;
size(pic,0,3cm,keepAspect=true);
path f = (2.5,2.25){E}..(2.75,1.75)..(3,1.5)..(3.25,1.25);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=2;

draw(pic, f, FCNPEN);

// the region to be integrated
real th_start = 25; // starting theta
real th_end = 35; // ending theta
real tm_start = intersections(f, (0,0), (cos(radians(th_start)),sin(radians(th_start))))[0];
real tm_end = intersections(f, (0,0), (cos(radians(th_end)),sin(radians(th_end))))[0];
path region = ( (0,0)--point(f,tm_start)
		&subpath(f, tm_start, tm_end)
		&point(f, tm_end)--(0,0) )--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, point(f,tm_start)--(0,0)--point(f,tm_end));  // rays at region sides

label(pic, "$\Delta \theta$", (0.4,0.3), 2*N, highlight_color);

real th_star = 28;
real tm_star = intersections(f, (0,0), (cos(radians(th_star)),sin(radians(th_star))))[0];
path ell = (0,0)--point(f,tm_star);
draw(pic, ell, highlight_color+dotted);
draw(pic, arc((0,0), arclength(ell), th_start, th_end), highlight_color);
dotfactor = 4;
dot(pic, "$(f(\theta_i^*),\theta_i^*)$", point(f,tm_star), NE, highlight_color);

shipout(format(OUTPUT_FN,picnum),pic);




// ============= cartioid area ============
pair F9(real theta) {
  return polar_to_rect( (1+cos(theta), theta) );
}

picture pic;
int picnum = 9;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-0.25; xright=2.15;
ybot=-1.25; ytop=1.25;

path f = graph(pic, F9, 0, 2*pi);
filldraw(pic, f--cycle, FILLCOLOR, FCNPEN);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ......... area of one petal of rose, r=3 sin 2theta ......................
pair F10(real theta) {
  return polar_to_rect( (3*sin(2*theta), theta) );
}

picture pic;
int picnum = 10;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-3; ytop=3;

path f = graph(pic, F10, 0, 2*pi);
filldraw(pic, f--cycle, FILLCOLOR, FCNPEN);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.3, xmax=xright+0.3,
      p=currentpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.3, ymax=ytop+0.3,
      p=currentpen,
      ticks=LeftTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ......... area between two regions ......................
pair F11(real theta) {
  return polar_to_rect( (2+2*sin(theta), theta) );
}
pair F11a(real theta) {
  return polar_to_rect( (6*sin(theta), theta) );
}

picture pic;
int picnum = 11;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-6; xright=6;
ybot=-1; ytop=6;

path f = graph(pic, F11, 0, 2*pi);
path fa = graph(pic, F11a, 0, 2*pi);

// the parts that bound the region
path f_edge = graph(pic, F11, pi/6, 5*pi/6);
path fa_edge = graph(pic, F11a, pi/6, 5*pi/6);

path region = ( f_edge
		&reverse( fa_edge ))--cycle;
fill(pic, region, FILLCOLOR);

draw(pic, f, highlight_color);
draw(pic, fa, black);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.3, xmax=xright+0.3,
      p=currentpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.3, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




//  area of a circle =================
pair F12(real theta) {
  return polar_to_rect( (cos(theta), theta) );
}

picture pic;
int picnum = 12;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1;
ybot=-1; ytop=1;

path f = graph(pic, F12, 0, pi);

filldraw(pic, f--cycle , FILLCOLOR, FCNPEN);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// x-axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.3, xmax=xright+0.3,
      p=currentpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=LeftTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ......... roses, n=2 ......................
// pair F4(real theta) {
//   return polar_to_rect( (cos(2*theta), theta) );
// }

for (int halfleaf=1; halfleaf<=8;  ++halfleaf) {
  picture pic;
  int picnum = 13+halfleaf-1;
  size(pic,0,4cm,keepAspect=true);

  real xleft, xright, ybot, ytop; // limits of graph
  xleft=-1; xright=1;
  ybot=-1; ytop=1;

  path f = graph(pic, F4, 0, 2*pi);
  draw(pic, f, FCNPEN);

  dotfactor = 4;
  if (halfleaf==1) {
    dot(pic,"$\frac{\pi}{12}$", F4(pi/12), N, highlight_color);
    dot(pic,"$\frac{2\pi}{12}$", F4(2*pi/12), N, highlight_color);
    dot(pic,"$\frac{3\pi}{12}$", F4(3*pi/12), N, highlight_color);
  }
  if (halfleaf==2) {
    dot(pic,"$\frac{4\pi}{12}$", F4(4*pi/12), W, highlight_color);
    dot(pic,"$\frac{5\pi}{12}$", F4(5*pi/12), W, highlight_color);
    dot(pic,"$\frac{6\pi}{12}$", F4(6*pi/12), W, highlight_color);
  }
  if (halfleaf==3) {
    dot(pic,"$\frac{7\pi}{12}$", F4(7*pi/12), E, highlight_color);
    dot(pic,"$\frac{8\pi}{12}$", F4(8*pi/12), E, highlight_color);
    dot(pic,"$\frac{9\pi}{12}$", F4(9*pi/12), E, highlight_color);
  }
  if (halfleaf==4) {
    dot(pic,"$\frac{10\pi}{12}$", F4(10*pi/12), N, highlight_color);
    dot(pic,"$\frac{11\pi}{12}$", F4(11*pi/12), N, highlight_color);
    dot(pic,"$\frac{12\pi}{12}$", F4(12*pi/12), N, highlight_color);
  }
  if (halfleaf==5) {
    dot(pic,"$\frac{13\pi}{12}$", F4(13*pi/12), S, highlight_color);
    dot(pic,"$\frac{14\pi}{12}$", F4(14*pi/12), S, highlight_color);
    dot(pic,"$\frac{15\pi}{12}$", F4(15*pi/12), S, highlight_color);
  }
  if (halfleaf==6) {
    dot(pic,"$\frac{16\pi}{12}$", F4(16*pi/12), E, highlight_color);
    dot(pic,"$\frac{17\pi}{12}$", F4(17*pi/12), E, highlight_color);
    dot(pic,"$\frac{18\pi}{12}$", F4(18*pi/12), E, highlight_color);
  }
  if (halfleaf==7) {
    dot(pic,"$\frac{19\pi}{12}$", F4(19*pi/12), W, highlight_color);
    dot(pic,"$\frac{20\pi}{12}$", F4(20*pi/12), W, highlight_color);
    dot(pic,"$\frac{21\pi}{12}$", F4(21*pi/12), W, highlight_color);
  }
  if (halfleaf==8) {
    dot(pic,"$\frac{22\pi}{12}$", F4(22*pi/12), S, highlight_color);
    dot(pic,"$\frac{23\pi}{12}$", F4(23*pi/12), S, highlight_color);
    dot(pic,"$\frac{24\pi}{12}$", F4(24*pi/12), S, highlight_color);
  }

  // x and y axes
  Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
  // The x axis
  xaxis(pic, L="",  
	axis=YZero,
	xmin=xleft-0.2, xmax=xright+0.2,
	p=currentpen,
	ticks=NoTicks,
	arrow=Arrows(TeXHead));

  // y axis
  Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
  yaxis(pic, L="",  
	axis=XZero,
	ymin=ybot-0.2, ymax=ytop+0.2,
	p=currentpen,
	ticks=NoTicks,
	arrow=Arrows(TeXHead));

  shipout(format(OUTPUT_FN,picnum),pic);
}





// ======= spiral =========================
pair F21(real theta) {
  return polar_to_rect( (1.5*theta, theta) );
}

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1.5*3*pi; xright=1.5*3*pi-5;
ybot=-8; ytop=14;

path f = graph(pic, F21, 0, 3*pi+0.1);

real angles[] = {0, pi/6, pi/4, pi/3, pi/2, pi/2+pi/6, pi/2+pi/4, pi/2+pi/3, pi};
for (int angle_no=0; angle_no<angles.length;  ++angle_no) {
  picture pic;
  int picnum = 21+angle_no;
  size(pic,0,4cm,keepAspect=true);

  draw(pic, f, FCNPEN);

  real angle = angles[angle_no];
  real line_len = max(5, length(F21(angle))+0.5);
  draw(pic, (0,0)--line_len*dir(degrees(angle)), highlight_color+white);
  dotfactor = 5;
  dot(pic, F21(angle), highlight_color);
  // dot(pic, "$\theta=7\pi/4$", F3(7*pi/4), SE, highlight_color);

  // The x axis
  xaxis(pic, L="",  
	axis=YZero,
	xmin=xleft-1.25, xmax=xright+1.5,
	p=currentpen,
	ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
	arrow=Arrows(TeXHead));

  // y axis
  yaxis(pic, L="",  
	axis=XZero,
	ymin=ybot-0.5, ymax=ytop+0.5,
	p=currentpen,
	ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
	arrow=Arrows(TeXHead));
  
  shipout(format(OUTPUT_FN,picnum),pic);
}
