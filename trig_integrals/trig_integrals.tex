\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}
\newcommand{\itempause}{\hspace*{0.618em}\pause}
\newcommand{\itemhead}[1]{\textcolor{ManiacMansion}{#1}\itempause}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
\hypersetup{colorlinks=true,linkcolor=blue} 

\title[Trig Integrals] % (optional, use only with long paper titles)
{Section 7.2:\hspace{1.5em}Trigonometric Integrals}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont
}
\date{}

\usepackage{siunitx}

\subject{Trigonometric integrals}
% This is only inserted into the PDF information catalog. Can be left
% out. 

% \newcommand{\integral}[4]{\int_{#1}^{#2} #3 \;\mathrm{d}{#4}}
% \newcommand{\itempause}{\hspace*{0.618em}\pause}
% \newcommand{\itemhead}[1]{\textcolor{blue}{#1}\itempause}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................


\begin{frame}
\frametitle{Basic trig integrals}  

\begin{itemize}

\item \itemhead{$\integral{}{}{\cos(\theta)}{\theta}$} ${}= \sin(\theta)+C$

\item \itemhead{$\integral{}{}{\sin(\theta)}{\theta}$} ${}= -\cos(\theta)+C$

\item \itemhead{$\integral{}{}{\tan\theta}{\theta}$}
  Use substitution $u=\cos\theta$ and 
  $\mathrm{d}u=-\sin\theta\;\mathrm{d}\theta$
  to get
  \begin{equation*}
    \integral{}{}{\frac{\sin\theta}{\cos\theta}}{\theta}
    =\integral{}{}{\frac{1}{u}\cdot(-1)}{u}
    =-\ln| \cos\theta |+C
  \end{equation*}
  Similarly 
  $\integral{}{}{\cot\theta}{\theta}=\ln | \sin\theta | +C$

\item \itemhead{$\integral{}{}{\sec\theta}{\theta}$}
  Here is a gadget.
  \begin{equation*}
    \integral{}{}{\sec\theta
                   \cdot\frac{\sec\theta+\tan\theta}{\sec\theta+\tan\theta}}{\theta}
    =\integral{}{}{\frac{\sec^2\theta+\sec\theta\cdot\tan\theta}{\sec\theta+\tan\theta}}{\theta}  
  \end{equation*}
  Now substitution of $u=\sec\theta+\tan\theta$ gives
  $=\ln| \sec\theta+\tan\theta |+C$.
  Similarly 
  $\integral{}{}{\csc\theta}{\theta}=\ln | \csc\theta-\cot\theta | +C$
\end{itemize}
\end{frame}


\section{Powers of sine and cosine}

\begin{frame}
\Ex 
$\displaystyle \integral{}{}{\sin^3x}{x}$

\pause
Rewrite $\sin^3x$ as $\sin^2x\cdot\sin x=(1-\cos^2x)\sin x$.
Use the substitution $u=\cos x$ so that $du=-\sin x\;dx$.
\begin{multline*}
  =\integral{}{}{(1-\cos^2x)\sin x}{x}
  =\integral{}{}{(1-u^2)\cdot(-1)}{u}
  =-\integral{}{}{1-u^2}{u}   \\
  =\frac{u^3}{3}-u+C
  =\frac{\cos^3x}{3}-\cos x+C
\end{multline*}

\pause
\Ex 
$\displaystyle \integral{}{}{\cos^5x}{x}$

\pause
Rewrite as $\cos^4x\cdot\cos x=(1-\sin^2x)^2\cos x$.
Use the substitution $u=\sin x$ so that $du=\cos x\;dx$.
\begin{multline*}
  =\integral{}{}{(1-\sin^2x)^2\cos x}{x}
  =\integral{}{}{(1-u^2)^2}{u}
  =\integral{}{}{1-2u^2+u^4}{u}   \\
  =\frac{u^5}{5}-\frac{2u^3}{3}+u+C
  =\frac{\sin^5x}{5}-\frac{2\sin^3x}{3}+\sin x +C
\end{multline*}
\end{frame}

\begin{frame}
\Ex $\displaystyle \integral{}{}{\sin^4x}{x}$

\pause
Use the half angle formula  
\begin{equation*}
  \sin^2x=\frac{1-\cos 2x}{2}
\end{equation*}
to rewrite.
\begin{equation*}
  =\integral{}{}{\biggl(\frac{1-\cos 2x}{2}\biggr)^2}{x}
  =\frac{1}{4}\integral{}{}{1-2\cos 2x+\cos^22x}{x}
\end{equation*}
On $\cos^22x$, use the half angle formula.
\begin{equation*}
  =\frac{1}{4}\integral{}{}{1-2\cos 2x+\frac{1+\cos 4x}{2}}{x}
  =\frac{1}{4}\integral{}{}{\frac{3}{2}-2\cos 2x+\frac{1}{2}\cos 4x}{x}
\end{equation*}
Antidifferentiate the second and third terms by substituting $u=2x$ and $u=4x$.
\begin{equation*}
  =\frac{3x}{8}-\frac{1}{4}\sin 2x+\frac{1}{32}\sin 4x+C  
\end{equation*}
\end{frame}


\begin{frame}{Strategy for $\integral{}{}{sin^mx\cdot\cos^nx}{x}$, $m,n\geq 0$}
\begin{itemize}
\item \itemhead{If at least one of $m$ and $n$ is odd}
  If $n$ is odd,
  rewrite all but one cosine with $\cos^2x=1-\sin^2 x$.
  Then use substitution with $u=\sin x$ so that $du=\cos x\;dx$.

  Otherwise,
  rewrite all but one sine with $\sin^2x=1-\cos^2 x$, and
  use substitution with $u=\cos x$ so that $du=-\sin x\;dx$.
\item \itemhead{If both $m$ and $n$ are even}
  Use the half angle formulas.
  \begin{equation*}
    \sin^2x=\frac{1-\cos 2x}{2}
    \qquad
    \cos^2x=\frac{1+\cos 2x}{2}
  \end{equation*}
  You may need to use them more than once.
\end{itemize}

\pause
\Ex $\displaystyle \integral{}{}{\sin^3x\cos^2x}{x}$

Rewrite as $(1-\cos^2x)\sin x\cdot\cos^2x$
and use $u=\cos x$ with $du=-\sin x\;dx$.
\begin{multline*}
  =\integral{}{}{(\cos^2x-\cos^4x)\cdot \sin x}{x}
  =\integral{}{}{(u^2-u^4)\cdot (-1)}{u}
  =-\integral{}{}{u^2-u^4}{u}           \\
  =\frac{u^5}{5}-\frac{u^3}{3}+C
  =\frac{\cos^5x}{5}-\frac{\cos^3x}{3}+C
\end{multline*}
\end{frame}


\begin{frame}
\Ex $\displaystyle \integral{}{}{\sin^2\theta\cos^2\theta}{\theta}$
\pause
\begin{align*}
    =\integral{}{}{\frac{1}{2}(1-\cos 2\theta)\cdot\frac{1}{2}(1+\cos 2\theta)}{\theta} 
    &=\frac{1}{4}\integral{}{}{1-\cos^2 2\theta}{\theta} \\
    &=\frac{1}{4}\integral{}{}{1-\frac{1}{2}(1+\cos 4\theta)}{\theta} \\
    &=\frac{1}{4}\integral{}{}{\frac{1}{2}-\frac{1}{2}\cos 4\theta}{\theta} \\
    &=\frac{1}{8}\theta-\frac{1}{32}\sin 4\theta+C
\end{align*}
  
\pause
\Ex $\displaystyle \integral{}{}{\sin^4\theta\cos^3\theta}{\theta}$

\pause
\begin{equation*}
=\integral{}{}{\sin^4\theta(1-\sin^2\theta)\cos\theta}{\theta} 
=\integral{}{}{(\sin^4\theta-\sin^6\theta)\cos\theta}{\theta}
\end{equation*}
To finish, substitute 
$u=\sin\theta$, $\textrm{d}u=\cos\theta\;\textrm{d}\theta$.
\begin{equation*}
  =\integral{}{}{u^4-u^6}{u}
  =-\frac{\sin^7\theta}{7}+\frac{\sin^5\theta}{5}+C
\end{equation*}

% \pause\medskip
% It is instructive to differentiate, remembering the Chain Rule.
% \begin{align*}
%   \frac{d\,\bigl(-\frac{\sin^7\theta}{7}+\frac{\sin^5\theta}{5}\bigr)}{dx}
%   &=-\sin^6\theta\cdot\cos\theta+\sin^4\theta\cdot\cos\theta  \\
%   &=\sin^4\theta\bigl(-\sin^2+1\bigr)\cdot\cos\theta  
% \end{align*}
\end{frame}




\begin{frame}{Practice}

\begin{enumerate}
\item $\displaystyle\integral{x=0}{\pi}{ \cos^4 x }{x}$ \\[1ex]

Apply the half angle formula.
\pause
\begin{equation*}
  \integral{x=0}{\pi}{ \biggl( \frac{1+\cos 2x}{2} \biggr)^2 }{x}
  =\frac{1}{4}\integral{x=0}{\pi}{ 1+2\cos 2x+\cos^2 2x }{x}
\end{equation*}
It worked once, let's try it again.
\pause
\begin{equation*}
  =\frac{1}{4}\integral{x=0}{\pi}{ 1+2\cos 2x+\frac{1+\cos 4x}{2} }{x}
\end{equation*}
\pause
Do the second and third terms by substitution.

\pause
\item $\displaystyle\integral{}{}{ \sin^5x\sqrt{\cos x} }{x}$ \\[1ex]

Set aside one sine and rewrite the others with the Pythagorean relation.
\pause
\begin{equation*}
  \integral{}{}{ (1-\cos^2x)^2\sqrt{\cos x}\cdot\sin x }{x}
  \integral{}{}{ (1-2\cos^2x+\cos^4x)\cos^{1/2} x\cdot\sin x }{x}
\end{equation*}
Finish by substituting $u=\cos x$.
\end{enumerate}
  
\end{frame}




\section{Powers of tangent and secant}

\begin{frame}{Strategy for $\integral{}{}{tan^mx\cdot\sec^nx}{x}$, $m,n\geq 0$}
\begin{itemize}
\item \itemhead{If $n$ is even}
  Rewrite all but two secants with $\sec^2x=1+\tan^2 x$.
  Then use substitution with $u=\tan x$ so that $du=\sec^2 x\;dx$.

\item \itemhead{If $m$ is odd}
  Save a $\sec x\tan x$ and rewrite all the remaining tangents with
  $\tan^2x=\sec^2x-1$.
  Then use substitution with $u=\sec x$ so that $du=\sec x\tan x\;dx$.

\item \itemhead{Otherwise}
Uh-oh.
Thinking time.
\end{itemize}

\pause
\Ex $\displaystyle \integral{}{}{\tan^2\theta\cdot \sec^6\theta}{\theta}$
\pause
\begin{align*}
  =\integral{}{}{\tan^2\theta\sec^4\theta\cdot \sec^2\theta}{\theta} 
    &=\integral{}{}{\tan^2\theta(1+\tan^2\theta)^2\cdot \sec^2\theta}{\theta} \\
    &=\integral{}{}{(\tan^2\theta +2\tan^4\theta+\tan^6\theta)\cdot \sec^2\theta}{\theta} 
\end{align*}
Substitute 
$u=\tan\theta$, $\textrm{d}u=\sec^2\theta\,\textrm{d}\theta$.
\begin{equation*}
    =\integral{}{}{u^2+2u^4+u^6}{u}
    =\frac{u^7}{7}+\frac{2u^5}{5}+\frac{u^3}{3}+C
    =\frac{\tan^7\theta}{7}+\frac{2\tan^5\theta}{5}+\frac{\tan^3\theta}{3}+C
\end{equation*}
\end{frame}



\begin{frame}
\Ex $\displaystyle \integral{}{}{\tan^3x\cdot\sec^4x}{x}$
\pause
\begin{align*}
  \integral{}{}{\tan^3x\sec^2x\cdot\sec^2x }{x}
  &=\integral{}{}{\tan^3x(\tan^2x+1)\cdot\sec^2x }{x}  \\
  &=\integral{}{}{(\tan^5x+\tan^3x)\cdot\sec^2x }{x}  
\end{align*}
Substitute $u=\tan x$ so that $du=\sec^2x\;dx$.
\begin{equation*}
  =\integral{}{}{u^5+u^3}{u}           
  =\frac{u^6}{6}+\frac{u^4}{4}+C       
  =\frac{\tan^6x}{6}+\frac{\tan^4x}{4}+C       
\end{equation*}
\end{frame}







% \begin{frame}
% \frametitle{$\integral{}{}{\tan^m\theta\sec^n\theta}{\theta}$
%             with $m$ odd}
% Recall that $\tan^2\theta=\sec^2\theta-1$.

% \Ex 
%   \begin{align*}
%     \integral{}{}{\tan^5\theta\sec^2\theta}{\theta}
%     &=\integral{}{}{tan^4\theta\sec\theta\cdot \sec\theta\tan\theta}{\theta} \\
%     &=\integral{}{}{(\sec^2\theta-1)^2\sec\theta\cdot \sec\theta\tan\theta}{\theta} \\
%     &=\integral{}{}{(\sec^5\theta-2\sec^3\theta+\sec\theta)\cdot \sec\theta\tan\theta}{\theta} 
%   \end{align*}
%   Substitute 
%   $u=\sec\theta$, $\textrm{d}u=\sec\theta\tan\theta\,\textrm{d}\theta$.
%   \begin{equation*}
%     =\frac{1}{6}\sec^6\theta-\frac{1}{2}\sec^4\theta+\frac{1}{2}\sec^2\theta +C
%   \end{equation*}
% \end{frame}



% \begin{frame}
% \frametitle{$\integral{}{}{\tan^m\theta\sec^n\theta}{\theta}$
%             with $n$ even}
% Recall that $\sec^2\theta=1+\tan^2\theta$.

% \Ex 
%   \begin{align*}
%     \integral{}{}{\tan^2\theta\sec^6\theta}{\theta}
%     &=\integral{}{}{\tan^2\theta\sec^4\theta\cdot \sec^2\theta}{\theta} \\
%     &=\integral{}{}{\tan^2\theta(1+\tan^2\theta)^2\cdot \sec^2\theta}{\theta} \\
%     &=\integral{}{}{(\tan^2\theta +2\tan^4\theta+\tan^6\theta)\cdot \sec^2\theta}{\theta} 
%   \end{align*}
%   Substitute 
%   $u=\tan\theta$, $\textrm{d}u=\sec^2\theta\,\textrm{d}\theta$.
%   \begin{equation*}
%     =\frac{1}{3}\tan^3\theta+\frac{2}{5}\tan^5\theta+\frac{1}{7}\tan^7\theta +C
%   \end{equation*}
% \end{frame}



\begin{frame}
\Ex $\displaystyle \integral{}{}{\tan^2\theta\cdot\sec\theta}{\theta}$
\pause
\begin{equation*}
    =\integral{}{}{(\sec^2\theta-1)\sec\theta}{\theta} 
    =\integral{}{}{\sec^3\theta-\sec\theta}{\theta} 
\end{equation*}
The $\sec\theta$ term we know how to integrate.
The $\sec^3\theta$ term is trickier.
By Parts gives this.
\begin{center}
  \begin{tabular}{*{2}{l}}
    $u=\sec\theta$ &$du=\uncover<3->{\sec\theta\tan\theta\;d\theta}$  \\
    $v=\uncover<3->{\tan\theta}$   &$dv=\sec^2\theta\;d\theta$  
  \end{tabular}
\end{center}
% $u=\sec\theta$ so $du=\sec\theta\tan\theta\;d\theta$
% and $dv=\sec^2\theta\;d\theta$ so $v=\tan\theta$.
\pause
\begin{align*}
  \integral{}{}{\sec^3\theta}{\theta}
  &=\sec\theta\tan\theta-\integral{}{}{\sec\theta\tan^2\theta}{\theta} \\
  &=\sec\theta\tan\theta-\integral{}{}{\sec\theta(\sec^2\theta-1)}{\theta} \\
  &= \sec\theta\tan\theta
    -\integral{}{}{\sec^3\theta}{\theta}
    +\integral{}{}{\sec\theta}{\theta}     
\end{align*}
Here is the answer to the hard term.
\begin{equation*}
  \integral{}{}{\sec^3\theta}{\theta}=\frac{1}{2}\bigl(\sec\theta\tan\theta
  +\ln |\sec\theta+\tan\theta|\bigr)
  +C
\end{equation*}

\end{frame}




\begin{frame}{Practice}

\begin{enumerate}
\item $\displaystyle\integral{}{}{ \sec^5 x\tan x }{x}$ \\[1ex]
\pause
\begin{equation*}
  \integral{}{}{ \sec^4x\cdot \sec x \tan x }{x}
\end{equation*}
Take $u=\sec x$ so $du=\sec x\tan x\,dx$.
\pause
\begin{equation*}
  =\integral{}{}{ u^4 }{u}
  =\frac{\sec^5 x}{5}+C
\end{equation*}
\end{enumerate}
  
\end{frame}




% \begin{frame}
% \frametitle{}
% 
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
