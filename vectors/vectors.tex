\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 12.1, 12.2\hspace{1em} Vectors in 2D and 3D}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Inverse trig functions}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage stub_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
% uncomment for 3D graphics: 
\input asy/vectors_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



% \section{}


\begin{frame}{Coordinates in 3D}
We can locate a point in three dimensions using \alert{rectangular coordinates}
with a triple $(x,y,z)$.
\begin{center}
  \vcenteredhbox{\input asy/vectors_3d000.tex }%
\end{center}
In the plane the axes divide two-space into quadrants.
In three-space we get \alert{octants}.
The one shown, where all three coordinates are positive, is the
\alert{first octant}.
\end{frame}

\begin{frame}{Distances}
\Df The \alert{distance} between two points in three-space is
the Pythagorean distance.
\begin{equation*}
  |P_1P_2|=\sqrt{(x_2-x_1)^2+(y_2-y_1)^2+(z_2-z_1)^2}
\end{equation*}

\Lm
The distance between two points is non-negative, $0\leq|P_1P_2|$, and is zero 
if and only if the two points are the same.
The distance function is symmetric: $|P_1P_2|=|P_2P_1|$.
The distance function satisfies the Triangle Inequality, 
$|P_1P_3|\leq |P_1P_2|+|P_2P_3|$,
with equality if and only if the three lie on a line.

\Lm
The equation of a sphere of radius and centered at the origin
is $x^2+y+2+z^2=r^2$.
If it is centered at $(a,b,c)$ then the equation is 
$(x-a)^2+(y-b)^2+(z-c)^2=r^2$.
\begin{center}
  \vcenteredhbox{\input asy/vectors_3d002.tex }%
\end{center}
\end{frame}



\begin{frame}{Surfaces in 3D}
In 2D you have functions $y=f(x)$ that result in curves.
In 3D you have functions $z=f(x,y)$ that result in surfaces.
\begin{center}
  \vcenteredhbox{\input asy/vectors_3d001.tex }%
\end{center}
Instead of 2D's tangent line, here you can look for a tangent plane.
And instead of 2D's area under the curve, here you can find the 
volume under.
\end{frame}

\begin{frame}{Practice}
\begin{enumerate}
\item Plot the point $P=(1,-2,2)$.

\pause\item How far is the point $Q=(4,-2,3)$ from the $xz$~plane?

\pause\item The $xy$~plane?

\pause\item
Which of these three points is closest to the $yz$~plane?
\begin{equation*}
  A=(2,3,1)
  \quad B=(-3,-2,-3)
  \quad C=(4,-5,2)
\end{equation*}

\pause\item
What surface is described by the equation $y=2$?

\pause\item 
By $z=-1$? 

\pause\item
Sketch the 3D surface described by $x^2+y^2=9$.

\pause\item
Sketch the sphere with radius~$4$ and center $(1,3,2)$.
Give its equation.
What is its intersection with the $yz$~plane?

\pause\item This describes a sphere.
Find its center and radius.
\begin{equation*}
  x^2+y^2+z^2=6x-4y-10z
\end{equation*}

\pause\item Do these points lie in a straight line?
\begin{equation*}
  (5,-1,3)
  \quad (2,0,1)
  \quad (11,-3,7)
\end{equation*}
\end{enumerate}  
\end{frame}





\section{Vectors in two dimensions}

\begin{frame}{Motivation}
This shows a parametrized curve.
We most naturally describe its instantaneous rate of change 
using a magnitude and a direction.

\begin{center}
  \vcenteredhbox{\includegraphics{vectors000.pdf}}%
\end{center}

\Df A \alert{vector} in space consists of length and a direction.
We denote vectors with an over-arrow, \alert{$\vec{v}$} 
(also common is boldface type, $\mathbf{v}$).
The single exceptional case is the \alert{zero vector, $\vec{0}$}, 
of length~$0$ but with no direction.
\end{frame}


\begin{frame}
A vector describes a displacement; 
it has a magnitude and a direction but it does not have an anchor 
point.

On the left is a $2$-over-$1$-up vector, and on the right are a number of 
such vectors.
\begin{center}
  \vcenteredhbox{\includegraphics{vectors002.pdf}}%
  \qquad
  \vcenteredhbox{\includegraphics{vectors003.pdf}}%
\end{center}
Vectors in space are equal if they describe the same displacement.
The vectors shown are not just alike\Dash they are equal.
\end{frame}


\begin{frame}{Vector representations}
We represent a $n$-over and $m$-up vector as written on the left, in a vertical
array.
Thus the ``$2$-over-$1$-up'' vector is represented as on the right.
\begin{equation*}
  \vec{v}=\colvec{n \\ m}
  \qquad
  \vec{w}=\colvec{2 \\ 1}
\end{equation*}
An array's entries are \alert{components}.
The zero vector's components are all $0$'s.

Here is the vector $\vec{v}$ in its \alert{canonical} position, rooted at the
origin.
\begin{center}
  \vcenteredhbox{\includegraphics{vectors004.pdf}}
\end{center}
We often don't trouble to distinguish between a point $(x,y)$
and the vector that, when in canonical position, ends at that point.
\end{frame}



\begin{frame}{Operations on vectors}
It will be convenient to describe lines, such as tangent lines, using
vectors.
In a line a vector gives more information than just the slope because it also
gives a direction.
We can describe the line below with something like 
``$(x,y)=(1,2)+t\cdot(2,-1)$.''    
\begin{center}
  \vcenteredhbox{\includegraphics{vectors001.pdf}}%
\end{center}
To make that description precise we must define how to add vectors, 
multiply a vector by a real number, etc.
\end{frame}



\begin{frame}{Addition}
Since vectors can be understood as displacements, this is how the
accumulation of vectors must go.  
\begin{center}
  \vcenteredhbox{\includegraphics{vectors007.pdf}}
\end{center}
For instance, imagine that you walk across the deck of a boat
 while at the same time
the boat is moving. 
If your displacement with respect to the boat is $\vec{v}_0$
and the boat's displacement with respect to the earth is $\vec{v}_1$
then the combined displacement with respect to the earth
is the vector shown as $\vec{v}_0+\vec{v}_1$.

Consequently we add two vectors by placing the tail of the second on the head
of the first, and then the sum goes from the tail of the first to the head
of the second.
\end{frame}

\begin{frame}
\Df The \alert{sum} of two vector representations is the entry-by-entry sum
of their representations.
\begin{equation*}
  \colvec{x_0 \\ y_0}
  +
  \colvec{x_1 \\ y_1}
  =
  \colvec{x_0+x_1 \\ y_0+y_1}
\end{equation*}
This is the \alert{parallelogram pattern}.
\begin{center}
  \vcenteredhbox{\includegraphics{vectors005.pdf}}
\end{center}
\end{frame}




\begin{frame}
\Df The \alert{difference} of two vectors is the entry-by-entry difference
of the representations.
\begin{equation*}
  \vec{v}_1-\vec{v}_0
  =\colvec{x_1 \\ y_1}
  -
  \colvec{x_0 \\ y_0}
  =
  \colvec{x_1-x_0 \\ y_1-y_0}
\end{equation*}
The picture shows 
that $\vec{v}_1-\vec{v}_0$ is the vector needed to solve the equation
$\vec{v}_0+x=\vec{v_1}$.
\begin{center}
  \vcenteredhbox{\includegraphics{vectors008.pdf}}
\end{center}
\end{frame}





\begin{frame}{Rescaling}
\Df The \alert{scalar multiplication} operation of a real number
and a vector rescales the components.
\begin{equation*}
  r\cdot \vec{v}
  =r\cdot\colvec{a \\ b}
  =\colvec{ra \\ rb}
\end{equation*}
\begin{center}
  \vcenteredhbox{\includegraphics{vectors009.pdf}}
\end{center}
\end{frame}




\begin{frame}{Comment about representations}
When we make definitions we make them so that they 
reflect the underlying reality.
Restated:~it is not that displacements behave according to their definitions,  
it is that the definitions are chosen to reflect the behavior
of displacements.

For example, the parallelogram pattern shows that the definition of
vector representation correctly reflects that addition of displacements
is commutative: $\vec{v}_0+\vec{v}_1=\vec{v}_0+\vec{v}_1$.
\begin{center}
  \vcenteredhbox{\includegraphics{vectors006.pdf}}
\end{center}
From now on we will just take the representation to be faithful, 
and say
``vector addition is commutative''
instead of ``addition of vector representatives is commutative.''
\end{frame}


\begin{frame}{Practice}
Use these to perform the operations.
\begin{equation*}
  \vec{a}=\colvec{1 \\ 2}
  \quad
  \vec{b}=\colvec{-3 \\ 1}
  \quad
  \vec{\i}=\colvec{1 \\ 0}
  \quad
  \vec{d}=\colvec{-2 \\ -1}
  \quad
  \vec{0}=\colvec{0 \\ 0}
\end{equation*}
\begin{enumerate}
\item $\vec{a}+\vec{b}$ and $\vec{b}+\vec{a}$     

\pause\item $\vec{a}+(\vec{b}+\vec{d})$     
and $(\vec{a}+\vec{b})+\vec{d}$     

\pause\item $\vec{d}+\vec{0}$     

\pause\item $\vec{a}-\vec{b}$     

\pause\item $-2\vec{\i}$     

% \pause\item $1\vec{d}$     

% \pause\item $3\vec{a}+2\vec{d}$     

\pause\item $5\vec{a}-\vec{b}+12\vec{d}$     

\end{enumerate}  
\end{frame}



\begin{frame}{Algebra of vectors}
\Lm \begin{enumerate}
\item Addition is commutative: $\vec{v}_0+\vec{v}_1=\vec{v}_1+\vec{v}_0$.
\item  The zero vector is the additive identity: $\vec{v}+\vec{0}=\vec{v}$
  and $\vec{v}+(-1\cdot\vec{v})=\vec{0}$
\item Addition is associative: $\vec{v}_0+(\vec{v}_1+\vec{v}_2)=(\vec{v}_0+\vec{v}_1)+\vec{v}_2$.
\item Scalar multiplication respects the vector 
operations:~$1\cdot\vec{v}=\vec{v}$,
  and $r\cdot(\vec{v}_0+\vec{v}_1)=r\cdot\vec{v_0}+r\cdot\vec{v_1}$,
  and $(r+s)\cdot\vec{v}=r\cdot\vec{v}+s\cdot\vec{v}$
  and $(rs)\cdot\vec{v}=r\cdot(s\cdot\vec{v})$
\end{enumerate}

\Pf These are routine to work through from the definition.
For instance, here is the first.
\begin{equation*}
  \colvec{a \\ b}+\colvec{c \\ d}
  =\colvec{a+c \\ b+d}
  =\colvec{c+a \\ d+b}
  =\colvec{c \\ d}+\colvec{a \\ b}
\end{equation*}
\end{frame}

\begin{frame}{Length}\vspace*{-1ex}
\Df The \alert{length} or \alert{magnitude} of a vector 
is its Pythagorean length.
\begin{equation*}
  |\vec{v}|
  =\biggl|\colvec{a \\ b}\biggr|
  =\sqrt{a^2+b^2}
\end{equation*}

\Ex Find the length of the vector whose tail is at $(1,3)$ and whose
head is at $(-2,5)$.

\pause
The vector is
\begin{equation*}
  \vec{v}=\colvec{-2-1 \\ 5-3}
         =\colvec{-3 \\ 2}
\end{equation*}
Its length is $|\vec{v}|=\sqrt{(-3)^2+2^2}=\sqrt{13}$.

\pause
This gives the interaction of length with the vector operations that 
we have already seen.

\Lm The length of the scalar multiple
$r\cdot \vec{v}$ is the absolute value of~$r$ times the 
length of~$\vec{v}$.
The \alert{Triangle Inequality} is that 
$|\vec{v}+\vec{w}|\leq |\vec{v}|+|\vec{w}|$.
\begin{center}
  \vcenteredhbox{\includegraphics{vectors007.pdf}}
\end{center}
\end{frame}


\begin{frame}{Comment on the Triangle Inequality}

The Triangle Inequality tells you that your space is flat.

In a space that is not flat, you can have paths where it is
longer to go straight from the start to the end than it is to go around.
\begin{center}
  \vcenteredhbox{\input asy/vectors_3d004.tex }%
\end{center}
\end{frame}


\begin{frame}{Practice}
Use these to perform the operations.
\begin{equation*}
  \vec{a}=\colvec{1 \\ 2}
  \quad
  \vec{b}=\colvec{-3 \\ 1}
  \quad
  \vec{\i}=\colvec{1 \\ 0}
  \quad
  \vec{d}=\colvec{-2 \\ -1}
  \quad
  \vec{0}=\colvec{0 \\ 0}
\end{equation*}
\begin{enumerate}
\item $|\vec{a}|$ and $|\vec{b}|$

\pause\item $|\vec{a}+\vec{b}|$     
and $|\vec{a}|+|\vec{b}|$     

\pause\item $|\vec{a}-\vec{b}|$     

\pause\item $|\vec{\i}|$     

% \pause\item $|-2\vec{\i}|$     

\pause\item $|4\vec{a}+\vec{d}|$     

\pause\item $|\vec{0}|$     
\end{enumerate}  
\end{frame}



\begin{frame}
A length-one vector is a \alert{unit vector}.  
For any nonzero vector~$\vec{v}$, 
finding a unit vector in the same
direction is \alert{normalizing} that starting vector.
\begin{equation*}
  \frac{1}{|\vec{v}|}\cdot\vec{v}
\end{equation*}

\Ex Find the unit vector in the same direction as the vector
that starts at $(1,-1)$ and ends at $(5,4)$.
\begin{equation*}
  \vec{v}=\colvec{5-1 \\ 4-(-1)}=\colvec{4 \\ 5}
  \qquad
  \frac{\vec{v}}{|\vec{v}|}
  =\frac{\vec{v}}{\sqrt{16+25}}
  =\colvec{4/\sqrt{41} \\ 5/\sqrt{41}}
\end{equation*}

All unit vectors in the plane have this form.
\begin{equation*}
  \vec{u}=
  \colvec{\cos\theta \\ \sin\theta}
\end{equation*}
The unit vectors in the directions of the axes are the 
\alert{standard basis vectors} and are denoted with
\begin{equation*}
  \vec{\imath}=\colvec{1 \\ 0}
  \quad
  \vec{\jmath}=\colvec{0 \\ 1}
\end{equation*}
or with $\vec{e}_1$ and $\vec{e}_2$.
\end{frame}





\begin{frame}{Practice}
Use these to perform the operations.
\begin{equation*}
  \vec{a}=\colvec{1 \\ 2}
  \quad
  \vec{b}=\colvec{-3 \\ 1}
  \quad
  \vec{0}=\colvec{0 \\ 0}
\end{equation*}
\begin{enumerate}
\item Find the unit vector in the direction of $\vec{a}$.

% \item Do the same for $\vec{b}$.

\item What about $\vec{0}$?
\end{enumerate}  
\end{frame}


\begin{frame}{Forces}
\Ex Two cables hold up a hundred pound weight hanging at point $W\!$.
The first cable shows a $55$~degree angle while the second shows $30$~degrees.
What are the forces on the cables? 
\begin{center}
  \includegraphics{vectors012.pdf}    
\end{center}
The weight exerts a force $\vec{W}$ downward of magnitude~$100$.
We want the magnitude $f_1$ of $\vec{F}_1$ and the magnitude 
$f_2$ of $\vec{F}_2$.

The force system is static so we have $\vec{F}_1+\vec{F}_2+\vec{W}=\vec{0}\!$.
\begin{equation*}
  \vec{F}_1 = f_1\colvec{\cos 125^\circ \\ \sin 125^\circ}
  \quad
  \vec{F}_2 = f_2\colvec{\cos 30^\circ \\ \sin 30^\circ}
  \quad
  \vec{W} = \colvec{0 \\ -100}
\end{equation*}
That gives a system of two equations with two unknowns.
\begin{equation*}
  \begin{linsys}{2}
    -0.573f_1 &+ &0.866f_2 &= &0 \\
    0.819f_1  &+ &0.5f_2      &= &100
  \end{linsys}
  \quad\grstep{}\quad
  \begin{linsys}{1}
    f_1 &\approx &87  \\
    f_2 &\approx &58
  \end{linsys}
\end{equation*}
\end{frame}


\section{Vectors in 3D}

\begin{frame}
\Df The \alert{vector in three dimensions} from the tail $(a_1,a_2,a_3)$
to the head  $(b_1,b_2,b_3)$ is represented by this column
\begin{equation*}
  \vec{v}
  =\colvec{b_1-a_1 \\ b_2-a_2 \\ b_3-a_3}
\end{equation*}
(you also often see boldface: $\mathbf{v}$).
The three entries are \alert{components}.
These vectors add, subtract, and rescale componentwise.
\begin{equation*}
  \vec{v}+\vec{w}
  =\colvec{v_1 \\ v_2 \\ v_3}+\colvec{w_1 \\ w_2 \\ w_3}
  =\colvec{v_1+w_1 \\ v_2+w_2 \\ v_3+w_3}
  \qquad
  r\cdot\vec{v}
  =r\cdot\colvec{v_1 \\ v_2 \\ v_3}
  =\colvec{rv_1 \\ rv_2 \\ rv_3}
\end{equation*}
Their \alert{length} is Pythagorean.
\begin{equation*}
  \Big|\colvec{v_1 \\ v_2 \\ v_3} \Big|
  =\sqrt{v_1^2+v_2^2+v_3^2}
\end{equation*}
These are the \alert{standard basis vectors}.
\begin{equation*}
  \vec{\i}=\colvec{1 \\ 0 \\ 0}
  \quad
  \vec{\j}=\colvec{0 \\ 1 \\ 0}
  \quad
  \vec{k}=\colvec{0 \\ 0 \\ 1}
\end{equation*}
\end{frame}

\begin{frame}
They follow the same rules of algebra.
\begin{enumerate}
\item Addition is commutative: $\vec{v}_0+\vec{v}_1=\vec{v}_1+\vec{v}_0$.
\item  The zero vector is the additive identity: $\vec{v}+\vec{0}=\vec{v}$
  and $\vec{v}+(-1\cdot\vec{v})=\vec{0}$.
\item Addition is associative: $\vec{v}_0+(\vec{v}_1+\vec{v}_2)=(\vec{v}_0+\vec{v}_1)+\vec{v}_2$.
\item Scalar multiplication acts as you'd expect: $1\cdot\vec{v}=\vec{v}$,
  and $r\cdot(\vec{v}_0+\vec{v}_1)=r\cdot\vec{v_0}+r\cdot\vec{v_1}$,
  and $(r+s)\cdot\vec{v}=r\cdot\vec{v}+s\cdot\vec{v}$
  and $(rs)\cdot\vec{v}=r\cdot(s\cdot\vec{v})$.
\end{enumerate}
Further, the rules for length are the 
same as for 2D:~$r\cdot \vec{v}=|r|\cdot |\vec{v}|$ and 
$|\vec{v}+\vec{w}|\leq |\vec{v}|+|\vec{w}|$.
\begin{center}
  \vcenteredhbox{\input asy/vectors_3d003.tex }%
\end{center}
\end{frame}

\begin{frame}{Practice}
\begin{enumerate}
\item Find this linear combination.
\begin{equation*}
  4\colvec{2 \\ 1 \\ -1}    
  -3\colvec{0 \\ 1 \\ 5}    
  +\colvec{-2 \\ -2 \\ 0}    
\end{equation*}

\pause\item Find the length of this vector.
\begin{equation*}
  \colvec{5 \\ 1 \\ -3}
\end{equation*}

\pause\item Normalize.
\begin{equation*}
  \colvec{-2 \\ 0 \\ -2}
\end{equation*}
\end{enumerate}
  
\end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
