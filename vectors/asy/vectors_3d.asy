// vectors_3d.asy

// compile with asy -inlineimage stub_3d
// In the .tex file put this in the preamble ... 
// % For 3D PRC asymptote
// % compile with asy -inlineimage stub_3d
// \def\asydir{asy/}
// \graphicspath{{asy/}}
// \input asy/stub_3d.pre
// \usepackage[bigfiles]{media9}
// \RequirePackage{asymptote}
// ... and then include the 3D material with something like this.
// \begin{center}
//   \uncover<2->{\input asy/stub_3d001.tex }% need the space following .tex   
// \end{center}


cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = true;
settings.embed = true;
settings.render = 0;
settings.tex="lualatex";  // for compiling in-pic text
settings.outformat="pdf";

import embed;

import fontsize;

import graph3; import solids;
projection default_projection = orthographic(1.2,3,2,up=Z);
currentprojection = default_projection;
currentlight=White; defaultrender.merge=true;
// unitsize(1cm);

string OUTPUT_FN = "vectors_3d%03d";

material figure_material = material(diffusepen=bold_color+white+opacity(0.5),
				 emissivepen=bold_color+white,
				 specularpen=bold_color+white);
material slice_material = material(diffusepen=gray_color+opacity(0.25),
				   emissivepen=gray_color,
				   specularpen=gray_color);
pen boundary_pen = gray(0.2)+opacity(0.5);

currentlight = nolight;

// ==== general function in 2D ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);

// ==== general function in 3D ====
real GENFCN_maxx=3;
real GENFCN_maxy=4;
triple GENFCN_front_left = (GENFCN_maxx,0,1.8);
triple GENFCN_front_right = (GENFCN_maxx,GENFCN_maxy,1.75);
triple GENFCN_back_right = (0,GENFCN_maxy,1.95);
triple GENFCN_back_left = (0,0,2);

path3 GENFCN_left_edge = GENFCN_back_left..(0.6*GENFCN_maxx,0,2.05)..GENFCN_front_left;
path3 GENFCN_front_edge = GENFCN_front_left..(GENFCN_maxx,0.2*GENFCN_maxy,1.7)..(GENFCN_maxx,0.3*GENFCN_maxy,1.85)..(GENFCN_maxx,0.45*GENFCN_maxy,1.72)..GENFCN_front_right;
path3 GENFCN_right_edge = GENFCN_front_right..(0.4*GENFCN_maxx,GENFCN_maxy,2.0)..GENFCN_back_right;
path3 GENFCN_back_edge = GENFCN_back_right..(0,0.65*GENFCN_maxy,2.05)..(0,0.5*GENFCN_maxy,1.85)..(0,0.3*GENFCN_maxy,1.95)..(0,0.15*GENFCN_maxy,2.1)..GENFCN_back_left;

path3 GENFCN_edge = ( GENFCN_left_edge & GENFCN_front_edge & GENFCN_right_edge & GENFCN_back_edge )--cycle;
surface GENFCN=surface(GENFCN_edge, new triple[]);


// ===== locate a point ============
currentprojection = orthographic(1.2,3,1.1,up=Z);
picture pic;
int picnum = 0;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

triple P = (2,3,1);
draw(pic, P--(0,P.y,P.z), dotted);
draw(pic, (0,P.y,P.z)--(0,P.y,0), dotted);
draw(pic, (0,P.y,P.z)--(0,0,P.z), dotted);
draw(pic, P--(0,P.y,P.z), dotted);
draw(pic, P--(P.x,0,P.z), dotted);
draw(pic, (P.x,0,P.z)--(0,0,P.z), dotted);
draw(pic, (P.x,0,P.z)--(P.x,0,0), dotted);
draw(pic, P--(P.x,P.y,0), dotted);
draw(pic, (P.x,P.y,0)--(P.x,0,0), dotted);
draw(pic, (P.x,P.y,0)--(0,P.y,0), dotted);

dotfactor = 4;
dot(pic, "$P=(2,3,1)$", P, N+NW, highlight_color);

xaxis3(pic,Label("\footnotesize $x$"),
       0,2.5, black,
       OutTicks("%",Step=1,Size=2pt),
       Arrow3(TeXHead2));
yaxis3(pic,Label("\footnotesize $y$"),
       0,3.5, black,
       OutTicks("%",Step=1,Size=2pt), 
       Arrow3(TeXHead2));
zaxis3(pic,Label("\footnotesize $z$"),
       0,1.5, black,
       OutTicks("%",Step=1,Size=2pt),
       Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


// ===== General surface in 3D  ============
currentprojection = orthographic(8,1,0.9,up=Z);
picture pic;
int picnum = 1;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

draw(pic, GENFCN_edge, boundary_pen);
draw(pic, GENFCN, surfacepen=figure_material);
draw(pic, GENFCN_front_edge, boundary_pen);
draw(pic, GENFCN_right_edge, boundary_pen);

xaxis3(pic,Label("\footnotesize $x$"),
       0,GENFCN_maxx+0.5, black,
       OutTicks("%",Step=1,Size=2pt),
       Arrow3(TeXHead2));
yaxis3(pic,Label("\footnotesize $y$"),
       0, GENFCN_maxy+0.5, black,
       OutTicks("%",Step=1,Size=2pt), 
       Arrow3(TeXHead2));
zaxis3(pic,Label("\footnotesize $z$"),
       0,2.5, black,
       OutTicks("%",Step=1,Size=2pt),
       Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


// ========= Sphere in R^3 ========
currentprojection = orthographic(5,2,0.9,up=Z);
picture pic;
int picnum = 2;
size(pic,0,3cm);
size3(pic,0,3cm,0,keepAspect=true);

draw(pic, scale3(2)*unitcircle3, boundary_pen);
// draw(pic, scale3(2)*unitcircle3, black);
draw(pic, scale3(2)*unitsphere, surfacepen=figure_material);

xaxis3(pic,Label("\footnotesize $x$"),
       0,5.5, black,
       OutTicks("%",Step=1,Size=2pt),
       Arrow3(TeXHead2));
yaxis3(pic,Label("\footnotesize $y$"),
       0, 3.5, black,
       OutTicks("%",Step=1,Size=2pt), 
       Arrow3(TeXHead2));
zaxis3(pic,Label("\footnotesize $z$"),
       0,3.5, black,
       OutTicks("%",Step=1,Size=2pt),
       Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ========= vectors in R^3 ========
currentprojection = orthographic(5,2,0.9,up=Z);
picture pic;
int picnum = 3;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

triple v1 = (1,3,1);
triple v2 = (1,1,1);
triple v3 = (2,4,2);

draw(pic, subpath(O--v1, 0, 0.975), Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, subpath(v1--(v1+v2),0, 0.9), Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, O--v3, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

xaxis3(pic,Label("\footnotesize $x$"),
       0,3.5, black,
       NoTicks3, //OutTicks("%",Step=1,Size=2pt),
       Arrow3(TeXHead2));
yaxis3(pic,Label("\footnotesize $y$"),
       0, 3.5, black,
       NoTicks3, // OutTicks("%",Step=1,Size=2pt), 
       Arrow3(TeXHead2));
zaxis3(pic,Label("\footnotesize $z$"),
       0,2.5, black,
       NoTicks3, // OutTicks("%",Step=1,Size=2pt),
       Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ========= space with a hill on it ========
currentprojection = orthographic(5,2,0.9,up=Z);
material space_material = material(diffusepen=bold_color,
				 emissivepen=bold_color+white,
				 specularpen=white);

triple f4(pair p) {
  real z = 1-(p.x^2+p.y^2);
  if (z>=0) {
    return (p.x,p.y,z);
  } else {
    return (p.x,p.y,0);
  }
}

picture pic;
int picnum = 4;
size(pic,8cm);
size3(pic,8cm,0,keepAspect=true);
light sun=light(specularfactor=5,(0.25,0.25,1));
currentlight=sun;

surface s = surface(f4, (-3,-3), (3,3), 10, 10, Spline);

real start_angle = -70;
real mid_angle = 0;
real end_angle = 80;
triple startpt = 1.2*(Cos(start_angle),Sin(start_angle),0);
triple midpt = 2*(Cos(mid_angle),Sin(mid_angle),0);
triple endpt = 1.2*(Cos(end_angle),Sin(end_angle),0);
// find intermediate pt
pair of_the_way(real t) {
 triple p=(1-t)*startpt+t*endpt;
 return (p.x,p.y);
}
// find coords of the mountain path
real mtn_path_x(real t) {return of_the_way(t).x;}
real mtn_path_y(real t) {return of_the_way(t).y;}
real mtn_path_z(real t) {return f4(of_the_way(t)).z;}

// draw the surface
draw(pic, s, surfacepen=space_material);
// draw the line paths
draw(pic, startpt--midpt, black);
dot(pic,"start",startpt,W);
draw(pic, midpt--endpt, black);
dot(pic,midpt);
dot(pic,"end",endpt,E);
// draw the mountain path
path3 mtn_path=graph(mtn_path_x,mtn_path_y,mtn_path_z,0,1,operator ..);
draw(pic, mtn_path, highlight_color+dotted+1);  // number is linewidth

// xaxis3(pic,Label("\footnotesize $x$"),
//        0,3.5, black,
//        NoTicks3, //OutTicks("%",Step=1,Size=2pt),
//        Arrow3(TeXHead2));
// yaxis3(pic,Label("\footnotesize $y$"),
//        0, 3.5, black,
//        NoTicks3, // OutTicks("%",Step=1,Size=2pt), 
//        Arrow3(TeXHead2));
// zaxis3(pic,Label("\footnotesize $z$"),
//        0,2.5, black,
//        NoTicks3, // OutTicks("%",Step=1,Size=2pt),
//        Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);




