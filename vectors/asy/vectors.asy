// vectors.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "vectors%03d";

import graph;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// =========== Vectors ===============
void draw_vector_2d(picture pic, pair start, pair end, pen p=defaultpen) {
  draw(pic, start--end, p, Arrow(ARROW_SIZE));  
}





// ================ Displacement ============
picture pic;
int picnum = 0;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;
draw(pic, f, FCNPEN, Arrow(ARROW_SIZE));


// pair displacement = (2,-1);
// draw_vector_2d(pic, (1,2), (1,2)+displacement, highlight_color);
real a = 1.2;
real a_t = times(f, a)[0];
pair d = dir(f, a_t, normalize=false);
draw(pic, point(f,a_t)--shift(point(f,a_t))*d, highlight_color, Arrow(ARROW_SIZE));
dotfactor = 4;
dot(pic, point(f,a_t), highlight_color);
// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
// xaxis(pic, L="",  
//       axis=YEquals(ytop+0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// xaxis(pic, L="",  
//       axis=YEquals(ybot-0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));

real[] T0 = {a};
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
// yaxis(pic, L="",  
//       axis=XEquals(xleft-0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// yaxis(pic, L="",  
//       axis=XEquals(xright+0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ............. In canonical position ...........
real f1(real x) { return((-1/2)*x+(5/2)); }

picture pic;
int picnum = 1;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=3;

pair displacement = (2,-1);
path f = graph(pic, f1, xleft-0.2, xright+0.2);
draw(pic, f, gray(0.8));
draw_vector_2d(pic, (1,2), (1,2)+displacement, highlight_color);

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ========== first 2D vector ========
picture pic;
int picnum = 2;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=3;
ybot=-1; ytop=3;

pair displacement = (2,1);
draw_vector_2d(pic, (1,1), (1,1)+displacement, highlight_color);

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ................ bunch of vectors ...........
picture pic;
int picnum = 3;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=3;
ybot=-1; ytop=3;

pair displacement = (2,1);

// I used this to come up with a good seed.
// int secs = seconds();
// write(format("Seed for srand is %d",secs));
// srand(secs);
srand(1710617292);
int num_vectors = 8;
int i = 0;
while (i<num_vectors) {
  real xi = xleft+(xright-xleft)*unitrand();
  real yi = ybot+(ytop-ybot)*unitrand();
  if ((xi+displacement.x <= xright)
      && (yi+displacement.y <= ytop)) {
    i = i+1;
    draw_vector_2d(pic, (xi,yi), (xi,yi)+displacement, highlight_color);
  }
}

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ................ vector in canonical position ..........
picture pic;
int picnum = 4;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=2;

pair displacement = (2,1);
draw_vector_2d(pic, (0,0), (0,0)+displacement, highlight_color);

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// =============== addition and subtraction =============

// ........... addition .........
picture pic;
int picnum = 5;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=3;

pair displacement0 = (2,1);
pair displacement1 = (1,2);
draw_vector_2d(pic, (0,0), (0,0)+displacement0);
draw_vector_2d(pic, (0,0), (0,0)+displacement1);
draw(pic, displacement0--displacement0+displacement1, dotted);
draw(pic, displacement1--displacement1+displacement0, dotted);
draw_vector_2d(pic, (0,0), (0,0)+displacement0+displacement1, highlight_color);

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ........... addition reflects commutativity .........
picture pic;
int picnum = 6;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=3;

pair displacement0 = (2,1);
pair displacement1 = (1,2);
draw(pic, Label("$\vec{v}_0$",filltype=Fill(white)), (0,0)--displacement0, Arrow(ARROW_SIZE));
draw(pic, Label("$\vec{v}_1$",filltype=Fill(white)), (0,0)--displacement1, NW, Arrow(ARROW_SIZE));
draw(pic,  Label("$\vec{v}_0+\vec{v}_1$",filltype=Fill(white)), displacement0--displacement0+displacement1, Arrow(ARROW_SIZE));
draw(pic,  Label("$\vec{v}_1+\vec{v_0}$",filltype=Fill(white)), displacement1--displacement1+displacement0, NW, Arrow(ARROW_SIZE));
draw(pic, (0,0)--displacement0+displacement1, highlight_color+dotted);

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ........... addition as accumulation of displacement .........
picture pic;
int picnum = 7;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=3;
ybot=-1; ytop=3;

pair displacement0 = (2,1);
pair displacement1 = (1,1);

draw(pic, subpath((0,0)--displacement0,0.05,1), Arrow(ARROW_SIZE));
draw(pic, subpath((0,0)--displacement0+displacement1,0.03,0.97), highlight_color, Arrow(ARROW_SIZE));
draw(pic, subpath(displacement0--displacement0+displacement1,0,0.95), Arrow(ARROW_SIZE));

// draw_vector_2d(pic, (0,0), (0,0)+displacement0);
// draw_vector_2d(pic, displacement0, displacement0+displacement1);
// draw_vector_2d(pic, (0,0), displacement0+displacement1, highlight_color);
label(pic, "$\vec{v}_0$", displacement0/2, S);
label(pic, "$\vec{v}_1$", displacement0+(displacement1/2), E);
label(pic, "$\vec{v}_0+\vec{v_1}$", (displacement0+displacement1)/2, NW, highlight_color);

dotfactor = 3;
dot(pic, "start", (0,0), W);
dot(pic, "finish", displacement0+displacement1, E);
	       
// // x and y axes
// Label L = rotate(0)*Label("{\scriptsize $x$}");
// // Draw graph paper
// xaxis(pic, L=L,  
//       axis=YEquals(ytop+0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// xaxis(pic, L="",  
//       axis=YEquals(ybot-0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// // The x axis
// xaxis(pic, L="",  
//       axis=YZero,
//       xmin=xleft-0.2, xmax=xright+0.2,
//       p=currentpen,
//       ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
//       arrow=Arrows(TeXHead));

// // y axis
// Label L = rotate(0)*Label("\scriptsize $y$");
// // Graph paper
// yaxis(pic, L="",  
//       axis=XEquals(xleft-0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// yaxis(pic, L="",  
//       axis=XEquals(xright+0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// // y-axis
// yaxis(pic, L=L,  
//       axis=XZero,
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=currentpen,
//       ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
//       arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ........... subtraction .........
picture pic;
int picnum = 8;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=3;
ybot=-1; ytop=3;

pair displacement0 = (2,1);
pair displacement1 = (1,2);
draw(pic, "$\vec{v}_0$", (0,0)--displacement0, Arrow(ARROW_SIZE));
draw(pic, "$\vec{v}_1$", (0,0)--displacement1, NW, Arrow(ARROW_SIZE));
draw(pic, "$\vec{v}_1-\vec{v_0}$", displacement0--displacement1, NE, highlight_color, Arrow(ARROW_SIZE));

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ........... scalar multiplication .........
picture pic;
int picnum = 9;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=3;
ybot=-1; ytop=3;

pair displacement0 = (1,1);
pair displacement1 = 3*displacement0;
pair displacement2 = -1*displacement0;
draw_vector_2d(pic, (0,0), displacement0);
draw_vector_2d(pic, (0,0), displacement1, highlight_color+dashed);
draw_vector_2d(pic, (0,0), displacement2, highlight_color+dashed);
label(pic, "$-1\cdot\vec{v}$",-0.75*displacement0, NW, highlight_color);
label(pic, "$3\cdot\vec{v}$",2.25*displacement0, SE, highlight_color);


// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);






// =============== angle =============

// ............ Not in canonical position ........
picture pic;
int picnum = 10;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=3;

pair displacement0 = (2.5,1);
pair displacement1 = (1,2);
draw_vector_2d(pic, (0.15,-0.15), (0.15,-0.15)+displacement0);
draw_vector_2d(pic, (-0.5,0.75), (-0.5,0.75)+displacement1);

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
// xaxis(pic, L=L,  
//       axis=YEquals(ytop+0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// xaxis(pic, L="",  
//       axis=YEquals(ybot-0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
// yaxis(pic, L="",  
//       axis=XEquals(xleft-0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// yaxis(pic, L="",  
//       axis=XEquals(xright+0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ............. In canonical position ...........
picture pic;
int picnum = 11;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=3;

pair displacement0 = (2.5,1);
pair displacement1 = (1,2);
draw_vector_2d(pic, (0,0), (0,0)+displacement0);
draw_vector_2d(pic, (0,0), (0,0)+displacement1);

// angle arc
path c = circle((0,0),0.4);
real v0_t = intersect(c, (0,0)--displacement0)[0];
real v1_t = intersect(c, (0,0)--displacement1)[0];
draw(pic, displacement0--displacement1, dotted);
draw(pic, "$\theta$", subpath(c, v0_t, v1_t), highlight_color, Arrow(ARROW_SIZE/2));


// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
// xaxis(pic, L=L,  
//       axis=YEquals(ytop+0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// xaxis(pic, L="",  
//       axis=YEquals(ybot-0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
// yaxis(pic, L="",  
//       axis=XEquals(xleft-0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// yaxis(pic, L="",  
//       axis=XEquals(xright+0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ======= vectors and forces ======
picture pic;
int picnum = 12;
size(pic,0,1.75cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=0; ytop=3;

pair wgt = (0,0);
real line_hgt = 3;
path lne = (xleft,line_hgt)--(xright,line_hgt);  
real cable1_ang = 35; // past 90 degrees
pair cable1_line_int = ( (-1)*sin(radians(cable1_ang))*line_hgt, line_hgt);
path F1_line = wgt--cable1_line_int;
real cable2_ang = 60; // before 90 degrees
pair cable2_line_int = ( sin(radians(cable2_ang))*line_hgt, line_hgt);
path F2_line = wgt--cable2_line_int;

// angle arcs
path c1 = rotate(90,cable1_line_int)*circle(cable1_line_int, 0.65); // hack to avoid 0 degs
real f1_t = intersect(c1, F1_line)[0];
real lne1_t = intersect(c1, cable1_line_int--cable2_line_int)[0];
path angle1_arc = subpath(c1, f1_t, lne1_t);
draw(pic, "{\tiny $55^\circ$}", angle1_arc, Arrow(ARROW_SIZE/2));

path c2 = circle(cable2_line_int, 0.65);
real f2_t = intersect(c2, F2_line)[0];
real lne2_t = intersect(c2, cable2_line_int--cable1_line_int)[0];
path angle2_arc = subpath(c2, f2_t, lne2_t);
draw(pic, "{\tiny $30^\circ$}", angle2_arc, SW, Arrow(ARROW_SIZE/2));

// draw the vectors
draw(pic, lne, FCNPEN);
draw(pic, "$\vec{F}_1$", F1_line, W, highlight_color, Arrow(ARROW_SIZE));
draw(pic, "$\vec{F}_2$", F2_line, highlight_color, Arrow(ARROW_SIZE));
dotfactor = 4;
dot(pic, "$W$", wgt, highlight_color);

shipout(format(OUTPUT_FN,picnum),pic);



