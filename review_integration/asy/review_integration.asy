// review_integration.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc=true;
settings.embed=true;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "review_integration%03d";

import graph;



// ==== solar panel ====
path panel_fcn = (0,-2)..(1,-0.5)..(2,1)..(3,2.25)..(4,2.0)..(5.25,0.25);
path f = shift(0,1)*panel_fcn;

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-1; ytop=3;

real a = 0; real b = 4.6;

picture pic;
int picnum = 0;
size(pic,0,4cm,keepAspect=true);

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_0_time = times(f, 0)[0];
real f_b_time = times(f, b)[0];
path left_region = ( point(f,f_a_time)&subpath(f, f_a_time, f_0_time)&point(f,f_0_time)--(0,0)--point(f,f_a_time) )--cycle;
path right_region = ( point(f,f_0_time)&subpath(f, f_0_time, f_b_time)&point(f,f_b_time)--(b,0)--(0,0) )--cycle;
fill(pic, left_region, FILLCOLOR);
fill(pic, right_region, FILLCOLOR);
draw(pic, (b,0)--point(f, f_b_time));
draw(pic, f, FCNPEN);

  // // sweep left to right
  // real x = a+(b-a)*(i/9);
  // real f_x_time = times(f, x)[0];
  // path region_done = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_x_time)&point(f,f_x_time)--(x,ybot)--(a,ybot))--cycle;
  // fill(pic, region_done, light_color);
  // draw(pic, (x,ybot)--point(f,f_x_time), highlight_color);
  
real[] T0 = {b};
xaxis(pic, L="{\scriptsize hr}",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T0,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$b$", b);

Label L = Label("\scriptsize kw",EndPoint,W);
yaxis(pic, L=rotate(0)*L,  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ==== general function ====

path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);
real a = 1; real b = 4;

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;

for(int i=0; i < 10; ++i) {
  picture pic;
  int picnum = 1+i;
  size(pic,4cm,0,keepAspect=true);

  // region to be integrated  
  real f_a_time = times(f, a)[0];
  real f_b_time = times(f, b)[0];
  path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
  fill(pic, region, FILLCOLOR);
  draw(pic, f, FCNPEN);

  // sweep left to right
  real x = a+(b-a)*(i/9);
  real f_x_time = times(f, x)[0];
  path region_done = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_x_time)&point(f,f_x_time)--(x,ybot)--(a,ybot))--cycle;
  fill(pic, region_done, light_color);
  draw(pic, (x,ybot)--point(f,f_x_time), highlight_color);

  real[] T1 = {a,b};
  xaxis(pic, L="{\scriptsize $t$}",  
	axis=YZero,
	xmin=xleft-0.75, xmax=xright+.75,
	p=currentpen,
	ticks=RightTicks("%",T1,Size=2pt),
	arrow=Arrows(TeXHead));
  labelx(pic, "$a$", a);
  labelx(pic, "$b$", b);
  
  yaxis(pic, L="{\scriptsize $y$}",  
	axis=XZero,
	ymin=ybot-0.75, ymax=ytop+0.75,
	p=currentpen,
	ticks=NoTicks,
	arrow=Arrows(TeXHead));
  
  shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
}


// ===== usual integration picture ======
picture pic;
int picnum = 11;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, (a,0)--point(f,f_a_time));
draw(pic, (b,0)--point(f,f_b_time));

// fcn
draw(pic, f, FCNPEN);

real[] T11 = {a, b};
xaxis(pic, L="{\scriptsize $t$}",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T11,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);

yaxis(pic, L="{\scriptsize $y$}",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ===== Lots of boxes ======
picture pic;
int picnum = 12;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, f, FCNPEN);

// Reimann boxes
for(int i=0; i < 19; ++i) {
  real x_i = a+(b-a)*(i/19);
  real x_iplus = a+(b-a)*((i+1)/19);
  real f_x_i_time = times(f, x_i)[0];
  real f_x_iplus_time = times(f, x_iplus)[0];
  path reimann_box = ((x_i,ybot)--point(f,f_x_i_time)--(x_iplus,point(f,f_x_i_time).y)--(x_iplus,ybot)--(x_i,ybot))--cycle;
  filldraw(pic, reimann_box, light_color, highlight_color);
}

real[] T11 = {a,b};
xaxis(pic, L="{\scriptsize $t$}",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T11,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);

yaxis(pic, L="{\scriptsize $y$}",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ... one box .......
picture pic;
int picnum = 13;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, (a,0)--point(f,f_a_time));
draw(pic, (b,0)--point(f,f_b_time));

// a single box
real i = 12;
real x_i = a+(b-a)*(i/19);
real x_iplus = a+(b-a)*((i+1)/19);
real f_x_i_time = times(f, x_i)[0];
real f_x_iplus_time = times(f, x_iplus)[0];
path reimann_box = ((x_i,ybot)--point(f,f_x_i_time)--(x_iplus,point(f,f_x_i_time).y)--(x_iplus,ybot)--(x_i,ybot))--cycle;
filldraw(pic, reimann_box, light_color, highlight_color);

// fcn
draw(pic, f, FCNPEN);

real[] T11 = {a, x_i, b};
xaxis(pic, L="{\scriptsize $t$}",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T11,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$x_i$", x_i);
labelx(pic, "$b$", b);

yaxis(pic, L="{\scriptsize $y$}",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ... abbreviate that .......
picture pic;
int picnum = 14;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, (a,0)--point(f,f_a_time));
draw(pic, (b,0)--point(f,f_b_time));

// slice
real x = a+(b-a)*(0.618);
real f_x_time = times(f, x)[0];
draw(pic, (x,0)--point(f,f_x_time), highlight_color);

// fcn
draw(pic, f, FCNPEN);

real[] T11 = {a, x, b};
xaxis(pic, L="{\scriptsize $t$}",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T11,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$x$", x);
labelx(pic, "$b$", b);

yaxis(pic, L="{\scriptsize $y$}",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== Practice definite integrals ====
real f15(real x) {return x^2;}

picture pic;
int picnum = 15;
size(pic,0,2.75cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1;
ybot=0; ytop=1;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

// Draw the region
path f = graph(pic, f15, xleft-0.2, xright+0.1);

real f_0_time = times(f, 0)[0];
real f_1_time = times(f, 1)[0];
path region = ((0,0)--point(f,f_0_time)&subpath(f, f_0_time, f_1_time)&point(f,f_1_time)--(1,0)--(0,0))--cycle;
fill(pic, region, FILLCOLOR);

// Draw the graph
draw(pic, f, FCNPEN);

// Axes
real[] T15 = {1};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks("%", T15, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, Label("$1$",filltype=Fill(white)), 1);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ..... y = sin (x) ...........
real f16(real x) {return sin(x);}

picture pic;
int picnum = 16;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=1;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

// Draw the region
path f = graph(pic, f16, xleft-0.1, xright+0.3);
  
real f_0_time = times(f, 0)[0];
real f_pi_time = times(f, pi)[0];
path region = ((0,0)--point(f,f_0_time)&subpath(f, f_0_time, f_pi_time)&point(f,f_pi_time)--(pi,0)--(0,0))--cycle;
fill(pic, region, FILLCOLOR);

// Draw the function
draw(pic, f, FCNPEN);

// Axes
real[] T16 = {pi/2, pi};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks("%", T16, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$\pi/2$", pi/2);
labelx(pic, "$\pi$", pi);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ====== particle velocity, compute distance ====
real f17(real x) {return x^3-10*x^2+24*x;}

picture pic;
int picnum = 17;
scale(pic, Linear(3), Linear);
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=-5; ytop=17;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// Draw the region
path f = graph(pic, f17, xleft-0.1, xright+0.3);
  
// real f_0_time = times(f, 0)[0];
// real f_pi_time = times(f, pi)[0];
// path region = ((0,0)--point(f,f_0_time)&subpath(f, f_0_time, f_pi_time)&point(f,f_pi_time)--(pi,0)--(0,0))--cycle;
// fill(pic, region, FILLCOLOR);

// Draw the function
draw(pic, f, FCNPEN);

// Axes
xaxis(pic, L="$t$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks(Step=2, step=1, OmitTick(0), Size=2pt, size=1pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("$v(t)$");
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=5, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ..... area between........
picture pic;
int picnum = 18;
scale(pic, Linear(3), Linear);
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=-5; ytop=17;

// Draw the region
path f = graph(pic, f17, xleft-0.1, xright+0.3);

// path region = buildcycle(f, (-1,0)--(7,0) );
// draw(pic, region, red);
real f_0_time = times(f, 0)[0];
real f_4_time = times(f, ScaleX(pic, 4))[0];
real f_6_time = times(f, ScaleX(pic, 6))[0];
path region_0_4 = ((0,0)--point(f,f_0_time)&subpath(f, f_0_time, f_4_time)&point(f,f_4_time)--(4,0)--(0,0))--cycle;
fill(pic, region_0_4, FILLCOLOR);
path region_4_6 = ((4,0)--point(f,f_4_time)&subpath(f, f_4_time, f_6_time)&point(f,f_6_time)--(6,0)--(4,0))--cycle;
fill(pic, region_4_6, FILLCOLOR);

// Draw the function
draw(pic, f, FCNPEN);

// Axes
xaxis(pic, L="$t$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks(Step=2, step=1, OmitTick(0), Size=2pt, size=1pt),
      arrow=Arrows(TeXHead));
  
Label L = rotate(0)*Label("$v(t)$");
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-1.5, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=5, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ..... one-D graph ........
picture pic;
int picnum = 19;
scale(pic, Linear, Linear);
size(pic,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=50;
ybot=0; ytop=1;

real vert_offset = 0.2cm;  
real r = 0.10cm;  //radius of a turn
// Draw the region
real rt_end = 128/3; 
path d = (0,vert_offset)--(rt_end-2*r,vert_offset)
  &arc((rt_end-r,vert_offset+r), r, -90, 90, CCW)
  &(rt_end-2*r,vert_offset+2*r)--(36,vert_offset+2*r);
draw(pic, d, neutral_color, MidArrow(ARROW_SIZE));
path d_slice = point(d, 0.4)--point(d,0.44);
draw(pic, Label("$dt$",black), d_slice, 1.5*N, highlight_color, Bars(3));

dotfactor = 5;
dot(pic, Label("$t=0$", align=S, black), (0,vert_offset), neutral_color);
dot(pic, Label("$t=4$", align=E, black), (rt_end,vert_offset+r), neutral_color);
dot(pic, Label("$t=6$", align=N, black), (36,vert_offset+2*r), neutral_color);


// Axes
real[] T19 = {0, 36, 128/3};
xaxis(pic, L="",  
      // axis=YZero,
      xmin=xleft-2.5, xmax=xright+2.75,
      p=currentpen,
      ticks=RightTicks("%", T19, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$0$", 0);
labelx(pic, "$36$", 36);
labelx(pic, "\makebox[0em][l]{$128/3$}", 128/3);

// yaxis(pic, L="",  
//       axis=XZero,
//       ymin=ybot-1.5, ymax=ytop+0.75,
//       p=currentpen,
//       ticks=LeftTicks(Step=5, OmitTick(0), Size=2pt),
//       arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ====== area between two functions ====
real f20(real x) {return x^2+1;}
real f20a(real x) {return exp(-3*x);}

picture pic;
int picnum = 20;
// scale(pic, Linear(3), Linear);
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

// Draw the region
path f = graph(pic, f20, xleft-0.1, xright+0.1);
path fa = graph(pic, f20a, xleft-0.1, xright+0.1);
  
real f_0_time = times(f, 0)[0];
real f_2_time = times(f, 2)[0];
real fa_0_time = times(fa, 0)[0];
real fa_2_time = times(fa, 2)[0];
path region = ( point(f,f_0_time)&subpath(f, f_0_time, f_2_time)&point(f,f_2_time)--point(fa_2_time)&subpath(fa,fa_2_time,fa_0_time) )--cycle;
fill(pic, region, FILLCOLOR);

x = 0.618*2;
real f_x_time = times(f, x)[0];
real fa_x_time = times(fa, x)[0];
draw(pic, point(f, f_x_time)--point(fa, fa_x_time), highlight_color);

// Draw the function
draw(pic, f, FCNPEN);
draw(pic, fa, FCNPEN);

// Axes
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ===== between sin x and cos x ===============
real f21(real x) {return sin(x);}
real f21a(real x) {return cos(x);}

real a = 0; real b = pi;

picture pic;
int picnum = 21;
scale(pic, Linear, Linear);
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=-1; ytop=1;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path sine = graph(f21, a-0.1, b+0.1);
path cosine = graph(f21a, a-0.1, b+0.1);

real c = pi/4;

// region to be integrated  
real sine_a_time = times(sine, a)[0];
real sine_b_time = times(sine, b)[0];
real sine_c_time = times(sine, c)[0];
real cosine_a_time = times(cosine, a)[0];
real cosine_b_time = times(cosine, b)[0];
real cosine_c_time = times(cosine, c)[0];
path region0 = ( point(sine,sine_a_time)--point(cosine,cosine_a_time)&subpath(cosine, cosine_a_time, cosine_c_time)&point(sine,sine_c_time)&subpath(sine, sine_c_time, sine_a_time)&point(sine,sine_a_time) )--cycle;
fill(pic, region0, FILLCOLOR);
path region1 = ( point(sine,sine_c_time)&subpath(sine, sine_c_time, sine_b_time)&point(sine,sine_b_time)--point(cosine,cosine_b_time)&subpath(cosine, cosine_b_time, cosine_c_time)&point(cosine,cosine_c_time) )--cycle;
fill(pic, region1, FILLCOLOR);

// Show slices
real x0 = a+(0.1*(b-a));
real x1 = a+(0.75*(b-a));
path slice0 = (x0,f21a(x0))--(x0,f21(x0));
path slice1 = (x1,f21(x1))--(x1,f21a(x1));
draw(pic,slice0,highlight_color);
draw(pic,slice1,highlight_color);
draw(pic, (b,0)--(b,f21a(b)));

// Draw curves
draw(pic, sine, FCNPEN);
draw(pic, cosine, FCNPEN);

real[] T21 = {x0, x1, b};
xaxis(pic, L="",  
	axis=YZero,
      xmin=xleft-0.25, xmax=xright+.50,
      p=currentpen,
      ticks=RightTicks("%",T21,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$x_0$", x0);
labelx(pic,"$x_1$", x1, SE);
labelx(pic,"$\pi$", b, N);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ===== between y=x^3 and y=x+1 ===============
real f22(real x) {return x^3;}
real f22a(real x) {return x+6;}

real a = -6; real b = 2;

picture pic;
int picnum = 22;
scale(pic, Linear, Linear);
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=a; xright=b;
ybot=0; ytop=8;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = graph(f22, 0-0.75, b+0.04);
path fa = graph(f22a, a-0.1, b+0.15);

// region to be integrated  
real f_0_time = times(f, 0)[0];
real f_b_time = times(f, b)[0];
real fa_a_time = times(fa, a)[0];
real fa_b_time = times(fa, b)[0];
path region = ( subpath(fa, fa_a_time, fa_b_time)&subpath(f, f_b_time, f_0_time)&point(f,f_0_time)--point(fa, fa_a_time) )--cycle;
fill(pic, region, FILLCOLOR);

// Show slices
real x0 = a+(0.4*(b-a));
real x1 = a+(0.85*(b-a));
path slice0 = (x0,0)--(x0,f22a(x0));
path slice1 = (x1,f22(x1))--(x1,f22a(x1));
draw(pic,slice0,highlight_color);
draw(pic,slice1,highlight_color);

// Draw curves
draw(pic, f, FCNPEN);
draw(pic, fa, FCNPEN);

real[] T22 = {a, x0, x1, b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.50,
      p=currentpen,
      ticks=RightTicks("%", T22, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$x_0$", x0);
labelx(pic,"$x_1$", x1);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... slices dy ......
picture pic;
int picnum = 23;
scale(pic, Linear, Linear);
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=a; xright=b;
ybot=0; ytop=8;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = graph(f22, 0-0.75, b+0.04);
path fa = graph(f22a, a-0.1, b+0.15);

// region to be integrated  
real f_0_time = times(f, 0)[0];
real f_b_time = times(f, b)[0];
real fa_a_time = times(fa, a)[0];
real fa_b_time = times(fa, b)[0];
path region = ( subpath(fa, fa_a_time, fa_b_time)&subpath(f, f_b_time, f_0_time)&point(f,f_0_time)--point(fa, fa_a_time) )--cycle;
fill(pic, region, FILLCOLOR);

// Show slices
real y0 = 0+(0.4*(8-0));
path slice0 = (y0-6,y0)--(y0^(1/3),y0);
draw(pic,slice0,highlight_color);

// Draw curves
draw(pic, f, FCNPEN);
draw(pic, fa, FCNPEN);

real[] T23 = {a, b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.50,
      p=currentpen,
      ticks=RightTicks("%", T23, Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== Hooke's Law =====

// From github.com/vectorgraphics/asymptote/blob/master/examples/spring.asy
pair coilpoint(real lambda, real r, real t)
{
  return (2.0*lambda*t+r*cos(t),r*sin(t));
}

guide coil(guide g=nullpath, real lambda, real r, real a, real b, int n)
{
  real width=(b-a)/n;
  for(int i=0; i <= n; ++i) {
    real t=a+width*i;
    g=g..coilpoint(lambda,r,t);
  }
  return g;
}

guide coils(real tightness=1, real rad=8, int num_coils=5) {
  real t1=-pi;
  real t2=2*num_coils*pi;
  real lambda=(t2-t1+tightness)/(t2-t1);
  pair b=coilpoint(lambda,rad,t1);
  pair c=coilpoint(lambda,rad,t2);
  return coil(lambda,rad,t1,t2,100);

  
  // pair a=b-20;
  // pair d=c+20;

  // draw(pic,a--b,BeginBar(2*barsize()));
  // draw(pic,c--d);
  // draw(pic,));
  // dot(pic,d);

  // pair h=20*I;
  // draw(pic,label,a-h--d-h,red,Arrow,Bars,PenMargin);
}

picture pic;
int picnum = 24;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=50;
ybot=-10; ytop=10;

path c = coils();
pair c_start =  point(c, reltime(c,0));
pair c_end =  point(c, reltime(c,1));
real leader_len = 15;
path spring = (c_start-(leader_len,0))--c_start
  &c
  &c_end--(c_end+(leader_len,0));
pair rhs = point(spring,reltime(spring,1));
pair lhs = point(spring,reltime(spring,0));

draw(pic, spring, DARKPEN+bold_color, BeginBar(6));

pair top = rhs+(0,10);
pair bot = rhs-(0,15);
pair bot_lft = lhs-(40,15);
path border = top--bot--bot_lft--(bot_lft-(0,10))--( bot+(10,-10) )--( top+(10,0))--cycle;
filldraw(pic, border, FILLCOLOR, neutral_color);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

