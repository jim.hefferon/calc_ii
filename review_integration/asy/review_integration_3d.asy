// review_integration_3d.asy

// cd("../../asy");
// import settexpreamble;
// cd("");
// settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = true;
settings.render = 0;
settings.tex="lualatex";  // for compiling in-pic text
// settings.outformat="pdf";
// settings.render=16;
import fontsize;

import graph3; import solids;
currentprojection = orthographic(4,1,10,up=Y);
currentlight=White; defaultrender.merge=true;
// unitsize(10cm,0);

string OUTPUT_FN = "review_integration_3d%03d";

material figure_material = material(diffusepen=bold_color+white+opacity(0.5),
				 emissivepen=bold_color+white,
				 specularpen=bold_color+white);
material slice_material = material(diffusepen=gray_color+opacity(0.25),
				   emissivepen=gray_color,
				   specularpen=gray_color);

currentlight = nolight;


// ===== mass of a thin rod =======

picture pic;
int picnum = 0;
size(pic,6cm,0);
size3(pic,6cm,0,keepAspect=true);

real rad = 0.075;
real len  = 5;
transform3 region_t = rotate(90,Y) * scale(rad,rad,len);
surface region =  region_t * unitcylinder;

// Add slice
transform3 slice_t = shift((0.618*len,0,0))*rotate(90,Y)*scale(rad,rad,1); 
surface slice = slice_t*unitdisk;
draw(pic, slice, surfacepen=slice_material);
// add a circular edge
draw(pic, slice_t*unitcircle3, highlight_color);

draw(pic, region, surfacepen=figure_material);
draw(pic, region_t*unitcircle3, gray(0.85));
draw(pic, shift(len,0,0)*region_t*unitcircle3, gray(0.75));


xaxis3(pic,"$x$",
       0,len+0.5, black, Arrow3(TeXHead2));
yaxis3(pic,Label(""),
        0,0.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
        0,1, black, Arrow3(TeXHead2));

  
shipout(format(OUTPUT_FN,picnum),pic);


// ........ the slice ..........
picture pic;
int picnum = 1;
size(pic,0,1.5cm);
size3(pic,0,1.5cm,0,keepAspect=true);

real rad = 0.5;
real len  = 0.15;

transform3 slice_t = rotate(90,Y)*scale(rad,rad,len); 
surface slice = slice_t*unitcylinder;
surface slice_front = shift(len,0,0)*slice_t*unitdisk;
draw(pic, slice, surfacepen=figure_material);
draw(pic, slice_t*unitcircle3, highlight_color+opacity(0.5));
// surface in front
draw(pic, slice_front, surfacepen=slice_material);
draw(pic, shift(len,0,0)*slice_t*unitcircle3, highlight_color+opacity(0.1));

// Label the slice
// draw(pic, "$r$", (len,0,0)--(len,rad,0));
path3 t = (0,0,0)--(len,0,0);
draw(pic, "$dx$", shift(0,1.3*rad,0)*t, N, Bars3(2));

shipout(format(OUTPUT_FN,picnum),pic);



