\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}
% \documentclass[9pt,t,serif,professionalfont]{beamer}
% \PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
% \PassOptionsToPackage{usenames,dvipsnames}{color}
% \usepackage{../../presentation}
% % \usepackage{concmath-otf}
% \definecolor{Burgundy}{HTML}{8D001A}
% \definecolor{ManiacMansion}{HTML}{00394F}
% \setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
% \setbeamercolor{part title}{fg=ManiacMansion,bg=white}
% \setbeamercolor{alerted text}{fg=Burgundy}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Review of integration}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{textcomp}
\usepackage{listings}
\lstset{basicstyle=\ttfamily\scriptsize,
        upquote=true,
        language=Python,
        keepspaces=true}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\begin{frame}{Accumulating a continuous quantity}
A house with solar panels sometimes takes energy
from the power grid
and sometimes instead supplies it to that grid.
\begin{center}
  \vcenteredhbox{\includegraphics[height=0.35\textheight]{pix/rooftop-solar-panels.jpg}}
  \qquad
  \vcenteredhbox{\includegraphics{asy/review_integration000.pdf}}
\end{center}
What is the total?

\pause
We want to add up all these positives and negatives.
But they are not discrete, they are continuous.
To accumulate a continuous quantity we integrate.
\begin{equation*}
  \int_{0}^{b}p(t)\,dt
\end{equation*}
\end{frame}



\begin{frame}{Prototype}
A simple example is finding the area under a curve and above the axis,
between $a$ and~$b$.
\begin{center}
  \begin{minipage}{0.25\linewidth}
    \begin{equation*}
      \text{Area}=\int_{t=a}^{b} f(t)\,dt  
    \end{equation*}
  \end{minipage}
  \qquad
  \vcenteredhbox{\includegraphics{asy/review_integration011.pdf}}%
\end{center}  

\only<2->{Think that we are sweeping from $a$ to~$b$, accumulating 
  the continuous quantity of area.}
\begin{center}
  \only<2>{\vcenteredhbox{\includegraphics{asy/review_integration001.pdf}}}%
  \only<3>{\vcenteredhbox{\includegraphics{asy/review_integration002.pdf}}}%
  \only<4>{\vcenteredhbox{\includegraphics{asy/review_integration003.pdf}}}%
  \only<5>{\vcenteredhbox{\includegraphics{asy/review_integration004.pdf}}}%
  \only<6>{\vcenteredhbox{\includegraphics{asy/review_integration005.pdf}}}%
  \only<7>{\vcenteredhbox{\includegraphics{asy/review_integration006.pdf}}}%
  \only<8>{\vcenteredhbox{\includegraphics{asy/review_integration007.pdf}}}%
  \only<9>{\vcenteredhbox{\includegraphics{asy/review_integration008.pdf}}}%
  \only<10>{\vcenteredhbox{\includegraphics{asy/review_integration009.pdf}}}%
  \only<11->{\vcenteredhbox{\includegraphics{asy/review_integration010.pdf}}}%
\end{center}
\end{frame}



\section{Reimann's formalization}

\begin{frame}
For a precise approach, divide the interval $\closed{a}{b}$
into many subintervals $\closed{x_i}{x_{i+1}}$.
We make them all of the same width, $\Delta x=x_{i+1}-x_i$. 
Over each subinterval erect a box from the axis up to the function.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/review_integration013.pdf}}%
\end{center}
The sum of the areas of these boxes approximates the total area.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/review_integration012.pdf}}%
\end{center}
\end{frame}


\begin{frame}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/review_integration012.pdf}}%
\end{center}
We take that the approximation improves as the interval width gets smaller.
Consequently, we define the area to be the limit of the approximating sum,
as $\Delta x$ goes to zero.
\begin{equation*}
  \text{Total area}=\int_{x=a}^b f(x)\, dx
                   =\lim_{\Delta x\to 0}\;\biggl(\sum_{i}f(x_i)\Delta x\biggr)
\end{equation*}
\end{frame}





\begin{frame}{Prototype picture}
We may abbreviate that work by showing one 
\alert{slice} or \alert{Reimann box}.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/review_integration014.pdf}}%
\end{center}
We say that it is infinitesimal, that it is $dx$~wide, 
and runs from $y=0$ to $y=f(x)$.
\end{frame}





\section{Fundamental Theorem of Calculus}


\begin{frame}{Fundamental Theorem of Calculus}
\Tm Let $f(t)$ be continuous on the interval $\closed{a}{b}$.
Then this function
\begin{equation*}
  F(x) = \int_{t=a}^{x}f(t)\,dt
  \qquad a\leq x\leq b
\end{equation*}
is such that $F'(x)=f(x)$, so 
it is an \alert{antiderivative} of $f(x)$. 
% (It is also continuous on $\closed{a}{b}$ and differentiable on
% $\open{a}{b}$.)
Consequently, 
\begin{equation*}
  \int_{t=a}^bf(t)\,dt=F(b)-F(a)
\end{equation*}
for any antiderivative~$F\!$.

\medskip
Denote this difference with a vertical bar or square brackets.
\begin{equation*}
  F(b)-F(a)
  =\Bigl. F(x)\Bigr|_{x=a}^b=
  \Bigl[ F(x)\Bigr]_{x=a}^b
\end{equation*}
\end{frame}


\begin{frame}
\Ex Find the area under $f(x)=x^2$ between $x=0$ and~$x=1$.

\pause
We want this.
\begin{equation*}
  \int_{x=0}^{1}x^2\,dx
\end{equation*}
An antiderivative of $x^2$ is $x^3/3$ so the FTC gives this. 
\begin{equation*}
  =\Bigl[\frac{x^3}{3}\Bigr]_{x=0}^{1}
  =\frac{1^3}{3}-\frac{0^3}{3}
  =\frac{1}{3}
\end{equation*}
\begin{center}
  \includegraphics{asy/review_integration015.pdf}
\end{center}
\end{frame}


\begin{frame}
\Ex Find the area under $y=\sin\theta$ between $\theta=0$ and $\theta=\pi$.

\pause
We want this.
\begin{equation*}
  \int_{\theta=0}^{\pi}\sin\theta \,d\theta
\end{equation*}
An antiderivative is $-\cos(\theta)$.
The FTC gives this.
\begin{equation*}
  \Bigl. -\cos\theta \Bigr|_{\theta=0}^{\pi}
  =-\cos(\pi)-(-\cos(0))
  =-(-1)-1=2
\end{equation*}
\bigskip
\begin{center}
  \includegraphics{asy/review_integration016.pdf}
\end{center}
\end{frame}




\section{Net change is the integral of the rate}

\begin{frame}{Net Change Theorem}
This is a restatement of the Fundamental Theorem.

\Tm
The net change in the function~$F$ over the interval $\closed{a}{b}$ 
is given by the integral of the rate.
\begin{equation*}
  \text{Net change}=F(b)-F(a)=\int_{x=a}^bF'(x)\,dx
\end{equation*}

Again, as we sweep from $a$ to $b$ we are accumulating
a continuous quantity.
The quantity we accumulate is the rate of change of some~$F$.
\end{frame}


\begin{frame}
\Ex 
An object moves with velocity given by 
$v(t)=t^3-10t^2+24t$.
How does its position change?
\begin{center}
  \vcenteredhbox{\includegraphics{asy/review_integration017.pdf}}%
\end{center}

\pause
The velocity is positive from $t=0$ to~$4$ and negative from
$4$ to~$6$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/review_integration019.pdf}}%
\end{center}
Over the $dt$-wide interval shown it moves a distance of $v(t)\,dt$.
We accumulate those with an integral.
\begin{equation*}
  p(x)=\int_{t=0}^x v(t)\,dt
\end{equation*}
\end{frame}

\begin{frame}
Of course, we already knew that
position and velocity are related by $p'(t)=v(t)$.
\begin{equation*}
  p(t)=\int t^3-10t^2+24t\,dt
    =\frac{1}{4}t^4-\frac{10}{3}t^3+12t^2+C 
\end{equation*}
So position is the integral of velocity, 
the signed area between the velocity's graph and the axis.
Taking an initial position of $p(0)=0$ gives that the constant is zero, $C=0$.
\begin{center}\small
  \vcenteredhbox{\includegraphics{asy/review_integration018.pdf}}%
  \qquad
  \begin{tabular}{r|r}
    \multicolumn{1}{c}{\textit{Time}} 
      &\multicolumn{1}{c}{\textit{Position}}  \\ \hline
    $0$ &$0$ \\
    $1$ &$8.92$ \\
    $2$ &$25.33$ \\
    $3$ &$38.25$ \\
    $4$ &$42.67$ \\
    $5$ &$39.58$ \\
    $6$ &$36.00$ \\
  \end{tabular}
\end{center}
\end{frame}
% sage: def p(x):
% ....:     return((1/4)*x^4-(10/3)*x^3+12*x^2)
% ....: 
% sage: for i in range(7):
% ....:     print("i=",i," p(i)=",p(i), " value=",n(p(i)))
% ....: 
% ....: 
% i= 0  p(i)= 0  value= 0.000000000000000
% i= 1  p(i)= 107/12  value= 8.91666666666667
% i= 2  p(i)= 76/3  value= 25.3333333333333
% i= 3  p(i)= 153/4  value= 38.2500000000000
% i= 4  p(i)= 128/3  value= 42.6666666666667
% i= 5  p(i)= 475/12  value= 39.5833333333333
% i= 6  p(i)= 36  value= 36.0000000000000



\begin{frame}
\Ex 
This shows a thin rod aligned on the axis, between $x=a$ and $x=b$.
Imagine that its density varies from spot to spot, given by the
function $\rho(x)$.
We want to find the rod's total mass.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/review_integration_3d000.pdf}}%
  \qquad
  \vcenteredhbox{\includegraphics{asy/review_integration_3d001.pdf}}%
\end{center}
\pause
As we sweep from $a$ to~$b$, at the instant~$x$ the rate at which we 
are accumulating mass is $\rho(x)$.
\begin{equation*}
  \text{Total mass}=\int_{x=a}^b \rho(x)\,dx
\end{equation*}
So the mass is the integral of the density.
\end{frame}


\begin{frame}
\Ex
Hooke's Law is that if you take a spring and squeeze or pull it 
by a distance~$x$ from its natural length, then holding it there 
takes a force of $F(x)=k\cdot x$, 
where $k$ is the spring's stiffness factor. 
\begin{center}
  \vcenteredhbox{\includegraphics{asy/review_integration024.pdf}}%
\end{center}
Given a spring with $k=3.2$, find the work done in stretching it
from $a$ to $b$ away from its natural length.

\pause
Work is the integral of force.
\begin{equation*}
  \text{Work} =\int_{x=a}^b k x\;dx
              =3.2\cdot\Bigl[ \frac{x^2}{2}\Bigr]_a^b
              =3.2\cdot\frac{b^2-a^2}{2}
\end{equation*}
\end{frame}





\section{More integrating}

\begin{frame}
\Ex Find the area enclosed by $f(x)=x^2+1$ and $g(x)=e^{-3x}\!$,
between $a=0$ and $b=2$.
\begin{center}
  \includegraphics{asy/review_integration020.pdf}
\end{center}

\pause
We want to compute this.
\begin{equation*}
  \int_{x=0}^2(x^2+1)-e^{-3x}\,dx
\end{equation*}
For the antiderivative of $e^{-3x}$ we take $u=-3x$, so that $du=-3\,dx$ 
and thus $(-1/3)\,du=dx$.
\begin{equation*}
  \Bigl[ \frac{x^3}{3}+x+\frac{1}{3}e^{-3x}\Bigr]_{x=0}^2
  =\Bigl[ \frac{8}{3}+2+\frac{e^{-6}}{3}\Bigr]
    -\Bigl[ \frac{0}{3}+0+\frac{e^{0}}{3}\Bigr]
  =\frac{13+e^{-6}}{3}
  \approx 4.334
\end{equation*}
\end{frame}
% sage: n((13+exp(-6))/3)
% 4.33415958405889



\begin{frame}
\Ex Find the geometric area between $\cos(x)$ and $\sin(x)$,
to the right of $a=0$ and to the left of $b=\pi$.
That is, take the slices to always give a positive contribution.
\begin{center}
  \includegraphics{asy/review_integration021.pdf}%
\end{center}

\pause
\begin{align*}
  % \text{Geometric area}=
  \int_{0}^{\pi}|\cos(x)-\sin(x)|\,dx      
  &=\int_{x=0}^{\pi/4}\cos(x)-\sin(x)\,dx
   +\int_{x=\pi/4}^{\pi}\sin(x)-\cos(x)\,dx   \\
  &=\Bigl[\sin(x)+\cos(x) \Bigr]_{0}^{\pi/4}
    +\Bigl[-\cos(x)-\sin(x) \Bigr]_{\pi/4}^{\pi}  \\
  &=\biggl[(\frac{\sqrt{2}}{2}+\frac{\sqrt{2}}{2})-(0+1) \biggr]
    +\biggl[(1-0)-(-\frac{\sqrt{2}}{2}-\frac{\sqrt{2}}{2}) \biggr]  \\
  &=2\,\sqrt{2} 
\end{align*}
\end{frame}



\begin{frame}{Integrating $dy$}
\Ex Find the area between $y=x^3$ and $y=x+6$ and above the $x$~axis.
\begin{center}
  \includegraphics{asy/review_integration022.pdf}%
\end{center}

\pause
As shown, with slices that are $dx$~thick, 
because there are two types of slices this takes two integrals.
\begin{equation*}
  \int_{x=-6}^{0} x+6\,dx
  +\int_{x=0}^2 x+6-x^3\,dx      
\end{equation*}
\end{frame}


\begin{frame}
If we instead take the slices to be $dy$~thick then there is only one slice
type.
\begin{center}
  \includegraphics{asy/review_integration023.pdf}%
\end{center}
\begin{equation*}
  \int_{y=0}^{8} y^{1/3}-(y-6)\,dy
  =      
  \int_{0}^{8} y^{1/3}-y+6\,dy
  =\Bigl[ \frac{3}{4}y^{4/3}-\frac{1}{2}y^2+6y\Bigr]_{0}^{8}
  =28
\end{equation*}
\end{frame}





\section{Indefinite integrals}

\begin{frame}
The Fundamental Theorem reduces the problem of computing $\int_{a}^bf(x)\,dx$
to that of finding a function whose derivative is $f\!$. 

Where the function~$f$ is defined on some interval, 
an \alert{antiderivative} of~$f$ is a function~$F$
such that $F'(x)=f(x)$ on that interval.

\Ex Let $f(x)=(1/2)x+2$. 
One antiderivative is $(1/4)x^2+2x$.
Another is $(1/4)x^2+2x+1$, and 
still another is $(1/4)x^2+2x-1/3$

\pause
\Tm Given $f$, where $F$ is any particular 
antiderivative, any other function is an antiderivative
if and only if it has 
the form $F(x)+C$ for some real number~$C$.

We denote the operation of antidifferentiation 
with the \alert{indefinite integral}.
\begin{equation*}
  \int f(x)\,dx = F(x)+C
\end{equation*}
It represents a family of functions, one for each real constant $C$.

\pause
\Ex Find all antiderivatives of $\int e^x+x^5\,dx$.
Name three particular ones.

\pause Members of the family have the form
$F(x)=e^x+(1/6)x^6+C$.
Three are $e^x+(1/6)x^6+1$,  and $e^x+(1/6)x^6-1$, and  $e^x+(1/6)x^6\!$.
\end{frame}


\begin{frame}{Integration formulas}\vspace*{-1ex}
Every differentiation rule is an antidifferentiation rule.
\begin{center}
  \begin{tabular}{@{}ll@{}}
    $\displaystyle \int k\,dx=kx+C$ 
     &$\displaystyle \int x^n\,dx=\frac{1}{n+1}x^{n+1}+C\quad n\neq 1$ \\
    $\displaystyle \int e^x\,dx=e^x+C$ 
     &$\displaystyle \int \frac{1}{x}\,dx=\ln |x|+C$  \\ 
    $\displaystyle \int \cos x\,dx=\sin x+C$ 
     &$\displaystyle \int \sin x\,dx=-\cos x+C$ 
  \end{tabular}
\end{center}
\begin{itemize}
\item The antiderivative of a constant times a function is the
constant time the antiderivative of the function.
\begin{equation*}
  \int k\cdot f(x)\,dx=k\cdot\int f(x)\,dx
\end{equation*} 

\item The antiderivative of a sum or difference of two functions
is the sum or difference of the antiderivatives.
\begin{align*}
  &\int f(x)+ g(x)\,dx=\int f(x)\,dx + \int g(x)\,dx   \\
  &\int f(x)- g(x)\,dx=\int f(x)\,dx -\int g(x)\,dx   
\end{align*} 
\end{itemize}
\end{frame}



\begin{frame}{Warning!}
As with derivatives, while integrals play nice with sums, that's
not true for products or quotients or compositions.
\begin{itemize}
\item 
This example shows that the antiderivative of a product
can't be the product of the antiderivatives.
\begin{equation*}
  \int x\cdot x\,dx
  \quad\text{does not equal}\quad 
  \int x\,dx\cdot\int x\,dx    
\end{equation*}
On the left we get some kind of $x^3$ function while on the right is
some kind of $x^4\!$.
\item On the left below we get an $x^2$ function while on the right is 
some kind of $x^{3/2}\!$.
\begin{equation*}
  \int \frac{x^2}{x}\,dx
  \quad\text{does not equal}\quad 
  \frac{\int x^2\,dx}{\int x\,dx}    
\end{equation*}
\item
Where $f(x)=x^3$ and $g(x)=x^2\!$, the integral of the
composition $\composed{f}{g}$ gives an $x^7$ function while the
composition of the integrals gives an $x^{13}$ function.
\end{itemize}
We need separate antiderivative rules for these.
\end{frame}




\begin{frame}{Practice}

Find the antiderivative of each.
\begin{enumerate}
\item $\displaystyle \int x^2+2x+4\,dx$
\pause
\item $\displaystyle \int \sqrt[4]{x^5}\,dx$
\pause
\item $\displaystyle \int 5e^x\,dx$
\pause
\item $\displaystyle \int (x-5)^2\,dx$
\pause
% \item $\displaystyle \frac{5x^2-6x+4}{x^2}$
% \pause
\item $\displaystyle \int 3\sec x\tan x\,dx$
\end{enumerate}
\end{frame}




\begin{frame}{Comments about antiderivatives}

\begin{itemize}
\item
In this class we work with \alert{elementary functions}, those given
by formulas using sums, products, roots and compositions of finitely many 
polynomial, rational, trigonometric, hyperbolic, and exponential functions,
and their inverse functions.
There are elementary functions that have no elementary antiderivative.
Two examples are $e^{-x^2}$ and $\sqrt{1-x^4}$.

\item   Taking a derivative is a straightforward computation.
But finding an antiderivative, going in reverse, can be like doing a puzzle.
Sometimes it takes cleverness.

\item
There is an easy way to verify the
correctness of a claimed antiderivative\Dash just check that it gives the
required derivative.
For instance, we can 
verify the antiderivative
$\int \ln x\,dx=x\ln x-x+C$
by taking the derivative of the right-hand side.
\end{itemize}

\medskip
We will spend the first few weeks learning some techniques for
antidifferentiation.
\end{frame}


\section{Antidifferentiation technique: Substitution}

\begin{frame}
Recall the Chain Rule: where $F(x)=\composed{f}{u}\,(x)$, the derivative is
$F'(x)=f'(\,u(x)\,)\cdot u'(x)$.
In the context of taking antiderivatives this is called   
\alert{Substitution}.

\Tm Let $F$ be an antiderivative of $f\!$.
If $u(x)$ is a differentiable function whose range is some interval on
which~$f$ is continuous then this holds.
\begin{equation*}
  \int f(\,u(x)\,)\cdot u'(x)\,dx
  =F(u(x))+C
\end{equation*}
We often write $u$ instead of $u(x)$.    

\pause
\Ex
$\displaystyle \int e^{8x}\cdot 8\,dx$

A clue that substitution will work 
is that $e^{8x}$ is the composition 
$\composed{f}{u}$ of
$f(x)=e^x$ with $u(x)=8x$.

\pause
Let $u=8x$.
Then $du/dx=8$ so that $du=8\,dx$.
\begin{equation*}
  \int e^{8x}\cdot 8\,dx
  =\int e^u\,du
  =e^u+C
  =e^{8x}+C
\end{equation*}
\end{frame}


\begin{frame}
\Ex $\displaystyle \int \frac{3x^2}{x^3-1}\,dx$

\pause
Let $u=x^3-1$ and then $du/dx=3x^2$, giving $du=3x^2\,dx$.
\begin{equation*}
  \int \frac{3x^2}{x^3-1}\,dx
  =\int u^{-1}\,du
  =\ln|u|+C
\end{equation*}
The answer is $\ln|x^3-1| +C$.

\pause
The next one illustrates how antidifferentiation can be like a puzzle.

\Ex $\displaystyle \int \tan\theta \,d\theta$

\pause
Write it as
\begin{equation*}
  \int \frac{1}{\cos\theta}\cdot \sin\theta\,d\theta
\end{equation*}
and take $u=\cos\theta$ so that $du/d\theta=-\sin\theta$, giving  
$du=-\sin\theta\,d\theta$.
Convert to $-du=\sin\theta\,d\theta$.
\begin{equation*}
  =\int \frac{1}{u}\cdot(-1)\,du
  =-\int \frac{1}{u}\,du        
  =-\ln|u|+C =-\ln|\cos\theta|+C
\end{equation*}
A rule of logarithms allows us to bring the minus sign inside,
$\ln\bigl|1/\cos\theta\bigr|+C=\ln\bigl|\sec\theta\bigr|+C$.
\end{frame}


\begin{frame}{Substitution with definite integrals}
\Tm
$\displaystyle \int_{x=a}^bf(u(x))\cdot u'(x)\,dx
  =\int_{u=u(a)}^{u(b)}f(u)\,du $ 

\pause
\Ex $\displaystyle \int_{x=0}^2x^2\sqrt{x^3+1}\,dx$

\pause
Take $u(x)=u=x^3+1$ so that $du=3x^2\,dx$ and $(1/3)\,du=x^2\,dx$.
As $x$ varies from $0$ to~$2$, we have that $u$ varies from $1$ to~$9$.
\begin{equation*}
  =\int_{u=1}^9\sqrt{u}\cdot\frac{1}{3}\,du
  =\frac{1}{3}\int_{u=1}^9u^{1/2}\,du               
  =\biggl[ \frac{1}{3}\cdot\frac{2}{3}u^{3/2} \biggr]_{u=1}^{9}
  =\frac{52}{9}
\end{equation*}
  
\pause
\Ex 
$\displaystyle \int_{x=0}^2e^{-3x}\,dx$

\pause
Take $u=-3x$ so that $du=-3\,dx$, giving $(-1/3)\,du=dx$.
As $x$ goes from $0$ to~$2$, we have that 
$u$ goes from $0$ to~$-6$.
\begin{equation*}
   =\int_{u=0}^{-6} e^u\cdot \frac{-1}{3}\,du
         =\frac{-1}{3}\cdot \int_{u=0}^{-6} e^u\,du    
         =\frac{-1}{3}\cdot \biggl[ e^{u}\biggr]_{u=0}^{-6}
        =\frac{-1}{3}\cdot(e^{-6}-1)
\end{equation*}
\end{frame}

\begin{frame}{What's next}
We will extend our ability to do indefinite integrals.
For instance, we will learn about trigonometric integrals, and
learn about how to do products in this context.
\end{frame}


% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
