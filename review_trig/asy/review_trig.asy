// review_trig.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "review_trig%03d";

import graph;


real f2(real x) {return x^2;}

// ================ unit circle =======
picture pic;
int picnum = 0;
unitsize(pic,1.5cm);
// size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-1; ytop=1;

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = unitcircle;

// The triangle inside the unit circle
real theta = radians(35);
pair P = (cos(theta), sin(theta));
path hyp = (0,0)--P;
path hyp_extended = (0,0)--(5*P);
path interior_triangle = (0,0)--P--(P.x,0)--(0,0)--cycle;
filldraw(pic, interior_triangle, FILLCOLOR, highlight_color);

// The arc marking the angle
path full_arc = arc( (0,0), 0.25, 0, 90, CCW);
real full_arc_hyp_time = intersect(full_arc, hyp)[0];
path theta_arc = subpath(full_arc, 0, full_arc_hyp_time); 
draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

draw(pic, f, FCNPEN);
dotfactor = 4;
Label L = Label("$(\cos \theta, \sin \theta)$",filltype=Fill(white));
dot(pic, L, P, NE);
label(pic, "$(1,0)$", (1,0), SE);

xaxis(pic, L="", 
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="", 
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ......... tangent ...............
picture pic;
int picnum = 1;
unitsize(pic,1.5cm);
// size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-1; ytop=1;

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = unitcircle;

// The triangle inside the unit circle
real theta = radians(35);
pair P = (cos(theta), sin(theta));
pair P1 = (1, tan(theta));
path hyp = (0,0)--P;
path hyp_extended = (0,0)--P1;
path interior_triangle = (0,0)--P--(P.x,0)--(0,0)--cycle;
path exterior_triangle = (0,0)--P1--(P1.x,0)--(0,0)--cycle;
path tangent_line = (1,-0.35)--(1,P1.y+0.35);
draw(pic, interior_triangle);
draw(pic,tangent_line);
filldraw(pic, exterior_triangle, FILLCOLOR, highlight_color);

// The arc marking the angle
path full_arc = arc( (0,0), 0.25, 0, 90, CCW);
real full_arc_hyp_time = intersect(full_arc, hyp)[0];
path theta_arc = subpath(full_arc, 0, full_arc_hyp_time); 
draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

draw(pic, f, FCNPEN);
dotfactor = 4;
// Label L = Label("$(\cos \theta, \sin \theta)$",filltype=Fill(white));
dot(pic, P);
Label L1 = Label("$(1, \tan \theta)$",filltype=Fill(white));
dot(pic, L1, P1, NE);

xaxis(pic, L="", 
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="", 
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ========== hgt of tree =======
picture pic;
int picnum = 2;
unitsize(pic,1.8cm);


// The triangle
real theta = radians(35);
pair P = (cos(theta), sin(theta));
path hyp = (0,0)--P;
path interior_triangle = (0,0)--P--(P.x,0)--(0,0)--cycle;
filldraw(pic, interior_triangle, FILLCOLOR, highlight_color);

// The arc marking the angle
path full_arc = arc( (0,0), 0.25, 0, 90, CCW);
real full_arc_hyp_time = intersect(full_arc, hyp)[0];
path theta_arc = subpath(full_arc, 0, full_arc_hyp_time); 
draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

// The tree
pair anchor_pt = (P.x-0.01,0.29);
label(pic,graphic("../pix/tree.png","width=1.03cm,angle=0"),
      anchor_pt);

// Label the graph
dotfactor = 4;
Label L = Label("$x\cos \theta$",filltype=Fill(white));
dot(pic, L, P, NE);
label(pic, "$x$", (0.5,0), S);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ====== radians ===========
picture pic;
int picnum = 3;
unitsize(pic,1.5cm);
// size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-1; ytop=1;

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = unitcircle;

// The region inside the unit circle
real theta = radians(105);
pair P = (cos(theta), sin(theta));
path rad_arc = arc( (0,0), 1, 0, degrees(theta), CCW);
path region = ( (0,0) -- relpoint(rad_arc, 0)
  & rad_arc
		& relpoint(rad_arc, 1) -- (0,0)) --cycle;
filldraw(pic, region, FILLCOLOR);
draw(pic, f, FCNPEN);
draw(pic, rad_arc, FCNPEN_NOCOLOR+highlight_color);

// The arc marking the angle; see picnum 25
// path full_arc = arc( (0,0), 0.25, 0, 180, CCW);
// real full_arc_theta_time = intersect(full_arc, relpoint(rad_arc, 1)--(0,0))[0];
// path theta_arc = subpath(full_arc, 0, full_arc_theta_time); 
// draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

dotfactor = 4;
Label L = Label("$(\cos \theta, \sin \theta)$",filltype=Fill(white));
dot(pic, L, P, NW);
label(pic, "$\theta$-long arc", relpoint(rad_arc,0.4), NE, filltype=Fill(white));

xaxis(pic, L="", 
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="", 
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ================ arcsin =======
real f7(real x) {return sin(x);}

picture pic;
int picnum = 7;

size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-1; ytop=1;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f7,-1*pi/2,pi/2), FCNPEN);
// draw(pic,(xleft-0.1,1)--(xright+0.1,1),highlight_color);

// filldraw(pic, circle((1,6),0.05), highlight_color, highlight_color);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

real[] T7 = {-1*pi/2,pi/2};

xaxis(pic, L="$\theta$",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T7, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$-\frac{\pi}{2}$",-1*pi/2,S);
labelx(pic,"$\frac{\pi}{2}$",pi/2,S);
  
Label L = rotate(0)*Label("$\sin(\theta)$");
yaxis(pic, L=L,  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ arcsin =======
real f8(real x) {return asin(x);}

picture pic;
int picnum = 8;

size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-2; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f8,-1,1), FCNPEN);
// draw(pic,(xleft-0.1,1)--(xright+0.1,1),highlight_color);

// filldraw(pic, circle((1,6),0.05), highlight_color, highlight_color);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

xaxis(pic, L="$y$",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T7, Size=2pt),
      arrow=Arrows(TeXHead));

real[] T8 = {-1*pi/2,pi/2};

Label L = rotate(0)*Label("$\sin^{-1}(y)$");
yaxis(pic, L=L, 
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks("%", T8, Size=2pt),
      arrow=Arrows(TeXHead));
labely(pic,"$-\frac{\pi}{2}$",-1*pi/2,W);
labely(pic,"$\frac{\pi}{2}$",pi/2,W);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ value of arctan =======
picture pic;
int picnum = 9;

size(pic,0,3cm,keepAspect=true);

pair origin = (0,0);
real x = 3;
real y = 4;
pair z = (x,y);
pair a = (x,0);

path triangle = origin--a--z--cycle;
draw(pic, triangle, black);
label(pic, "$(x,y)$", z, E);

path c = circle(origin, 0.8);
real t = intersect(c,origin--z)[0];

draw(pic, "$\theta$", subpath(c,0,t), 2*E, arrow=Arrow(ARROW_SIZE));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ Graph of a function and its inverse =======
real f13(real x) {return exp(x);}
real f13a(real x) {return log(x);}

picture pic;
int picnum = 13;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=2;
ybot=-2; ytop=3;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f13,xleft-0.1,log(3)+0.1), black);
draw(pic, graph(f13a,0.18,xright+0.2), black);

filldraw(pic, circle((0.3,exp(0.3)), 0.05), highlight_color, highlight_color);
label(pic, "$(a,b)$", (0.3,exp(0.3)), NW, filltype=Fill(white));
filldraw(pic, circle((exp(0.3),0.3), 0.05), highlight_color, highlight_color);
label(pic, "$(b,a)$", (exp(0.3),0.3), SE, filltype=Fill(white));

// draw(pic, (xleft,xleft)--(xright,xright), highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ...... add line y=x .............
picture pic;
int picnum = 14;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=2;
ybot=-2; ytop=3;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f13,xleft-0.1,log(3)+0.1), black);
draw(pic, graph(f13a,0.18,xright+0.2), black);

filldraw(pic, circle((0.3,exp(0.3)), 0.05), highlight_color, highlight_color);
// label(pic, "$(a,b)$", (0.3,exp(0.3)), NW, filltype=Fill(white));
filldraw(pic, circle((exp(0.3),0.3), 0.05), highlight_color, highlight_color);
// label(pic, "$(b,a)$", (exp(0.3),0.3), SE, filltype=Fill(white));

draw(pic, (xleft,xleft)--(xright,xright), highlight_color+dashed);


xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ...... add tangent lines .............
picture pic;
int picnum = 15;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=2;
ybot=-2; ytop=3;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path fcn =  graph(f13,xleft-0.1,log(3)+0.1);
path fcn_inv = graph(f13a,0.18,xright+0.2); 
draw(pic,fcn,black);
draw(pic,fcn_inv,black);

real t = 0.3;
pair fcn_pt = (t,exp(t));
pair fcn_inv_pt = (fcn_pt.y,fcn_pt.x);

filldraw(pic, circle(fcn_pt, 0.05), highlight_color, highlight_color);
// label(pic, "$(a,b)$", (0.3,exp(0.3)), NW, filltype=Fill(white));
filldraw(pic, circle(fcn_inv_pt, 0.05), highlight_color, highlight_color);
// label(pic, "$(b,a)$", (exp(0.3),0.3), SE, filltype=Fill(white));

// draw(pic, (xleft,xleft)--(xright,xright), highlight_color+dashed);
real fcn_pt_time = times(fcn, fcn_pt)[0];
pair fcn_tangent_dir = 1.5*dir(fcn, fcn_pt_time);
path fcn_tangent = (fcn_pt-fcn_tangent_dir)--(fcn_pt+fcn_tangent_dir);
draw(pic, fcn_tangent, highlight_color);

real fcn_inv_pt_time = times(fcn_inv, fcn_inv_pt)[0];
pair fcn_inv_tangent_dir = 1.5*dir(fcn_inv, fcn_inv_pt_time);
path fcn_inv_tangent = (fcn_inv_pt-fcn_inv_tangent_dir)--(fcn_inv_pt+fcn_inv_tangent_dir); 
draw(pic, fcn_inv_tangent, highlight_color);

real p = 0.6; // length of fcn_tangent_slope
path fcn_tangent_slope_path = fcn_pt--(fcn_pt+(p,0))--point(fcn_tangent,times(fcn_tangent,fcn_pt.x+p)[0]);
draw(pic, fcn_tangent_slope_path,highlight_color+dotted);
draw(pic, reflect((0,0),(1,1))*fcn_tangent_slope_path,highlight_color+dotted);



xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ================cos and  arccos =======
real f18(real x) {return cos(x);}

picture pic;
int picnum = 18;

size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3.25;
ybot=-1; ytop=1;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f18,0,pi), FCNPEN);

real[] T18 = {pi/2,pi};

xaxis(pic, L="$\theta$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T18, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$\frac{\pi}{2}$",pi/2,S);
labelx(pic,"$\pi$",pi,S);
  
Label L = rotate(0)*Label("$\cos(\theta)$");
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .......... arccos ...........
real f19(real x) {return acos(x);}

picture pic;
int picnum = 19;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=0; ytop=3.2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f19,-1,1), FCNPEN);

xaxis(pic, L="$y$",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T7, Size=2pt),
      arrow=Arrows(TeXHead));

real[] T19 = {pi/2,pi};

Label L = rotate(0)*Label("$\cos^{-1}(y)$");
yaxis(pic, L=L,  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=LeftTicks("%", T19, Size=2pt),
      arrow=Arrows(TeXHead));
labely(pic,"$\frac{\pi}{2}$",pi/2,W);
labely(pic,"$\pi$",pi,E);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ tan and  arctan =======
real f20(real x) {return tan(x);}

picture pic;
int picnum = 20;

size(pic,0,4.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1.75; xright=1.75;
ybot=-3.75; ytop=3.75;

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f20,-pi/2+0.25,pi/2-0.25), FCNPEN);

// asymptotes
draw(pic, (-pi/2,ybot)--(-pi/2,ytop), dashed);
draw(pic, (pi/2,ybot)--(pi/2,ytop), dashed);

real[] T18 = {-pi/2,pi/2};
xaxis(pic, L="$\theta$",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%", T18, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$-\frac{\pi}{2}$",-pi/2,SW);
labelx(pic,"$\frac{\pi}{2}$",pi/2,NE);
  
Label L = rotate(0)*Label("$\tan(\theta)$", filltype=Fill(white));
yaxis(pic, L="",   // L,  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
label(pic, "$\tan(\theta)$", (0,3.5),  W, filltype=Fill(white));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .......... arctan ...........
real f21(real x) {return atan(x);}

picture pic;
int picnum = 21;

size(pic,4.5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-4; xright=4;
ybot=-2; ytop=2;

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f21,-4,4), FCNPEN);
// asymptotes
draw(pic, (xleft,-pi/2)--(xright,-pi/2), dashed);
draw(pic, (xleft,pi/2)--(xright,pi/2), dashed);

xaxis(pic, L="$y$",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T7, Size=2pt),
      arrow=Arrows(TeXHead));

real[] T21 = {pi/2,-pi/2};

// Label L = rotate(0)*Label("$\tan^{-1}(y)$", filltype=Fill(white));
yaxis(pic, L="", //L,  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=LeftTicks("%", T21, Size=2pt),
      arrow=Arrows(TeXHead));
labely(pic,"$-\frac{\pi}{2}$",-pi/2,SE);
labely(pic,"$\frac{\pi}{2}$",pi/2,NE);
label(pic, "$\tan^{-1}(y)$", (0,2.25),  W, filltype=Fill(white));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ sin, cos, tan =======
real f22(real x) {return sin(x);}

picture pic;
int picnum = 22;

size(pic,0,1.75cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-pi/4; xright=9*pi/4;
ybot=-1; ytop=1;

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f22,xleft-0.25,xright+0.25), FCNPEN);

real[] T22 = {-pi/4,pi/2,3*pi/2,2*pi};
xaxis(pic, L="$\theta$",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%", T22, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$\frac{\pi}{2}$",pi/2,S);
labelx(pic,"$\pi$",pi,S);
labelx(pic,"$\frac{3\pi}{2}$",3*pi/2,S);
labelx(pic,"$2\pi$",2*pi,S);
  
Label L = rotate(0)*Label("$\sin(\theta)$",filltype=Fill(white));
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
label(pic, L, (0,1), W);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// ......... cos ..........
real f23(real x) {return cos(x);}

picture pic;
int picnum = 23;

size(pic,0,1.75cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-pi/4; xright=9*pi/4;
ybot=-1; ytop=1;

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f23,-0.1,xright+0.25), FCNPEN);

real[] T22 = {-pi/4,pi/2,3*pi/2,2*pi};
xaxis(pic, L=Label("$\theta$",filltype=Fill(white)),  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%", T22, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$\frac{\pi}{2}$",pi/2,S);
labelx(pic,"$\pi$",pi,S);
labelx(pic,"$\frac{3\pi}{2}$",3*pi/2,S);
labelx(pic,"$2\pi$",2*pi,S);
  
Label L = rotate(0)*Label("$\cos(\theta)$",NW,filltype=Fill(white));
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
label(pic, L, (0,1), W);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// ...... tan ..........
real f24(real x) {return tan(x);}

picture pic;
int picnum = 24;

size(pic,0,4.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-pi; xright=pi;
ybot=-3.75; ytop=3.75;

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f24,-pi-0.2,-pi/2-0.25), FCNPEN);
draw(pic, graph(f24,-pi/2+0.25,pi/2-0.25), FCNPEN);
draw(pic, graph(f24,pi/2+0.25,pi+0.2), FCNPEN);

// asymptotes
draw(pic, (-pi/2,ybot)--(-pi/2,ytop), dashed);
draw(pic, (pi/2,ybot)--(pi/2,ytop), dashed);

real[] T24 = {-pi/2,pi/2};
xaxis(pic, L="$\theta$",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%", T24, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic,"$-\frac{\pi}{2}$",-pi/2,SW);
labelx(pic,"$\frac{\pi}{2}$",pi/2,NE);
  
Label L = rotate(0)*Label("$\tan(\theta)$",filltype=Fill(white));
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
label(pic, L, (0,3.5), W);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ====== Second half of radians ===========
picture pic;
int picnum = 25;
unitsize(pic,1.5cm);
// size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-1; ytop=1;

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = unitcircle;

// The region inside the unit circle
real theta = radians(105);
pair P = (cos(theta), sin(theta));
path rad_arc = arc( (0,0), 1, 0, degrees(theta), CCW);
path region = ( (0,0) -- relpoint(rad_arc, 0)
  & rad_arc
		& relpoint(rad_arc, 1) -- (0,0)) --cycle;
filldraw(pic, region, FILLCOLOR);
draw(pic, f, FCNPEN);
draw(pic, rad_arc, FCNPEN_NOCOLOR+highlight_color);

// The arc marking the angle
path full_arc = arc( (0,0), 0.25, 0, 180, CCW);
real full_arc_theta_time = intersect(full_arc, relpoint(rad_arc, 1)--(0,0))[0];
path theta_arc = subpath(full_arc, 0, full_arc_theta_time); 
draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

dotfactor = 4;
Label L = Label("$(\cos \theta, \sin \theta)$",filltype=Fill(white));
dot(pic, L, P, NW);
label(pic, "$\theta$-long arc", relpoint(rad_arc,0.4), NE, filltype=Fill(white));

xaxis(pic, L="", 
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="", 
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");








