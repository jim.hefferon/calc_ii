// cross_product.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "cross_product%03d";

import graph;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// =========== cross product circle of unit vectors ===============

picture pic;
int picnum = 0;
size(pic,0,2.5cm,keepAspect=true);

path c = reverse(circle( (0,0), 1));

draw(pic, c, FCNPEN_NOCOLOR+highlight_color,Arrow(ARROW_SIZE,position=1));
draw(pic, c, FCNPEN_NOCOLOR+highlight_color,Arrow(ARROW_SIZE,position=2));
draw(pic, c, FCNPEN_NOCOLOR+highlight_color,Arrow(ARROW_SIZE,position=3));
draw(pic, c, FCNPEN_NOCOLOR+highlight_color,Arrow(ARROW_SIZE,position=4));

// label the unit vectors
dotfactor =  5;
dot(pic, "$\vec{i}$", (Cos(90),Sin(90)), 1.5*N);
dot(pic, "$\vec{j}$", (Cos(-30),Sin(-30)), 1.5*SE);
dot(pic, "$\vec{k}$", (Cos(-150),Sin(-150)), 1.5*SW);


shipout(format(OUTPUT_FN,picnum),pic);


// =========== area of parallelogram ===============

picture pic;
int picnum = 1;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=3;

pair v1 = (3,1);
pair v2 = (1,2);
pair tp = v1+v2;  // top of box

real x1= v1.x;
real x2 =v2.x;
real y1= v1.y;
real y2 =v2.y;

path frm = (0,0)--(tp.x,0)--tp--(0,tp.y)--cycle;
path parallelogram = (0,0)--v1--tp--v2--cycle;
path parallelogram_rest = v1--tp--v2;

// draw the figures
fill(pic, parallelogram, gray(0.90));
draw(pic, frm, black);
draw(pic, parallelogram_rest, black);
draw(pic, (0,y2)--(x2,y2), black);  // region A
draw(pic, (x2,tp.y)--(x2,y2), black);
draw(pic, (x1,y1)--(tp.x,y1), black); // region F
draw(pic, (x1,y1)--(x1,0), black);

label(pic, "{\scriptsize $A$}", (x2/2,y2+(tp.y-y2)/2) );
label(pic, "{\scriptsize $B$}", (x2+((x1-x2)/2),y2+2*(tp.y-y2)/3) );
label(pic, "{\scriptsize $C$}", (0.35*x2,1.25*y1) );
label(pic, "{\scriptsize $D$}", (x1+0.6(tp.x-x1),0.8*y2) );
label(pic, "{\scriptsize $E$}", (x2+((x1-x2)/2),0.35*y1) );
label(pic, "{\scriptsize $F$}", (x1+((tp.x-x1)/2),y1/2) );

draw(pic, (0,0)--v1, highlight_color, Arrow(ARROW_SIZE));
draw(pic, (0,0)--v2, highlight_color, Arrow(ARROW_SIZE));

real[] T1x = {x1, x2}; 
// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
// xaxis(pic, L="",  
//       axis=YEquals(ytop+0.2),
//       xmin=xleft-0.25, xmax=xright+0.25,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// xaxis(pic, L="",  
//       axis=YEquals(ybot-0.2),
//       xmin=xleft-0.25, xmax=xright+0.25,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.3, xmax=xright+0.3,
      p=currentpen,
      ticks=RightTicks("%", T1x, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x_1$", x1);
labelx(pic, "$x_2$", x2);

real[] T1y = {y1, y2}; 
// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
// yaxis(pic, L="",  
//       axis=XEquals(xleft-0.2),
//       ymin=ybot-0.25, ymax=ytop+0.25,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// yaxis(pic, L="",  
//       axis=XEquals(xright+0.2),
//       ymin=ybot-0.25, ymax=ytop+0.25,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.3, ymax=ytop+0.3,
      p=currentpen,
      ticks=LeftTicks("%", T1y, Size=2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$y_1$", y1);
labely(pic, "$y_2$", y2);

shipout(format(OUTPUT_FN,picnum),pic);




