// cross_product_3d.asy

// compile with asy -inlineimage stub_3d
// In the .tex file put this in the preamble ... 
// % For 3D PRC asymptote
// % compile with asy -inlineimage stub_3d
// \def\asydir{asy/}
// \graphicspath{{asy/}}
// \input asy/stub_3d.pre
// \usepackage[bigfiles]{media9}
// \RequirePackage{asymptote}
// ... and then include the 3D material with something like this.
// \begin{center}
//   \uncover<2->{\input asy/stub_3d001.tex }% need the space following .tex   
// \end{center}


cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = true;
settings.embed = true;
settings.render = 0;
settings.tex="lualatex";  // for compiling in-pic text
settings.outformat="pdf";

import embed;

import fontsize;

import graph3; import solids;
currentprojection = orthographic(1.2,3,2,up=Z);
currentlight=White; defaultrender.merge=true;
// unitsize(1cm);

string OUTPUT_FN = "cross_product_3d%03d";

material figure_material = material(diffusepen=bold_color+white+opacity(0.5),
				 emissivepen=bold_color+white,
				 specularpen=bold_color+white);
material slice_material = material(diffusepen=gray_color+opacity(0.25),
				   emissivepen=gray_color,
				   specularpen=gray_color);
pen boundary_pen = gray(0.2)+opacity(0.5);

currentlight = nolight;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// ===== Definition of cross product =======
// currentprojection = orthographic(4,1,10,up=Y);


// .............. two vectors and the plane they define ............
picture pic;
int picnum = 0;
size(pic,0,3cm);
size3(pic,0,3cm,0,keepAspect=true);

path3 p_edge = (0,0,0)--(3,0,0)--(3,5,0)--(0,5,0)--cycle;
surface p = surface(p_edge);
draw(pic, p_edge, boundary_pen+highlight_color);
draw(pic, p, surfacepen = figure_material);

triple v = (1,3,0);
triple u = (2,2,0);
triple c = cross(u,v);
draw(pic, Label("$\vec{v}$",Relative(0.75)), O--v, 0.8*E, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{u}$",Relative(0.75)), O--u, Arrow3(DefaultHead2,ARROW_SIZE));
// draw(pic, "$\vec{w}$", O--unit(c), highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

// triple v_norm = unit(v);
// triple u_norm = unit(u);
// draw(pic, arc(O, u_norm/2, v_norm/2), Arrow3(DefaultHead2,ARROW_SIZE/2));

// xaxis3(pic,Label("$x$"),
//        0,3.2, black, Arrow3(TeXHead2));
// yaxis3(pic,Label("$y$"),
//         0,3.2, black, Arrow3(TeXHead2));
// zaxis3(pic,Label(""),\
//         0,2.2, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


// .......... a vector perp to the two ................
picture pic;
int picnum = 1;
size(pic,0,3cm);
size3(pic,0,3cm,0,keepAspect=true);

path3 p_edge = (0,0,0)--(3,0,0)--(3,5,0)--(0,5,0)--cycle;
surface p = surface(p_edge);
draw(pic, p_edge, boundary_pen);
draw(pic, p, surfacepen = figure_material);

triple v = (1,3,0);
triple u = (2,2,0);
triple w = cross(u,v);
draw(pic, Label("$\vec{v}$",Relative(0.75)), O--v, 0.8*E, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{u}$",Relative(0.75)), O--u, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, "$\vec{w}$", O--unit(w), highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

// arc marking angle between vectors
// draw(pic, arc(O, unit(u)/2, unit(v)/2), Arrow3(DefaultHead2,ARROW_SIZE/2));

shipout(format(OUTPUT_FN,picnum),pic);


// .......... a vector perp to the two, but in opposite dir ................
picture pic;
int picnum = 2;
size(pic,0,3cm);
size3(pic,0,3cm,0,keepAspect=true);

path3 p_edge = (0,0,0)--(3,0,0)--(3,5,0)--(0,5,0)--cycle;
surface p = surface(p_edge);

triple v = (1,3,0);
triple u = (2,2,0);
triple w = cross(u,v);
draw(pic, "$\vec{w}$", O--(-1*unit(w)), highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

// surface over the vector
draw(pic, p_edge, boundary_pen);
draw(pic, p, surfacepen = figure_material);

draw(pic, Label("$\vec{v}$",Relative(0.75)), O--v, 0.8*E, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{u}$",Relative(0.75)), O--u, Arrow3(DefaultHead2,ARROW_SIZE));


shipout(format(OUTPUT_FN,picnum),pic);



// .......... a vector perp to the two, right hand rule ................
picture pic;
int picnum = 3;
size(pic,0,3cm);
size3(pic,0,3cm,0,keepAspect=true);

path3 p_edge = (0,0,0)--(3,0,0)--(3,5,0)--(0,5,0)--cycle;
surface p = surface(p_edge);
draw(pic, p_edge, boundary_pen);
draw(pic, p, surfacepen = figure_material);

triple v = (1,3,0);
triple u = (2,2,0);
triple w = cross(u,v);
draw(pic, Label("$\vec{v}$",Relative(0.75)), O--v, 0.8*E, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{u}$",Relative(0.75)), O--u, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, "$\vec{w}$", O--unit(w), highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

// arc marking angle between vectors
draw(pic, arc(O, unit(u)/2, unit(v)/2), highlight_color, Arrow3(DefaultHead2,ARROW_SIZE/2));

shipout(format(OUTPUT_FN,picnum),pic);




// .......... a vector perp to the two, length is area of parallelogram ......
picture pic;
int picnum = 4;
size(pic,0,3cm);
size3(pic,0,3cm,0,keepAspect=true);

path3 p_edge = (0,0,0)--(3,0,0)--(3,5,0)--(0,5,0)--cycle;
surface p = surface(p_edge);
draw(pic, p_edge, boundary_pen);
draw(pic, p, surfacepen = figure_material);

triple v = (1,3,0);
triple u = (2,2,0);
triple w = cross(u,v);
draw(pic, Label("$\vec{v}$",Relative(0.75)), O--v, 0.8*E, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{u}$",Relative(0.75)), O--u, Arrow3(DefaultHead2,ARROW_SIZE));
path3 parallelogram = O--v--(v+u)--u--cycle;
draw(pic, surface(parallelogram), surfacepen=slice_material);
// draw(pic, v--(v+u), highlight_color+dotted);
// draw(pic, u--(v+u), highlight_color+dotted);

draw(pic, "$\vec{w}$", O--w, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

// arc marking angle between vectors
// draw(pic, arc(O, unit(u)/2, unit(v)/2), Arrow3(DefaultHead2,ARROW_SIZE/2));

shipout(format(OUTPUT_FN,picnum),pic);




// ============ example of cross product ============
currentprojection = orthographic(2,0.5,0.5,up=Z);

picture pic;
int picnum = 5;
size(pic,0,3cm);
size3(pic,0,3cm,0,keepAspect=true);

triple u = (1,2,3);
triple v = (4,5,6);
triple w = cross(u,v);
draw(pic, Label("$\vec{v}$",Relative(0.75)), O--v, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{u}$",Relative(0.75)), O--u, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{w}$", Relative(0.75)), O--w, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

xaxis3(pic,Label("$x$"),
       -3.5,4.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));
yaxis3(pic,Label("$y$"),
        -0.5,6.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));
zaxis3(pic,Label(""),
         -3.5,6.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);





// ============ cross product satisfies right hand rule ============
currentprojection = orthographic(2,0.5,0.5,up=Z);


// .......... two vectors ............
picture pic;
int picnum = 6;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

triple u = (3,2,2);
triple v = (1,4,3);
// triple w = cross(u,v);

// triple u_proj = (u.x, u.y, 0);
// triple v_proj = (v.x, v.y, 0);

// draw(pic, u--u_proj, dotted);
// draw(pic, v--v_proj, dotted);

draw(pic, Label("$\vec{u}$",Relative(0.75)), O--u, 2*W, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{v}$",Relative(0.75)), O--v, 2*W, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

// draw(pic, Label("$p(\vec{u})$",Relative(0.75)), O--u_proj, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
// draw(pic, Label("$p(\vec{v})$",Relative(0.75)), O--v_proj, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

xaxis3(pic,Label("$x$"),
       -0.5,4.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));
yaxis3(pic,Label("$y$"),
        -0.5,4.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));
zaxis3(pic,Label(""),
         -0.5,3.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// .......... two vectors plus cross product ............
picture pic;
int picnum = 7;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

triple u = (3,2,2);
triple v = (1,4,3);
triple w = cross(u,v);

triple u_proj = (u.x, u.y, 0);
triple v_proj = (v.x, v.y, 0);

// draw(pic, u--u_proj, dotted);
// draw(pic, v--v_proj, dotted);

draw(pic, Label("$\vec{u}$",Relative(0.9)), O--u, black, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{v}$",Relative(0.9)), O--v, black, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{u}\times\vec{v}$",Relative(2*W)), O--w, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

// draw(pic, Label("$p(\vec{u})$",Relative(0.75)), O--u_proj, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
// draw(pic, Label("$p(\vec{v})$",Relative(0.75)), O--v_proj, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

xaxis3(pic,Label("$x$"),
       -0.5,4.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));
yaxis3(pic,Label("$y$"),
        -0.5,4.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));
zaxis3(pic,Label(""),
         -0.5,3.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);




// .......... two vectors plus viewer ............
picture pic;
int picnum = 8;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

triple u = (3,2,2);
triple v = (1,4,3);
// triple w = cross(u,v);

triple u_proj = (u.x, u.y, 0);
triple v_proj = (v.x, v.y, 0);

// draw(pic, u--u_proj, dotted);
// draw(pic, v--v_proj, dotted);

draw(pic, Label("$\vec{u}$",Relative(0.9)), O--u, black, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{v}$",Relative(0.9)), O--v, black, Arrow3(DefaultHead2,ARROW_SIZE));
// draw(pic, Label("$\vec{u}\times\vec{v}$",Relative(2*W)), O--w, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
label(pic,graphic("../pix/man.png","width=0.5cm"),(0,0,4));

// draw(pic, Label("$p(\vec{u})$",Relative(0.75)), O--u_proj, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
// draw(pic, Label("$p(\vec{v})$",Relative(0.75)), O--v_proj, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

xaxis3(pic,Label("$x$"),
       -0.5,4.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));
yaxis3(pic,Label("$y$"),
        -0.5,4.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));
zaxis3(pic,Label(""),
         -0.5,3.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// .......... two vectors plus projections ............
picture pic;
int picnum = 9;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

triple u = (3,2,2);
triple v = (1,4,3);
// triple w = cross(u,v);

triple u_proj = (u.x, u.y, 0);
triple v_proj = (v.x, v.y, 0);

draw(pic, u--u_proj, dotted);
draw(pic, v--v_proj, dotted);

draw(pic, Label("$\vec{u}$",Relative(0.75)), O--u, 2*W, black, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{v}$",Relative(0.75)), O--v, 2*W, black, Arrow3(DefaultHead2,ARROW_SIZE));
// draw(pic, Label("$\vec{u}\times\vec{v}$",Relative(2*W)), O--w, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
// label(pic,graphic("../pix/man.png","width=0.5cm"),(0,0,4));

draw(pic, Label("$p(\vec{u})$",Relative(0.75)), O--u_proj, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$p(\vec{v})$",Relative(0.75)), O--v_proj, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

xaxis3(pic,Label("$x$"),
       -0.5,4.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));
yaxis3(pic,Label("$y$"),
        -0.5,4.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));
zaxis3(pic,Label(""),
         -0.5,3.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrows3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// .......... projections with angles ............
picture pic;
int picnum = 10;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

triple u = (3,2,2);
triple v = (1,4,3);

triple u_proj = (u.x, u.y, 0);
triple v_proj = (v.x, v.y, 0);

// draw(pic, Label("$\vec{u}$",Relative(0.75)), O--u, 2*W, black, Arrow3(DefaultHead2,ARROW_SIZE));
// draw(pic, Label("$\vec{v}$",Relative(0.75)), O--v, 2*W, black, Arrow3(DefaultHead2,ARROW_SIZE));
// draw(pic, Label("$\vec{u}\times\vec{v}$",Relative(2*W)), O--w, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
// label(pic,graphic("../pix/man.png","width=0.5cm"),(0,0,4));

draw(pic, Label("$p(\vec{u})$",Relative(0.75)), O--u_proj, black, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$p(\vec{v})$",Relative(0.75)), O--v_proj, black, Arrow3(DefaultHead2,ARROW_SIZE));
path3 u_arc = arc(O, 0.5*X, 0.5*unit(u_proj));
path3 v_arc = arc(O, 2*X, 2*unit(v_proj));
draw(pic, "$\alpha$", u_arc, S, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE/2));
draw(pic, Label("$\beta$",Relative(0.925)), v_arc, S, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE/2));

xaxis3(pic,Label("$x$"),
       0,4.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
       0,4.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrow3(TeXHead2));
zaxis3(pic,Label(""),
         0,3.5, black,
       OutTicks("%", Step=1, step=0, Size=2pt),
       Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ================= scalar triple product ========
currentprojection = orthographic(3,0.25,0.25,up=Z);

picture pic;
int picnum = 11;
size(pic,0,3cm);
size3(pic,0,3cm,0,keepAspect=true);

triple a = (0,0.5,3);
triple b = (0,3,0);
triple c = (-3,1,0);

draw(pic, b--(b+c)--c, gray(0.8));  // base
draw(pic, a--(a+b)--(a+b+c)--(a+c)--cycle, gray(0.8));  //top
draw(pic, b--(a+b), gray(0.8));  // side
draw(pic, (b+c)--(a+b+c), gray(0.8));  // side
draw(pic, c--(a+c), gray(0.8));  // side

draw(pic, Label("$\vec{b}\times\vec{c}$",Relative(0.70)), O--(0,0,4), W, Arrow3(DefaultHead2,ARROW_SIZE));  // cross product

draw(pic, Label("$\vec{a}$",Relative(0.75)), O--a, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{b}$",Relative(0.75)), O--b, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{c}$",Relative(0.85)), O--c, NW, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

// xaxis3(pic,Label("$x$"),
//        0,4.5, black,
//        OutTicks("%", Step=1, step=0, Size=2pt),
//        Arrow3(TeXHead2));
// yaxis3(pic,Label("$y$"),
//        0,4.5, black,
//        OutTicks("%", Step=1, step=0, Size=2pt),
//        Arrow3(TeXHead2));
// zaxis3(pic,Label(""),
//          0,3.5, black,
//        OutTicks("%", Step=1, step=0, Size=2pt),
//        Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);
