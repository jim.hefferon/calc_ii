\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 12.4\hspace{1em}Cross product}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Cross product}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage stub_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
% uncomment for 3D graphics: 
\input asy/cross_product_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}
\usepackage{arydshln}  % dashed lines in arrays


\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



% \section{}


\begin{frame}{Motivation}
Here we will develop an operation that takes us into the third 
dimension,
that inputs two vectors such as these two
\begin{center}
  \input asy/cross_product_3d000.tex %
\end{center}
\pause
and outputs a third vector perpendicular to
those them, and in fact perpendicular to every other vector in
the plane defined by those two.
\begin{center}
  \input asy/cross_product_3d001.tex %
\end{center}
\end{frame}

\begin{frame}
The prior slide shows a vector perpendicular to the given two but it is not
the only such vector.
That is, what we've said so far isn't enough to
fully determine the output vector.
One thing is that in terms of the view in the prior drawing,
we must decide whether the vector will go up or down.  
\begin{center}
  \input asy/cross_product_3d002.tex %
\end{center}
We've hedged with ``in terms of'' because 
we can rotate the plane to make `down' into `up'. 
So those words 
say something about the view rather than 
about the intrinsic properties of the vectors.
\end{frame}

\begin{frame}
We eliminate this ambiguity by requiring that 
the output vector satisfy the 
\alert{right hand rule}:~place your right hand with your fingers 
curling from $\vec{u}$
to~$\vec{v}$, and then the new vector will go with your thumb.
\begin{center}
  \vcenteredhbox{\includegraphics[height=0.25\textheight]{pix/right.png}}
  \qquad
  \vcenteredhbox{\input asy/cross_product_3d003.tex }%
\end{center}
A left hand system would have the new vector\Dash your thumb\Dash go
the opposite way. 

\pause
Here is a potential snag:~what if the two vectors are co-linear, 
where one is a multiple of the other?
Then they don't suffice to determine a plane, so they don't suffice to
determine a vector erected perpendicular to the plane.
In this special case we take the operation to output the zero vector.
\end{frame}


\begin{frame}
To fully determine the output vector, the second thing that we must decide 
is its length. 
Experience shows that the most convenient thing is to make its  
length equal to the area of the parallelogram formed by the two vectors,
$|\vec{u}|\,|\vec{v}|\cdot\sin\theta$.
\begin{center}
  \vcenteredhbox{\input asy/cross_product_3d004.tex }%
\end{center}
\pause
\textit{Comment:}~why not a unit vector?
One reason is to consider what happens
in the special case where $\vec{u}$ and $\vec{v}$ are colinear.
We want the operation to output the zero vector.
But we cannot make the zero vector into a unit vector in the same direction
because the zero vector has no direction.
So defining it to be a unit vector would introduce a discontinuity.
In contrast, the expression
$|\vec{u}|\,|\vec{v}|\sin\theta$ has the 
property that if we imagine rotating the vector $\vec{v}$ until it  
lies on top of~$\vec{u}$ then the formula's output vector comes to have
length zero.
\end{frame}


\begin{frame}{Definition}
\Df The \alert{cross product} or \alert{vector product} 
of the three dimensional vectors $\vec{u}$ and $\vec{v}$ is the vector
\alert{$\vec{u}\times\vec{v}$} that is perpendicular to those two,
that satisfies the right hand rule, and that has length 
$|\vec{u}|\,|\vec{v}|\cdot\sin\theta$
where $\theta$ is the angle between the two.

\pause
Since the definition uniquely determines the cross product there
must be a formula.
Let the components be $u_1,u_2,u_3$
and~$v_1,v_2,v_3$.
We are looking for $\vec{u}\times\vec{v}$'s components $x,y,z$.
Perpendicularity gives this linear system.
\begin{equation*}
  \begin{linsys}{3}
    u_1\cdot x &+ &u_2\cdot y &+ &u_3\cdot z &= 0   \\  
    v_1\cdot x &+ &v_2\cdot y &+ &v_3\cdot z &= 0         
  \end{linsys}
\end{equation*}
Perform the row operations
$v_1\cdot \rho_1$ and $u_1\cdot \rho_2$ and then subtract, $\rho_1-\rho_2$.
\begin{equation*}
  (u_2v_1-u_1v_2)\cdot y+(u_3v_1-u_1v_3)\cdot z=0
\end{equation*}
This one-equation, two-variable system has infinitely many solutions.
The natural one takes these.
\begin{equation*}
  y=u_3v_1-u_1v_3
  \qquad
  z=-(u_2v_1-u_1v_2)
\end{equation*}
Plugging those in gives an~$x$.
\begin{align*}
    u_1\cdot x + u_2\cdot (u_3v_1-u_1v_3)  - u_3\cdot (u_2v_1-u_1v_2) &= 0   \\  
    u_1\cdot x  &= -u_2\cdot (u_3v_1-u_1v_3)  + u_3\cdot (u_2v_1-u_1v_2)   \\  
             x  &= u_2v_3-u_3v_2
\end{align*}
\end{frame}


\begin{frame}
Although the solution on the prior slide is only one of infinitely many, 
it proves to also satisfy the right hand rule 
and area conditions. 
Since they uniquely determine the vector, this must be the 
right formula
(the verification that it satisfies the conditions 
is at the end.)

\Lm This formula gives the cross product.  
\begin{equation*}
  \colvec{u_1 \\ u_2 \\ u_3}\times\colvec{v_1 \\ v_2 \\ v_3}
  =\colvec{u_2v_3-u_3v_2 \\ u_3v_1-u_1v_3 \\ u_1v_2-u_2v_1}
\end{equation*}

\pause
\Ex
\begin{equation*}
  \colvec{1 \\ 2 \\ 3}\times\colvec{4 \\ 5 \\ 6}
  =\colvec{2\cdot 6-3\cdot 5 \\ 3\cdot 4-1\cdot 6 \\ 1\cdot 5-2\cdot 4}
  =\colvec{-3 \\ 6 \\ -3}
\end{equation*}
Here $\vec{w}$ is orthogonal to the $\vec{u}\vec{v}$ plane.
\begin{center}
  \vcenteredhbox{\input asy/cross_product_3d005.tex }%
\end{center}
\end{frame}

\begin{frame}{Practice}
\begin{enumerate}
\item Use the formula to find the cross product of the two vectors.
\begin{equation*}
  \colvec{4 \\ 2 \\ 1}\times\colvec{3 \\ 0 \\ -2}
\end{equation*}

\pause
\begin{equation*}
  \colvec{4 \\ 2 \\ 1}\times\colvec{3 \\ 0 \\ -2}
  =\colvec{2\cdot (-2)-1\cdot 0 \\ 1\cdot 3-4\cdot(-2) \\ 4\cdot 0-2\cdot 3}
  =\colvec{-4 \\ 11 \\ -6}
\end{equation*}

\pause\item
Take the cross product in the opposite order.

\pause
\begin{equation*}
  \colvec{3 \\ 0 \\ -2}\times\colvec{4 \\ 2 \\ 1}
  =\colvec{4 \\ -11 \\ 6}
\end{equation*}
\end{enumerate}
  
\end{frame}
% sage: A = vector([4,2,1])
% sage: B = vector([3,0,-2])
% sage: A.cross_product(B)
% (-4, 11, -6)
% sage: B.cross_product(A)
% (4, -11, 6)

\begin{frame}
\Lm Cross product is \alert{anticommutative}, 
$\vec{v}\times\vec{u}=-(\vec{u}\times\vec{v})$

\Pf Compare curling the fingers of your right hand from $\vec{u}$ to~$\vec{v}$
with curling them from $\vec{v}$ to~$\vec{u}$.
Your thumb points in opposite directions. 
\end{frame}


\section{Determinants as a mnemonic for computing cross product}
\begin{frame}{Determinants}
This expression is the \alert{determinant}
of a $2$-by-$2$ array of numbers.
\begin{equation*}
  \begin{vmatrix}
    a  &b  \\
    c  &d  
  \end{vmatrix}
  =ad-bc
\end{equation*}
This is the determinant
of a $3$-by-$3$ array.
\begin{equation*}
  \begin{vmatrix}
    a  &b  &c  \\
    d  &e  &f  \\
    g  &h  &i 
  \end{vmatrix}
  =aei+bfg+cdh-gec-hfa-bid
\end{equation*}
A way to remember this one is to repeat the first two columns, 
and then add multiples down the left to right diagonals while 
subtracting the right to left diagonals. 
\begin{equation*}
  \left|\begin{array}{ccc:cc}
    a  &b  &c  &a  &b   \\
    d  &e  &f  &d  &e   \\
    g  &h  &i  &g  &h   \\
  \end{array}\right|
  =
  \begin{array}[t]{l}
    a\cdot(ei-hf)  \\[0.25ex]
    +b\cdot(fg-id) \\[0.25ex]
    +c\cdot(dh-ge)
  \end{array}
\end{equation*}
\end{frame}

\begin{frame}
Determinants are perfectly sensible operations with a quite reasonable
meaning. 
They find the area or volume of the box whose sides are given 
by the column vectors in the array.
\begin{center}
  \vcenteredhbox{\includegraphics{cross_product001.pdf}}
  \qquad
  \begin{minipage}{0.4\textwidth}
    parallelogram determined by
    \begin{equation*}
      \colvec{x_1 \\ y_1},\colvec{x_2 \\ y_2}
    \end{equation*}
  \end{minipage} 
\end{center}
\begin{align*}
  \text{gray area}
      &=(x_1+x_2)(y_1+y_2)-x_2y_1-\frac{x_1y_1}{2}-\frac{x_2y_2}{2}
      -\frac{x_1y_1}{2}-x_2y_1   \\
      &=x_1y_2-x_2y_1
\end{align*}

But in this class we use them only as a memory aid so we will not pause to 
think more about them. 
\end{frame}

\begin{frame}{Practice}
Compute each determinant.
\begin{enumerate}
\item
$\displaystyle 
  \begin{vmatrix}
    3  &2  \\
    1  &4
  \end{vmatrix}
$
\pause\qquad\textit{Ans:}~$3\cdot 4-1\cdot 2=10$

% \pause
% \item
% $\displaystyle
%   \begin{vmatrix}
%     4  &5  \\
%     3  &0
%   \end{vmatrix}
% $
% \pause\qquad\textit{Ans:}~$4\cdot 0-3\cdot 5=-15$

\pause
\item
$\displaystyle
  \begin{vmatrix}
    1  &2 &-1  \\
    2  &2 &3   \\
    1  &-1  &0
  \end{vmatrix}
$
\pause
\begin{equation*}
  \left|\begin{array}{ccc:cc}
    1  &2  &-1 &1  &2   \\
    2  &2  &3  &2  &2   \\
    1  &-1 &0  &1  &-1  \\
  \end{array}\right|
  =
  \begin{array}[t]{l}
    1\cdot(2\cdot 0-(-1)\cdot 3)  \\[0.25ex]
    +2\cdot(3\cdot 1-0\cdot 2) \\[0.25ex]
    -1\cdot(2\cdot(-1)-1\cdot 2)=13
  \end{array}
\end{equation*}

% \pause
% \item
% $\displaystyle
%   \begin{vmatrix}
%     3  &1 &1  \\
%    -2  &-2 &-2   \\
%     0  &1  &5
%   \end{vmatrix}
% $
% \pause
% \begin{equation*}
%   \left|\begin{array}{ccc:cc}
%     3  &1  &1  &3  &1   \\
%     -2 &-2 &-2 &-2 &-2  \\
%     0  &1  &5  &0  &1   \\
%   \end{array}\right|
%   =
%   \begin{array}[t]{l}
%     3\cdot((-2)\cdot 5-1\cdot(-2))  \\[0.25ex]
%     -1\cdot((-2)\cdot 0-5\cdot(-2)) \\[0.25ex]
%     +1\cdot((-2)\cdot 1-0\cdot(-2))=0
%   \end{array}
% \end{equation*}
\end{enumerate}
\end{frame}



\begin{frame}{Using determinants to compute cross products}
The cross product $\vec{u}\times\vec{v}$
is the determinant of this array.
\begin{equation*}
  \begin{vmatrix}
    \vec{\i}  &\vec{\j}  &\vec{k}  \\
    u_1       &u_2       &u_3      \\
    v_1       &v_2       &v_3      \\
  \end{vmatrix}
  \tag{$*$}
\end{equation*}
Again, we usually
put in a dashed vertical line and repeat the first two columns.
\begin{equation*}
  \left|\begin{array}{ccc:cc}
    \vec{\i}  &\vec{\j}  &\vec{k} &\vec{\i}  &\vec{\j}   \\
    u_1       &u_2       &u_3     &u_1       &u_2       \\
    v_1       &v_2       &v_3     &v_1       &v_2       \\
  \end{array}\right|
  =
  \begin{array}[t]{l}
    \vec{\i}\cdot(u_2v_3-v_2u_3)  \\[0.75ex]
    +\vec{\j}\cdot(u_3v_1-u_1v_3) \\[0.75ex]
    +\vec{k}\cdot(u_1v_2-u_2v_1)
  \end{array}
\end{equation*}

\Ex
Use this method to
find the cross product $\vec{u}\times\vec{v}$ 
of $\vec{u}=3\vec{\i}-2\vec{j}+4\vec{k}$
and $\vec{v}=-\vec{\i}+3\vec{j}-5\vec{k}$.

\begin{equation*}
  \left|\begin{array}{ccc:cc}
    \vec{\i}  &\vec{\j}  &\vec{k} &\vec{\i}  &\vec{\j}   \\
    3         &-2        &4       &3         &-2       \\
    -1        &3         &-5      &-1       &3       \\
  \end{array}\right|
  =
  \begin{array}[t]{l}
    \vec{\i}\cdot((-2)\cdot(-5)-3\cdot 4)  \\[0.75ex]
    +\vec{\j}\cdot(4\cdot(-1)-(-5)\cdot 3) \\[0.75ex]
    +\vec{k}\cdot(3\cdot 3-(-1)\cdot(-2))
  \end{array}
\end{equation*}

% \pause
% \Ex
% Use it to
% find the cross product $\vec{v}\times\vec{u}$.
\end{frame}




\section{Properties of the cross product operation}

\begin{frame}{Cross product properties}
\Lm Nonzero vectors are parallel if and only if their cross product is
zero.

\Pf 
The length of $\vec{u}\times\vec{v}$
is $|\vec{u}|\,|\vec{v}|\cdot\sin\theta$.
Nonzero vectors are parallel if and only if the angle between them is either
$0$ or~$\pi$ (for angles in the defined range), 
which happens if and only if $\sin\theta=0$.

\pause
\Lm (Algebra of cross product) 
Let $\vec{u}$, $\vec{v}$, and $\vec{w}$ be vectors and let $r$
be a scalar.
\begin{enumerate}
\item Cross product is anticommutative, 
$\vec{u}\times\vec{v}=-1\cdot(\vec{v}\times\vec{u})$.
\item The zero vector acts like zero, 
$\vec{u}\times\vec{0}=\vec{0}$ and $\vec{0}\times\vec{u}=\vec{0}$.
\item The cross product of a vector with itself is zero,
$\vec{u}\times\vec{u}=\vec{0}$.
\item Scalars factor out, 
$(r\vec{u})\times\vec{v}=r(\vec{u}\times\vec{v})$ 
and
$\vec{u}\times(r\vec{v})=r(\vec{u}\times\vec{v})$.
\item The cross product distributes over addition, 
$\vec{u}\times(\vec{v}+\vec{w})=\vec{u}\times\vec{v}+\vec{u}\times\vec{w}$
and 
$(\vec{u}+\vec{v})\times\vec{w})=\vec{u}\times\vec{w}+\vec{v}\times\vec{w}$.

\end{enumerate}

\Pf
The first we have already seen.
Check the others by working through the formula.

\pause
\Lm
Cross product is not associative:~in general
$(\vec{u}\times\vec{v})\times\vec{w}$ does not
equal $\vec{u}\times(\vec{v}\times\vec{w})$.

\Pf
We can verify this by exhibiting three vectors.
It happens that $(\vec{\i}\times\vec{\i})\times\vec{\j}$
is not equal to  $\vec{\i}\times(\vec{\i}\times\vec{\j})$.
\end{frame}


\begin{frame}{Cross product and the standard basis vectors}
\Lm Each of these follows from the definition of cross product.
\begin{equation*}
  \vec{\i}\times\vec{\j}=\vec{k}
  \qquad
  \vec{\j}\times\vec{k}=\vec{\i}
  \qquad
  \vec{k}\times\vec{\i}=\vec{\j}
\end{equation*}
This is a circular relationship.
\begin{center}
  \includegraphics{cross_product000.pdf}
\end{center}

With those three, anticommutivity fills in the others:   
$\vec{\j}\times\vec{\i}=-\vec{k}$,
and
$\vec{k}\times\vec{\j}=-\vec{\i}$,
and
$\vec{\i}\times\vec{k}=-\vec{\j}$.
(Recall that the cross product of a vector with itself is~$\vec{0}$.)

\end{frame}


\begin{frame}{Scalar triple product}
\Lm The \alert{scalar triple product} 
$\vec{a}\dotprod(\vec{b}\times\vec{c})$
computes the volume of a parallelepiped determined by the three vectors.
Its value is given by this determinant.
\begin{equation*}
  \begin{vmatrix}
    a_1  &a_2  &a_3 \\
    b_1  &b_2  &b_3 \\
    c_1  &c_2  &c_3    
  \end{vmatrix}
\end{equation*}
(The three vectors are coplanar if and only if the value is zero.)
\begin{center}
  \input asy/cross_product_3d011.tex %
\end{center}

\Pf
The volume is the height times the area of the base.
The area of the base is the magnitude $|\vec{b}\times\vec{c}|$.
Where $\theta$ is the angle between the vectors $\vec{a}$ and
$\vec{b}\times\vec{c}$, the height is $|\vec{a}|\cdot\cos\theta$.
Thus the area of the base times the height is 
$|\vec{b}\times\vec{c}|\,|\vec{a}|\cdot\cos\theta
=\vec{a}\dotprod(\vec{b}\times\vec{c})$.
\end{frame}



\begin{frame}{Extra: connection with dot product}
By definition, the magnitude of the cross product is this.
\begin{equation*}
  |\vec{u}\times\vec{v}|=|\vec{u}|\,|\vec{v}|\cdot\sin\theta
\end{equation*}
\pause
Square both sides.
\begin{align*}
  |\vec{u}\times\vec{v}|^2
  &=|\vec{u}|^2\,|\vec{v}|^2\cdot\sin^2\theta   \\
  &=|\vec{u}|^2\,|\vec{v}|^2\cdot(1-\cos^2\theta)   \\
  &=|\vec{u}|^2\,|\vec{v}|^2-|\vec{u}|^2\,|\vec{v}|^2\cdot\cos^2\theta
\end{align*}
\pause
The second term is the dot product.
\begin{equation*}
  |\vec{u}\times\vec{v}|^2=|\vec{u}|^2\,|\vec{v}|^2-(\vec{u}\dotprod\vec{v})^2
\end{equation*}
\end{frame}



\section{Extra: the formula meets the specifications}

\begin{frame}{The length of the cross product is right}
Start with the definition of the operation as a formula.
\begin{equation*}
  \colvec{u_1 \\ u_2 \\ u_3}\times\colvec{v_1 \\ v_2 \\ v_3}
  =\colvec{u_2v_3-u_3v_2 \\ u_3v_1-u_1v_3 \\ u_1v_2-u_2v_1}
\end{equation*}
This is the square of the length, $|\vec{u}\times\vec{v}|^2\!$.
\begin{align*}
  &\hspace{-3em}(u_2b_3-u_3v_2)^2
    +(u_3v_1-u_1v_3)^2
    +(u_1v_2-u_2v_1)^2   \\
  &=u_2^2b_3^2-2u_2u_3v_2v_3+u_3^2v_2^2   \\
    &\qquad+u_3^2v_1^2-2u_1u_3v_1v_3+u_1^2v_3^2   \\
    &\qquad+u_1^2v_2^2-2u_1u_2v_1v_2+u_2^2v_1^2   \\
  &=(u_1^2+u_2^2+u_3^2)(v_1^2+v_2^2+v_3^2)-(u_1v_1+u_2v_2+u_3v_3)^2 \\
  &=|\vec{u}|^2\,|\vec{v}|^2-(\vec{u}\dotprod\vec{y})^2   \\
  &=|\vec{u}|^2\,|\vec{v}|^2-|\vec{u}|^2|\vec{v}|^2\cos^2\theta  \\
  &=|\vec{u}|^2\,|\vec{v}|^2\cdot(1-\cos^2\theta)             \\
  &=|\vec{u}|^2\,|\vec{v}|^2\cdot\sin^2\theta
\end{align*}
Take the square root and note that $\sqrt{\sin^2\theta}$ equals $\sin\theta$
because the sine is positive when $\theta$ is in the range 
$0\leq\theta\leq\pi$ (angles between vectors are in this range by definition).  
\end{frame}



\begin{frame}{The cross product satisfies the right-hand rule}
% Argument from An Even Simpler Proof of the Right-Hand Rule, Eric Thurschwell
% VOL. 46, NO. 3, MAY 2015 THE COLLEGE MATHEMATICS JOURNAL 
First we briefly discuss projection.
% When we say that a parabolic mirror brings in the parallel rays from the
% sun and focuses them to a point, we of course don't mean that the rays
% from the sun are really parallel.
% We mean that the sun is so far away that we can take its rays to be
% parallel\Dash if the sun was infinitely far away its rays would be parallel,
% and the actual sun is so far that it is for practical purposes at that limit.
% \pause
Imagine two vectors in the first octant.
% Here are such a $\vec{u}$ and $\vec{v}$.
\begin{center}
  \input asy/cross_product_3d006.tex %
\end{center}
An observer on the $z$~axis at $(0,0,1)$ takes a picture.
There is distortion in the picture because the rays reaching
the camera are not parallel.
They step back, maybe to $(0,0,10)$, 
refocus to make everything the same size as in the starting picture, 
and take another.
Then they stand back to $(0,0,100)$, refocus, and take another.

In the limit this observer is standing very high up on the $z$~axis and
looking toward the $xy$~plane. 
They are so far away that we can take the light rays
to be parallel. 
The projection from this observer to the plane is \alert{orthographic}.
\end{frame}

\begin{frame}
We are going to make an argument that to be complete requires a number of cases.
But in this presentation 
we will only consider one case,
where the two vectors are in the first octant and orthographically project 
to non-multiples. 

Here is the cross product  of $\vec{u}$ and $\vec{v}$.  
\begin{center}
  \input asy/cross_product_3d007.tex %
\end{center}
\pause
In this context, the operation satisfies the right-hand rule
if and only if the third component of $\vec{u}\times\vec{v}$ is positive when,
as viewed by the observer very high up on the
$z$~axis, $\vec{u}$
is to the right of~$\vec{v}$.

Restated, the observer can match $\vec{u}$ with their right arm and $\vec{v}$
with their left\Dash of course, without crossing arms\Dash and the observer's
head points toward positive infinity.  
\end{frame}


\begin{frame}
Here is the orthographic projection 
of the vectors to the $xy$ plane.
\begin{equation*}
  \vec{u}=\colvec{u_1 \\ u_2 \\ u_3}
  \quad
  \vec{v}=\colvec{v_1 \\ v_2 \\ v_3}
  \quad\text{with projections}\quad
  p(\vec{u})=\colvec{u_1 \\ u_2 \\ 0}
  \quad
  p(\vec{v})=\colvec{v_1 \\ v_2 \\ 0}
\end{equation*}
\begin{center}
  \input asy/cross_product_3d009.tex %
\end{center}
The dotted lines are perpendicular to the plane.  

Observe that $\vec{u}$ is to the right of~$\vec{v}$
if and only if the projection of $\vec{u}$ is to the
right of the projection of~$\vec{v}$.
\end{frame}


\begin{frame}
Consider the angles.  
\begin{center}
  \input asy/cross_product_3d010.tex %
\end{center}
The vector  $p(\vec{u})\times p(\vec{v})$ has the same third component 
as $\vec{u}\times\vec{v}$, namely $u_1\cdot v_2-u_2\cdot v_1$.
\begin{align*}
  u_1\cdot v_2-u_2\cdot v_1
  &=|p(\vec{u})|\cos\alpha\cdot|p(\vec{v})|\sin\beta-
     |p(\vec{u})|\sin\alpha\cdot|p(\vec{v})|\cos\beta   \\
  &=|p(\vec{u})|\,|p(\vec{v})|\cdot(\cos\alpha\cdot\sin\beta-
      \sin\alpha\cdot\cos\beta)   \\
  &=|p(\vec{u})|\,|p(\vec{v})|\cdot\sin(\beta-\alpha)
\end{align*}
From the perspective of the observer $p(\vec{u})$ is to the
right of~$p(\vec{v})$ 
if and only if $\beta-\alpha$ is between $0$ and~$\pi/2$, 
and so the third component of the cross product, $\sin(\beta-\alpha)$, 
is positive.
\end{frame}





\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
