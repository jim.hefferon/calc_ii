\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 11.3 \& 11.4\hspace{1em} Series of positive terms}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Series of positive terms}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage stub_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
% \input asy/arc_length_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\begin{frame}
Here is our list of prototypes of where we are headed.
\begin{center}
\begin{tabular}{c|l}
   \multicolumn{1}{c}{\tabulartext{Function}}
     &\multicolumn{1}{c}{\tabulartext{Taylor series}}  \\ \hline
   $\displaystyle \frac{1}{1-x}$ &$1+x+x^2+x^3+\cdots$ \rule{0pt}{14pt} \\[1ex]
   $\displaystyle e^x$           &$1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\cdots$  \\[1ex]
   $\displaystyle \cos x$        &$1-\frac{x^2}{2!}+\frac{x^4}{4!}-\frac{x^6}{6!}+\cdots$  \\[1ex]
   $\displaystyle \sin x$        &$x-\frac{x^3}{3!}+\frac{x^5}{5!}-\frac{x^7}{7!}+\cdots$  \\[1ex]
   $\displaystyle \ln(1+x)$      &$x-\frac{x^2}{2}+\frac{x^3}{3}-\frac{x^4}{4}+\cdots$  \\
\end{tabular}
\end{center}
Again, we will often find ourselves knowing what a series adds to if only
it converges.
So, often the question will be of convergence.

Some of these series, such as $e^x\!$, sum to a finite total
while having entirely positive terms.
Others mix pluses and minuses.
We will consider these cases separately.
In this section we do all positive terms.

(What about all negative?
The sum of a series 
$A=a_0+a1+a_2+\cdots$
is the limit of the sequence $\set{A_n}$ of partial sums,
$A_0=a_0$ and $A_1=a_0+a_1$, and $A_2=a_0+a_1+a_2$, etc.
If a series had only negative terms then we could factor a $-1$
out of each such partial sum~$A_n$. 
So the question of convergence reduces to that question
for all-positive series.)
\end{frame}


\begin{frame}
\Df A \alert{positive series} is one where each term is 
greater than or equal to zero.

This pictures the partial sums 
\begin{equation*}
  S_0=a_0\quad S_1=a_0+a_1\quad S_2=a_0+a_1+a_2\quad \ldots
\end{equation*}
as the total area of the first $n$ boxes. 
\begin{center}
  \vcenteredhbox{\includegraphics{series_with_positive_terms000.pdf}}
\end{center}

\pause
\Tm
If the partial sums of a positive series 
are bounded above then the series converges.
If the partial sums are not bounded above then the series diverges. 

\Pf
By definition,
the sum of a series is the limit of the sequence of partial sums.
In a positive series the 
partial sums form an increasing sequence.
The limit of an increasing sequence exists if and only if that
sequence is bounded.

\pause
\Ex For the series $(1/2)+(1/4)+(1/8)+\cdots$
the sequence of partial sums $S_0=1/2$, 
$S_1=3/4$, $S_2=7/8$, \ldots{} is bounded above by~$1$. 

\Ex The harmonic series is a positive series.
Its sequence of partial sums is unbounded
so the series goes to infinity. 
\end{frame}

% \begin{frame}
% \Ex The series $\sum_{0\leq i}1$ diverges because the limit of its terms is
% not zero.
% So also does any infinite sum of non-zero terms.

% \Ex The series $\sum_{0\leq i}i$ diverges because the limit of its terms is
% infinity, not zero.

% \pause
% \Ex While the series
% \begin{equation*}
%   \sum_{n\geq 0}(-1)^n=1-1+1-1+1-1+\cdots
% \end{equation*}
% has a sequence of partial sums
% \begin{equation*}
%   S_0=1\quad S_1=0\quad S_2=1\quad S_3=0\quad\ldots
% \end{equation*}
% that is bounded, the result does not apply because this is
% not a positive series.
% Of course, this series diverges because the sequence of 
% partial sums does not have a limit.

% \medskip
% This result explains why in the remainder of the
% chapter the series  
% share a characteristic:~they tend to have terms that go to zero.
% If the terms don't get closer to zero then the series don't converge. 
% \end{frame}




\section{Integral test}

\begin{frame}{Integral test}
From the section on improper integrals we have the intuition
that $\integral{0}{\infty}{f(x)}{x}$
converges if the integrand $f(x)$ falls to zero fast enough.
From the prior section we have the intuition that
$\sum_{0\leq n}a_n$ converges if the terms $a_n$ fall to zero fast enough.
The next result says that our two ideas of ``fast enough'' coincide.

\Tm (Integral Test)
Let the terms of a series be described by a function, $a_n=f(n)$,
where $f$ is positive, decreasing, and continuous when $M\leq x$ for some 
real constant~$M$.
Then $\sum_{M\leq n}a_n$ converges if and only if 
$\integral{M}{\infty}{ f(x) }{x}$ converges.

\pause\medskip
Because $a_n=f(n)$, the discrete terms and the continuous function
track together. 
\begin{center}
  \includegraphics{series_with_positive_terms003.pdf}%
\end{center}
Note that the result does not say that if they both converge then they
converge to the same value; the values can be different.
\end{frame}


\begin{frame}
\Pf
The picture on the left shows that the integral
$\integral{0}{\infty}{ f(x) }{x}$
is greater than the sum $\sum_{1\leq n}a_n$. 
Hence, if the integral converges to a bounded value then
so also does the sum. 
(We have previously shown that $\sum_{1\leq n}a_n$ converges if and only
if $\sum_{0\leq n}a_n$ converges.)
\begin{center}
  \includegraphics{series_with_positive_terms001.pdf}%
  \qquad
  \includegraphics{series_with_positive_terms002.pdf}%
\end{center}
The picture on the right shows that the sum 
$\sum_{0\leq n}a_n$ is greater than the integral
$\integral{0}{\infty}{ f(x) }{x}$.
Thus if the 
integral is unbounded then so is the sum. 
\end{frame}


\begin{frame}
\Ex Decide if the series converges or diverges, using the Integral test.
\begin{equation*}
  \sum_{0\leq n}\frac{2n}{n^2+5}
\end{equation*}
\pause 
Fix the real function $\map{f}{\R}{\R}$ given by $f(x)=2x/(x^2+5)$.
This function is positive,
continuous and decreasing for $0\leq x$, as required by the result.
\begin{align*}
  \integral{x=0}{\infty}{ \frac{2x}{x^2+5} }{x}
  &=\lim_{R\to\infty}\;\integral{x=0}{R}{ \frac{2x}{x^2+5} }{x}  \\
  &=\lim_{R\to\infty}\Bigl[\ln(x^2+5) \Bigr]_{0}^R                \\
  &=\lim_{R\to\infty}\bigl(\ln(R^2+5)-\ln(5) \bigr)   
  =\infty
\end{align*}
As $R\to\infty$ we have $R^2+5\to\infty$ and so $\ln(R^2+5)\to\infty$.
Thus integral diverges, and therefore the series diverges.
\end{frame}

\begin{frame}
\Ex Decide if this series diverges or converges.
\begin{equation*}
  \sum_{0\leq j}\frac{5}{j^2+1}
\end{equation*}
\pause
With the Integral Test in mind,
consider $f(x)=5/(x^2+1)$ and note that it 
is positive,
continuous and decreasing over the interval of interest.
\begin{align*}
  \integral{x=0}{\infty}{ \frac{5}{x^2+1} }{x}
  &=5\cdot\lim_{R\to\infty}\;\integral{x=0}{R}{ \frac{1}{x^2+1} }{x}  \\
  &=5\cdot\lim_{R\to\infty}\Bigl[\tan^{-1}(x) \Bigr]_{0}^R                \\
  &=\lim_{R\to\infty}\bigl(\tan^{-1}(R)-\tan^{-1}(0) \bigr)  \\  
  &=\lim_{R\to\infty}\bigl(\tan^{-1}(R)-0 \bigr)  
  =\pi/2
\end{align*}
(Recall that $R\to\infty$ gives $\tan^{-1} (R)\to \pi/2$.)
Because the integal converges, the series also converges.
\end{frame}


\begin{frame}
\Ex Decide if the series conveges.
\begin{equation*}
  \sum_{5\leq n}\frac{1}{\sqrt{n-4}}
\end{equation*}
Consider $f(x)=1/(x-4)^{1/2}$ for $x\geq 5$ and note that it satisfies the 
conditions of the Integral Test.
\begin{align*}
  \integral{x=5}{\infty}{ \frac{1}{(x-4)^{1/2}} }{x}
  &=\lim_{R\to\infty}\;\integral{x=5}{R}{ \frac{1}{(x-4)^{1/2}} }{x}  \\
  &=\lim_{R\to\infty}\Bigl[2\cdot(x-4)^{1/2} \Bigr]_{5}^R                \\
  &=2\cdot \lim_{R\to\infty}\bigl(\sqrt{R-4}-1 \bigr)  
  =\infty
\end{align*}
The series diverges.
\end{frame}


\begin{frame}{Practice}
Use the Integral Test to decide if the series converges or diverges.
\begin{enumerate}
\item $\displaystyle 1+\frac{1}{3}+\frac{1}{5}+\cdots$

\pause\smallskip
The series $\sum_j 1/(2j+1)$ diverges by the Integral Test.
% \pause
% \item $\displaystyle 1+\frac{1}{\sqrt{2}}+\frac{1}{\sqrt{3}}+\cdots$

% \pause\smallskip
% The series $\sum_{1\leq j} 1/\sqrt{j}$ diverges by the Integral Test.
\pause
\item $\displaystyle -1+\frac{1}{3}+\frac{1}{15}+\frac{1}{35}+\cdots=\sum_n\frac{1}{4n^2-1}$

\pause\smallskip
Factor the denominator into $(2x-1)\cdot(2x+1)$,
and then do Partial Fractions.
\begin{equation*}
  \integral{x=0}{R}{\frac{1/2}{2x-1}-\frac{1/2}{2x+1}}{x}
  =\netchange{x=0}{R}{\frac{1}{4}\ln(2x-1)-\frac{1}{4}\ln(2x+1)}
  =\netchange{0}{R}{\frac{1}{4}\ln(\frac{2x-1}{2x+1})}
\end{equation*}
Then the Integral Test gives that it converges.
% \pause
% \item $\displaystyle \sum_n\frac{n}{(n^2+3)^2}$

% \pause\smallskip
% The Integral Test gives that it converges.
\end{enumerate}
\end{frame}



\begin{frame}{$p$-series}
\Df
Fix a power~$p\in\R$.
A \alert{$p$~series} has this form.
\begin{equation*}
  \sum_{1\leq n}\frac{1}{n^p}
  =1+\frac{1}{2^p}+\frac{1}{3^p}+\frac{1}{4^p}+\cdots
\end{equation*}

\Ex
The $p$~series with $p=1$ is the harmonic series.
\begin{equation*}
  1+\frac{1}{2}+\frac{1}{3}+\frac{1}{4}+\frac{1}{5}+\cdots
\end{equation*}
Here we take $p=1/2$.
\begin{equation*}
  1+\frac{1}{\sqrt{2}}
   +\frac{1}{\sqrt{3}}
   +\frac{1}{\sqrt{4}}
   +\frac{1}{\sqrt{5}}+\cdots
\end{equation*}

\pause
\Tm
A $p$~series converges if $p$ is greater than one, and diverges otherwise.

\pause
\Pf
We will apply the Integral Test.
When $p\neq 1$ we have this.
\begin{equation*}
  \integral{x=1}{\infty}{ \frac{1}{x^p} }{x}
  =\lim_{R\to\infty}\;\biggl( \,\integral{1}{R}{ \frac{1}{x^p} }{x} \biggr)
  =\lim_{R\to\infty}\biggl[\frac{x^{-p+1}}{-p+1}\biggr]_1^R
  =\lim_{R\to\infty}\frac{R^{1-p}-1}{1-p}
\end{equation*}
If $p>1$ then $1-p$ is negative, hence as $R\to\infty$ we have $R^{1-p}\to 0$, 
so the integral converges.
If $p<1$ then $1-p$ is positive and $R^{1-p}\to \infty$ so 
the integral diverges.
If $p=1$ then the series is the harmonic series and diverges.
\end{frame}


\begin{frame}
\Ex The series $\sum_{0\leq i}1/i^3$ converges.
The power is $p=3$.

In contrast, 
the series $\sum_{1\leq i}\sqrt{1/\sqrt{i}}$ diverges
since the power is $p=1/2$.  
\end{frame}


% ========================================
\section{Comparison tests}

\begin{frame}{Direct comparison}
\Tm (Comparison Test)
Let $\sum_i a_i$ and $\sum_i b_i$ be positive series,
and suppose that $a_i\leq b_i$ for all~$i$.
If $\sum_i b_i$ converges then so also does $\sum_i a_i$.
If $\sum_i a_i$ diverges then so does $\sum_i b_i$.

\Pf
Write these for the partial sums.
\begin{equation*}
  A_n=\sum_{0\leq i\leq n}a_i
  \qquad
  B_n=\sum_{0\leq i\leq n}b_i
\end{equation*}
The two are positive series so the partial sums 
form an increasing sequence.
For the first half,
$B=\sum_{0\leq i}b_i$ exists so the sequence of partial sums $\set{B_n}$ 
is bounded above.
Because $a_n\leq b_n$, the sequence of partial sums $\set{A_n}$
is also bounded above, and therefore $A=\sum_{0\leq i}a_i$ exists.  

For the second half,
by assumption $A=\sum_{0\leq i}a_i$ diverges so the sequence
of partial sums $\set{ A_n}$ is not bounded above.
Because $a_n\leq b_n$ for all~$n$,
the sequence $\set{B_n}$ is also unbounded
and thus $B=\sum_{0\leq i}b_i$ diverges.

\medskip
This summarizes.
\begin{center}
  \begin{tabular}{r|cc}
   \multicolumn{1}{r}{\ } 
      &\multicolumn{1}{c}{$d_i\leq c_i$} 
      &\multicolumn{1}{c}{$c_i\leq d_i$} \\ \cline{2-3}
    $\sum_i c_i$ converges &$\sum_i d_i$ converges &--no conclusion-- \\
    $\sum_i c_i$ diverges  &--no conclusion--     &$\sum_i d_i$ diverges 
  \end{tabular}
\end{center}
\end{frame}

\begin{frame}
For applications of the Comparision Test we often compare to a $p$~series
or to a geometric series.

\Ex Does this series converge or diverge?
\begin{equation*}
  \sum_{0\leq i<\infty} \frac{1}{i^3+2i+6}
  \tag{$*$}
\end{equation*}
\pause
The intuition is that the long-term behavior of the terms 
depends the most on the denominator's $i^3\!$.
We will compare with $\sum_i 1/i^3\!$, which 
converges because it is a $p$~series with the power greater than~$1$.

This holds 
\begin{equation*}
    \frac{1}{i^3+2i+6}\leq \frac{1}{i^3}
\end{equation*}
because the left side has the same numerator but a bigger denominator, 
and therefore the fraction as a whole is smaller.
So the starting series~($*$) is convergent.

\Ex Does $\sum_{4<i} 1/(\sqrt{i}-2)$ converge or diverge?

\pause
The natural series to compare with is $\sum_{i>4} 1/\sqrt{i}=\sum_{i>4} 1/i^{1/2}$, 
which diverges because it is a $p$~series with $p<1$.
\begin{equation*}
  \frac{1}{\sqrt{i}} \leq \frac{1}{\sqrt{i}-2}
\end{equation*}
The fraction on the right is bigger because its denominator is smaller.
So the given series diverges.
\end{frame}



\begin{frame}{Comparison in the limit}
\Ex Use the Comparison test to show that this series converges.
\begin{equation*}
  \sum_{0\leq n} \frac{1}{2^n+1}
\end{equation*}
Term by term we have this.
\begin{equation*}
  \frac{1}{2^n+1}\leq \frac{1}{2^n}
\end{equation*}
and $\sum_n(1/2)^n$ is a geometric series with common ratio $r=1/2$,
which therefore converges.

\pause\medskip
Now, what about this series?
\begin{equation*}
  \sum_{0\leq n} \frac{1}{2^n-1}
  \tag{$*$}
\end{equation*}
We look to also compare this series to  
$\sum_i(1/2^n)$ but the necessary inequality is just not there.
\begin{equation*}
  \frac{1}{2^n-1}\not\leq \frac{1}{2^n}
\end{equation*}
Nonetheless series~($*$) is an awful lot like $\sum_i(1/2^n)$.
Next we will extend the Comparison Test to apply 
to series that are ``like'' a benchmark one.
\end{frame}



\begin{frame}
\Tm (Limit Comparison Test)
Let $\sum_i a_i$ and $\sum_i b_i$ be positive series
and suppose that the limit of the sequence of ratios exists.
\begin{equation*}
  L=\lim_{i\to\infty}\frac{a_i}{b_i}
\end{equation*}
If $L>0$ then the two series do the same:~either both converge
or both diverge (this does not apply if the limit diverges to infinity).
If $L=0$ and $\sum_i b_i$ converges then so does $\sum_i a_i$.

\pause\Pf
We first show that if $\sum_i b_i$ converges then so does $\sum_i a_i$,
in either the $L>0$ or $L=0$ cases. 
The terms $a_i$ and $b_i$ are positive and the ratio $a_i/b_i$ approaches~$L$,
so we have $0\leq a_i/b_i\leq L+1$
for all sufficiently large indices~$i$
(that is, there is a $N$ large enough so that if $i\geq N$ then the inequality
holds).
Thus $a_i\leq (L+1)\cdot b_i$ and since convergence of $\sum_i b_i$ implies 
convergence of $\sum_i (L+1)\cdot b_i$, we can apply the Comparison Test
to conclude that $\sum_i a_i$ also converges.
 
To finish, consider the case that $0<L<\infty$ and $\sum_i a_i$ converges.
We adapt the method of the prior paragraph, here using~$L/2$.
Since the ratio $a_i/b_i$ approaches~$L$,
we have $L/2\leq a_i/b_i$
for all sufficiently large indices~$i$,
which gives that $(L/2)\cdot b_i\leq a_i$.
The Comparison Test therefore says that  
$\sum_i (L/2)\cdot b_i$ converges,
and thus $\sum_i b_i$ also converges. 
\end{frame}

\begin{frame}
\Ex
We show that this series converges
\begin{equation*}
  \sum_{n\geq 0} \frac{1}{2^n-1}
\end{equation*}
by doing a Limit Comparison Test with $\sum_n(1/2^n)$.

\pause
Both are positive series.
This is the limit of the sequence of ratios.
\begin{equation*}
  \lim_{n\to\infty}\frac{1/2^n}{1/(2^n-1)}
  =\lim_{n\to\infty}\frac{2^n-1}{2^n}
  =\lim_{n\to\infty}1-\frac{1}{2^n}
  =1
\end{equation*}
Because the limit is greater than zero, 
and because we know that $\sum_n1/2^n$ converges, 
the Limit Comparison Test gives that the other series
also converges.
\end{frame}



\begin{frame}{Practice}
Determine convergence or divergence with the Comparison test.
\begin{enumerate}
\item $\displaystyle \frac{1}{1\cdot 3}+\frac{1}{2\cdot 4}
  +\frac{1}{3\cdot 5}+\cdots$

\pause\smallskip
This is $\sum_n1/(n\cdot (n+2))$.
Compare $1/(n^2+2n)$ with $1/n^2$.
\pause
\item $\displaystyle \frac{1}{1\cdot 2}+\frac{1}{2\cdot 2^2}
  +\frac{1}{3\cdot 2^3}+\cdots$

\pause\smallskip
This is $\sum_n1/(n\cdot 2^n)$.
Compare it with $1/2^n$.
% \pause
% \item $\displaystyle \sum_n \frac{n^3}{3n^4-3}$

% \pause\smallskip
% Compare with $1/3n$.
\end{enumerate}
% \end{frame}

% \begin{frame}{Practice}
Use the Limit Comparison Test to determine whether
each converges or diverges.
\begin{enumerate}
\item $\displaystyle \sum_{2\leq j} \frac{j^2+1}{j^4-15}$
\quad\textit{Hint:} this is like $\sum_j 1/j^2$.

\pause\smallskip Consider
\begin{equation*}
  \lim_{j\to\infty}\frac{(j^2+1)/(j^4-15)}{1/j^2}
  =\lim_{j\to\infty}\frac{(j^2+1)(j^2)}{(j^4-15)(1)}
  =\lim_{j\to\infty}\frac{j^4+j^2}{j^4-15}
\end{equation*}
The simplest way to proceed is to convert to real values.
\begin{equation*}
  \lim_{x\to\infty}\frac{x^4+x^2}{x^4-15}
\end{equation*}
You can show that this limit is $1$ in a variety of ways, one of which 
is L'H\^{o}pital's Rule.  
\end{enumerate}
\end{frame}


\begin{frame}
\begin{enumerate}\setcounter{enumi}{1}
\item $\displaystyle \sum_{0\leq j} \frac{1}{j+2}$

\pause\smallskip
Compare with $\sum_i 1/j$.
 
\item $\displaystyle \sum_{2\leq j} \frac{1}{\sqrt{j-1}}$

\pause\smallskip
Compare with $\sum_i 1/\sqrt{j}$.

\end{enumerate}
  
\end{frame}


\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
