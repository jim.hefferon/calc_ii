// series_wth_positive_terms.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "series_with_positive_terms%03d";

import graph;

// ==== general function ====
// path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// =========== ===============
real f0(real x) { return 1.5*exp(-x/3); }

picture pic;
int picnum = 0;
size(pic,6cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=10;
ybot=0; ytop=1.5;

real a = 0; real b = 9.5;
real box_hgt;
path box;
for (int i=0; i<xright; ++i) {
  box_hgt = f0(i);
  box = (i,0)--(i,box_hgt)--(i+1,box_hgt)--(i+1,0)--cycle;
  draw(pic, box, highlight_color);
  if (i<4) {
    label(pic, format("$a_{%d}$",i), (i+0.5,box_hgt/2));
  }
}
label(pic, "\ldots", (4.5,f0(4)/2));

path f = graph(pic, f0, a, b);
// draw(pic, f, FCNPEN);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
// xaxis(pic, L="",  
//       axis=YEquals(ytop+0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// xaxis(pic, L="",  
//       axis=YEquals(ybot-0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
// yaxis(pic, L="",  
//       axis=XEquals(xleft-0.2),
//       ymin=ybot-0.5, ymax=ytop+0.5,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// yaxis(pic, L="",  
//       axis=XEquals(xright+0.2),
//       ymin=ybot-0.5, ymax=ytop+0.5,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic); // ,format="pdf"



// ======== integral test ======
real f1(real x) { return 1.5*exp(-x/3)+0.5; }

// ......... converges .............
picture pic;
int picnum = 1;
size(pic,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=0; ytop=2.5;

real a = 0; real b = xright+0.2;
real box_hgt;
path box;
for (int i=1; i<3; ++i) {
  box_hgt = f1(i);
  box = (i,0)--(i,box_hgt)--(i-1,box_hgt)--(i-1,0)--cycle;
  draw(pic, box);
  label(pic, format("$a_{%d}$",i), (i-0.5,box_hgt/2));
}
label(pic, "\ldots", (4,f1(4)/2));
box_hgt = f1(6);
box = (6,0)--(6,box_hgt)--(5,box_hgt)--(5,0)--cycle;
draw(pic, box);
label(pic, "$a_N$", (5.5,f1(6)/2));

path f = graph(pic, f1, a, b);
draw(pic, f, FCNPEN);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ......... diverges .............
picture pic;
int picnum = 2;
size(pic,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=0; ytop=2.5;

real a = 0; real b = xright+0.2;
real box_hgt;
path box;
for (int i=0; i<3; ++i) {
  box_hgt = f1(i);
  box = (i,0)--(i,box_hgt)--(i+1,box_hgt)--(i+1,0)--cycle;
  draw(pic, box);
  label(pic, format("$a_{%d}$",i), (i+0.5,box_hgt/2));
}
label(pic, "\ldots", (4,f1(4)/2));
box_hgt = f1(5);
box = (5,0)--(5,box_hgt)--(5+1,box_hgt)--(5+1,0)--cycle;
draw(pic, box);
label(pic, "$a_N$", (5.5,f1(5)/2));

path f = graph(pic, f1, a, b);
draw(pic, f, FCNPEN);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic); // ,format="pdf"




// ......... just the dots .............
picture pic;
int picnum = 3;
size(pic,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=0; ytop=2.5;

real a = 0; real b = xright+0.2;
real box_hgt;
path box;
dotfactor = 4;
for (int i=0; i<=6; ++i) {
  dot(pic, (i,f1(i)), highlight_color);
}

path f = graph(pic, f1, a, b);
draw(pic, f, FCNPEN);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic); 




// // ================= block stacking ===============
// real block_width = 2;
// real block_hgt = 1/4*block_width;
// path block = (0,0)--(block_width,0)--(block_width,block_hgt)--(0,block_hgt)--cycle;

// // Return an array of blocks, as they should be stacked, starting at origin
// path[] blocks(int num_blocks) {
//   real cent_mass[];  // cent_mass[i] is x value of center of mass of i-tall system
//   cent_mass.push(0);
//   for (int i=1; i<=num_blocks; ++i){
//     cent_mass.push(cent_mass[i-1]+(1/(i)));
//   }
//   // write("cent_mass: ",cent_mass);

//   real slide_over[];  // the overhang of the i-th block down from top
//   for (int i=0; i<=num_blocks; ++i){
//     slide_over.push(0);
//   }
//   for (int i=1; i<=num_blocks; ++i){
//     for (int j=i; j <= num_blocks; ++j){
//       slide_over[i]=slide_over[i]+1/(j);
//     }
//   }
//   real slide_back = slide_over[num_blocks]; // slide them back to the origin
//   for (int i=0; i<=num_blocks; ++i){
//     slide_over[i]=slide_over[i]-slide_back;
//   }
//   // write("slide_over: ",slide_over);

//   path[] T;
//   for (int i=1; i<=num_blocks; ++i){
//     T.append(shift(slide_over[i],(num_blocks-i)*block_hgt)*block);
//   }
//   return T;
// }

// // ........... blocks on x-axis .............

// for (int num_blocks=2; num_blocks<=4; ++num_blocks) {
//   picture pic;
//   int picnum = 4+(num_blocks-2);
//   size(pic,4cm,keepAspect=true);

//   real xleft, xright, ybot, ytop; // limits of graph
//   xleft=0; xright=4;
//   ybot=0; ytop=2.5;

//   for (path b : blocks(num_blocks)){
//     filldraw(pic,b,FILLCOLOR, black);  
//   };

//   // x and y axes
//   Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
//   // The x axis
//   xaxis(pic, L="",  
// 	// axis=YZero,
// 	xmin=xleft-0.2, xmax=xright+0.5,
// 	p=currentpen,
// 	ticks=RightTicks("%", Step=1, Size=2pt),
// 	arrow=EndArrow(TeXHead));
//   labelx(pic, "$0$", 0);
//   // for center of mass of num_block-sized system
//   int numerator  = 2*num_blocks-1;
//   int denominator = num_blocks;
//   real T4[] = {numerator/denominator};
//   xaxis(pic, L="",  
// 	// axis=YZero,
// 	xmin=xleft-0.2, xmax=xright+0.5,
// 	p=currentpen,
// 	ticks=RightTicks("%", T4, Size=3pt, highlight_color),
// 	arrow=EndArrow(TeXHead));
//   labelx(pic, format("$%d", numerator)+format("/%d$", denominator), numerator/denominator, highlight_color);

//   shipout(format(OUTPUT_FN,picnum),pic); 
// }


// // ........... blocks on table .............

// for (int num_blocks=2; num_blocks<=4; ++num_blocks) {
//   picture pic;
//   int picnum = 7+(num_blocks-2);
//   unitsize(pic,1cm);

//   real xleft, xright, ybot, ytop; // limits of graph
//   xleft=0; xright=4;
//   ybot=0; ytop=2.5;

//   // make the table as a separate picture so I can clip, then add
//   picture tmp_pic;
//   unitsize(tmp_pic, 1cm);
  
//   path table = (-1,0)--(2,0)--(2,-1.0){W}..(1,-0.75)..{N}(-1,0)--cycle;
//   for(int i=0; i<=20; ++i) {
//     draw(tmp_pic, (-1+(i/5),-1)--(-1+(i+1)/5,0) , GRAPHPAPERPEN);
//   }
//   clip(tmp_pic, table);
//   draw(tmp_pic, table);
//   label(tmp_pic, Label("table",filltype=Fill(white)), (0.5,-0.3));
//   add(pic, tmp_pic.fit(), (0,0));

//   // for center of mass of num_block-sized system
//   int numerator  = 2*num_blocks-1;
//   int denominator = num_blocks;
//   real overhang = 2-(numerator/denominator);
//   for (path b : blocks(num_blocks)){
//     filldraw(pic, shift(overhang,0)*b, FILLCOLOR, black);  
//   };
//   pair label_pt = (4.25,-0.5);
//   draw(pic, label_pt{W}..{NW}(2.3,-0.15), arrow=EndArrow(ARROW_SIZE), highlight_color);
//   label(pic, Label(format("overhang $\frac{1}{%d}$", num_blocks),filltype=Fill(white)), label_pt);

//   shipout(format(OUTPUT_FN,picnum),pic); 
// }




