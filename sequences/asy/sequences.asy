// sequences.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "sequences%03d";

import graph;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// =========== Sequence of taylor polys for e^x ===============
picture pic;
int picnum = 0;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=10;
ybot=0; ytop=8;

real e_squared = exp(2);
real[] S0 = {1.000,
  3.000,
  5.000,
  6.333,
  7.000,
  7.267,
  7.356,
  7.381,
  7.387,
  7.389};

draw(pic, (0,e_squared)--(xright,e_squared), dotted);
dotfactor = 4;
for (int i=0; i<10; ++i) {
  dot(pic, (i,S0[i]), highlight_color);
}

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
// xaxis(pic, L="",  
//       axis=YEquals(ytop+0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// xaxis(pic, L="",  
//       axis=YEquals(ybot-0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

real[] T0a = {e_squared,1,2,3,4,5,6,7,8};
// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
// yaxis(pic, L="",  
//       axis=XEquals(xleft-0.2),
//       ymin=ybot-0.5, ymax=ytop+0.5,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// yaxis(pic, L="",  
//       axis=XEquals(xright+0.2),
//       ymin=ybot-0.5, ymax=ytop+0.5,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks("%", T0a, Size=2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$e^2$", e_squared);

shipout(format(OUTPUT_FN,picnum),pic); // ,format="pdf"


// ............. with error bars for epsilon=1, 1/10 ...........
for (int i=0; i<=1; ++i){
  picture pic;
  int picnum = 1+i;
  size(pic,0,4cm,keepAspect=true);

  real eps = 1/10^i;
  path region = (0,e_squared-eps)
    --(0,e_squared+eps)
    --(xright,e_squared+eps)
    --(xright,e_squared-eps)
    --cycle;
  fill(pic, region, light_color+0.3*white);
  draw(pic, (0,e_squared-eps)--(xright,e_squared-eps), light_color);
  draw(pic, (0,e_squared+eps)--(xright,e_squared+eps), light_color);

  draw(pic, (0,e_squared)--(xright,e_squared), dotted);
  dotfactor = 4;
  for (int i=0; i<10; ++i) {
    dot(pic, (i,S0[i]), highlight_color);
  }

  // x and y axes
  Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
  // The x axis
  xaxis(pic, L="",  
	axis=YZero,
	xmin=xleft-0.5, xmax=xright+0.5,
	p=currentpen,
	ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
	arrow=Arrows(TeXHead));
  
  // y axis
  Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
  yaxis(pic, L="",  
	axis=XZero,
	ymin=ybot-0.5, ymax=ytop+0.75,
	p=currentpen,
	ticks=LeftTicks("%", T0a, Size=2pt),
	arrow=Arrows(TeXHead));
  labely(pic, "$e^2$", e_squared);
  
  shipout(format(OUTPUT_FN,picnum),pic);
}



// ========== diverges to infty ============== 
picture pic;
int picnum = 3;
scale(pic, Linear(10), Linear);
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=11;
ybot=0; ytop=125;

dotfactor = 4;
for (int i=0; i<=11; ++i) {
  dot(pic, Scale(pic,(i,i^2)), highlight_color);
}

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-5, ymax=ytop+2.5,
      p=currentpen,
      ticks=LeftTicks(Step=50, step=10, OmitTick(0), Size=2pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);

// ......... show the N's ...........
for (int i=0; i<=1; ++i){
  picture pic;
  int picnum = 4+i;
  scale(pic, Linear(10), Linear);
  size(pic,0,5cm,keepAspect=true);
  
  real xleft, xright, ybot, ytop; // limits of graph
  xleft=0; xright=11;
  ybot=0; ytop=125;

  real N = 10^(i+1);
  path region = Scale(pic,(0,N))
    --Scale(pic,(0,ytop))
    --Scale(pic,(xright+0.25,ytop))
    --Scale(pic,(xright+0.25,N))
    --cycle;
  fill(pic, region, light_color+0.3*white);
  draw(pic, Scale(pic,(0,N))--Scale(pic,(xright+0.25,N)), light_color);

  dotfactor = 4;
  for (int i=0; i<=11; ++i) {
    dot(pic, Scale(pic,(i,i^2)), highlight_color);
  }

  // x and y axes
  Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
  // The x axis
  xaxis(pic, L="",  
	axis=YZero,
	xmin=xleft-0.5, xmax=xright+0.5,
	p=currentpen,
	ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
	arrow=Arrows(TeXHead));
  
  // y axis
  Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
  yaxis(pic, L="",  
	axis=XZero,
	ymin=ybot-5, ymax=ytop+2.5,
	p=currentpen,
	ticks=LeftTicks(Step=50, step=10, OmitTick(0), Size=2pt, size=2pt),
	arrow=Arrows(TeXHead));
  
  shipout(format(OUTPUT_FN,picnum),pic);
}




// ========== convergent sequence is bounded ============== 
real f6(real x) { return (5*cos(x))/x; }

picture pic;
int picnum = 6;
size(pic,0,3.25cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=9;
ybot=-2; ytop=3;

dotfactor = 5;
for (int i=1; i<=9; ++i) {
  dot(pic, (i,f6(i)), highlight_color);
}

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks(Step=5, step=1, OmitTick(0), Size=2pt, size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ========== continuous fcn fills in between the dots ============== 
real f7(real x) { return (5*x^2+3*x+2)/(2*x^3+x); }

picture pic;
int picnum = 7;
size(pic,0,3.25cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=9;
ybot=0; ytop=4;

dotfactor = 5;
for (int i=1; i<=9; ++i) {
  dot(pic, (i,f7(i)), highlight_color);
}

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks(Step=5, step=1, OmitTick(0), Size=2pt, size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// .......... fill in with fcn ...........
picture pic;
int picnum = 8;
size(pic,0,3.25cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=9;
ybot=0; ytop=4;

draw(pic, graph(f7, 0.8, 9.2), FCNPEN);
dotfactor = 5;
for (int i=1; i<=9; ++i) {
  dot(pic, (i,f7(i)), highlight_color);
}

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks(Step=5, step=1, OmitTick(0), Size=2pt, size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);

