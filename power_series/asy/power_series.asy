// power_series.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "power_series%03d";

import graph;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// =========== Area of convergence ===============

picture convergence_area(real lft, real rgt, bool fill_lft, bool fill_rgt) {
  picture pic;
  unitsize(pic,1cm);

  real xleft, xright; // limits of graph
  xleft=lft-1; xright=rgt+1;

  dotfactor = 6;
  draw(pic, (lft,0)--(rgt,0), FCNPEN_NOCOLOR+highlight_color);
  if (fill_lft) {
    dot(pic, (lft,0), highlight_color);
  } else {
    dot(pic, (lft,0), highlight_color, filltype=Fill(white));
  }
  if (fill_rgt) {
    dot(pic, (rgt,0), highlight_color);
  } else {
    dot(pic, (rgt,0), highlight_color, filltype=Fill(white));
  }

  real[] T = {lft, rgt};
  // The x axis
  xaxis(pic, L="",  
	axis=YZero,
	xmin=xleft-0.5, xmax=xright+0.5,
	p=currentpen,
	ticks=RightTicks("%", Step=1, Size=2.5pt),
	arrow=Arrows(TeXHead));
  // xaxis(pic, L="",  
  // 	axis=YZero,
  // 	xmin=xleft-0.75, xmax=xright+0.75,
  // 	p=currentpen,
  // 	ticks=RightTicks("%", T, Size=3pt),
  // 	arrow=Arrows(TeXHead));
  real EPS = 0.05;
  for (real p : T) {
    // treat p as an integer if it is within EPS
    if (p-floor(p) < EPS) {
      labelx(pic, format("$%d$", floor(p)), p);
    } else {
      if (ceil(p)-p < EPS) {
	labelx(pic, format("$%d$", ceil(p)), p);
      } else {
	labelx(pic, format("$%0.2f$", p), p);
      }
    }
  }

  return(pic);
}

// ........  geometric series .............
picture pic;
int picnum = 0;
unitsize(pic,1cm);

pic = convergence_area(-1,1,false,false);

shipout(format(OUTPUT_FN,picnum),pic);




// ........  sum (x-2)^n/n .............
picture pic;
int picnum = 1;
unitsize(pic,1cm);

pic = convergence_area(1,3,true,false);

shipout(format(OUTPUT_FN,picnum),pic);


// ...............sum x^n/2^n ............................
picture pic;
int picnum = 2;
unitsize(pic,1cm);

pic = convergence_area(-2,2,false,false);

shipout(format(OUTPUT_FN,picnum),pic);


// ...............sum (x+2)^n/sqrt{n} ............................
picture pic;
int picnum = 3;
unitsize(pic,1cm);

pic = convergence_area(-3,-1,true,false);

shipout(format(OUTPUT_FN,picnum),pic);



// ============= Graph of 1/1-x ========
real F4(real x) {
  return( (1)/(1-x) );
}

picture pic;
int picnum = 4;
unitsize(pic,1cm);  // to match the convergence interval

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=5;

real EPS = 0.19;
path f = graph(pic, F4, -1, 1-EPS);
draw(pic, f, FCNPEN);
// dotfactor = 12;
// dot(pic, (-1,1/2), FCNPEN);
filldraw(pic, circle( (-1,1/2), 0.05), FCNPEN, FCNPEN);
draw(pic, (1,0)--(1,5.2), dashed);

add(pic, convergence_area(-1,1,true,false).fit() );

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// =========== sum k! x^k ============= 
picture pic;
int picnum = 5;
unitsize(pic,1cm);

pic = convergence_area(0,0,true,true);

shipout(format(OUTPUT_FN,picnum),pic);




// ============= Graph of ln(1+x) ========
real F6(real x) {
  return( log(1+x) );
}

picture pic;
int picnum = 6;
unitsize(pic,1cm);  // to match the convergence interval

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-2; ytop=2;

real EPS = 0.1;
path f = graph(pic, F6, -1+EPS, 1);
draw(pic, f, FCNPEN);
// put in circle ends
filldraw(pic, circle( (1,F6(1)), 0.05), FCNPEN, FCNPEN);
draw(pic, (-1,ybot-0.2)--(-1,ytop+0.2), dashed);

add(pic, convergence_area(-1,1,false,true).fit() );

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


