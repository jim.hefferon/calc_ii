\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Sections 11.8 and 11.9\hspace{1em}Power series}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Power series}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage stub_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
% \input asy/arc_length_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................




\begin{frame}{Recall our goal: the prototypes}
We are trying to make mathematical sense of these, to see whether we can
write the left side as equal to the right.
\begin{center}
\begin{tabular}{c|l}
   \multicolumn{1}{c}{\tabulartext{Function}}
     &\multicolumn{1}{c}{\tabulartext{Taylor series}}  \\ \hline
   $\displaystyle \frac{1}{1-x}$ &$1+x+x^2+x^3+\cdots$ \rule{0pt}{14pt} \\[1ex]
   $\displaystyle e^x$           &$1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\cdots$  \\[1ex]
   $\displaystyle \cos x$        &$1-\frac{x^2}{2!}+\frac{x^4}{4!}-\frac{x^6}{6!}+\cdots$  \\[1ex]
   $\displaystyle \sin x$        &$x-\frac{x^3}{3!}+\frac{x^5}{5!}-\frac{x^7}{7!}+\cdots$  \\[1ex]
   $\displaystyle \ln(1+x)$      &$x-\frac{x^2}{2}+\frac{x^3}{3}-\frac{x^4}{4}+\cdots$  \\
\end{tabular}
\end{center}
% As with these five, 
% we will often find ourselves knowing what a series adds to if only
% it converges and
% so often the question will be of convergence.

We have defined the sum of an infinite series as
the limit of the sequence of partial sums.
Then we developed a number of tests for convergence.

Our next step to look at series that involves variables.
Our tests will find which values of the variable give convergence.
\end{frame}


\begin{frame}{Power series}
\Df 
A \alert{power series in variable~$x$ with coefficients~$c_i$}
has this form.
\begin{equation*}
  c_0+c_1x+c_2x^2+c_3x^3+\cdots\;=\sum_i c_i\cdot x^i
\end{equation*}
A \alert{power series centered at~$a$} 
has this form.
\begin{equation*}
  c_0+c_1(x-a)+c_2(x-a)^2+c_3(x-a)^3+\cdots\;=\sum_i c_i\cdot (x-a)^i
\end{equation*}
(In these summations we take $0^0=1$.)

\medskip
A power series may well converge for some $x$'s and diverge for others.
% For those $x$ where the series converges, it gives a function $\map{F}{\R}{\R}$
\end{frame}



\section{Radius of convergence}

\begin{frame}
\Ex For which $x$ does this power series converge?
For which does it diverge?
\begin{equation*}
  1\cdot(x-2)+\frac{1}{2}\cdot(x-2)^2+\frac{1}{3}\cdot(x-2)^3+\cdots
  =\sum_{0< n}\frac{(x-2)^n}{n}
  \tag{$*$}
\end{equation*}

\pause
The Ratio Test gives convergence when this limit is less than one
and divergence when it is greater.
\begin{multline*}
  \lim_{n\to\infty}\biggl| \frac{(x-2)^{n+1}/(n+1)}{(x-2)^n/n} \biggr|
  =\lim_{n\to\infty}\biggl|\frac{(x-2)^{n+1}\cdot n}{(x-2)^n\cdot (n+1)}\biggr| 
  =\lim_{n\to\infty}\biggl|\frac{(x-2)\cdot n}{n+1}\biggr|    \\
  =\lim_{n\to\infty}|x-2|\cdot\frac{n}{n+1}               
  =|x-2|\cdot\lim_{n\to\infty}\frac{n}{n+1}
\end{multline*}
Solving $1=|x-2|\cdot\lim_{n\to\infty}\frac{n}{n+1}$
gives $1=|x-2|$. 
For those veriables~$x$ where $|x-2|<1$ the power series is 
absolutely convergent.
If $|x-2|>1$ then it diverges.

\pause
The Ratio Test is inconclusive when the ratio is one.
Here that happpens at $x=3$ and $x=1$.
Can we figure out whether the series converges or diveges there by 
using another test?

Plugging $x=3$ into ($*$) gives $\sum_{0<n}1/n$, which is
the harmonic series and we know that it diverges.
And if $x=1$ then we have $\sum_{0<n}(-1)^n/n$, which is the alternating
harmonic series and we know that it converges.
\begin{center}
  \vcenteredhbox{\includegraphics{power_series001.pdf}}
\end{center}
\end{frame}



\begin{frame}
\Ex For which $x$ does this power series converge
and for which does it diverge?
\begin{equation*}
  1+\frac{1}{2}\cdot x+\frac{1}{4}\cdot x^2+\frac{1}{8}\cdot x^3+\cdots 
  =\sum_{0\leq n}\frac{x^n}{2^n}
\end{equation*}

\pause
Apply the Ratio Test.
\begin{equation*}
  \lim_{n\to\infty}\biggl| \frac{x^{n+1}/2^{n+1}}{x^n/2^n} \biggr|
  =\lim_{n\to\infty}\biggl|\frac{x^{n+1}\cdot 2^n}{x^n\cdot 2^{n+1}}\biggr| 
  =\lim_{n\to\infty}\biggl|\frac{x}{2}\biggr|    
  =\lim_{n\to\infty}|x|\cdot\frac{1}{2}               
  =|x|\cdot\frac{1}{2}
\end{equation*}
Solving $1=|x|\cdot(1/2)$ gives that
if $|x|<2$ then the power series 
absolutely converges.
If $|x|>2$ then the series diverges.

We finish by checking the endpoints, where $|x|=2$.
\begin{equation*}
  \sum_i\frac{(-2)^n}{2^n}=1-1+1-1+\cdots
  \qquad
  \sum_i\frac{2^n}{2^n}=1+1+1+\cdots
\end{equation*}
Both of these series diverge.
\begin{center}
  \vcenteredhbox{\includegraphics{power_series002.pdf}}
\end{center}
\end{frame}



\begin{frame}
\Ex Where does this power series converge
and where does it diverge?
\begin{equation*}
  1+x+2x^2+6x^3+24x^4+120x^5+\cdots
  =\sum_{0\leq k} k!\cdot x^k
\end{equation*}
\pause
The Ratio Test gives convergence when this 
limit is less than one.
\begin{equation*}
  \lim_{k\to\infty} \biggl|\frac{(k+1)!\cdot x^{k+1}}{k!\cdot x^k} \biggr|
  =\lim_{k\to\infty}\biggl|\frac{x\cdot (k+1)!}{k!}\biggr| 
  =\lim_{k\to\infty}|x|\cdot\frac{(k+1)!}{k!} 
  =|x|\cdot\lim_{k\to\infty}k+1    
\end{equation*}
Note that $\lim_{k\to\infty}k+1$ is infinite.
This power series converges only at the single point $x=0$.
\begin{center}
  \vcenteredhbox{\includegraphics{power_series005.pdf}}
\end{center}

\pause
\Ex For which $x$ does this power series converge
and for which does it diverge?
\begin{equation*}
  \sum_{0\leq k} \frac{(x-1)^k}{k!}
\end{equation*}

\pause
Find when this limit is less than one.
\begin{equation*}
  \lim_{k\to\infty}\biggl| \frac{(x-1)^{k+1}/(k+1)!}{(x-1)^k/k!} \biggr|
  =\lim_{k\to\infty}|x-1|\cdot\frac{k!}{(k+1)!} 
  =|x-1|\cdot\lim_{k\to\infty}\frac{1}{k+1}
  =|x-1|\cdot 0    
\end{equation*}
The power series converges absolutely at all real numbers~$x$.
\end{frame}


\begin{frame}{Radius of convergence}

\Tm
Each power series  $\sum_n c_n(x-a)^n$ has a 
\alert{radius of convergence $\rho$} with $0\leq \rho \leq \infty$.
The series converges at $x=a$.
Beyond that, inside the radius of convergence, that is,
for those $x$ with $|x-a|<\rho$,
the power series converges absolutely.

Further, if $\rho<\infty$ then (1)~the series diverges
outside of that radius, and 
(2)~as to the boundary where $|x-a|=\rho$, there are series that converge
at the left boundary and series that do not, and
the same holds for the right boundary. 

\Pf
See the book.

\Lm
For a power series  $\sum_n c_n(x-a)^n$,
consider the limit of the ratio of coefficients. 
\begin{equation*}
  R=\lim_{n\to\infty} |c_{n+1}/c_n|
  \tag{$*$}
\end{equation*}
If this limit exists then the radius of convergence is $\rho=1/R$,
where 
if $R=0$ then we take $\rho=\infty$ while if $R=\infty$ then we
take $\rho=0$.
\end{frame}

\begin{frame}
\Ex Find the interval of convergence for $\sum_{1\leq n}(1/\sqrt{n})\cdot(x+2)^n$.

\pause
This series is centered at $a=-2$.
To find the interval start with the limit of the ratio of coefficients.
\begin{equation*}
  R
  =\lim_{n\to\infty} \biggl| \frac{c_{n+1}}{c_n} \biggr|
  =\lim_{n\to\infty} \frac{1/\sqrt{n+1}}{1/\sqrt{n}}
  =\lim_{n\to\infty} \frac{\sqrt{n}}{\sqrt{n+1}}
  =1
\end{equation*}
The radius of convergence $\rho$ is $1/R=1$. 
The series converges absolutely for all $x$ in the interval 
$\open{-3}{-1}$ and diverges outside of that interval.

As to the interval's left boundary, substituting $x=-3$ gives
\begin{equation*}
  \sum_{1\leq n}(-1)^n\cdot \frac{1}{\sqrt{n}}
\end{equation*}
and the Alternating Series Test shows that it converges.
On the right boundary, substituting $x=-1$ gives
\begin{equation*}
  \sum_{1\leq n}\frac{1}{\sqrt{n}}
\end{equation*}
which is a $p$~series that diverges.
\begin{center}
  \vcenteredhbox{\includegraphics{power_series003.pdf}}
\end{center}
\end{frame}


\begin{frame}{Practice}
For each series find the radius of convergence and the interval
of convergence.
\begin{enumerate}
\item $\displaystyle \sum_i (-1)^i\frac{(x+1)^i}{i+1}$

\pause\smallskip
First, $a=-1$. 
The limit of the ratio of coefficients
\begin{equation*}
  R
    =\lim_{i\to\infty}\biggl| \frac{c_{i+1}}{c_i} \biggr|
    =\lim_{i\to\infty}\biggl| \frac{(-1)^{i+1}/(i+1)}{(-1)^i/i} \biggr|
   =\lim_{i\to\infty}\frac{i}{i+1}
   =1
\end{equation*}
gives a radius of convergence $\rho$ of $1/R=1$. 
The series converges, absolutely, for $x$ strictly between $-1$ and~$1$.
It diverges outside of that interval.
As to the two boundary points, at $x=1$ the Ratio Test gives divergence
while at $x=-1$ the terms are all zero so the series converges.
The interval of convergence is $\leftclosed{-1}{1}$.

\pause
\item $\displaystyle \sum_i \frac{(x-2)^i}{(i+1)\cdot 3^i}$

\pause\smallskip
\begin{equation*}
  R
    =\lim_{i\to\infty}\biggl| \frac{c_{i+1}}{c_i} \biggr|
    =\lim_{i\to\infty}\biggl| \frac{1/((i+1)3^{i+1})}{1/(i3^i} \biggr|
   =\lim_{i\to\infty}\frac{i+1}{3i}
   =1/3
\end{equation*}
The radius of convergence is $\rho=1/R=3$. 
Because $a=2$
the series converges, absolutely, for $x$ strictly between $-1$ and~$5$.
It diverges outside of that interval.
At $x=-1$ the Alternating Series Test gives
convergence
while at $x=5$ it is a version of the harmonic series so it diverges.
The interval of convergence is $\leftclosed{-1}{5}$. 



% \pause
% \item $\displaystyle x+2!\cdot x^2+3!\cdot x^3+\cdots$

% \pause\smallskip
% The series is 
% $\sum_i i!\cdot x^i$.
% The Ratio Test on the coefficients gives
% \begin{equation*}
%   R=\lim_{i\to\infty}\biggl| \frac{(i+1)!}{i!} \biggr|
%    =\lim_{i\to\infty}i+1
%    =\infty
% \end{equation*}
% The radius of convergence is $\rho=0$ and the series
% converges only at $x=0$.
\end{enumerate}
  
\end{frame}



% ====================
\section{Function defined by a power series}


\begin{frame}
Power series converge for some input~$x$'s.
Where they converge, they define a function
$\map{f}{\R}{\R}$.
\begin{equation*}
  f(x)=c_0+c_1x+c_2x^2+\cdots
  \quad\text{or}\quad
  f(x)=c_0+c_1(x-a)+c_2(x-a)^2+\cdots
\end{equation*}
We say that the series \alert{represents} the function.
\end{frame}


\begin{frame}
\Ex
The geometric series with common ratio~$x$ is a power series.
\begin{equation*}
  1+x+x^2+x^3+\cdots =\sum_i x^i
\end{equation*}
% (Again, in the summation expression we take the value to be~$1$ when
% $x=0$ and $i=0$.)
We know that it converges for $-1\leq x<1$ and diverges elsewhere.
When it converges we know that it gives this function.
\begin{center}
  \begin{minipage}{0.4\textwidth}
    \begin{equation*}
      F(x)=\sum_i x^i
    \end{equation*}
  \end{minipage}
  \quad
  \vcenteredhbox{\includegraphics{power_series004.pdf}}
\end{center}  
\end{frame}


\begin{frame}
\Ex
One of our prototypes is this
\begin{equation*}
  \ln(x+1) 
  = x-\frac{x^2}{2}+\frac{x^3}{3}-\frac{x^4}{4}\cdots
  =\sum_{1\leq i} (-1)^i\frac{x^i}{i}
\end{equation*}
The radius of convergence is $\rho=1/R$ where
\begin{equation*}
  R=\lim_{i\to\infty}\biggl| \frac{1/(i+1)}{1/i} \biggr|
   =\lim_{i\to\infty} \frac{i}{i+1}
   =1
\end{equation*}

As to the boundaries, $x=-1$ gives the negative of the 
harmonic series so it diverges.
And $x=1$ gives the alternating harmonic series, 
which converges. 
\begin{center}
  \begin{minipage}{0.4\textwidth}
    \begin{equation*}
      F(x)=\sum_{1\leq i} (-1)^i\frac{x^i}{i}
    \end{equation*}
  \end{minipage}
  \quad
  \vcenteredhbox{\includegraphics{power_series006.pdf}}
\end{center}  
\end{frame}

\begin{frame}
\Tm Let these two
converge absolutely on an open interval~$I$.
\begin{equation*}
  f(x)=c_0+c_1x+c_2x^2+\cdots=\sum_i c_ix^i
  \quad
  g(x)=d_0+d_1x+d_2x^2+\cdots=\sum_i d_ix^i
\end{equation*}\vspace{-4ex}
\begin{enumerate}
\item Those functions are continuous on~$I$.
\item The sum of the two series $\sum_i c_ix^i+\sum_i d_ix^i$
  converges absolutely to $f(x)+g(x)$, and the difference to 
  $f(x)-g(x)$.
  More generally, where $a,b\in\R$, the
  linear combination $a\cdot \sum_i c_ix^i+b\cdot \sum_i d_ix^i$
  converges absolutely on~$I$ to the function $a\cdot f(x)+b\cdot g(x)$.
\item The power series $x^m\cdot \sum_i c_ix^i=\sum_i c_ix^{i+m}$
  converges absolutely on~$I$ to the function $x^m\cdot f(x)$
  (provided $m$ is such that $m+i\geq 0$ for all~$i$).
\item Where $h(x)=s\cdot x^m$, the power series
  $\sum_i c_i(\,h(x)\,)^i$ converges absolutely on~$I$
  to the composition $f(h(x))$, provided that $h(x)$ is an element of~$I$.
\item The term-by-term derivative 
  \begin{equation*}
    f'(x)=c_1+2c_2x+3c_3x^2+4c_4x^3+\cdots=\sum_{i\leq 1} ic_i\cdot x^{i-1}
  \end{equation*}
  and antiderivative 
  \begin{equation*}
    \integral{}{}{f(x)}{x}
    =C+c_0x+\frac{c_1}{2}x^2+\frac{c_2}{3}x^3+\frac{c_3}{4}x^4+\cdots
    =C+\sum_{i} \frac{c_i}{i+1}\cdot x^{i+1}
\end{equation*}
converge on~$I$, to the indicated functions.
\end{enumerate}
\end{frame}


\begin{frame}
\Ex Starting from the geometric series 
and following the rules on the prior slide,
find the series for $1/(6-x)$.   

\pause
From this,
\begin{equation*}
  1+x+x^2+x^3+x^4+x^5+\cdots=\frac{1}{1-x}
\end{equation*}
use rule~4 to substitute $x/6$ in place of~$x$.
\begin{equation*}
  1+\frac{1}{6}x+\frac{1}{6^2}x^2+\frac{1}{6^3}x^3+\cdots
  =\frac{1}{1-\frac{x}{6}}
  =\frac{1}{(\frac{6-x}{6})}
  =\frac{6}{6-x}
\end{equation*}
Now use rule~2 to divide by $6$.
\begin{equation*}
  \frac{1}{6-x}
  =(1/6)\cdot(1+\frac{1}{6}x+\frac{1}{6^2}x^2+\frac{1}{6^3}x^3+\cdots)
  =\frac{1}{6}+\frac{1}{6^2}x+\frac{1}{6^3}x^2+\frac{1}{6^4}x^3+\cdots
  =\sum_{0\leq n}\frac{1}{6^{n+1}}x^n
\end{equation*}
As a check, this is a geometric series with common ratio $x/6$ and the formula 
does indeed give $a\cdot(1/(1-r))=(1/6)\cdot1/(1-(x/6)))=1/(6-x)$.
%\end{frame}

% \begin{frame}
\pause
\Ex
Starting from the geometric series, 
get the power series representing $1/(1-x)^2$.
\pause

The derivative of $(1-x)^{-1}$ is $(-1)(1-x)^{-2}\cdot(-1)=(1-x)^{-2}\!$.
Rule~5 gives this.
\begin{equation*}
  \frac{d (1+x+x^2+\cdots)}{dx} = 1+2x+3x^2+\cdots =\sum_{1\leq n} nx^{n-1}
\end{equation*}
\end{frame}

\begin{frame}
\Ex
Starting from the geometric series, 
get the power series representing $\ln(1-x)$.

\pause
We spot that $\ln(1-x)$ is like the antiderivative of $1/(1-x)$.
\begin{equation*}
  \integral{}{}{ \frac{1}{1-x} }{x} 
  =\integral{}{}{ 1+x+x^2+\cdots }{x} 
  = C+x+\frac{1}{2}x^2+\frac{1}{3}x^3\cdots 
  = C+\sum_{1\leq n} \frac{1}{n}x^{n}
\end{equation*}
Take negatives to get $\ln(1-x)=C-x-x^2/2-x^3/3-\cdots$
and then plug in $x=0$ to get $C=0$. 
% \end{frame}

% \begin{frame}
% \Ex
% We know that the derivative of $\arctan x$ is $1/(1+x^2)$.
% In the geometric series replace the $x$ with $-x^2$
% and then antidifferentiate to get the power series
% representing $\tan^{-1}x$.
% \begin{align*}
%   \frac{1}{1-x}       &= 1+x+x^2+x^3+x^4+x^5\cdots =\sum_n x^n\\
%   \frac{1}{1+x^2}=\frac{1}{1-(-x^2)} &= 1-x^2+x^4-x^6+x^8-\cdots=\sum_n(-1)^nx^{2n} \\
%   \tan^{-1}x=\integral{}{}{ \frac{1}{1+x^2} }{x} &=C+x-\frac{x^3}{3}+\frac{x^5}{5}-\frac{x^7}{7}+\cdots=\sum_n (-1)^n\frac{x^{2n+1}}{2n+1}
% \end{align*}
% (On the last line, compute the value of $C=0$ by plugging in $x=0$.)

\pause\Ex
One of our prototype five series is 
$\ln(x+1) 
  = x-\frac{x^2}{2}+\frac{x^3}{3}-\frac{x^4}{4}\cdots$\sentencespace
In the geometric series replace the $x$ with $-x$
and then antidifferentiate.
\begin{align*}
  \frac{1}{1-x} &= 1+x+x^2+x^3+x^4+\cdots \\
  \frac{1}{1+x} &= 1-x+x^2-x^3+x^4+\cdots  \\
  \ln(x+1)+C=\integral{}{}{ \frac{1}{1+x} }{x} &=C+x-\frac{x^2}{2}+\frac{x^3}{3}-\frac{x^4}{4}+\frac{x^5}{5}+\cdots
  =C+\sum_{1\leq n}(-1)^n\frac{1}{n}x^n
\end{align*}
Plug in $x=0$ to compute that $C=0$.
\end{frame}





\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
