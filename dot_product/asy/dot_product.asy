// dot_product.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "dot_product%03d";

import graph;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// =========== Vectors ===============
void draw_vector_2d(picture pic, pair start, pair end, pen p=defaultpen) {
  draw(pic, start--end, p, Arrow(ARROW_SIZE));  
}




// =============== angle =============

// ............ Not in canonical position ........
picture pic;
int picnum = 0;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=3;

pair displacement0 = (2.5,1);
pair displacement1 = (1,2);
draw(pic, "$\vec{u}$", (0.5,0.75)--((0.5,0.75)+displacement1), E, Arrow(ARROW_SIZE));
draw(pic, "$\vec{v}$", (0.15,-0.15)--((0.15,-0.15)+displacement0), NW, Arrow(ARROW_SIZE));

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
// xaxis(pic, L=L,  
//       axis=YEquals(ytop+0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// xaxis(pic, L="",  
//       axis=YEquals(ybot-0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.3, xmax=xright+0.3,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
// yaxis(pic, L="",  
//       axis=XEquals(xleft-0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// yaxis(pic, L="",  
//       axis=XEquals(xright+0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.3, ymax=ytop+0.3,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ............. In canonical position ...........
picture pic;
int picnum = 1;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=3;

// pair displacement0 = (2.5,1);
// pair displacement1 = (1,2);
draw(pic, "$\vec{u}$", (0,0)--displacement1, NW, Arrow(ARROW_SIZE));
draw(pic, "$\vec{v}$", (0,0)--displacement0, (E+SE)/2, Arrow(ARROW_SIZE));
draw(pic, "$\vec{u}-\vec{v}$", subpath(displacement0--displacement1,0.025,0.975), NE, Arrow(ARROW_SIZE));

// angle arc
path c = circle((0,0),0.4);
real v0_t = intersect(c, (0,0)--displacement0)[0];
real v1_t = intersect(c, (0,0)--displacement1)[0];
draw(pic, "$\theta$", subpath(c, v0_t, v1_t), highlight_color, Arrow(ARROW_SIZE/2));


// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
// xaxis(pic, L=L,  
//       axis=YEquals(ytop+0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// xaxis(pic, L="",  
//       axis=YEquals(ybot-0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.3, xmax=xright+0.3,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
// yaxis(pic, L="",  
//       axis=XEquals(xleft-0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// yaxis(pic, L="",  
//       axis=XEquals(xright+0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.3, ymax=ytop+0.3,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// =============== orthogonal projection =================

// ................. woman walking ...........
picture pic;
int picnum = 2;
size(pic,0,2.3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=3;

pair v = (1,3);
pair s = 1.5*(2,1);
pair proj = ( (v.x*s.x+v.y*s.y)/(s.x*s.x+s.y*s.y) )*s;
draw(pic, Label("$\vec{v}$",Relative(0.8)), (0,0)--v, NW, Arrow(ARROW_SIZE));
draw(pic, Label("$\vec{s}$",Relative(0.8)), (0,0)--s, SE, Arrow(ARROW_SIZE));
// draw(pic, "$\vec{v}-c\vec{s}$", subpath(v--proj,0.025,0.975), NE, Arrow(ARROW_SIZE));
draw(pic, proj--v, dotted+highlight_color);
label(pic,graphic("../pix/walking.png","width=0.30cm,angle=20"),
      proj+(-0.25,0.25));

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.3, xmax=xright+0.3,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.3, ymax=ytop+0.3,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);

// ............. triple of vectors ............
picture pic;
int picnum = 3;
size(pic,0,2.3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=3;

pair v = (1,3);
pair s = 1.5*(2,1);
pair proj = ( (v.x*s.x+v.y*s.y)/(s.x*s.x+s.y*s.y) )*s;
draw(pic, Label("$\vec{v}$",Relative(0.8)), (0,0)--v, NW, Arrow(ARROW_SIZE));
draw(pic, Label("$\vec{s}$",Relative(0.9)), (0,0)--s, SE, Arrow(ARROW_SIZE));
draw(pic, "$\vec{v}-c\cdot\vec{s}$", subpath(proj--v,0.025,0.975), NE, highlight_color, Arrow(ARROW_SIZE));

draw(pic, 0.95*proj--1.025*proj, FCNPEN_NOCOLOR+white);
draw(pic, Label("$c\cdot \vec{s}$",Relative(0.75)), (0,0)--proj, S, highlight_color, Arrow(ARROW_SIZE));
// draw(pic, proj--v, dotted+highlight_color);
// label(pic,graphic("../pix/walking.png","width=0.40cm,angle=20"),
//       proj+(-0.25,0.25));

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.3, xmax=xright+0.3,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.3, ymax=ytop+0.3,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ============== cos(beta-alpha) ================
picture pic;
int picnum = 4;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1.1;
ybot=0; ytop=1.15;

// part of unit circle
path unit_circle = arc( (0,0), (1,0), (0,1) );
draw(pic, unit_circle, FCNPEN_NOCOLOR+gray(0.85));

real beta = pi/3;
real alpha = pi/7;
pair u = (cos(beta),sin(beta));
pair v = (cos(alpha),sin(alpha));
pair proj = ( (v.x*s.x+v.y*s.y)/(s.x*s.x+s.y*s.y) )*s;
draw(pic, Label("$\vec{u}$",Relative(0.8)), (0,0)--u, NW, Arrow(ARROW_SIZE));
draw(pic, Label("$\vec{v}$",Relative(0.8)), (0,0)--v, SE, Arrow(ARROW_SIZE));
dotfactor = 3;
dot(pic, Label("$(\cos\beta,\sin\beta)$",filltype=Fill(white)), u, NE);
dot(pic, Label("$(\cos\alpha,\sin\alpha)$",filltype=Fill(white)), v, NE);

real beta_r = 0.4;
real alpha_r = 0.25;
path beta_arc = arc( (0,0), (beta_r,0), beta_r*unit(u));
path alpha_arc = arc( (0,0), (alpha_r,0), alpha_r*unit(v));
draw(pic, Label("$\beta$",Relative(0.75)), beta_arc, highlight_color, Arrow(ARROW_SIZE/2));
draw(pic, Label("$\alpha$",Relative(0)), alpha_arc, S, highlight_color, Arrow(ARROW_SIZE/2));

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ========= projection onto reverse of vector =========
picture pic;
int picnum = 5;
size(pic,0,2cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=1.5;

pair v = 0.75*(-2,1.25);
pair s = (2,1);
// pair proj = ( (v.x*s.x+v.y*s.y)/(s.x*s.x+s.y*s.y) )*s;
draw(pic, Label("$\vec{v}$",Relative(0.75)), (0,0)--v, SW, Arrow(ARROW_SIZE));
draw(pic, Label("$\vec{s}$",Relative(0.75)), (0,0)--s, SE, Arrow(ARROW_SIZE));
// draw(pic, "$\vec{v}-c\cdot\vec{s}$", subpath(proj--v,0.025,0.975), NE, highlight_color, Arrow(ARROW_SIZE));

// draw(pic, 0.95*proj--1.025*proj, FCNPEN_NOCOLOR+white);
// draw(pic, Label("$c\cdot \vec{s}$",Relative(0.75)), (0,0)--proj, S, highlight_color, Arrow(ARROW_SIZE));
// draw(pic, proj--v, dotted+highlight_color);
// label(pic,graphic("../pix/walking.png","width=0.40cm,angle=20"),
//       proj+(-0.25,0.25));

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ========= dot product as a measure of vector orthogonality =========
picture pic;
int picnum = 6;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1;
ybot=0; ytop=1;

real theta = 35;  // degrees
pair i = (1,0);
pair v = (Cos(theta),Sin(theta));

// unit circle part
draw(pic, subpath(circle( (0,0), 1), -0.1, 1.1), gray(0.8));

// vectors
draw(pic, Label("$\vec{i}$",Relative(0.75)), (0,0)--i, S, highlight_color, Arrow(ARROW_SIZE));
draw(pic, Label("$\vec{v}$",Relative(0.75)), (0,0)--v, NW, highlight_color, Arrow(ARROW_SIZE));
dotfactor = 4;
dot(pic, "$(\cos\theta,\sin\theta)$", v, E, highlight_color);

// angle arc
real arc_radius = 0.3;
path angle_arc =  arc( (0,0), arc_radius*(1,0), arc_radius*(v));
draw(pic, "{\scriptsize $\theta$}", angle_arc, Arrow(ARROW_SIZE));

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


