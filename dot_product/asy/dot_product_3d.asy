// dot_product_3d.asy

// compile with asy -inlineimage stub_3d
// In the .tex file put this in the preamble ... 
// % For 3D PRC asymptote
// % compile with asy -inlineimage stub_3d
// \def\asydir{asy/}
// \graphicspath{{asy/}}
// \input asy/stub_3d.pre
// \usepackage[bigfiles]{media9}
// \RequirePackage{asymptote}
// ... and then include the 3D material with something like this.
// \begin{center}
//   \uncover<2->{\input asy/stub_3d001.tex }% need the space following .tex   
// \end{center}


cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = true;
settings.embed = true;
settings.render = 0;
settings.tex="lualatex";  // for compiling in-pic text
settings.outformat="pdf";

import embed;

import fontsize;

import graph3; import solids;
projection default_projection = orthographic(1.2,3,2,up=Z);
currentprojection = default_projection;
currentlight=White; defaultrender.merge=true;
// unitsize(1cm);

string OUTPUT_FN = "dot_product_3d%03d";

material figure_material = material(diffusepen=bold_color+white+opacity(0.5),
				 emissivepen=bold_color+white,
				 specularpen=bold_color+white);
material slice_material = material(diffusepen=gray_color+opacity(0.25),
				   emissivepen=gray_color,
				   specularpen=gray_color);
pen boundary_pen = gray(0.2)+opacity(0.5);

currentlight = nolight;

// ==== general function in 2D ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);

// ==== general function in 3D ====
real GENFCN_maxx=3;
real GENFCN_maxy=4;
triple GENFCN_front_left = (GENFCN_maxx,0,1.8);
triple GENFCN_front_right = (GENFCN_maxx,GENFCN_maxy,1.75);
triple GENFCN_back_right = (0,GENFCN_maxy,1.95);
triple GENFCN_back_left = (0,0,2);

path3 GENFCN_left_edge = GENFCN_back_left..(0.6*GENFCN_maxx,0,2.05)..GENFCN_front_left;
path3 GENFCN_front_edge = GENFCN_front_left..(GENFCN_maxx,0.2*GENFCN_maxy,1.7)..(GENFCN_maxx,0.3*GENFCN_maxy,1.85)..(GENFCN_maxx,0.45*GENFCN_maxy,1.72)..GENFCN_front_right;
path3 GENFCN_right_edge = GENFCN_front_right..(0.4*GENFCN_maxx,GENFCN_maxy,2.0)..GENFCN_back_right;
path3 GENFCN_back_edge = GENFCN_back_right..(0,0.65*GENFCN_maxy,2.05)..(0,0.5*GENFCN_maxy,1.85)..(0,0.3*GENFCN_maxy,1.95)..(0,0.15*GENFCN_maxy,2.1)..GENFCN_back_left;

path3 GENFCN_edge = ( GENFCN_left_edge & GENFCN_front_edge & GENFCN_right_edge & GENFCN_back_edge )--cycle;
surface GENFCN=surface(GENFCN_edge, new triple[]);


// ===== two vectors in R^3 determine a plane ============
// currentprojection = orthographic(1.2,3,1.1,up=Z);

picture pic;
int picnum = 0;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

triple u = (-1,1,2);
triple v = (1,1,3);
// eqn of plane determined by the two vectors
triple f0(real x, real y) { return( (x,y,(x+5*y)/2) ); }

draw(pic, O--u, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, O--v, Arrow3(DefaultHead2,ARROW_SIZE));

path3 plane_edge = f0(1.2,1.2)--f0(-1.2,1.2)--f0(-1.2,-0.2)--f0(1.2,-0.2)--cycle;
surface s = surface(plane_edge);
draw(pic, s, surfacepen=figure_material);
draw(pic, plane_edge, boundary_pen);

xaxis3(pic,Label("\footnotesize $x$"),
       0,2.5, black,
       OutTicks("%",Step=1,Size=2pt),
       Arrow3(TeXHead2));
yaxis3(pic,Label("\footnotesize $y$"),
       0,3.5, black,
       OutTicks("%",Step=1,Size=2pt), 
       Arrow3(TeXHead2));
zaxis3(pic,Label("\footnotesize $z$"),
       0,3.5, black,
       OutTicks("%",Step=1,Size=2pt),
       Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);




// ========= direction angles in R^3 ========
currentprojection = orthographic(5,3,0.9,up=Z);
picture pic;
int picnum = 1;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

triple v = (2,3,3);
triple v_norm = v/sqrt(v.x^2+v.y^2+v.z^2);

draw(pic, Label("$\vec{v}$",Relative(0.75)), O--v, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\alpha$",filltype=Fill(white)), arc(O, (1,0,0), v_norm), N, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE/2));
draw(pic, Label("$\beta$",filltype=Fill(white)), arc(O, (0,1,0), v_norm), highlight_color, Arrow3(DefaultHead2,ARROW_SIZE/2));
draw(pic, Label("$\gamma$",filltype=Fill(white)), arc(O, (0,0,1), v_norm), highlight_color, Arrow3(DefaultHead2,ARROW_SIZE/2));

xaxis3(pic,Label("\footnotesize $x$"),
       0,2.5, black,
       NoTicks3, //OutTicks("%",Step=1,Size=2pt),
       Arrow3(TeXHead2));
yaxis3(pic,Label("\footnotesize $y$"),
       0, 3.5, black,
       NoTicks3, // OutTicks("%",Step=1,Size=2pt), 
       Arrow3(TeXHead2));
zaxis3(pic,Label("\footnotesize $z$"),
       0,3.5, black,
       NoTicks3, // OutTicks("%",Step=1,Size=2pt),
       Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);






// // ===== General surface in 3D  ============
// currentprojection = orthographic(8,1,0.9,up=Z);
// picture pic;
// int picnum = 1;
// size(pic,0,4cm);
// size3(pic,0,4cm,0,keepAspect=true);

// draw(pic, GENFCN_edge, boundary_pen);
// draw(pic, GENFCN, surfacepen=figure_material);
// draw(pic, GENFCN_front_edge, boundary_pen);
// draw(pic, GENFCN_right_edge, boundary_pen);

// xaxis3(pic,Label("\footnotesize $x$"),
//        0,GENFCN_maxx+0.5, black,
//        OutTicks("%",Step=1,Size=2pt),
//        Arrow3(TeXHead2));
// yaxis3(pic,Label("\footnotesize $y$"),
//        0, GENFCN_maxy+0.5, black,
//        OutTicks("%",Step=1,Size=2pt), 
//        Arrow3(TeXHead2));
// zaxis3(pic,Label("\footnotesize $z$"),
//        0,2.5, black,
//        OutTicks("%",Step=1,Size=2pt),
//        Arrow3(TeXHead2));

// shipout(format(OUTPUT_FN,picnum),pic);


// // ========= Sphere in R^3 ========
// currentprojection = orthographic(5,2,0.9,up=Z);
// picture pic;
// int picnum = 2;
// size(pic,0,3cm);
// size3(pic,0,3cm,0,keepAspect=true);

// draw(pic, scale3(2)*unitcircle3, boundary_pen);
// // draw(pic, scale3(2)*unitcircle3, black);
// draw(pic, scale3(2)*unitsphere, surfacepen=figure_material);

// xaxis3(pic,Label("\footnotesize $x$"),
//        0,5.5, black,
//        OutTicks("%",Step=1,Size=2pt),
//        Arrow3(TeXHead2));
// yaxis3(pic,Label("\footnotesize $y$"),
//        0, 3.5, black,
//        OutTicks("%",Step=1,Size=2pt), 
//        Arrow3(TeXHead2));
// zaxis3(pic,Label("\footnotesize $z$"),
//        0,3.5, black,
//        OutTicks("%",Step=1,Size=2pt),
//        Arrow3(TeXHead2));

// shipout(format(OUTPUT_FN,picnum),pic);




