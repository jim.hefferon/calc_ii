\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 11.3\hspace{1em}Dot product}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Dot product}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage stub_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
\input asy/dot_product_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



% \section{}


\begin{frame}{Angle between vectors in 2D}
Given two vectors as on the left,
we can take them to be in canonical position as on the right.
\begin{center}
  \vcenteredhbox{\includegraphics{dot_product000.pdf}}
  \qquad
  \vcenteredhbox{\includegraphics{dot_product001.pdf}}
\end{center}
This is standard plane geometry, so 
we have the law of cosines.
$|\vec{u}-\vec{v}|^2
  =
  |\vec{u}|^2+|\vec{v}|^2-
    2\,|\vec{u}|\,|\vec{v}|\cdot\cos\theta$ 
where \( \theta \) is the angle between the vectors.
The left side of that equation gives 
\begin{equation*}
(u_1-v_1)^2+(u_2-v_2)^2
     =(u_1^2-2u_1v_1+v_1^2)+(u_2^2-2u_2v_2+v_2^2)
\end{equation*}
while the right side gives this.
\begin{equation*}
(u_1^2+u_2^2)+(v_1^2+v_2^2)-
     2\,|\vec{u}|\,|\vec{v}|\cdot\cos\theta
\end{equation*}
Canceling squares and dividing by two gives 
a formula for the angle.
\begin{equation*}
  \theta
  =
  \arccos(\,\frac{u_1v_1+u_2v_2}{
         |\vec{u}|\,|\vec{v}| }\,)
\end{equation*}
\end{frame}

\begin{frame}
In higher dimensions essentially the same thing holds 
because the two vectors determine a plane and
then we can make the argument in that plane.
\begin{center}
  \input asy/dot_product_3d000.tex %
\end{center}
For instance, in $\R^3$ the angle between two vectors is this. 
\begin{equation*}
  \theta
  =
  \arccos(\,\frac{u_1v_1+u_2v_2+u_3v_3}{
         |\vec{u}|\,|\vec{v}| }\,)
\end{equation*}
\end{frame}

\begin{frame}
\Df The \alert{dot product}, 
or \alert{scalar product} or \alert{inner product}, of  
a pair of $n$~component vectors 
is the linear combination of their components.
\begin{equation*}
  \vec{u}\dotprod\vec{v}
  =\colvec{u_1 \\ \vdots \\ u_n}\dotprod\colvec{v_1 \\ \vdots \\ v_n}
  =u_1v_1+\cdots+u_nv_n
\end{equation*}

\Ex The dot product returns a number, not a vector.
\begin{equation*}
  \colvec{1 \\ 2}\dotprod\colvec{3 \\ 4}=1\cdot 3+2\cdot 4=11
  \quad
  \colvec{1 \\ 2 \\ 3}\dotprod\colvec{4 \\ 5 \\ 6}=32
\end{equation*}

\Lm 
The dot product of a vector with itself is the square of its length,
$\vec{v}\dotprod\vec{v}=v_1\cdot v_1+\cdots  v_n\cdot v_n
  =v_1^2+\cdots v_n^2=|\vec{v}|^2\!$.
\end{frame}


\begin{frame}{Practice}
\begin{enumerate}
\item Find the dot product of the vectors.
\begin{equation*}
  \colvec{1 \\ 3}\dotprod\colvec{2 \\ -5}    
  \quad\text{and}\quad
  \colvec{2 \\ 0 \\ -1}\dotprod\colvec{1 \\ 1 \\ 4}    
  \quad\text{and}\quad
  \colvec{3 \\ 1}\dotprod\colvec{2 \\ -1 \\ 5}    
\end{equation*}

\pause\smallskip
$1\cdot 2+3\cdot(-5)=-13$,
and $2\cdot 1+0\cdot 1-1\cdot 4=-2$,
and not defined.

\pause
\item Find the dot product of these vectors from $\R^4\!$.
\begin{equation*}
  \colvec{3 \\ -3 \\ 1/2 \\ 1}\dotprod\colvec{2 \\ 0 \\ 4\\ -2}    
\end{equation*}

\pause\smallskip
$3\cdot 2-3\cdot 0+(1/2)\cdot 4+1\cdot(-2)=6$

\pause
\item Find the dot product of the vector $\vec{\i}+\vec{\j}+4\vec{k}$ with itself.
What is the vector's length?

\pause\smallskip
From $1^2+1^2+4^2=18$ we get that the length is $\sqrt{18}$.

\pause
\item
What must $y$ be for the dot product to be $20$? To be $9$?
\begin{equation*}
  \colvec{1 \\ 2 \\ 2}\quad\colvec{3 \\ y \\ 6}    
\end{equation*}

\pause\smallskip
First, $20=1\cdot 3+2\cdot y+2\cdot 6$ gives $5/2=y$.
The other is $-3=y$.
\end{enumerate}
\end{frame}
  



\begin{frame}
\Lm  (Algebra of dot product)
The dot product operation is commutative, 
$\vec{v}\dotprod\vec{u}=\vec{u}\dotprod\vec{v}$.
Any dot product with the zero vector equals zero,
$\vec{v}\dotprod\vec{0}=0$.
The dot product operation allows scalars to factor out,
$(r\cdot\vec{v})\dotprod\vec{u}=r\cdot(\vec{v}\dotprod\vec{u})$
and $\vec{v}\dotprod(r\cdot\vec{u})=r\cdot(\vec{v}\dotprod\vec{u})$.
It also distributes over addition,
$\vec{u}\dotprod(\vec{v}+\vec{w})=(\vec{u}\dotprod\vec{v})+(\vec{u}\dotprod\vec{w})$, and
$(\vec{v}+\vec{w})\dotprod\vec{u}=(\vec{v}\dotprod\vec{u})+(\vec{w}\dotprod\vec{u})$.

\Pf These are easy to check;
as an example here is commmutativity.
\begin{equation*}
\vec{v}\dotprod\vec{u}=v_1u_1+\cdots+v_nu_n
  =u_1v_1+\cdots+u_nv_n=\vec{u}\dotprod\vec{v}
\end{equation*}
\end{frame}



\begin{frame}
\Df 
The \alert{angle} between two nonzero vectors $\vec{u},\vec{v}\in\R^n$ is this.
\begin{equation*}
 \theta=\cos^{-1}\bigl(\frac{\vec{u}\dotprod\vec{v}}{|\vec{u}|\,|\vec{v}|}\bigr) 
\end{equation*}
Because the angle is an arccosine, we know that $0\leq\theta \leq\pi$.

\Co Two vectors are orthogonal if and only if their dot product is zero.
They are parallel if and only if their dot product equals the product of their
lengths.

(The zero vector is orthogonal and parallel to all other vectors.)

\pause
So one take on the dot product is that it provides a measure
of vector orthogonality and
non-orthogonality.
Vectors where the dot product is close to zero are nearly orthogonal.
\begin{center}
  \includegraphics{dot_product006.pdf}
\end{center}
As $\theta$ grows from $0$ to~$\pi/2$, the dot product 
$\vec{v}\dotprod\vec{\i}=\cos\theta$
shrinks from~$1$ to~$0$. 
\end{frame}

\begin{frame}
\Ex Find the angle between the vectors $\vec{u}=3\vec{\i}-2\vec{j}$
and $\vec{v}=5\vec{\i}-(1/2)\vec{j}$.

\pause
The angle has this cosine 
$(3\cdot 5+2\cdot(1/2))/(\sqrt{9+4}\cdot\sqrt{5^2+(1/4)})$.
That's $\cos\theta=64/\sqrt{1313/4}$ and so $\theta\approx 0.155$~radians.  


% \Ex Find $y$ so that these two form an angle of $\pi/6$.
% \begin{equation*}
%   \vec{u}=\colvec{-3 \\ 1 \\ 2}
%   \qquad
%   \vec{v}=\colvec{5 \\ y \\ -3}
% \end{equation*}

% \pause
% We need $y$ so this holds.
% \begin{equation*}
%   \frac{\sqrt{3}}{2}
%   =\frac{(-3)5+y+2(-3)}{\sqrt{14}\sqrt{261+y^2}}
% \end{equation*}
% We get these.
% \begin{align*}
%   \frac{\sqrt{3}\sqrt{14}}{2}+15+6
%   &=\frac{y}{\sqrt{261+y^2}}  \\
%   \bigl( \frac{\sqrt{42}}{2}+21\bigr)\cdot\sqrt{261+y^2}
%   &=y                       \\
%   \bigl( \frac{\sqrt{42}}{2}+21\bigr)^2\cdot(261+y^2)
%   &=y^2                       \\
%   \frac{(21+\sqrt{42}/2)^2\cdot 261}{1-(21+\sqrt{42}/2)^2} 
%   &=y
% \end{align*}
\end{frame}
% sage: s = 16/(13*(21/4))
% sage: s
% 64/273
% sage: arccos(s)
% arccos(64/273)
% sage: round(arccos(s),ndigits=3)
% 1.334

\begin{frame}{Practice}
\begin{enumerate}
\item Find the angle between the vectors.
\begin{equation*}
  \colvec{1 \\ 3},\colvec{2 \\ -5}    
  \quad\text{and}\quad
  \colvec{2 \\ 0 \\ -1},\colvec{1 \\ 1 \\ 4}    
\end{equation*}
\pause\smallskip
$\cos^{-1}(-13/\sqrt{290})\approx 2.439$~rad,
and
$\cos^{-1}(-2/\sqrt{5}\sqrt{18})\approx 1.783$~rad.
\item Find the angle between the vectors.
\begin{equation*}
  \colvec{3 \\ -3 \\ 1/2 \\ 1},\colvec{2 \\ 0 \\ 4\\ -2}    
\end{equation*}

\pause\smallskip
$\cos^{-1}(6/\sqrt{77/4}\sqrt{24})\approx 1.288$~rad
\pause
\item Are these vectors 
$2\vec{\i}+\vec{k}$ and $\vec{\i}+\vec{\j}+4\vec{k}$
orthogonal?  Parallel?

\pause\smallskip
The dot product is $6$.
Because it is not zero they are not orthogonal.
Because it is not the product of their lengths, 
$|\vec{v}|\,|\vec{u}|=\sqrt{5}\cdot\sqrt{18}\approx 9.487$,
they are not parallel.
\end{enumerate}
\end{frame}

\begin{frame}
\begin{enumerate}\setcounter{enumi}{3}
\item
What must $y$ be for the vectors to be orthogonal?
Parallel?
\begin{equation*}
  \colvec{1 \\ 2 \\ 2},\colvec{3 \\ y \\ 6}    
\end{equation*}

\pause\smallskip
For  orthogonality we need $0=1\cdot 3+2\cdot y+2\cdot 6=15+2y$,
and so $y=-15/2$.
For parallel we need one vector to be a multiple of the other, 
and for that $y=6$ works.

\end{enumerate}
\end{frame}
% sage: A = vector([1,3])
% sage: B = vector([2,-5])
% sage: A.dot_product(B)/( norm(A)*norm(B) )
% -13/290*sqrt(29)*sqrt(10)  
% sage: A = vector([2,0,-1])
% sage: B = vector([1,1,4])
% sage: A.dot_product(B)/( norm(A)*norm(B) )
% -1/15*sqrt(5)*sqrt(2)
% sage: round( A.dot_product(B)/( norm(A)*norm(B) ), ndigits=3)
% -0.211
% sage: A = vector([3,-3,1/2,1])
% sage: B = vector([2,0,4,2])
% sage: A.dot_product(B)/( norm(A)*norm(B) )
% 5/231*sqrt(77)*sqrt(6)
% sage: A.dot_product(B)
% 10
% sage: norm(A)
% 1/2*sqrt(77)
% sage: norm(B)
% 2*sqrt(6)
% sage: 9+9+(1/4)+1
% 77/4
% sage: 8+16
% 24
% sage: round( A.dot_product(B)/( norm(A)*norm(B) ), ndigits=3)
% 0.465


\begin{frame}{Direction angles}
For a vector~$\vec{v}$, the angle it makes with the $x$~axis is the
\alert{direction angle}~$\vec{\alpha}$.
The angle that~$\vec{v}$ makes with the $y$~axis is the direction angle~$\beta$,
and the angle it makes with the $z$~axis is~$\gamma$.
\begin{center}
  \input asy/dot_product_3d001.tex %   
\end{center}  
\Df The \alert{direction cosines} are the three
$\cos\alpha$, $\cos\beta$, and $\cos\gamma$.

\Ex For $\vec{v}=2\vec{\i}+3\vec{\j}-\vec{k}$ we have these.
\begin{equation*}
  \cos\alpha=\frac{\colvec{2 \\ 3 \\ -1}\dotprod\colvec{1 \\ 0 \\ 0}}{\sqrt{4+9+1}\cdot\sqrt{1+0+0}}
  =\frac{2}{\sqrt{14}}
  \quad
  \cos\beta=\frac{3}{\sqrt{14}}
  \quad
  \cos\gamma=\frac{-1}{\sqrt{14}}
\end{equation*}
\end{frame}


\begin{frame}{Practice}
\begin{enumerate}
\item Find the direction cosines for this vector.
\begin{equation*}
  \colvec{2 \\ -1 \\ -4}    
\end{equation*}

\pause
\begin{equation*}
  \cos\alpha=\frac{2}{\sqrt{21}}
  \quad
  \cos\beta=\frac{-1}{\sqrt{21}}
  \quad
  \cos\gamma=\frac{-4}{\sqrt{21}}
\end{equation*}

\pause\item
If $\vec{v}$ is on the $x$~axis then what is $\cos\alpha$?

\pause\smallskip
$\cos\alpha=0$
\end{enumerate}
\end{frame}



\section{Orthogonal projection}

\begin{frame}  % {Definition}\vspace*{-1ex}
This person is not pulling in the most effective way because the direction
of their pull\Dash the direction in which they apply their force\Dash 
is not parallel to
the ground.
\begin{center}
  \includegraphics[height=0.16\textheight]{pix/pulling.jpg}    
\end{center}
Given two vectors, we can ask how much of $\vec{v}$ lies with~$\vec{s}$, 
that is, how much of $\vec{v}$ lies in the direction of~$\vec{s}$.

\pause
On the left below  
the person walks out on $\vec{s}$\,'s line, looking up, until they are directly 
underneath the tip of~$\vec{v}$, so that the dotted line is perpendicular
to~$\vec{s}$. 
\begin{center}
  \vcenteredhbox{\includegraphics{dot_product002.pdf}}    
  \qquad
  \vcenteredhbox{\includegraphics{dot_product003.pdf}}    
\end{center}
\pause%
We want the multiple~$c\cdot\vec{s}$ that makes the picture on the right work.
Because 
$\vec{v}-c\,\vec{s}$ is orthogonal to $c\,\vec{s}$, it must be orthogonal to
$\vec{s}$ itself.
Then $0=\vec{s}\dotprod(\vec{v}-c\,\vec{s})$
gives $0=\vec{s}\dotprod\vec{v}-c\,\vec{s}\dotprod\vec{s}$,
and so $c\,\vec{s}=(\vec{s}\dotprod\vec{v}/\vec{s}\dotprod\vec{s})\cdot\vec{s}$.
\end{frame}

\begin{frame}

\Df The \alert{orthogonal projection} of $\vec{v}$ onto the nonzero vector 
$\vec{s}$ is this vector.
\begin{equation*}
  \alert{\operatorname{proj}_{\,\,\,\,\vec{\!\!\!\!s}}(\vec{v})}
  =\frac{\vec{v}\dotprod\vec{s}}{\vec{s}\dotprod\vec{s}}\cdot\vec{s}
\end{equation*}
(The terminology is sloppy.
Really it is the projection onto the line described by~$\vec{s}$.) 

\pause
We sometimes want the length of this projection.
% \begin{equation*}
%   \operatorname{proj}_{\,\,\,\,\vec{\!\!\!\!s}}(\vec{v})
%   =\frac{\vec{v}\dotprod\vec{s}}{\vec{s}\dotprod\vec{s}}\cdot\vec{s} 
% \end{equation*}
Rewrite the denominator's $\vec{s}\dotprod\vec{s}$ as $|\vec{s}|^2$ and 
use one of the two $|\vec{s}\,|$'s to normalize $\vec{s}\,$'s length. 
\begin{equation*}  
  =\frac{\vec{v}\dotprod\vec{s}}{|\vec{s}|^2}\cdot\vec{s}  
  =\frac{\vec{v}\dotprod\vec{s}}{|\vec{s}|}\cdot\frac{\vec{s}}{|\vec{s}|}  
\end{equation*}
Since the vector $\vec{s}/|\vec{s}|$ has length one, the length
of the projection is the absolute value of the scalar multiple.
\begin{equation*}
  |\operatorname{proj}_{\,\,\,\,\vec{\!\!\!\!s}}(\vec{v})|
  =\left| \frac{\vec{v}\dotprod\vec{s}}{|\vec{s}|}\right|  
  =\frac{|\vec{v}\dotprod\vec{s}|}{|\vec{s}|}
\end{equation*}
Sometimes people call $\vec{v}\dotprod\vec{s}/|\vec{s}|$
(without the absolute value in the numerator)
the `scalar projection of $\vec{v}$ onto~$\vec{s}\,$'.
It is plus or minus the length of the projection.
% Your book uses the symbol 
% $\operatorname{comp}_{\,\,\,\vec{\!\!s}}(\vec{v})$, which perhaps
% abbreviates `component'.
\end{frame}


\begin{frame}{Practice}
\begin{enumerate}
\item Find the projection of $\vec{v}$ onto $\vec{s}$.
\begin{equation*}
  \vec{v}=\colvec{1 \\ -2 \\ -1}
  \qquad
  \vec{s}=\colvec{3 \\ -2 \\ 5}    
\end{equation*}

\pause
The formula gives this.
\begin{equation*}
  \operatorname{proj}_{\,\vec{s}}(\vec{v})
  =\frac{\vec{v}\dotprod\vec{s}}{\vec{s}\dotprod\vec{s}}\cdot\vec{s}
  =\frac{3+4-5}{9+4+25}\cdot\vec{s}  
  =\frac{2}{38}\cdot\colvec{3 \\ -2 \\ 5}  
  =\colvec{3/19 \\ -2/19 \\ 5/19}  
\end{equation*}

\item Decompose $\vec{a}$ into the sum of two vectors, one parallel to~$\vec{b}$
and the other perpendicular to~$\vec{b}$.
\begin{equation*}
  \vec{a}=\colvec{2 \\ 0 \\ 1}
  \qquad
  \vec{b}=\colvec{3 \\ -1 \\ -1}    
\end{equation*}

\pause
From the prior slide's picture we want $\operatorname{proj}_{\,\,\,\vec{\!\!b}}(\vec{a})$
and $\vec{a}-\operatorname{proj}_{\,\,\,\vec{\!\!b}}(\vec{a})$.

\pause
The formula gives this.
\begin{equation*}
  \operatorname{proj}_{\,\,\,\vec{\!\!b}}(\vec{a})
  =\frac{\vec{a}\dotprod\vec{b}}{\vec{b}\dotprod\vec{b}}\cdot\vec{b}
  =\frac{5}{11}\cdot\colvec{3 \\ -1 \\ -1}  
  =\colvec{15/11 \\ -5/11 \\ -5/11}  
  \quad
  \vec{a}-\operatorname{proj}_{\,\,\,\vec{\!\!b}}(\vec{a})
  =\colvec{7/11 \\ 5/11 \\ 16/11}
\end{equation*}
\end{enumerate}
\end{frame}

\begin{frame}
\begin{enumerate}\setcounter{enumi}{2}
\item For those two vectors, 
find the length of the projection of $\vec{a}$ onto $\vec{b}$.
\pause
\begin{equation*}
  \frac{|\vec{a}\dotprod\vec{b}|}{|\vec{b}|} 
  =\frac{|6+0-1|}{\sqrt{9+1+1}}
  =\frac{5}{\sqrt{11}}
  \qquad\text{(same as $\sqrt{\biggl(\frac{15}{11}\biggr)^2+\biggl(\frac{-5}{11}\biggr)^2+\biggl(\frac{-5}{11}\biggr)^2}$)}
\end{equation*}

\pause\item True or False: the value of 
$\operatorname{proj}_{\,\,\,\vec{\!\!s}}(\vec{v})$
is $\vec{0}$ because $\vec{v}$ points in a direction generally opposite 
to~$\vec{s}$'s direction.
\begin{center}
  \includegraphics{dot_product005.pdf}
\end{center}

\pause\smallskip
False. 
The projection of $\vec{v}$ onto $\vec{s}$ is really 
the projection onto the line through the origin 
that contains~$\vec{s}$.

\pause\item
For any vector $\vec{v}\in\R^3\!$, what is its projection onto the $x$~axis?

\pause\smallskip
It is $\vec{v}\,$'s first component, times $\vec{\i}$. 

\pause\item
For any vector, what is the projection 
$\operatorname{proj}_{\,\,\,\vec{\!\!v}}(\vec{v})$?

\pause\smallskip
For $\vec{v}\neq\vec{0}$ it is $\vec{v}$.
(For $\vec{v}=\vec{0}$ it is undefined.)

\pause\item
What is the projection of $\vec{v}$ onto $-\vec{v}$?

\pause\smallskip
The answer is the same as the answer to the prior question.
For any nonzero vector it is the vector itself, $\vec{v}$.

\pause\item
What is the projection of $\vec{v}$ onto $5\vec{v}$?

\pause\smallskip 
The vector itself, $\vec{v}$, provided it is nonzero.
\end{enumerate}
\end{frame}

\begin{frame}
\begin{enumerate}\setcounter{enumi}{8}
\item
Find
$\operatorname{proj}_{\,\,\,\,\vec{\!\!\!\!s}}(\vec{v})$.
\begin{equation*}
  \vec{v}=\colvec{1 \\ 2 \\ 3 \\ 4}
  \qquad
  \vec{s}=\colvec{5 \\ 0 \\ 0 \\ -1}
\end{equation*}
\pause
The formula gives this
\begin{equation*}
  \frac{\vec{v}\dotprod\vec{s}}{|\vec{v}|\,|\vec{s}|} 
  \frac{1}{\sqrt{30}\sqrt{26}}
\end{equation*}
So we can do geometry things such as dropping a perpendicular in spaces
where we cannot picture.
\end{enumerate}
\end{frame}
% sage: 30*26
% 780
% sage: round( arccos(1/sqrt(780)), ndigits=3)
% 1.535



\begin{frame}{Extra: cosine of the difference between angles}
Earlier in the course we've worked with the formula for $\cos(\beta-\alpha)$.
Deriving it is a simple application of the work above.
\begin{center}
  \vcenteredhbox{\includegraphics{dot_product004.pdf}}    
\end{center}
\begin{equation*}
  \cos(\beta-\alpha)
  =\frac{ \colvec{\cos\beta \\ \sin\beta}\dotprod\colvec{\cos\alpha \\ \sin\alpha} }{\sqrt{1}\cdot \sqrt{1}}
  =\cos\beta\cdot\cos\alpha+\sin\beta\cdot\sin\alpha
\end{equation*}
\end{frame}



\begin{frame}{Extra: Triangle Inequality}
\Tm  Any vectors $\vec{u},\vec{v}\in\R^n$ satisfy that
$|\vec{u}+\vec{v}|\leq |\vec{u}|+|\vec{v}|$.
Equality holds if and only if one of the vectors 
is a nonnegative scalar multiple of the other.

This is the source of the familiar saying, 
``The shortest distance between two points is in a straight line.''

\Pf
Since all three numbers are positive,
the inequality holds if and only if its square holds.
\begin{align*}
  |\vec{u}+\vec{v}|^2
  &\leq (\, |\vec{u}|+|\vec{v}| \,)^2                            \\
  (\,\vec{u}+\vec{v}\,)\dotprod(\,\vec{u}+\vec{v}\,)
  &\leq|\vec{u}|^2+2\,|\vec{u}|\,|\vec{v}|+|\vec{v}|^2          \\
  \vec{u}\dotprod\vec{u}+\vec{u}\dotprod\vec{v}
     +\vec{v}\dotprod\vec{u}+\vec{v}\dotprod\vec{v}
  &\leq\vec{u}\dotprod\vec{u}+2\,|\vec{u}|\,|\vec{v}|+\vec{v}\dotprod\vec{v}  \\
  2\,\vec{u}\dotprod\vec{v}
  &\leq 2\,|\vec{u}|\,|\vec{v}|
  \tag{$*$}
\end{align*}
In turn, that holds if the relationship obtained by multiplying 
both sides by the nonnegative numbers \( |\vec{u}| \) and \( |\vec{v}| \)
\begin{equation*}
  2\,(\,|\vec{v}|\,\vec{u}\,)\dotprod(\,|\vec{u}|\,\vec{v}\,)
  \leq
  2\,|\vec{u}|^2\,|\vec{v}|^2
\end{equation*}
and rewriting
\begin{equation*}
  0
  \leq
  |\vec{u}|^2\,|\vec{v}|^2
   -2\,(\,|\vec{v}|\,\vec{u}\,)\dotprod(\,|\vec{u}|\,\vec{v}\,)
  +|\vec{u}|^2\,|\vec{v}|^2
\end{equation*}
is true.
\end{frame}

\begin{frame}
But factoring shows that this statement is true
\begin{equation*}
  0
  \leq
  |\vec{u}|^2\,|\vec{v}|^2
   -2\,(\,|\vec{v}|\,\vec{u}\,)\dotprod(\,|\vec{u}|\,\vec{v}\,)
  +|\vec{u}|^2\,|\vec{v}|^2  
  =
  (\,|\vec{u}|\,\vec{v}-|\vec{v}|\,\vec{u}\,)\dotprod
  (\,|\vec{u}|\,\vec{v}-|\vec{v}|\,\vec{u}\,)
\end{equation*}
since it only says that the 
square of the length of the vector
\( |\vec{u}|\cdot\vec{v}-|\vec{v}|\cdot\vec{u}\, \) is
not negative.

\pause
As for the second sentence on equality, the equation above shows that
it holds when and only when
\( |\vec{u}|\cdot\vec{v}-|\vec{v}|\cdot\vec{u} = \vec{0} \).
We finish by checking that this holds if and only if
one of the vectors is a nonnegative scalar multiple of the other.

Assume for one direction 
that \( |\vec{u}|\,\vec{v}-|\vec{v}|\,\vec{u}=\vec{0} \),
aiming to show that one vector is a nonnegative scalar multiple of the other.
If either is the zero vector then it is a nonnegative multiple of the other, 
so what remains is the case that % we can assume that in $|\vec{u}|\,\vec{v}=|\vec{v}|\,\vec{u}$, 
neither vector is~$\vec{0}$.
Then $|\vec{u}|\,\vec{v}=|\vec{v}|\,\vec{u}$ gives that
$\vec{v}=(|\vec{v}|/|\vec{u}|)\cdot\vec{u}$, as required.

For the other direction assume that 
one of $\vec{u}$ or~$\vec{v}$ is a nonzero scalar multiple of the other.
We will do $\vec{v}=r\cdot\vec{u}$; the other case works the same way.
Nonnegativity gives $|\vec{v}|=r\cdot |\vec{u}|$ and then
$
  |\vec{u}|\,\vec{v}-|\vec{v}|\,\vec{u}
  =|\vec{u}|\cdot r\vec{u}-r|\vec{u}|\cdot \vec{u}
  =\vec{0}
$.

\pause\medskip
The crucial point is the line marked ($*$) on the prior slide.

\Co (Cauchy-Schwartz Inequality)
If $\vec{u},\vec{v}\in\R^n$ then
$|\vec{u}\dotprod\vec{v}|\leq |\vec{u}|\cdot|\vec{v}|$,
with equality if and only if one vector is a scalar multiple of the other.

This shows that the angle definition makes sense no matter what $\R^n$
we are in, because we are taking the arccosine of a fraction less than
or equal to one.
\end{frame}





\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
