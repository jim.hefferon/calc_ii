\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
% \usepackage{present}
% \usepackage{../calcii}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 7.4: Partial Fractions}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% \usepackage{polynom}

% From https://tex.stackexchange.com/a/131138/339
\newcommand\jhlongdivision[2]{%
\strut#1\kern.25em\smash{\raise.4ex\hbox{$\big)$}}\mkern-8mu
        \overline{\enspace\strut#2}}


\subject{Partial fractions}
% This is only inserted into the PDF information catalog. Can be left
% out. 

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}
\frametitle{Partial fractions} 

We want to integrate any expression of this form.
\begin{equation*}
  \frac{\text{\small\textit{--polynomial--}}}{\text{\small\textit{--polynomial--}}}
\end{equation*}
Here are examples.
\begin{equation*}
  \integral{}{}{\frac{x^3-3x+1}{x-1}}{x}
  \qquad
  \integral{}{}{\frac{3x-4}{x^2-1}}{x}
\end{equation*}
\end{frame}


\begin{frame}
\frametitle{The method of Partial Fractions}

We know how to compose, that is, to put together, fractions of polynomials.
\begin{equation*}
    \frac{1}{x+4}+\frac{2}{x-2}
    =\frac{3x-6}{x^2+2x-8}
\end{equation*}
Here we will learn to decompose them.
The advantage is that it will allow us to integrate.
\begin{equation*}
  \integral{}{}{\frac{3x-6}{x^2+2x-8}}{x}
  =\ln |x+4|+2\ln |x-2|+C
\end{equation*}

\pause\medskip
Sometimes in this class we use a result without proving it.
The Partial Fractions result depends on Linear Algebra but rather than 
postpone it until after that class, we use it now.

Given a fraction of polynomials, 
the method  involves three steps.
We first reduce to where the degree of the numerator is less
than the degree of the denominator.
We next factor the denominator.
Finally, we will decompose the fraction into a sum of 
fractions involving the factors of the denominator.
\end{frame}


\begin{frame}{First: recall polynomial division}
\Ex
Do the division to find the quotient and remainder.
\begin{equation*}
  \frac{3x^3+2x^2-x+4}{x^2+x}
\end{equation*}
% $\jhlongdivision{x^2+x}{3x^3+2x^2-x+4}$
\pause
Sometimes this is called long division or synthetic division.
\begin{equation*}
  \begin{array}{r@{}l}
          &\quad 3x-1                        \\
    x^2+x &\jhlongdivision{}{3x^3+2x^2-x+4}   \\
          &\hspace*{-3pt}\underline{-(3x^3+3x^2)}           \\
          &\hspace*{25pt}-x^2-x              \\
          &\hspace*{16pt}\underline{-(-x^2-x)}           \\
          &\hspace*{67pt}4              
  \end{array}
\end{equation*}
We have the quotient and remainder.
\begin{equation*}
  \frac{3x^3+2x^2-x+4}{x^2+x}=3x-1+\frac{4}{x^2+x}
\end{equation*}
\end{frame}

\begin{frame}
\Ex Divide $x-1$ into $x^3+x^2-1$

\pause
It helps the bookkeeping to add a $0x$ term.
\begin{equation*}
  \begin{array}{r@{}l}
          &\quad x^2+2x+2                        \\
    x-1   &\jhlongdivision{}{x^3+x^2+0x-1}   \\
          &\hspace*{-3pt}\underline{-(x^3-x^2)}           \\
          &\hspace*{25pt}2x^2+0x              \\
          &\hspace*{16pt}\underline{-(2x^2-2x)}           \\
          &\hspace*{50pt}2x-1                          \\
          &\hspace*{40pt}\underline{-(2x-2)}           \\
          &\hspace*{67pt}1              
  \end{array}
\end{equation*}
We have this.
\begin{equation*}
  \frac{x^3+x^2-1}{x-1}=x^2+2x+2+\frac{1}{x-1}
\end{equation*}
\end{frame}




\begin{frame}{Second: recall factoring}
\Tm
(Fundamental Theorem of Arithmetic)
Every real-coefficient polynomial of degree at least three factors
into a product of lower degree real polynomials.
A degree two polynomial $ax^2+bx+c$ factors into linear real polynomials
if and only if the \alert{discriminant}
$b^2-4ac$ is greater than~$0$.

Therefore any polynomial factors into a product of linears 
and irreducible quadratics.
  \begin{itemize}
  \item $x^2+1$ is irreducible because the discriminant is $b^2-4ac=-4$.
  \item In contrast with the prior one,
    $x^4+1=(x^2-\sqrt{2}\,x+1)(x^2+\sqrt{2}\,x+1)$.
    Each of those two quadratics is irreducible because for both 
    $b^2-4ac$ is $-2$.
  \item Any cubic must factor: $3x^3-2x^2+3x-2=(3x-2)(x^2+1)$
  \item Any fourth power must factor: $4x^4-8x^3-12x^2+16x+16=(x+1)^2(2x-4)^2$
  \item $60x^5+220x^4+220x^3+140x^2+160x-80=(3x-1)(2x+4)^3(x^2+1)$
\end{itemize}

The \alert{quadratic formula} factors reducible quadratics. 
\begin{equation*}
  ax^2+bx+c 
  =a\cdot(x-\frac{b+\sqrt{b^2-4ac}}{2a})\cdot(x-\frac{b-\sqrt{b^2-4ac}}{2a})
\end{equation*}
\end{frame}
% sage: x = PolynomialRing(QQ, 'x').gen()
% sage: f=(3*x-2)*(x^2+1); f
% 3*x^3 - 2*x^2 + 3*x - 2
% sage: f=(x+1)^2*(2*x-4)^2; f
% 4*x^4 - 8*x^3 - 12*x^2 + 16*x + 16
% sage: f=5*(3*x-1)*(2*x+4)^2*(x^2+1); f
% 60*x^5 + 220*x^4 + 220*x^3 + 140*x^2 + 160*x - 80



\begin{frame}
\frametitle{Third: partial fractions}
So from now on 
we assume that we have a rational function where 
the degree of the numerator less than the degree of 
the denominator, and where
the denominator is fully factored
into linears and irreducible quadratics.
\begin{equation*}
  f(x)=\frac{n(x)}{d(x)}=\frac{n(x)}{(a_1x+b_1)^{p_1}\cdots(c_1x^2+d_1x+e_1)^{q_1}\cdots}
\end{equation*}
\end{frame}



\begin{frame}{Case 1: denominator has only first power linears}
If the denominator factors into unequal linears
$d(x)=(a_1x+b_1)\cdots (a_kx+b_k)$
then there are reals
$A_1, \ldots{} A_k$ giving this.
\begin{equation*}
  \frac{n(x)}{(a_1x+b_1)\cdots(a_kx+b_k)}
   =\frac{A_1}{a_1x+b_1}+\cdots+\frac{A_k}{a_kx+b_k}
\end{equation*}

\pause
\Ex $\displaystyle\frac{2x+1}{(x+4)(3x-5)}$
\pause\quad
\begin{equation*}
  \frac{2x+1}{(x+4)(3x-5)}=\frac{A_1}{x+4}+\frac{A_2}{3x-5}
\end{equation*}
\pause
Get a common denominator.
\begin{equation*}
  \frac{2x+1}{(x+4)(3x-5)}=\frac{A_1}{x+4}\cdot\frac{3x-5}{3x-5}+\frac{A_2}{3x-5}\cdot\frac{x-4}{x-4}
\end{equation*}
Set the numerators equal.
\begin{equation*}
  2x+1=A_1(3x-5)+A_2(x+4)=(3A_1+A_2)\cdot x+(-5A_1+4A_2)
\end{equation*}
\pause
So the two sides have the same coefficient of~$x$ and the same constant.
\begin{align*}
  2 &= 3A_1+A_2 \\
  1  &= -5A_1+4A_2
\end{align*}
\end{frame}
\begin{frame}
Solve.
\begin{equation*}
  \grstep{(5/3)\rho_1+\rho_2}
  \begin{linsys}{2}
     3A_1 &+  &A_2       &=  &2  \\
          &   &(17/3)A_2 &=  &13/3 
  \end{linsys}
\end{equation*}
We have $A_2=13/17$ and so $A_1=7/17$.

\pause
\Ex $\displaystyle\frac{2x+3}{(x-1)\cdot (x+1)\cdot x}$

\pause
Partial Fractions gives this.
\begin{equation*}
  \frac{2x+3}{(x-1)\cdot (x+1)\cdot x}=\frac{A_1}{x-1}+\frac{A_2}{x+1}+\frac{A_3}{x}
\end{equation*}
Get a common denominator.
\begin{equation*}
  =\frac{A_1}{x-1}\cdot\frac{(x+1)\cdot x}{(x+1)\cdot x}
   +\frac{A_2}{x+1}\cdot\frac{(x-1)\cdot x}{(x-1)\cdot x}
   +\frac{A_3}{x}\cdot \frac{(x-1)(x+1)}{(x-1)(x+1)}
\end{equation*}
We have this.
\begin{equation*}
  \frac{2x+3}{(x-1)\cdot (x+1)\cdot x}=
  \frac{A_1(x^2+x)+A_2(x^2-x)+A_3(x^2-1)}{(x-1)\cdot (x+1)\cdot x}
\end{equation*}
\end{frame}

\begin{frame}
This is copied from the prior slide.
\begin{equation*}
  \frac{2x+3}{(x-1)\cdot (x+1)\cdot x}=
  \frac{A_1(x^2+x)+A_2(x^2-x)+A_3(x^2-1)}{(x-1)\cdot (x+1)\cdot x}
\end{equation*}
Compare the two sides for the coefficients of $x^2$,
of $x$, and the constants.
\begin{equation*}
  \begin{linsys}{3}
    A_1 &+ &A_2 &+ &A_3 &= &0  \\
    A_1 &- &A_2 &  &    &= &2  \\
        &  &   &  &-A_3 &= &3  \\
  \end{linsys}
  \grstep{-\rho_1+\rho_2}
  \begin{linsys}{3}
    A_1 &+ &A_2   &+ &A_3 &= &0  \\
        &  &-2A_2 &- &A_3 &= &2  \\
        &  &   &  &-A_3 &= &3  \\
  \end{linsys}
\end{equation*}
Back substitution shows that $A_3=-3$, $A_2=1/2$, and $A_1=5/2$.
\end{frame}
% sage: load("../gauss_method/sage/gauss_method_latex.sage")
% sage: P = matrix(QQ, [[1, 1, 1], [1,-1,0], [0,0,-1]])
% sage: w = vector(QQ, [0 ,2 ,3])
% sage: P_prime=P.augment(w, subdivide=True)
% sage: gauss_method(P_prime, latex=True)
% \begin{amat}{3}
%   1  &1  &1  &0  \\ 
%   1  &-1  &0  &2  \\ 
%   0  &0  &-1  &3  \\ 
% \end{amat}
% \grstep{-\rho_{1}+\rho_{2}}
% \begin{amat}{3}
%   1  &1  &1  &0  \\ 
%   0  &-2  &-1  &2  \\ 
%   0  &0  &-1  &3  \\ 
% \end{amat}







\begin{frame}{Case 2: some linears may be to a power greater than one}
If the denominator includes linears to a power greater than one,
$d(x)=\cdots (a_ix+b_i)^{q_i}\cdots$ then there is a term
for each rising power.
\begin{equation*}
  \frac{n(x)}{d(x)}=\cdots+\frac{A_1}{a_ix+b_i}
                     +\frac{A_2}{(a_ix+b_i)^{2}}
                      +\cdots+\frac{A_{n_i}}{(a_ix+b_i)^{q_i}}+\cdots
\end{equation*}

\pause
Set up each expansion but don't solve.
\begin{itemize}
\item $\displaystyle\frac{2x+1}{(x+2)^3(x-1)}$
\pause
\begin{equation*}
  \frac{2x+1}{(x+2)^3(x-1)}=\frac{A_1}{x+2}+\frac{A_2}{(x+2)^2}+\frac{A_3}{(x+2)^3}+\frac{A_4}{x-1}
\end{equation*}

\item $\displaystyle\frac{4}{(x-1)^2x^2}$
\pause
\begin{equation*}
  \frac{4}{(x-1)^2x^2}=\frac{A_1}{x-1}+\frac{A_2}{(x-1)^2}+\frac{A_3}{x}+\frac{A_4}{x^2}
\end{equation*}
\end{itemize}
\end{frame}

\begin{frame}
\Ex Give the Partial Fractions expansion.
\begin{equation*}
  \frac{x^2-1}{(x-3)^2(x+1)}
\end{equation*}
\pause
\begin{equation*}
  =\frac{A_1}{x-3}+\frac{A_2}{(x-3)^2}+\frac{A_3}{x+1}
\end{equation*}
Get the common denominator.
\begin{equation*}
  =\frac{A_1}{x-3}\cdot\frac{(x-3)(x+1)}{(x-3)(x+1)}
   +\frac{A_2}{(x-3)^2}\cdot\frac{x+1}{x+1}
   +\frac{A_3}{x+1}\cdot\frac{(x-3)^2}{(x-3)^2}
\end{equation*}
The numerators must be equal.
\begin{equation*}
  x^2-1=A_1\cdot (x^2-2x-3)+A_2\cdot (x-1)+A_3\cdot (x^2-6x+9)
\end{equation*}
We get a linear system.
\begin{equation*}
  \begin{linsys}{3}
      A_1 &   &     &+  &A_3  &=  &1  \\
    -2A_1 &+  &A_2  &-  &6A_3 &=  &0  \\
    -3A_1 &-  &A_2  &+  &9A_3 &=  &-1  
  \end{linsys}
\end{equation*}
\end{frame}


\begin{frame}
\begin{equation*}
  \grstep[3\rho_1+\rho_3]{2\rho_1+\rho_2}
  \begin{linsys}{3}
      A_1 &   &      &+  &A_3  &=  &1  \\
          &   &A_2   &-  &4A_3 &=  &2  \\
          &   &-A_2  &+  &12A_3 &=  &2  
  \end{linsys}
  \grstep{\rho_2+\rho_3}
  \begin{linsys}{3}
      A_1 &   &      &+  &A_3  &=  &1  \\
          &   &A_2   &-  &4A_3 &=  &2  \\
          &   &      &   &8A_3 &=  &4  
  \end{linsys}
\end{equation*}
Back substitution gives $A_3=1/2$,
$A_2-4(1/2)=2$ and so $A_2=4$,
and $A_1+(1/2)=1$ and so $A_1=1/2$.
\begin{equation*}
  \frac{x^2-1}{(x-3)^2(x+1)}
  =  
  \frac{1/2}{x-3}+\frac{4}{(x-3)^2}+\frac{1/2}{x+1}
\end{equation*}
\end{frame}
% sage: P = matrix(QQ, [[1, 0, 1], [-2,1,-6], [-3,-1,9]])
% sage: w = vector(QQ, [1 ,0 ,-1])
% sage: P_prime=P.augment(w, subdivide=True)
% sage: gauss_method(P_prime, latex=True)
% \begin{amat}{3}
%   1  &0  &1  &1  \\ 
%   -2  &1  &-6  &0  \\ 
%   -3  &-1  &9  &-1  \\ 
% \end{amat}
% \grstep[3\rho_{1}+\rho_{3}]{2\rho_{1}+\rho_{2}}
% \begin{amat}{3}
%   1  &0  &1  &1  \\ 
%   0  &1  &-4  &2  \\ 
%   0  &-1  &12  &2  \\ 
% \end{amat}
% \grstep{\rho_{2}+\rho_{3}}
% \begin{amat}{3}
%   1  &0  &1  &1  \\ 
%   0  &1  &-4  &2  \\ 
%   0  &0  &8  &4  \\ 
% \end{amat}



\begin{frame}{Case 3: the denominator has irredicible quadratics}
If these are to the power one
$d(x)=\cdots (c_ix^2+d_ix+e_i)\cdots$
then in the expansion's associated term the numerator is a linear,
\begin{equation*}
  f(x)=\cdots+\frac{B_ix+C_i}{(c_ix^2+d_ix+e_i)}+\cdots
\end{equation*}
where $B_i$, $C_i$ \ldots{} are real constants.

\pause
Set up but do not solve the Partial Fractions expansion of each.
\begin{itemize}
\item $\displaystyle \frac{2x}{(x^2+1)(x-1)}$
\pause
\begin{equation*}
  %\frac{2x}{(x^2+1)(x-1)}
  =\frac{B_1x+C_1}{x^2+1}+\frac{A_1}{x-1}
\end{equation*}

\item $\displaystyle \frac{1}{(x^2+x+2)(3x^2+4)(x+2)}$
\pause
\begin{equation*}
   =\frac{B_1x+C_1}{x^2+x+2}
    +\frac{B_2x+C_2}{3x^2+4}
    +\frac{A_1}{x+2}
\end{equation*}
\end{itemize}
\end{frame}

\begin{frame}
If the quadratics are to a power higher than one
$d(x)=\cdots (c_ix^2+d_ix+e_i)^{q_i}\cdots$ then there is a term
for each rising power
\begin{multline*}
  f(x)=\cdots+\frac{B_1x+C_1}{c_ix^2+d_ix+e_i}
      +\frac{B_2x+C_2}{(c_ix^2+d_ix+e_i)^{2}}+ \\
       \cdots+\frac{B_{n_i}x+C_{n_i}}{(c_ix^2+d_ix+e_i)^{q_i}}+\cdots
\end{multline*}
where $B_j$, $C_j$, \ldots{} are real constants.

\pause
Set up but do not solve the Partial Fractions expansion.
\begin{itemize}
\item $\displaystyle\frac{x^2+2x}{(x^2+2)^2(x-1)}$
\pause
\begin{equation*}
  \frac{x^2+2x}{(x^2+2)^2(x-1)}=\frac{B_1x+C_1}{x^2+2}+\frac{B_2x+C_2}{(x^2+2)^2}+\frac{A_1}{x-1}
\end{equation*}
\end{itemize}
\end{frame}



\section{Using Partial Fractions to antidifferentiate}

\begin{frame}
\Ex Antidifferentiate using partial fractions.
\begin{equation*}
  \frac{4x+1}{(x-2)(x+2)}
\end{equation*}
\pause
% We expand and get a common denominator.
% \begin{align*}
%   \frac{4x+1}{(x-2)(x+2)}
%   &=\frac{A_1}{x-2}+\frac{A_2}{x+2}    \\
%   &=\frac{A_1}{x-2}\cdot\frac{x+2}{x+2}+\frac{A_2}{x+2}\cdot\frac{x-2}{x-2}   \\
%   &=\frac{A_1(x+2)+A_2(x-2)}{(x-2)(x+2)}    
% \end{align*}
% That leads to a linear system.
% \begin{equation*}
%   \begin{linsys}{2}
%     A_1   &+ &A_2  &= &4 \\
%     A_1   &- &2A_2 &= &1 
%   \end{linsys}
%   \grstep{-2\rho_1+\rho_2}
%   \begin{linsys}{2}
%     A_1   &+ &A_2   &= &4 \\
%           &  &-4A_2 &= &-7 
%   \end{linsys}
% \end{equation*}
% Back substitution gives $A_2=7/4$ and $A_1=9/4$.
% \begin{equation*}
%   \frac{4x+1}{(x-2)(x+2)}
%   =\frac{9/4}{x-2}+\frac{7/4}{x+2}   
% \end{equation*}
% \end{frame}
% \begin{frame}
We can antidifferente the terms with $u=x-2$ and $u=x+2$.
\begin{align*}
  \integral{}{}{ \frac{4x+1}{(x-2)(x+2)} }{x}
  &=\integral{}{}{ \frac{9/4}{x-2} }{x}
   +\integral{}{}{ \frac{7/4}{x+2} }{x}  \\
  &=\frac{9}{4}\ln|x-2|
   +\frac{7}{4}\ln|x+2|
   +C   
\end{align*}

% \pause\medskip
% \textit{Comment}\quad
% It is instructive to differentiate back again.
% \begin{equation*}
%   \frac{9}{4}\cdot\frac{1}{x-2}+\frac{7}{4}\cdot\frac{1}{x+2}
% \end{equation*}
% Get a common denominator and combine.
% \begin{align*}
%   \frac{9}{4}\cdot\frac{1}{x-2}\cdot\frac{x+2}{x+2}
%     +\frac{7}{4}\cdot\frac{1}{x+2}\cdot\frac{x-2}{x-2}
%   &=\frac{9(x+2)+7(x-2)}{4(x+2)(x-2)}    \\
%   &=\frac{16x+4}{4(x-2)(x+2)}  \\
%   &=\frac{4x+1}{(x-2)(x+2)}
% \end{align*}
% So partial fractions really does take you through the reverse
% of combining the fractions.
\end{frame}



\begin{frame}
\Ex Antidifferentiate using partial fractions.
\begin{equation*}
  \integral{}{}{ \frac{4x^2-4x+6}{x(x-3)(x+2)} }{x}
\end{equation*}
\pause
% Decompose and get a common denominator.
% \begin{align*}
%   \frac{4x^2-4x+6}{x(x-3)(x+2)}
%   &=\frac{A_1}{x}+\frac{A_2}{x-3}+\frac{A_3}{x+2}  \\
%   &=\frac{A_1}{x}\cdot\frac{(x-3)(x+2)}{(x-3)(x+2)}
%     +\frac{A_2}{x-3}\cdot\frac{x(x+2)}{x(x+2)}
%     +\frac{A_3}{x+2}\cdot\frac{x(x-3)}{x(x-3)}   \\
%   &=\frac{A_1(x^2-x-6)+A_2(x^2+2x)+A_3(x^2-3x)}{x(x-3)(x+2)}
% \end{align*}
% We get a linear system. 
% \begin{align*}
%   \begin{linsys}{3}
%     A_1   &+ &A_2  &+ &A_3  &= &4 \\
%     -A_1  &+ &2A_2 &- &3A_3 &= &-4 \\
%     -6A_1 &  &     &  &     &= &6 
%   \end{linsys}
%   &\grstep[6\rho_1+\rho_3]{\rho_1+\rho_2}
%   \begin{linsys}{3}
%     A_1   &+ &A_2  &+ &A_3  &= &4 \\
%           &  &3A_2 &- &2A_3 &= &0 \\
%           &  &6A_2 &+ &6A_3 &= &30 
%   \end{linsys}                             \\
%   &\grstep{-2\rho_2+\rho_3}
%   \begin{linsys}{3}
%     A_1   &+ &A_2  &+ &A_3  &= &4 \\
%           &  &3A_2 &- &2A_3 &= &0 \\
%           &  &     &  &10A_3  &= &30 
%   \end{linsys}
% \end{align*}
% Back substitution gives $A_3=3$, and $A_2=2$, and $A_1=-1$.
% \end{frame}
% \begin{frame}
% We are ready to antidifferentiate.
\begin{equation*}
  \integral{}{}{ \frac{4x^2-4x+6}{x(x-3)(x+2)} }{x}
  =\integral{}{}{ \frac{-1}{x}+\frac{2}{x-3}+\frac{3}{x+2} }{x}  
\end{equation*}
For the second use $u=x-3$ and for the third use $u=x+2$.
\begin{equation*}
  =-\ln|x|+2\ln|x-3|+3\ln|x+2|+C
\end{equation*}
\end{frame}





\begin{frame}
\Ex Antidifferentiate.
\begin{equation*}
  \integral{}{}{ \frac{7x^2-13x+13}{(x-2)(x^2-2x+3)} }{x}
\end{equation*}
\pause
% Decompose using Partial Fractions.
% \begin{align*}
%   \frac{7x^2-13x+13}{(x-2)(x^2-2x+3)}
%   &=\frac{A_1}{x-2}+\frac{B_1x+C_1}{x^2-2x+3}  \\
%   &=\frac{A_1}{x-2}\cdot\frac{x^2-2x+3}{x^2-2x+3}
%      +\frac{B_1x+C_1}{x^2-2x+3}\cdot\frac{x-2}{x-2}  \\
%   &=\frac{A_1(x^2-2x+3)+(B_1x+C_1)(x-2)}{(x-2)(x^2-2x+3)}   
% \end{align*}
% Here is the linear system.
% \begin{align*}
%   \begin{linsys}{3}
%     A_1    &+ &B_1  &  &     &= &7 \\
%     -2A_1  &- &2B_1 &+ &C_1  &= &-13 \\
%      3A_1  &  &     &- &2C_1 &= &13 
%   \end{linsys}
%   &\grstep[-3\rho_1+\rho_3]{2\rho_1+\rho_2}
%   \begin{linsys}{3}
%     A_1    &+ &B_1  &  &     &= &7 \\
%            &  &     &  &C_1  &= &1  \\
%            &  &-3B_1&- &2C_1 &= &-8 
%   \end{linsys}                             \\
%   &\grstep{\rho_2\leftrightarrow\rho_3}
%   \begin{linsys}{3}
%     A_1    &+ &B_1  &  &     &= &7 \\
%            &  &-3B_1&- &2C_1 &= &-8 \\
%            &  &     &  &C_1  &= &1  
%   \end{linsys}
% \end{align*}
% Back substitution gives $C_1=1$, and $B_1=2$, and $A_1=5$.
% \end{frame}
% \begin{frame}
% Now we antidifferentiate.
\begin{equation*}
  \integral{}{}{ \frac{7x^2-13x+13}{(x-2)(x^2-2x+3)} }{x}
  =\integral{}{}{ \frac{5}{x-2} }{x}
   +\integral{}{}{ \frac{2x+1}{x^2-2x+3} }{x}  
\end{equation*}
The first is easy with $u=x-2$.
\begin{equation*}
  \integral{}{}{ \frac{5}{x-2} }{x}
  =5\cdot \ln|x-2|+C
\end{equation*}
For the second we take $u=x^2-2x+3$ so that $du=2x-2\;dx$.
Consequently we rewrite to get this.
\begin{equation*}
  \integral{}{}{ \frac{2x+1}{x^2-2x+3} }{x}
  =
  \integral{}{}{ \frac{2x-2}{x^2-2x+3} }{x}
  +\integral{}{}{ \frac{3}{x^2-2x+3} }{x}
\end{equation*}
Our $u=x^2-2x+3$ makes the first $\ln|x^2-2x+3|$.
For the second, complete the square in the denominator to get
$x^2-2x+3=(x^2-2x+1)+2=(x-1)^2+2$.
Trig Substitution with $x-1=\sqrt{2}\tan\theta$ gives the answer.
\begin{multline*}
  \integral{}{}{ \frac{3}{x^2-2x+3} }{x}
  =\integral{}{}{ \frac{3}{(x-1)^2+2} }{x}
  =\integral{}{}{ \frac{3}{(\sqrt{2}\tan\theta)^2+2}\cdot\sqrt{2}\sec^2\theta }{\theta} \\
  =\integral{}{}{ \frac{3}{2(\tan^2\theta+1)}\cdot\sqrt{2}\sec^2\theta }{\theta} 
  =\frac{3}{\sqrt{2}} \theta+C
  =\frac{3}{\sqrt{2}}\tan^{-1}\biggl(\frac{x-1}{\sqrt{2}}\biggr)+C
\end{multline*}
\end{frame}




% \begin{frame}
% \frametitle{}
% 
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
