\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 11.5\hspace{1em} Absolute and conditional convergence}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Absolute and conditional convergence}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage stub_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
% \input asy/arc_length_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\begin{frame}
To keep our eye on the goal, 
here again is our list of where we are headed.
\begin{center}
\begin{tabular}{c|l}
   \multicolumn{1}{c}{\tabulartext{Function}}
     &\multicolumn{1}{c}{\tabulartext{Taylor series}}  \\ \hline
   $\displaystyle \frac{1}{1-x}$ &$1+x+x^2+x^3+\cdots$ \rule{0pt}{14pt} \\[1ex]
   $\displaystyle e^x$           &$1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\cdots$  \\[1ex]
   $\displaystyle \cos x$        &$1-\frac{x^2}{2!}+\frac{x^4}{4!}-\frac{x^6}{6!}+\cdots$  \\[1ex]
   $\displaystyle \sin x$        &$x-\frac{x^3}{3!}+\frac{x^5}{5!}-\frac{x^7}{7!}+\cdots$  \\[1ex]
   $\displaystyle \ln(1+x)$      &$x-\frac{x^2}{2}+\frac{x^3}{3}-\frac{x^4}{4}+\cdots$  \\
\end{tabular}
\end{center}
We will typically find ourselves knowing what a series adds to, if only
that series converges.
So usually the question will be of convergence.

Some of these series, such as $e^x\!$, sum to a finite total
while having entirely positive terms.
Others mix pluses and minuses.
We now consider the mixes, specifically the ones where pluses alternate
with minuses.  

\Df 
In an \alert{alternating series} the signs differ on alternate terms.
\end{frame}

\begin{frame}
\Ex
We know that the harmonic series does not converge.
\begin{equation*}
  H=\sum_{0<i}\frac{1}{i}=1+\frac{1}{2}+\frac{1}{3}+\frac{1}{4}+\cdots = \infty
\end{equation*}
Here is the alternating harmonic series.
\begin{equation*}
  A=\sum_{0<i}(-1)^{i+1}\cdot\frac{1}{i}=1-\frac{1}{2}+\frac{1}{3}-\frac{1}{4}+\cdots 
\end{equation*}
This compares the partial sums of the two.
\begin{center}
  \begin{tabular}{r|*{10}{c}}
    $i$   &$1$ &$2$   &$3$    &$4$    &$5$    &$6$    &$7$    &$8$    &$9$    &$10$ \\ \cline{2-11}
    $H_i$ &$1$ &$1.5$ &$1.83$ &$2.08$ &$2.28$ &$2.45$ &$2.59$ &$2.72$ &$2.83$ &$2.93$ \\ 
    $A_i$ &$1$ &$0.5$ &$0.83$ &$0.58$ &$0.78$ &$0.62$ &$0.76$ &$0.63$ &$0.75$ &$0.65$  \\ 
  \end{tabular}
\end{center}
The numerical evidence is not a proof (the next slide suggests a proof).
But it appears that the alternating series converges.

So to understand when an alternating series converges 
we must understand when convergence is caused by 
it is from the terms going to zero quickly
and when it is caused by cancelling.   
\end{frame}
% sage: def f(n):
% ....:     tot = 0
% ....:     for i in range(1,n+1):
% ....:         tot = tot+(1/i)
% ....:     return tot
% ....: 
% sage: f(2)
% 3/2
% sage: f(5)
% 137/60
% sage: def alt_f(n):
% ....:     tot = 0
% ....:     for i in range(1,n+1):
% ....:         tot = tot+(-1)^(i+1)*(1/i)
% ....:     return tot
% ....: 
% sage: for i in range(1,11):
% ....:     print(i," f(i)=",f(i)," approx ",round(f(i),ndigits=2)," alt_f(i)=",al
% ....: t_f(i), " approx", round(alt_f(i), ndigits=2))
% ....: 
% 1  f(i)= 1  approx  1.0  alt_f(i)= 1  approx 1.0
% 2  f(i)= 3/2  approx  1.5  alt_f(i)= 1/2  approx 0.5
% 3  f(i)= 11/6  approx  1.83  alt_f(i)= 5/6  approx 0.83
% 4  f(i)= 25/12  approx  2.08  alt_f(i)= 7/12  approx 0.58
% 5  f(i)= 137/60  approx  2.28  alt_f(i)= 47/60  approx 0.78
% 6  f(i)= 49/20  approx  2.45  alt_f(i)= 37/60  approx 0.62
% 7  f(i)= 363/140  approx  2.59  alt_f(i)= 319/420  approx 0.76
% 8  f(i)= 761/280  approx  2.72  alt_f(i)= 533/840  approx 0.63
% 9  f(i)= 7129/2520  approx  2.83  alt_f(i)= 1879/2520  approx 0.75
% 10  f(i)= 7381/2520  approx  2.93  alt_f(i)= 1627/2520  approx 0.65



% https://math.stackexchange.com/a/3249729
\begin{frame}{Aside: the alternating harmonic series converges to $\ln 2$}\vspace*{-4ex}
\begin{center}
  \vcenteredhbox{\includegraphics[height=0.9\textheight]{pix/rtvva.png}}    
\end{center}
\end{frame}


\begin{frame}
\Tm (Alternating Series Test)
If in an alternating series 
$a_0-a_1+a_2-a_3+\cdots$ the $a_i$'s
monotonically decrease to zero
\begin{equation*}
  |a_n|\geq |a_{n+1}|
  \quad\text{and}\quad
  \lim_{n\to\infty}|a_n|=0
  \tag{$*$}
\end{equation*}
then the series converges.
\end{frame}

\begin{frame}
\Pf
Assume that the series satisfies ($*$) and 
write it as here, where $a_i\geq 0$.
\begin{equation*}
  \sum_{0\leq i} (-1)^ia_i
   =a_0-a_1+a_2-a_3+\cdots
\end{equation*}
Consider the partial sums with odd subscripts.
\begin{equation*}
  A_1=a_0-a_1
  \quad A_3=a_0-a_1+a_2-a_3=A_1+(a_2-a_3)
  \quad \ldots 
  \quad A_{2k+1}=A_{2k-1}+(a_{2k}-a_{2k+1}) 
\end{equation*}
The $a_i$ monotonically decrease so each term $a_{2k}-a_{2k+1}$ is positive,
and thus $0\leq A_1\leq A_3\leq A_5\leq \cdots$\sentencespace
Further, parenthesizing $A_{2k+1}$ in this way
\begin{align*}
  A_{2k+1} 
  &= a_0-a_1+a_2-a_3+a_4+\cdots -a_{2k-1}+a_{2k}-a_{2k+1}  \\
  &= a_0-(a_1-a_2)-(a_3-a_4)+\cdots -(a_{2k-1}-a_{2k})-a_{2k+1}  
\end{align*}
shows that the odd parital sums are bounded, $A_{2k+1}\leq a_0$.\
As the sequence $\set{A_{2k+1}}$ is increasing and bounded,
it has a limit.
Call that limit~$A$. 
% \end{frame}

% \begin{frame}
To finish the proof we find the limit of the partial sums with even subscripts,
$A_{2k}$.
For all $k$ except~$0$ we have $A_{2k}=A_{2k-1}+a_{2k}$ and so this holds. 
\begin{equation*}
  \lim_{k\to\infty} A_{2k}
  =
  \lim_{k\to\infty} A_{2k-1}+a_{2k}
  =
  \lim_{k\to\infty} A_{2k-1}+\lim_{k\to\infty}a_{2k}
  =
  \lim_{k\to\infty} A_{2k-1}+0
  =A
\end{equation*}
Since the two $\set{A_{2k+1}}$ and $\set{A_{2k}}$ have the same limit,
that must be the limit of the partial sums.
By definition, that is the sum of the series.

\pause\medskip
\Rm The proof shows in addition that $0\leq A\leq a_0$, and that
$A_{2n+1}\leq A\leq A_{2n}$ for all~$n$.
\end{frame}

\begin{frame}
\Ex By the Alternating Series Test, the alternating harmonic series
\begin{equation*}
  1-\frac{1}{2}+\frac{1}{3}-\frac{1}{4}+\cdots
  =\sum_{0\leq n} \frac{(-1)^n}{n+1}
\end{equation*}
converges.
Both of the conditions in that test are clearly satisfied.
\begin{equation*}
  \frac{1}{n+1}\geq\frac{1}{n+2}
  \qquad
  \frac{1}{n+1}\to 0
\end{equation*}
\end{frame}


\begin{frame}
\Ex Apply the test to show that this alternating series converges.
\begin{equation*}
  \sum_{0<n} (-1)^{n-1}\frac{2n}{5n^2-1}
\end{equation*}

\pause
For this series,
$|a_n|=2n/(5n^2-1)$. 
We need to show that they decrease monotonically, $|a_n|\geq|a_{n+1}|$
and that the limit is zero.

For the first condition, 
we show that $f(x)=2x/(5x^2-1)$ is a decreasing function 
for all real numbers $x\geq 1$.
\begin{equation*}
  f'(x)=\frac{(5x^2-1)(2)-(2x)(10x)}{(5x^2-1)^2}
       =\frac{(10x^2-2)-(20x^2)}{(5x^2-1)^2}
       =\frac{-(10x^2+2)}{(5x^2-1)^2}
\end{equation*}
The denominator is positive (it is only zero at $x=\sqrt{1/5}$), 
and the numerator is negative, so the derivative is negative.
Because the real function has a limit, the natural numbe function
must also have the same limit. 

For the second condition, 
L'H\^{o}pital's Rule applied to $f$ gives that the limit is 
zero.
\end{frame}


\begin{frame}{Practice}
Decide whether each converges or diverges.
\begin{enumerate}
\item $\displaystyle \sum_{0\leq n}(-1)^n\frac{1}{(n+1)^2}$

\pause\smallskip
Clearly
\begin{equation*}
  \frac{1}{(n+1)^2}\geq\frac{1}{(n+2)^2}
  \qquad
  \frac{1}{(n+1)^2}\to 0
\end{equation*}
and so by the Alternating Series Test the series converges.

\pause\item $\displaystyle \sum_{0\leq n}(-1)^n\frac{3n}{5n+(1/2)}$

\pause\smallskip
The limit of the $a_n$ is not zero
\begin{equation*}
  \frac{3n}{5n+(1/2)}\to\frac{3}{5}
\end{equation*}
(because for real numbers, $f(x)=3x/(5x+(1/2))$ goes to $3/5$
by L'H\^{o}pital's Rule).
So by the Alternating Series Test it diverges.
\end{enumerate}
\end{frame}


\begin{frame}
\begin{enumerate}\setcounter{enumi}{2}
\item $\displaystyle 2-\frac{3}{2}+\frac{4}{3}-\frac{5}{4}+\cdots$

\pause\smallskip
This is the series 
\begin{equation*}
  \sum_{1\leq i} \frac{i+1}{i}
\end{equation*}
and the terms do not go to zero in the limit, they go to one.
So by the Divergence Test it diverges.

% \pause
% \item $\displaystyle \sum_{0\leq i}\frac{(-1)^nn}{\sqrt{n^2+1}}$

% \pause\smallskip
% The square root of $n^2+1$ is approximately~$n$, so in absolute value the
% terms go to one.
% That is, consider $f(x)=x/\sqrt{x^2+1}$, take the limit as $x\to\infty$, 
% and apply L'H\^{o}pital's Rule.
% \begin{multline*}
%   \lim_{x\to\infty}\frac{x}{\sqrt{x^2+1}}
%   =
%   \lim_{x\to\infty}\frac{1}{(1/2)(x^2+1)^{-1/2}\cdot 2x}
%   =
%   \lim_{x\to\infty}\frac{\sqrt{x^2+1}}{x}           \\
%   =
%   \lim_{x\to\infty}\frac{\sqrt{x^2+1}}{\sqrt{x^2}}
%   =
%   \lim_{x\to\infty}\sqrt{\frac{x^2+1}{x^2}}
%   % =
%   % \lim_{x\to\infty}\sqrt{1+\frac{1}{x^2}}
%   =1
% \end{multline*}
% By the Alternating Series Test the series diverges.
\end{enumerate}
\end{frame}



\section{Absolute and conditional convergence}
\begin{frame}
\Df
A series $\sum_i a_i$ is \alert{absolutely convergent} if the series
of absolute values of the terms $\sum_i|a_n|$ converges.
A series is \alert{conditionally convergent} if it is convergent but
not absolutely convergent.

\Lm If a series is absolutely convergent then it is convergent.

\Pf
Suppose that the series $\sum_i a_i$ is absolutely convergent.
Because $\sum_i|a_i|$ converges, so does $\sum_i 2|a_i|$.
Each term in the series~$a_i$ 
is either positive or negative so $0\leq a_i+|a_i|\leq 2|a_i|$.
The Comparison Test then gives that $\sum_i (a_i+|a_i|)$ converges.
It then follows that $\sum_i a_i$ converges, since it is the 
difference $\sum_i a_i=\sum_i (a_i+|a_i|)-\sum_i|a_i|$
of two convergent series.  
\end{frame}


\begin{frame}
\Ex This series
\begin{equation*}
  \sum_{0<n} \frac{\cos n}{n^2}=\frac{\cos 1}{1}+\frac{\cos 2}{2^2}+\frac{\cos 3}{3^2}+\cdots
\end{equation*}
has a mix of positive and negative terms, although it is not
alternating.
\begin{center} \addtolength{\tabcolsep}{-0.2em}
  \begin{tabular}{r|*{9}{c}}
    $n$      &$1$    &$2$     &$3$     &$4$     &$5$    &$6$    &$7$    &$8$     &$9$ \\ \cline{2-10}
    $\cos n$ &$0.54$ &$-0.42$ &$-0.99$ &$-0.65$ &$0.28$ &$0.96$ &$0.75$ &$-0.15$ &$-0.91$ \\
  \end{tabular}
\end{center}
We have $|\cos n|\leq 1$ and so the Comparison Test with
\begin{equation*}
  \frac{|\cos n|}{n^2}\leq\frac{1}{n^2}
\end{equation*}
gives convergence, since we know $\sum_i 1/n^2$ converges 
(by the Integral Test).  
Therefore, because it is absolutely convergent, $\sum_n \cos(n)/n^2$ converges.
\end{frame}
% sage: for i in range(10):
% ....:     print("i=",i," cos(i)=",round(cos(i), ndigits=2))
% ....: 
% i= 0  cos(i)= 1.0
% i= 1  cos(i)= 0.54
% i= 2  cos(i)= -0.42
% i= 3  cos(i)= -0.99
% i= 4  cos(i)= -0.65
% i= 5  cos(i)= 0.28
% i= 6  cos(i)= 0.96
% i= 7  cos(i)= 0.75
% i= 8  cos(i)= -0.15
% i= 9  cos(i)= -0.91




\begin{frame}
\Ex
The alternating harmonic series
\begin{equation*}
  \sum_{1\leq i}(-1)^{i-1}\frac{1}{i}=1-\frac{1}{2}+\frac{1}{3}-\frac{1}{4}+\cdots
\end{equation*}
converges by the Alternating Series Test.
It is not absolutely convergent because the harmonic series
\begin{equation*}
  \sum_{1\leq i}\frac{1}{i}=1+\frac{1}{2}+\frac{1}{3}+\frac{1}{4}+\cdots
\end{equation*}
diverges.

\pause
\Ex Show that $\sum_{1\leq j}(-1)^{n+1}/\sqrt{j}$ is conditionally convergent.

\pause
For convergence apply the Alternating Series Test, since
these are clear.
\begin{equation*}
  \frac{1}{\sqrt{j}} \geq \frac{1}{\sqrt{j+1}}
  \qquad
  \frac{1}{\sqrt{j}}\to 0
\end{equation*}
To see that it is not absolutely convergent, rewrite
\begin{equation*}
  \sum_{1\leq j}\biggl|\frac{(-1)^{n+1}}{\sqrt{j}}\biggr|
  =\sum_{1\leq j}\frac{1}{j^{1/2}}
\end{equation*}
and recognize it as a $p$~series that is divergent.
\end{frame}

% \begin{frame}
% \Ex Decide if this series converges absolutely: $\sum_{2\leq n}(-1)^n/(n\ln n)$.

% \pause
% The series $\sum_{2\leq n}1/(n \ln n)$ is open to attack by the Integral Test.
% \begin{equation*}
%   \integral{x=2}{\infty}{ \frac{1}{x\ln x} }{x}
%   =\integral{u=\ln 2}{\infty}{ \frac{1}{u} }{u}
%   =\lim_{R\to\infty}\integral{u=\ln 2}{R}{ \frac{1}{u} }{u}
%   =\lim_{R\to\infty}\bigl( \ln R-\ln(\ln(2))\bigr)=\infty
% \end{equation*}
% (There the substitution is $u=\ln(x)$ so $du=(1/x)\,dx$.)
% Thus the sum of the absolute values diverges, and the 
% starting series is not absolutely convergent.
% \end{frame}



\begin{frame}{Practice}
Decide if each series is absoutely convergent, conditionally convergent,
or divergent.
\begin{enumerate}
\item  
$\displaystyle \sum_{1\leq n}(-1)^{n+1}/n^2$ 

\pause\smallskip
It is absolutely convergent since $\sum_{1\leq n}1/n^2$ is a convergent $p$~series.

\pause\item
$\displaystyle \sum_{1\leq n}(-1)^{n+1}/(3n+2)$

\pause\smallskip
It is conditionally convergent.
By the Alternating Series Test this series converges.
But the series of absolute values
$\sum_{1\leq n}1/(3n+2)$
is a $1/n$-ish series, and using the Limit Comparison Test
\begin{equation*}
  \lim_{n\to\infty} \frac{1/(3n+2)}{(1/n)}
  =\lim_{n\to\infty} \frac{n}{3n+2}
  =\frac{1}{3}
\end{equation*}
along with the fact that the harmonic series diverges shows
that this series diverges.
\end{enumerate}
  
\end{frame}






\begin{frame}{Comment: rearranging a series}
For finite sums we can rearrange any way that we like.
For instance, 
$1+2+3+4=4+1+2+3$.
But that is not true for all infinite sums.

We have seen that the alternating harmonic series adds to $\ln 2$.
\begin{equation*}
  A=1-\frac{1}{2}+\frac{1}{3}-\frac{1}{4}+\cdots=\ln 2
\end{equation*}
% Of course we mean that the sequence of partial sums
% $\set{A_n}$ has a limit of $\ln 2$.
% \begin{equation*}
%   A_1=1
%   \quad A_2=1-\frac{1}{2}
%   \quad A_3=1-\frac{1}{2}+\frac{1}{3}
%   \quad \ldots
% \end{equation*}
Consider this rearrangement, with all the same terms and all the same signs.
\begin{equation*}
  =(1-\frac{1}{2})
  -\frac{1}{4}
  +(\frac{1}{3}-\frac{1}{6})
  -\frac{1}{8}
  +(\frac{1}{5}-\frac{1}{10})
  -\cdots             
  =\frac{1}{2}-\frac{1}{4}+\frac{1}{6}-\frac{1}{8}+\cdots
\end{equation*}
But that's half of~$A$.  
We seem to have that $\ln(2)=(1/2)\ln 2$.
  
In fact, Reimann showed that if a series is conditionally convergent then 
given a real number R, we can find a rearrangement of the series 
that adds to~$R$
(or, we can make it diverge).

So while absolutely convergent series are robust,
conditionally convergent series are fragile.
\end{frame}


\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
