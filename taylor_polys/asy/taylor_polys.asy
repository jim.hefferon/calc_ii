// taylor_polys.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "taylor_polys%03d";

import graph;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// =========== Fcn graph moving away from t line ===============
picture pic;
int picnum = 0;
size(pic,0,4cm,keepAspect=true);
path f = shift(0,1)*general_fcn;

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=2;

draw(pic, f, FCNPEN);


dotfactor = 4;
// t-line
real x = 1.15;
real x_t = times(f, x)[0];
pair slope = dir(f, x_t, normalize=true);
pair t_line(real t) {
  return (point(f, x_t).x+t*slope.x, point(f, x_t).y+t*slope.y);
}
draw(pic, graph(pic, t_line, -1, 1), highlight_color);
dot(pic, point(f, x_t),  highlight_color);

real[] T0 = {x};
// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", x);

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic); // ,format="pdf"


// =========== Second degree Taylor poly for f(x)=sin(x) ===============
real f1(real x) { return sin(x); }
real T1(real x) { return sin(pi/3)+cos(pi/3)*(x-(pi/3)); }
real T2(real x) { return sin(pi/3)
                         +cos(pi/3)*(x-(pi/3))
                         +(-sin(pi/3)/2)*(x-(pi/3))^2; }

picture pic;
int picnum = 1;
size(pic,0,5cm,keepAspect=true);

real a = pi/3;
path f = graph(pic, f1, 0, a+(5*0.1)+0.2);
path t1 = graph(pic, T1, a-(5*0.1), a+(5*0.1));
path t2 = graph(pic, T2, a-(5*0.1), a+(5*0.1));

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1.2;
ybot=0; ytop=1;

draw(pic, f, FCNPEN);
label(pic, "$f(x)=\sin x$", (0.15, f1(0.15)), E, FCNCOLOR);
draw(pic, t1, blue);
label(pic, "$T_1$", (a+0.4, T1(a+0.4)), NW, blue);
draw(pic, t2, highlight_color);
label(pic, "$T_2$", (a-0.4, T2(a-0.4)), SE, highlight_color);

dotfactor = 4;
dot(pic, (a,f1(a)));

real[] T1 = {a-0.5,a,a+0.5};
// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks("%", T1, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a-0.5$", a-0.5);
labelx(pic, "$a=\pi/3$", a);
labelx(pic, "$a+0.5$", a+0.5);

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=LeftTicks(Step=1, step=0, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




