\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 11.0\hspace{1em}Taylor polynomials}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Taylor polynomials}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage stub_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
% \input asy/arc_length_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



% \section{}

\begin{frame}{Projecting}
Suppose that you know that yesterday's high temperature was $54$.
Predict today's high.

\pause
You predict $54$.

Now add the information that the day before it was $50$.
Revise your prediction of today's high.

\pause
The \alert{first-order}, or \alert{linear}, prediction is $58$.
It takes into account the change.

Now add the information that the day before the high was $44$.
Again revise your prediction.

\pause
The \alert{second-order} prediction is $56$.
It takes into account the change in the change, $\Delta^2 y$.

\pause\medskip
We will approximate functions by successively taking into 
account more and more change information. 
\end{frame}



\begin{frame}
  \frametitle{First order approximation}
Start with a function~$f$.
Recall the formula for the 
\alert{tangent line approximation}, or \alert{linearization}
at the anchor point~$a$.
\begin{equation*}
  T_1(x)=f(a)+f'(a)(x-a)
\end{equation*}
\begin{center}
  \includegraphics{taylor_polys000.pdf}
\end{center}
The function $T_1$ has these advantages as an approximation.
\begin{itemize}
\item At $x=a$ it is correct, $T_1(a)=f(a)$.
\item At $x=a$ it has the correct slope, $T_1'(a)=f'(a)$.
  This makes $T_1$ accurate for input $x$'s near to~$a$.
\end{itemize}
\end{frame}

\begin{frame}
\Ex Use the first order approximation and $a=8$
to estimate $\sqrt[3]{8.01}$,
$\sqrt[3]{8.1}$, and  $\sqrt[3]{8.5}$.   

\pause
We are approximating $f(x)=x^{1/3}$ near to $a=8$.
So $f'(x)=(1/3)\cdot x^{-2/3}$ and $f'(a)=(1/3)\cdot (1/4)=1/12$.
\begin{equation*}
  T_1(x)=f(a)+f'(a)(x-a)
       =2+\frac{1}{12}(x-8)
\end{equation*}
We get these.
\begin{center}
  \begin{tabular}{c|*{3}{l}}
    $x$
      &\multicolumn{1}{c}{$8.01$}
      &\multicolumn{1}{c}{$8.1$} 
      &\multicolumn{1}{c}{$8.5$} \\ \hline
    $\sqrt[3]{x}$  &$2.000\,83$  &$2.008\,30$  &$2.040\,83$  \\
    $T_1(x)$       &$2.000\,83$  &$2.008\,33$  &$2.041\,67$  
  \end{tabular}
\end{center}
\pause
The function $T_1$ is a pretty good approximator of~$f\!$.
But can we improve it?
Its accuracy falls off as the input~$x$ 
gets further from the anchor point~$x=a$.
\end{frame}
% sage: def f(x):
% ....:     return x^(1/3)
% ....: 
% sage: def T1(x):
% ....:     return 2+(x-8)/12
% ....: 
% sage: for i in [8.01, 8.1, 8.5]:
% ....:     round( f(i), ndigits=5), round( T1(i), ndigits=5)
% ....: 
% (2.00083, 2.00083)
% (2.0083, 2.00833)
% (2.04083, 2.04167)





\begin{frame}
  \frametitle{How to improve the first approximation}
Why do inaccuracies grow as the input~$x$ gets further from 
the anchor~$x=a$? 
\begin{center}
  \includegraphics{taylor_polys000.pdf}
\end{center}
The graph of~$f$ curves away from~$T_1\!$'s line.
This is because as $x$ moves from $a$ the slope of $f$ changes away 
from~$f'(a)$.
Restated, the source of error is:~$f$ has a nonzero second derivative 
but $T_1\!$'s second derivative is zero.
\end{frame}


\begin{frame}{Second order approximation}
Given a function $\map{f}{\R}{\R}$ and an anchor $a$, 
we want an improved approximating function $\map{T_2}{\R}{\R}$
such that:~$T_2(a)=f(a)$,
and $T_2'(a)=f'(a)$,
and $T_2''(a)=f''(a)$.  

\pause
The first order approximation is linear so its second derivative is zero
and it cannot meet the third condition.
Therefore add a second-order term.
Start with coefficients as unknowns $q_0,q_1,q_2$. 
\begin{equation*}
  T_2(x)=q_0+q_1\cdot (x-a)+q_2\cdot(x-a)^2
\end{equation*}
To determine the~$q_i$'s use the three conditions.
Here are the derivatives of $T_2$.
\begin{align*}
  T_2(x) &=q_0+q_1\cdot (x-a)+q_2\cdot(x-a)^2  \\
  T_2'(x) &=q_1+q_2\cdot 2(x-a)  \\
  T_2''(x) &=2q_2 
\end{align*}
Plugging the anchor~$x=a$ into the first equation 
and quoting the first condition gives that 
$q_0=T_2(a)=f(a)$. 
Plugging~$a$ into the secondd equation gives that 
$q_1=T_2'(a)=f'(a)$.
Likewise, the third equation gives $q_2=f''(a)/2$.

\pause
The \alert{second order Taylor polynomial} is this.
\begin{equation*}
  T_2(x)=f(a)+f'(a)\cdot (x-a)+\frac{f''(a)}{2}\cdot(x-a)^2
\end{equation*}
Observe that it extends the first order Taylor polynomial.
\end{frame}


\begin{frame}
\Ex Give the second order Taylor approximating function to 
$f(x)=\sqrt[3]{x}$ at the anchor point $a=8$.
Estimate   
estimate $\sqrt[3]{8.01}$,
$\sqrt[3]{8.1}$, and  $\sqrt[3]{8.5}$.   

We have these.
\begin{equation*}
  f'(x)=\frac{1}{3}x^{-2/3}
  \quad
  f'(8)=\frac{1}{12}
  \qquad
  f''(x)=-\frac{2}{9}x^{-5/3}
  \quad
  f''(8)=-\frac{2}{9}\cdot\frac{1}{32}=-\frac{2}{288}
\end{equation*}
This is the second order approximation.
\begin{equation*}
  T_2(x)=2+\frac{1}{12}\cdot(x-8)+\frac{-2/288}{2}\cdot(x-8)^2
\end{equation*}
We get these.
\begin{center}
  \begin{tabular}{c|*{3}{l}}
    $x$
      &\multicolumn{1}{c}{$8.01$}
      &\multicolumn{1}{c}{$8.1$} 
      &\multicolumn{1}{c}{$8.5$} \\ \hline
    $\sqrt[3]{x}$  &$2.000\,83$  &$2.008\,30$  &$2.040\,83$  \\
    $T_1(x)$       &$2.000\,83$  &$2.008\,33$  &$2.041\,67$  \\
    $T_2(x)$       &$2.000\,83$  &$2.008\,30$  &$2.040\,80$  
  \end{tabular}
\end{center}
\end{frame}
% sage: def T2(x):
% ....:     return T1(x)-(1/288)*((x-8)^2)
% ....: 
% sage: for i in [8.01, 8.1, 8.5]:
% ....:     round( f(i), ndigits=5), round( T1(i), ndigits=5), round(T2(i), ndigit
% ....: s=5)
% ....: 
% (2.00083, 2.00083, 2.00083)
% (2.0083, 2.00833, 2.0083)
% (2.04083, 2.04167, 2.0408)


\begin{frame}
\Ex  Take $f(x)=\sin x$ and $a=\pi/3$.
Then $f(a)=\sin(\pi/3)\approx 0.866$, 
and $f'(a)=\cos(\pi/3)=0.500$,
and $f''(a)=-\sin(\pi/3)\approx -0.866$.
\begin{align*}
  T_2(x) 
  &=\sin(\pi/3)+\cos(\pi/3)\cdot(x-\pi/3)+\frac{-\sin(\pi/3)}{2}\cdot(x-\pi/3)^2 \\
  &\approx 0.866+0.500\cdot(x-\pi/3)+\frac{-0.866}{2}\cdot(x-\pi/3)^2
\end{align*}
Here are the numbers.
\begin{center}
  \begin{tabular}{r|ccc}
    \multicolumn{1}{c}{$\Delta x$}
      &\multicolumn{1}{c}{$f(a+\Delta x)$}
      &\multicolumn{1}{c}{$T_1(a+\Delta x)$}
      &\multicolumn{1}{c}{$T_2(a+\Delta x)$}  \\ \hline
    $-0.5$ &$0.520$ &$0.616$ &$0.508$ \\
    $-0.4$ &$0.603$ &$0.666$ &$0.597$ \\
    $-0.3$ &$0.680$ &$0.716$ &$0.677$ \\
    $-0.2$ &$0.749$ &$0.766$ &$0.749$ \\
    $-0.1$ &$0.812$ &$0.816$ &$0.812$ \\
           &$0.866$ &$0.866$ &$0.866$ \\
    $0.1$  &$0.912$ &$0.916$ &$0.912$ \\
    $0.2$  &$0.948$ &$0.966$ &$0.949$ \\
    $0.3$  &$0.975$ &$1.016$ &$0.977$ \\
    $0.4$  &$0.992$ &$1.066$ &$0.997$ \\
    $0.5$  &$1.000$ &$1.116$ &$1.008$ \\
  \end{tabular}
\end{center}
\end{frame}
% sage: def f(x):
% ....:     return round(sin(x), ndigits=3)
% sage: a = pi/3
% sage: def T_1(x):
% ....:     return sin(pi/3)+cos(pi/3)*(x-(pi/3))
% sage: def T_2(x):
% ....:     return sin(pi/3)+cos(pi/3)*(x-(pi/3))+(-sin(pi/3)/2)*(x-pi/3)^2
% sage: for k in range(-5,6):
% ....:     print("delta x=",round(k*0.1,ndigits=3),
% ....:     " a+k*0.1=",round(a+k*0.1,ndigits=3),
% ....:     " f(a+k*0.1)=",f(a+k*0.1),
% ....:     "T_1(x)=",round(T_1(a+k*0.1),ndigits=3),
% ....:     "T_2(x)=",round(T_2(a+k*0.1),ndigits=3))
% delta x= -0.5  a+k*0.1= 0.547  f(a+k*0.1)= 0.52 T_1(x)= 0.616 T_2(x)= 0.508
% delta x= -0.4  a+k*0.1= 0.647  f(a+k*0.1)= 0.603 T_1(x)= 0.666 T_2(x)= 0.597
% delta x= -0.3  a+k*0.1= 0.747  f(a+k*0.1)= 0.68 T_1(x)= 0.716 T_2(x)= 0.677
% delta x= -0.2  a+k*0.1= 0.847  f(a+k*0.1)= 0.749 T_1(x)= 0.766 T_2(x)= 0.749
% delta x= -0.1  a+k*0.1= 0.947  f(a+k*0.1)= 0.812 T_1(x)= 0.816 T_2(x)= 0.812
% delta x= 0.0  a+k*0.1= 1.047  f(a+k*0.1)= 0.866 T_1(x)= 0.866 T_2(x)= 0.866
% delta x= 0.1  a+k*0.1= 1.147  f(a+k*0.1)= 0.912 T_1(x)= 0.916 T_2(x)= 0.912
% delta x= 0.2  a+k*0.1= 1.247  f(a+k*0.1)= 0.948 T_1(x)= 0.966 T_2(x)= 0.949
% delta x= 0.3  a+k*0.1= 1.347  f(a+k*0.1)= 0.975 T_1(x)= 1.016 T_2(x)= 0.977
% delta x= 0.4  a+k*0.1= 1.447  f(a+k*0.1)= 0.992 T_1(x)= 1.066 T_2(x)= 0.997
% delta x= 0.5  a+k*0.1= 1.547  f(a+k*0.1)= 1.0 T_1(x)= 1.116 T_2(x)= 1.008


\begin{frame}
The picture dramatizes those numbers, 
showing that near to the anchor point, $T_2$ is a very good 
approximator.
\begin{center}
  \includegraphics{taylor_polys001.pdf}    
\end{center}
\end{frame}


\begin{frame}{The third order approximation}
Given a function $\map{f}{\R}{\R}$ and an anchor $a$, 
we want an approximating function $\map{T_3}{\R}{\R}$
such that~$T_3(a)=f(a)$,
and $T_3'(a)=f'(a)$,
and $T_3''(a)=f''(a)$,  
and $T_3'''(a)=f'''(a)$.  

\pause
We bring in a third-order term:
$T_3(x)=q_0+q_1\cdot (x-a)+q_2\cdot(x-a)^2+q_3\cdot(x-a)^3$.
These are the derivatives.
\begin{align*}
  T_3(x)   &=q_0+q_1\cdot (x-a)+q_2\cdot (x-a)^2+q_3\cdot (x-a)^3  \\
  T_3'(x)  &=q_1+2q_2\cdot (x-a)+3q_3\cdot (x-a)^2  \\
  T_3''(x) &=2q_2+6q_3\cdot (x-a)  \\
  T_3'''(x)&=6q_3  
\end{align*}
Plugging $a$ into these equations and comparing with the conditions gives
the \alert{third order Taylor polynomial}.
\begin{equation*}
  T_3(x)=f(a)+f'(a)\cdot (x-a)+\frac{f''(a)}{2}\cdot(x-a)^2+\frac{f'''(a)}{6}\cdot(x-a)^3
\end{equation*}
The denominator on the cubic term is $3\cdot 2\cdot 1=3!$ because as we take
derivatives we first multiply by~$3$, then by~$2$, and then by~$1$.
Similarly, the denominator on the quadratic term is $2!=2\cdot 1$. 
\end{frame}

\begin{frame}{Definition of Taylor polynomial}
Given a function~$f$ defined on an open interval containing the 
anchor point~$a$, the \alert{$n$-th degree Taylor polynomial} centered at~$a$
is this.
\begin{equation*}
  T_n(x)=f(a)
         +f'(a)(x-a)
         +\frac{f''(a)}{2!}(x-a)^2
         +\cdots\,
         \frac{f^{(n)}(a)}{n!}(x-a)^n
\end{equation*}
We define $1!=1$ and $0!=1$ so that this formula holds.
\begin{equation*}
  T_n(x)=\sum_{0\leq i\leq n}\frac{f^{(i)}(a)}{i!}\cdot(x-a)^i
\end{equation*}
When the anchor point is $a=0$ this is also called
the \alert{$n$-th degree Maclaurin polynomial}.
\end{frame}


\begin{frame}
\Ex Find the third order Taylor polynomial for $\ln x$ about $a=1$.  

\pause
These are the derivatives.
\begin{center}
  \begin{tabular}{@{}r|*{4}{c}@{}}
    \multicolumn{1}{c}{$i$}
      &\multicolumn{1}{c}{$0$}
      &\multicolumn{1}{c}{$1$}
      &\multicolumn{1}{c}{$2$}
      &\multicolumn{1}{c}{$3$}  \\ \hline
    $f^{(i)}(x)$ &$\ln x$ 
                &$x^{-1}$ 
                &$-x^{-2}$ 
                &$2x^{-3}$  
                \rule{0pt}{9pt} \\
   $f^{(i)}(a)$  &$0$ 
                &$1$ 
                &$-1$ 
                &$2$  \\
  \end{tabular}  
\end{center}
The approximating polynomial is this.
\begin{equation*}
  T_3(x)=0+1\cdot (x-1)
          -\frac{1}{2!}\cdot (x-1)^2
          +\frac{2}{3!}\cdot (x-1)^3
\end{equation*}

\pause
\Ex Find the fourth degree Taylor polynomial approximation for $f(x)=\sqrt{x+1}$
about the anchor $a=3$.

\pause
These are the derivatives.
\begin{center}\small
  \begin{tabular}{@{}r|*{5}{c}@{}}
    \multicolumn{1}{c}{$i$}
      &\multicolumn{1}{c}{$0$}
      &\multicolumn{1}{c}{$1$}
      &\multicolumn{1}{c}{$2$}
      &\multicolumn{1}{c}{$3$}
      &\multicolumn{1}{c}{$4$}  \\ \hline
    $f^{(i)}(x)$ &$(x+1)^{1/2}$ &$\frac{1}{2}(x+1)^{-1/2}$ &$\frac{-1}{4}(x+1)^{-3/2}$ &$\frac{3}{8}(x+1)^{-5/2}$ &$\frac{-15}{16}(x+1)^{-7/2}$ \rule{0pt}{9pt} \\
   $f^{(i)}(a)$      &$2$ &$1/4$ &$-1/32$ &$3/256$ &$-15/2\,048$ \\
  \end{tabular}  
\end{center}
This is the approximating polynomial.
\begin{equation*}
  T_4(x)=2+\frac{1}{4}(x-3)
          -\frac{1}{64}(x-3)^2
          +\frac{1}{512}(x-3)^3
          -\frac{5}{16\,384}(x-3)^4
\end{equation*}
\end{frame}



\begin{frame}
\Ex Give the fifth degree Maclaurin polynomial for $f(x)=\sin x$.   
\pause
These are the derivatives.
\begin{center}
  \begin{tabular}{@{}r|*{6}{c}@{}}
    \multicolumn{1}{c}{$i$}
      &\multicolumn{1}{c}{$0$}
      &\multicolumn{1}{c}{$1$}
      &\multicolumn{1}{c}{$2$}
      &\multicolumn{1}{c}{$3$}  
      &\multicolumn{1}{c}{$4$}  
      &\multicolumn{1}{c}{$5$}  
\\ \hline
    $f^{(i)}(x)$ &$\sin x$ 
                &$\cos x$ 
                &$-\sin x$ 
                &$-\cos x$  
                &$\sin x$ 
                &$\cos x$  
                \rule{0pt}{9pt} \\
   $f^{(i)}(0)$  &$0$ 
                &$1$ 
                &$0$ 
                &$-1$  
                &$0$ 
                &$1$  \\
  \end{tabular}  
\end{center}
We get this.
\begin{align*}
  T_5(x) &=0+1\cdot x
           +\frac{0}{2!}\cdot x^2
           -\frac{1}{3!} x^3
           +\frac{0}{4!}\cdot x^4
           +\frac{1}{5!} x^5        \\
        &=x-\frac{x^3}{6}+\frac{x^5}{120}
\end{align*}
\end{frame}





\begin{frame}{Practice}
Calculate the Taylor polynomial $T_n$ for the given
function~$f$ and about the given anchor point~$a$.
\begin{enumerate}
\item $f(x)=\cos x$, $n=5$, $a=0$
\item $f(x)=\cos x$, $n=5$, $a=\pi/2$
\item $f(x)=1/(x+1)$, $n=3$, $a=0$
\item $f(x)=e^x$, $n=5$, $a=0$
% \item $f(x)=1/(x-1)$, $n=10$, $a=0$
\item $f(x)=2+3x+4x^2$, $n=3$, $a=0$
\end{enumerate}
\end{frame}



\section{Error bounds}

\begin{frame}
We want to know how far off a Taylor polynomial can be.

\Df
Let the function $f$ have derivatives of order up to~$n$ 
on an interval containing 
the anchor~$a$, and let $T_n$ be the $n$-th degree Taylor polynomial of~$f$
about~$a$.
The $n$-th \alert{remainder} is $R_n(x)=f(x)-T_n(x)$ and
the \alert{error} is its absolute value, $\varepsilon(x)=|R_n(x)|$.  

\medskip
\Tm Assume that the $n$-th order derivative $f^{(n+1)}$ exists and is continuous.
The remainder function is this.
\begin{equation*}
  R_n(x)=\frac{1}{n!}\integral{z=a}{x}{ (x-z)^n\cdot f^{(n+1)}(z) }{z}
  \tag{$*$}
\end{equation*}
In consequence, where the constant~$K$ satisfies $K\geq |f^{(n+1)}(z)|$
for all $z$ in $\closed{a}{x}$ we have this.
\begin{equation*}
  \varepsilon(x)\leq \frac{K}{(n+1)!}\cdot |x-a|^{n+1}
\end{equation*}

% \medskip
% The above result says that the error in $T_n$ falls with the $n+1$-th power. 
% When the input $x$ is close to the anchor point, 
% the powers of $x-a$ fall quite quickly.
% In addition, the factorials rise quite quickly.
% As a result, the error drops off sharply. 
% \end{frame}



% \begin{frame}
\Pf
We first prove that the remainder has the stated form and then we prove
the error has the stated bound.
For the first, consider the integral in ($*$).
\begin{equation*}
  I_n(x)=\frac{1}{n!}\integral{z=a}{x}{ (x-z)^n\cdot f^{(n+1)}(z)  }{z}
\end{equation*}
We will show that $R_n(x)=I_n(x)$ for all~$x$.

We start with $n$ equal to~$0$.
In this case $R_0(x)=f(x)-f(a)$ and
$I_o(x)=\integral{a}{x}{ f'(z) }{z}=f(x)-f(a)$,
so the two are equal.
\end{frame}

\begin{frame}
For $n$ larger than zero we will apply Integration By Parts to $I_n(x)$.
Taking
\begin{equation*}
  u(z)=\frac{1}{n!}\cdot (x-z)^n
  \qquad
  v(z)=f^{(n)}(z)
\end{equation*}
gives this.
\begin{align*}
  I_n(x)
  =\integral{z=a}{x}{ u(z)\cdot v'(z) }{z}  
  &=\biggl. u(z)v(z)\biggr|_{z=a}^{x}
      -\integral{z=a}{x}{ v(z)\cdot u'(z) }{z}   \\
  &=\biggl.\frac{1}{n!}(x-z)^nf^{(n)}(z) \biggr|_{z=a}^{x}
      -\frac{1}{n!}\integral{z=a}{x}{  f^{(n)}(z)\cdot(-n)(x-z)^{n-1} }{z} \\
  &=-\frac{1}{n!}(x-a)^nf^{(n)}(a)+I_{n-1}(x)
\end{align*}
Restated, we have a reduction formula
$I_{n-1}(x)=(f^{(n)}(a)/n!)\cdot (x-a)^n + I_n(x)$.
To find the $n$-th remainder, just apply the reduction over and over.
\begin{align*}
  f(x)
  &= f(a)+I_0(x)  \\
  &= f(a)+\frac{f'(a)}{1!}+ I_1(x)  \\
  &\vdotswithin{=}  \\
  &= f(a)+\frac{f'(a)}{1!}+ \cdots+ \frac{f^{(n)}(a)}{n!}(x-a)^  +I_n(x)  
\end{align*}
This has $f(x)=T_n(x)+I_n(x)$ and hence $I_n(x)$ is the remainder.
\end{frame}

\begin{frame}
We finish
by showing that the error has the specified bound.

Assume, as stated in the result that $K$ is a constant 
such that for all $z$ with 
$a\leq z\leq x$ we have $|f^{(n+1)}(z)|\leq K$.
(The other side, $x\leq z\leq a$, is similar.)
\begin{align*}
  \varepsilon(x)=|R_n(x)|
  &=\biggl| \frac{1}{n!}\integral{z=a}{x}{ (x-z)^n\cdot f^{(n+1)}(z) }{z} \biggr|  \\
  &\leq \frac{1}{n!}\integral{z=a}{x}{ \bigl| (x-z)^n\cdot f^{(n+1)}(z) \bigr| }{z} \\ 
  &\leq \frac{K}{n!}\integral{z=a}{x}{ (x-z)^n }{z} \\
  &=\frac{K}{n!}\cdot \left. \frac{-(x-z)^{n+1}}{n+1} \right|_{z=a}^{x}
  =\frac{K}{(n+1)!}\cdot|x-a|^{n+1} 
\end{align*}
(The first `$\leq$' is the triangle inequality for integrals.)
\end{frame}




\begin{frame}
\Ex
Earlier we did the third order Taylor polynomial for $\ln(x)$ at $a=1$.
\begin{equation*}
  T_3(x)=0+1\cdot (x-1)
          -\frac{1}{2!}\cdot (x-1)^2
          +\frac{2}{3!}\cdot (x-1)^3
\end{equation*}
Suppose that we are interested in $x=3/2$; what error can we expect?

\pause
Here is the error bound.
\begin{equation*}
  \varepsilon(x)=|T_n(x)-f(x)|\leq \frac{K}{(n+1)!}\cdot |x-a|^{n+1}
\end{equation*}
where~$K$ satisfies that $K\geq |f^{(n+1)}(z)|$ for $z$ in $\closed{a}{x}$.

We know that if $f(x)=\ln(x)$ then $f'(x)=x^{-1}$, $f''(x)=-x^{-2}$,
$f'''(x)=2x^{-3}$, and $f^{(4)}(x)=-6x^{-4}\!$.
Between $a=1$ and $x=3/2$ the maximum of the fourth derivative
is at~$a$, giving a maximum
fourth derivative of $-6$, and consequently we take $K=6$.
\begin{align*}
  \varepsilon(x)=|T_3(x)-\ln(x)| &\leq \frac{6}{4!}\cdot |x-1|^{4} \\
            &\leq \frac{1}{4}\cdot (1/2)^{4} 
            =\frac{1}{4}\cdot\frac{1}{16}\approx 0.015625
\end{align*}
\end{frame}



\begin{frame}
\Ex
Earlier we also did the second-order Taylor polynomial about $\pi/3$.
\begin{equation*}
  T_2(x) 
  =\sin(\pi/3)+\cos(\pi/3)\cdot(x-\pi/3)-\frac{\sin(\pi/3)}{2}\cdot(x-\pi/3)^2 
\end{equation*}
In the error bound,
\begin{equation*}
  \varepsilon(x)=|T_n(x)-\sin(x)|\leq \frac{K}{(n+1)!}\cdot |x-\pi/3|^{n+1}
\end{equation*}
because the derivatives are all sines and cosines, 
we have $|f^{(n)}(z)|\leq 1$ for all~$z$ and so we take $K=1$.

Suppose that we have a calculator that shows eight decimal places.
Noting that $a=\pi/3\approx 1.047$,
if we look at $x=1$ then what order~$n$ do we need to ensure the error is
less than $10^{-8}$?
\textit{Sage} says this.
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$n$}
      &\multicolumn{1}{c}{$\varepsilon(1)$}  \\ \hline
    $0$ &$0.0471975512$ \\
    $1$ &$0.0011138044$ \\
    $2$ &$1.75229\times 10^{-5}$ \\
    $3$ &$2.068\times 10^{-7}$ \\
    $4$ &$2\times 10^{-9}$ \\
  \end{tabular}
\end{center}
Roughly, every increase of one in the order of the polynomial
increases the accuracy by two decimal places.
\end{frame}
% sage: def e(n,x):
% ....:     return (abs(x-(pi/3))^(n+1))/factorial(n+1)
% ....: 
% sage: x = 1
% sage: for i in range(5):
% ....:     print("i=",i," e(i,x)",round(e(i,x),ndigits=10))
% ....: 
% ....: 
% i= 0  e(i,x) 0.0471975512
% i= 1  e(i,x) 0.0011138044
% i= 2  e(i,x) 1.75229e-05
% i= 3  e(i,x) 2.068e-07
% i= 4  e(i,x) 2e-09



\begin{frame}
\Ex Where $f(x)=\ln(x)$, find $T_4(x)$ about the anchor point $a=1$.
Give a bound on the error.

\pause
These are the derivatives.
\begin{center}
  \begin{tabular}{@{}r|*{6}{c}@{}}
    \multicolumn{1}{c}{$i$}
      &\multicolumn{1}{c}{$0$}
      &\multicolumn{1}{c}{$1$}
      &\multicolumn{1}{c}{$2$}
      &\multicolumn{1}{c}{$3$}  
      &\multicolumn{1}{c}{$4$}   
      &\multicolumn{1}{c}{$5$}   
\\ \hline
    $f^{(i)}(x)$ &$\ln x$ 
                &$x^{-1}$ 
                &$-x^{-2}$ 
                &$2x^{-3} x$  
                &$-6x^{-4}$  
                &$24x^{-5}$  
                \rule{0pt}{9pt} \\
   $f^{(i)}(1)$  &$0$ 
                &$1$ 
                &$-1$ 
                &$2$  
                &$-6$   
                &$24$   
                \\
  \end{tabular}  
\end{center}
The fourth degree Taylor polynomial is this.
\begin{equation*}
  T_4(x)=0
         +1\cdot(x-1)
         +\frac{-1}{2!}(x-1)^2
         +\frac{2}{3!}(x-1)^3
         +\frac{-6}{4!}(x-1)^4
\end{equation*}
Find the approximation with $x=1.1$.
\begin{equation*}
  T_4(1.1)\approx 0.09530833
  \qquad
  \ln(1.1)\approx 0.09531018
\end{equation*}
As to the bound, 
the fifth derivative is a decreasing function so it is largest at $x=1$,
and we take $K=24$.
\begin{equation*}
  \varepsilon(x)\leq \frac{K}{5!}\cdot |x-1|^{5}
                =\frac{24}{120}\cdot(0.1)^5
                =2\times 10^{-6}
\end{equation*}
\end{frame}
% sage: def T4(x):
% ....:     return (x-1)+(-1/2)*(x-1)^2+(2/6)*(x-1)^3-(6/24)*(x-1)^4
% ....: 
% sage: T4(1.1)
% 0.0953083333333334
% sage: round(T4(1.1), ndigits=5)
% 0.09531
% sage: round(ln(1.1), ndigits=5)
% 0.09531
% sage: round(T4(1.1), ndigits=8)
% 0.09530833
% sage: round(ln(1.1), ndigits=8)
% 0.09531018
% sage: err = (24/120)*(0.1^5)
% sage: err
% 2.00000000000000e-6


\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
