\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 10.5\hspace{1em}Conic sections}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Conic sections}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage arc_length_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
\input asy/conics_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



% \section{}


\begin{frame}
  \frametitle{A plane intersecting a cone}

If the plane is parallel to the $xy$~plane then the intersection region
is a circle.
% \begin{center}
%   \only<1>{\vcenteredhbox{\includegraphics{conics_3d001.pdf}}}%    
%   \only<2>{\vcenteredhbox{\includegraphics{conics_3d000.pdf}}}%    
%   \only<3>{\vcenteredhbox{\includegraphics{conics_3d002.pdf}}}%    
%   \only<4->{\vcenteredhbox{\includegraphics{conics_3d003.pdf}}}%    
% \end{center}
\begin{center}
  \only<1>{\input asy/conics_3d001.tex }%    
  \only<2>{\input asy/conics_3d000.tex }%    
  \only<3>{\input asy/conics_3d002.tex }%    
  \only<4->{\input asy/conics_3d003.tex }%    
\end{center}
\uncover<2->{If the plane is tilted a little then the region is an 
ellipse.}
\uncover<3->{If the plane is tilted just the right amount to make it parallel
to the cone's side then the region is a parabola.}
\uncover<4->{Finally, if the plane is tilted even more then 
the region is a hyperbola.}
\end{frame}


\section{Ellipses and circles}
\begin{frame}
Fix two points $F_1$ and~$F_2$ in the plane, each called a \alert{focus} 
(the convention is to put them on the $x$~axis equidistant from the origin,
at $(-c,0)$ and $(c,0)$).
Fix a constant greater than the distance between the points, $K>2c$.
Then the \alert{ellipse}~$\mathcal{C}$ is the set of points $P=(x,y)$ 
such that $K$ is the total distance from $F_1$ to~$P$
and then to~$F_2$.
\begin{center}
  \only<1>{\vcenteredhbox{\includegraphics{conics000.pdf}}}%    
  \only<2->{\vcenteredhbox{\includegraphics{conics005.pdf}}}%    
\end{center}
\Tm The ellipse with \alert{semimajor} axis~$a$ and \alert{semiminor} axis~$b$
is the set of $(x,y)$ satisfying this.
\begin{equation*}
  \biggl(\frac{x}{a}\biggr)^2+\biggl(\frac{y}{b}\biggr)^2=1
  \qquad
  a>b>0
  \tag{$*$}
\end{equation*}
Its foci are at $F_1=(c,0)$ and $F_2=(-c,0)$ where $c=\sqrt{a^2-b^2}$.
The constant sum is $K=2a$.
\end{frame}

\begin{frame}
The degenerate case is where the two foci are the same point,
making a circle of radius $K/2$. 

The parametric description is $F(t)=(a\cos t, b\sin t)$
with $0\leq t<2\pi$.

\pause\medskip
We won't derive all of the formulas but for illustration we do the
prior slide's formula ($*$).
Use the Pythagorean relation and then clear the radicals.
\begin{align*}
  \sqrt{(x+c)^2+y^2}+\sqrt{(x-c)^2+y^2} &= 2a  \\
  (x+c)^2+y^2  &=4a^2-4a\sqrt{(x-c)^2+y^2}+(x-c)^2+y^2 \\
  4a\sqrt{(x-c)^2+y^2} &=4a^2+(x-c)^2-(x+c)^2=4a^2-4cx \\
  a^2(x^2-2cx+c^2+y^2) &=a^4-2a^2cx+c^2x^2 \\
  (a^2-c^2)x^2+a^2y^2  &=a^4-a^2c^2=a^2(a^2-c^2)  
\end{align*}
Dividing by $a^2(a^2-c^2)$ gives this.
\begin{equation*}
  \frac{x^2}{a^2}+\frac{y^2}{a^2-c^2}=1
\end{equation*}
Take $b^2=a^2-c^2$.
\end{frame}




\section{Hyperbola}
\begin{frame}
Fix two \alert{foci} in the plane,  
by convention on the $x$~axis equidistant from the origin $F_1=(-c,0)$ 
and~$F_2=(c,0)$.
Let $K$ be a constant greater than zero.
Then the \alert{hyperbola}~$\mathcal{C}$,
is the set of points $P=(x,y)$ 
where the distance from $P$ to~$F_1$
minus the distance from $P$ to~$F_2$ equals~$K$ on the right branch, 
or equals $-K$ on the left.
\begin{center}
  \only<1>{\vcenteredhbox{\includegraphics{conics001.pdf}}}%    
  \only<2->{\vcenteredhbox{\includegraphics{conics006.pdf}}}%    
\end{center}
\Tm The hyperbola containing $(a,0)$ and $(-a,0)$, and with asymptotes
$y=(b/a)x$ and $y=(-b/a)x$
is the set of $(x,y)$ satisfying this.
\begin{equation*}
  \biggl(\frac{x}{a}\biggr)^2-\biggl(\frac{y}{b}\biggr)^2=1
\end{equation*}
Its foci are at $(c,0)$ and $(-c,0)$ where $c=\sqrt{a^2-b^2}$.
The constant difference is $K=\pm 2a$.
\end{frame}

\begin{frame}
The parametric description is $F(t)=(a\sec(t),b\tan(t))$ with $-\pi/2<t<\pi/2$.  
\end{frame}



\section{Parabola}
\begin{frame}
We can think of a \alert{parabola}~$\mathcal{C}$ 
as an ellipse where one
focus is at infinity.
Consequently,
fix one focus in the plane,  
by convention on the $y$~axis above the origin, $F=(0,p)$.
Also fix the line $y=-p$, the \alert{directrix}.
Then $\mathcal{C}$ is the set of points $P=(x,y)$ 
such that the distance from $P$ to~$F$
equals the distance from $P$ to the line.
\begin{center}
  \only<1>{\vcenteredhbox{\includegraphics{conics002.pdf}}}%    
  \only<2->{\vcenteredhbox{\includegraphics{conics007.pdf}}}%    
\end{center}
\Tm The parabola with focus $(0,p)$ and 
directrix $y=-p$
is the set of $(x,y)$ satisfying this.
\begin{equation*}
  y=x^2/4p
\end{equation*}

\medskip
The parametric description is $F(t)=(t,t^2/4p)$ with $-\infty<t<\infty$.  
\end{frame}


\section{Reflection properties}
\begin{frame}
In a parabola, rays coming from infinity are all sent to the focus.
\begin{center}
  \vcenteredhbox{\includegraphics{conics003.pdf}}%
  \qquad    
  \vcenteredhbox{\includegraphics[height=0.4\textheight]{pix/dish.jpg}}
\end{center}
\end{frame}

\begin{frame}
In an ellipse, rays eminating from one focus all go to the other focus.  
The old hall of the House of Representatives, now Statuary Hall, has an 
elliptical ceiling.
This painting by Samuel B Morse illustrates.  
\begin{center}
  \vcenteredhbox{\includegraphics{conics004.pdf}}%  
  \qquad  
  \vcenteredhbox{\includegraphics[height=0.4\textheight]{pix/House.jpg}}%
\end{center}
They say that John Quincy Adams arranged that his desk was at
one focus and the desk of the leader of the other party was
at the other.
\end{frame}


\begin{frame}{Moving around}
These figures can be moved to be around the $y$~axis by swapping $x$ with~$y$
in the equations.

\pause
We can move the figures to center $(a,b)$ by replacing $(x,y)$ with
$(x-a,y-b)$.
Thus
\begin{equation*}
  \frac{(x-5)^2}{16}+\frac{(y+2)^2}{9}=1
\end{equation*}
is an ellipse with semimajor axis~$4$, semiminor axis~$3$, and center $(5,-2)$. 

\pause
\medskip
The \alert{general equation of degree two}
\begin{equation*}
  Ax^2+Bxy+Cy^2+Dx+Ey+F=0
\end{equation*}
describes a conic section, perhaps degenerate, that may be shifted from the
origin or rotated.
(As to what `degenerate' means: for instance $x^2+y^2=0$ defines a single point,
which we take to be a degenerate ellipse,
and $x^2+y^2+1=0$ is satisfied by no points.)
\end{frame}

\begin{frame}{Practice}
\begin{enumerate}
\item Find the vertex, focus, and directrix of the parabola $y=5x^2$ and
sketch.
\item Find the vertex, focus, and directrix of the parabola $(x-3)^2=8(y+1)$ and
sketch.
\item Find the vertices and foci of the ellipse $x^2/4+y^2/3=1$ and
sketch.
\item Find the vertices and foci of the ellipse $4x^2+25y^2-10y=100$ and
sketch.
\end{enumerate}
\end{frame}


\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
