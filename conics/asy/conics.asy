// conics.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "conics%03d";

import graph;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// =========== Ellipse ===============
real ELLIPSE_A = 4;
real ELLIPSE_B = 3;
pair F0(real t) {
  return (ELLIPSE_A*cos(t), ELLIPSE_B*sin(t));
}

picture pic;
int picnum = 0;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1*ELLIPSE_A; xright=ELLIPSE_A;
ybot=-1*ELLIPSE_B; ytop=ELLIPSE_B;

path f = graph(pic, F0, 0, 2*pi);
draw(pic, f, FCNPEN);

// the foci
real c = sqrt(ELLIPSE_A^2-ELLIPSE_B^2);
dotfactor = 4;
dot(pic, Label("$(-c,0)$",filltype=Fill(white)), (-1*c, 0), S);
dot(pic, Label("$(c,0)$",filltype=Fill(white)), (c, 0), S);

real[] T0 = {-1*ELLIPSE_A,ELLIPSE_A};
// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$-a$", -1*ELLIPSE_A, SW);
labelx(pic, "$a$", ELLIPSE_A, SE);

real[] T0a = {-1*ELLIPSE_B, ELLIPSE_B}; 
// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks("%", T0a, Size=2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$-b$", -1*ELLIPSE_B, NE);
labely(pic, "$b$", ELLIPSE_B, SE);

shipout(format(OUTPUT_FN,picnum),pic);


// ============= hyperbola ===============
real HYPERBOLA_A = 2;
real HYPERBOLA_B = 1.75;

pair F1(real t){
  return (HYPERBOLA_A/cos(t), HYPERBOLA_B*tan(t));
} 
pair F1_minus(real t){
  return (-1*HYPERBOLA_A/cos(t), HYPERBOLA_B*tan(t));
} 

picture pic;
int picnum = 1;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=5;
ybot=-4; ytop=4;

real graph_limit = (pi/2)-0.4;
path f = graph(pic, F1, -graph_limit, graph_limit);
path f_minus = graph(pic, F1_minus, -graph_limit, graph_limit);
draw(pic, f, FCNPEN);
draw(pic, f_minus, FCNPEN);

// the foci
real c = sqrt(HYPERBOLA_A^2+HYPERBOLA_B^2);
dotfactor = 4;
dot(pic, Label("\makebox[0em][r]{$(-c,0)$}"), (-1*c, 0), S);
dot(pic, Label("\makebox[0em][l]{$(c,0)$}"), (c, 0), S);

// the asymptotes
draw(pic, (xleft,-xleft*HYPERBOLA_B/HYPERBOLA_A)--(xright,-xright*HYPERBOLA_B/HYPERBOLA_A), dashed);
draw(pic, (xleft,xleft*HYPERBOLA_B/HYPERBOLA_A)--(xright,xright*HYPERBOLA_B/HYPERBOLA_A), dashed);

real[] T0 = {-1*HYPERBOLA_A,HYPERBOLA_A};
// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$-a$", -1*HYPERBOLA_A, 0.5*SE);
labelx(pic, "$a$", HYPERBOLA_A, NW);

// real[] T0a = {-1*ELLIPSE_B, ELLIPSE_B}; 
// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks("%", T0a, Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ============= parabola ===============
real PARABOLA_P = 1;

real f2(real t){
  return t^2/(4*PARABOLA_P);
} 
picture pic;
int picnum = 2;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-PARABOLA_P; ytop=(xright/2)^2;

real graph_limit = 3;
path f = graph(pic, f2, -graph_limit, graph_limit);
draw(pic, f, FCNPEN);
// the focus
dotfactor = 4;
pair F = (0, PARABOLA_P); // focus

// line illustrating equal length
// real[] X = {-2.5, -1.618, 0.75};
// for(real x : X) {
//   real ell_t = times(f, x)[0];
//   path ell = (x,-1*PARABOLA_P)--point(f,ell_t)--F;
//   draw(pic, ell, dotted);
// }
dot(pic, Label("$F=(0,p)$", filltype=Fill(white)), F, E);

// the directrix
draw(pic, (xleft+0.2,-1*PARABOLA_P)--(xright-0.2,-1*PARABOLA_P), highlight_color+dotted, Arrows(ARROW_SIZE));

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks("%", T0a, Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


//  ............... reflection of a parabola  ..............
picture pic;
int picnum = 3;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-PARABOLA_P; ytop=(xright/2)^2;

real graph_limit = 3;
path f = graph(pic, f2, -graph_limit, graph_limit);
draw(pic, f, FCNPEN);
// the focus
dotfactor = 4;
pair F = (0, PARABOLA_P); // focus

// line illustrating rays coming from infinity
real[] X = {-2.5, -1.618, -0.5, 0.75, 2.1};
for(real x : X) {
  real ell_t = times(f, x)[0];
  path ell = (x,ytop)--point(f,ell_t)--F;
  draw(pic, ell, dotted+highlight_color, Arrow(3pt, position=0.3));
}
dot(pic, F);

// the directrix
draw(pic, (xleft+0.2,-1*PARABOLA_P)--(xright-0.2,-1*PARABOLA_P), grayed, Arrows(ARROW_SIZE));

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks("%", T0a, Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ========== reflections in ellipse ========
picture pic;
int picnum = 4;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1*ELLIPSE_A; xright=ELLIPSE_A;
ybot=-1*ELLIPSE_B; ytop=ELLIPSE_B;

path f = graph(pic, F0, 0, 2*pi);
draw(pic, f, FCNPEN);

// the foci
real c = sqrt(ELLIPSE_A^2-ELLIPSE_B^2);
pair F1 = (-1*c, 0);
pair F2 = (c, 0);
dotfactor = 4;
dot(pic, Label("$F_1$"), F1, N);
dot(pic, Label("$F_2$"), F2, S);

// lines illustrating reflections
real[] T4 = {length(f)/5, 0.6*length(f), 0.94*length(f)};  // times on the curve
for(real t : T4) {
  path ell = F1--point(f,t)--F2;
  draw(pic, ell, dotted+highlight_color, Arrow(3pt, position=0.3));
}

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));
// labelx(pic, "$-a$", -1*ELLIPSE_A, SW);
// labelx(pic, "$a$", ELLIPSE_A, SE);

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks("%", T0a, Size=2pt),
      arrow=Arrows(TeXHead));
// labely(pic, "$-b$", -1*ELLIPSE_B, NE);
// labely(pic, "$b$", ELLIPSE_B, SE);

shipout(format(OUTPUT_FN,picnum),pic);



// =========== Ellipse ===============
// real ELLIPSE_A = 4;
// real ELLIPSE_B = 3;
// pair F0(real t) {
//   return (ELLIPSE_A*cos(t), ELLIPSE_B*sin(t));
// }

picture pic;
int picnum = 5;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1*ELLIPSE_A; xright=ELLIPSE_A;
ybot=-1*ELLIPSE_B; ytop=ELLIPSE_B;

path f = graph(pic, F0, 0, 2*pi);
draw(pic, f, FCNPEN);

// the foci
real c = sqrt(ELLIPSE_A^2-ELLIPSE_B^2);

// total lines
path total_lines = (-1*c,0) -- F0(0.7) -- (c,0);
draw(pic, total_lines, highlight_color);
dot(pic, Label("$P=(x,y)$",filltype=Fill(white)), F0(0.7), 2*E);

dotfactor = 4;
dot(pic, Label("$(-c,0)$",filltype=Fill(white)), (-1*c, 0), S);
dot(pic, Label("$(c,0)$",filltype=Fill(white)), (c, 0), S);

real[] T0 = {-1*ELLIPSE_A,ELLIPSE_A};
// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$-a$", -1*ELLIPSE_A, SW);
labelx(pic, "$a$", ELLIPSE_A, SE);

real[] T0a = {-1*ELLIPSE_B, ELLIPSE_B}; 
// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks("%", T0a, Size=2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$-b$", -1*ELLIPSE_B, NE);
labely(pic, "$b$", ELLIPSE_B, SE);

shipout(format(OUTPUT_FN,picnum),pic);





// ============= hyperbola ===============
real HYPERBOLA_A = 2;
real HYPERBOLA_B = 1.75;

pair F1(real t){
  return (HYPERBOLA_A/cos(t), HYPERBOLA_B*tan(t));
} 
pair F1_minus(real t){
  return (-1*HYPERBOLA_A/cos(t), HYPERBOLA_B*tan(t));
} 

picture pic;
int picnum = 6;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=5;
ybot=-4; ytop=4;

real graph_limit = (pi/2)-0.4;
path f = graph(pic, F1, -graph_limit, graph_limit);
path f_minus = graph(pic, F1_minus, -graph_limit, graph_limit);
draw(pic, f, FCNPEN);
draw(pic, f_minus, FCNPEN);

// the foci
real c = sqrt(HYPERBOLA_A^2+HYPERBOLA_B^2);

// total lines
path total_lines = (-1*c,0) -- F1(0.8) -- (c,0);
draw(pic, total_lines, highlight_color);
dot(pic, Label("$P=(x,y)$",filltype=Fill(white)), F1(0.8), 2*E);

dotfactor = 4;
dot(pic, Label("\makebox[0em][r]{$(-c,0)$}"), (-1*c, 0), S);
dot(pic, Label("\makebox[0em][l]{$(c,0)$}"), (c, 0), S);

// the asymptotes
draw(pic, (xleft,-xleft*HYPERBOLA_B/HYPERBOLA_A)--(xright,-xright*HYPERBOLA_B/HYPERBOLA_A), dashed);
draw(pic, (xleft,xleft*HYPERBOLA_B/HYPERBOLA_A)--(xright,xright*HYPERBOLA_B/HYPERBOLA_A), dashed);

real[] T0 = {-1*HYPERBOLA_A,HYPERBOLA_A};
// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$-a$", -1*HYPERBOLA_A, 0.5*SE);
labelx(pic, "$a$", HYPERBOLA_A, NW);

// real[] T0a = {-1*ELLIPSE_B, ELLIPSE_B}; 
// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks("%", T0a, Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ============= parabola ===============
real PARABOLA_P = 1;

real f2(real t){
  return t^2/(4*PARABOLA_P);
} 
picture pic;
int picnum = 7;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-PARABOLA_P; ytop=(xright/2)^2;

real graph_limit = 3;
path f = graph(pic, f2, -graph_limit, graph_limit);
draw(pic, f, FCNPEN);
// the focus
dotfactor = 4;
pair F = (0, PARABOLA_P); // focus

// line illustrating equal length
real[] X = {-2.5, -1.618, 0.75};
for(real x : X) {
  real ell_t = times(f, x)[0];
  path ell = (x,-1*PARABOLA_P)--point(f,ell_t)--F;
  draw(pic, ell, highlight_color);
}
dot(pic, Label("$F=(0,p)$", filltype=Fill(white)), F, E);

// the directrix
draw(pic, (xleft+0.2,-1*PARABOLA_P)--(xright-0.2,-1*PARABOLA_P), highlight_color+dotted, Arrows(ARROW_SIZE));

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks("%", T0a, Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);

