// conics_3d.asy
// compile with asy -inlineimage conics_3d

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = true;
settings.embed = true;
settings.render = 0;
settings.tex="lualatex";  // for compiling in-pic text
settings.outformat="pdf";

import embed;

import fontsize;

import graph; import graph3; import solids;
currentprojection = orthographic(1.2,3,2,up=Z);
currentlight=White; defaultrender.merge=true;
// unitsize(1cm);

string OUTPUT_FN = "conics_3d%03d";

material figure_material = material(diffusepen=bold_color+white+opacity(0.5),
				 emissivepen=bold_color+white,
				 specularpen=bold_color+white);
material slice_material = material(diffusepen=gray_color+opacity(0.25),
				   emissivepen=gray_color,
				   specularpen=gray_color);
pen boundary_pen = gray(0.2)+opacity(0.5);

currentlight = nolight;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// ===== ellipse =======
currentprojection = orthographic(3,1,0.95,up=Z);

picture pic;
int picnum = 0;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

// cone
surface c_lower = shift(0,0,-1)*unitcone;
surface c_upper = rotate(180, X)*shift(0,0,-1)*unitcone;
draw(pic, c_lower, surfacepen=figure_material);
draw(pic, c_upper, surfacepen=figure_material);

// plane
triple v1 = 2*X;
triple v2 = 2*Y;
real ang = 25;  // degrees
path3 p = shift(-1,-1,0)*plane(v1, v2);  
// triple p_normal = cross(v1, v2);
// real p_normal_angle_with_z = colatitude(p_normal);
surface p_surface = surface(p);
transform3 t = shift(0,0,0.25)*rotate(ang, X);
draw(pic, t*p_surface, surfacepen=slice_material);

// ellipse
path3 e = shift(0,0.15,0.315)*rotate(ang,X)*scale(0.275, 0.35,1)*unitcircle3;
draw(pic, e, highlight_color);

xaxis3(pic,Label("$x$"),
       0,3, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,2, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),
        0,2, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);




// ===== circle =======
picture pic;
int picnum = 1;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

// cone
surface c_lower = shift(0,0,-1)*unitcone;
surface c_upper = rotate(180, X)*shift(0,0,-1)*unitcone;
draw(pic, c_lower, surfacepen=figure_material);
draw(pic, c_upper, surfacepen=figure_material);

// plane
triple v1 = 2*X;
triple v2 = 2*Y;
real ang = 0;  // degrees
path3 p = shift(-1,-1,0)*plane(v1, v2);  
// triple p_normal = cross(v1, v2);
// real p_normal_angle_with_z = colatitude(p_normal);
surface p_surface = surface(p);
transform3 t = shift(0,0,0.25);
draw(pic, t*p_surface, surfacepen=slice_material);

// circle
path3 e = shift(0,0,0.25)*scale(0.25,0.25,1)*unitcircle3;
draw(pic, e, highlight_color);

xaxis3(pic,Label("$x$"),
       0,3, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,2, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),
        0,2, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);




// ===== parabola =======
real PARABOLA_C = 0.09;
real f2(real x) {
  return x^2/(4*PARABOLA_C);
}

picture pic;
int picnum = 2;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

// cone
surface c_lower = shift(0,0,-1)*unitcone;
surface c_upper = rotate(180, X)*shift(0,0,-1)*unitcone;
draw(pic, c_lower, surfacepen=figure_material);
draw(pic, c_upper, surfacepen=figure_material);

// plane
triple v1 = 2*X;
triple v2 = 2*Y;
real ang = 45;  // degrees
path3 p = shift(-1,-1,0)*plane(v1, v2);  
// triple p_normal = cross(v1, v2);
// real p_normal_angle_with_z = colatitude(p_normal);
surface p_surface = surface(p);
transform3 t = shift(0,0,0.25)*rotate(ang, X);
draw(pic, t*p_surface, surfacepen=slice_material);

// parabola
real parabola_end = 0.65;
path f = graph(pic, f2, -1*parabola_end, parabola_end);
path3 f_3d = path3(f);
path3 e = shift(0,-0.13,0.13)*rotate(ang,X)*scale(1, 1,1)*f_3d;
draw(pic, e, highlight_color);

xaxis3(pic,Label("$x$"),
       0,3, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,2, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),
        0,2, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ===== hyperbola =======
real HYPERBOLA_A = 1.04/2;  // from eyeballing the a's below
real HYPERBOLA_B = 0.58/0.85-0.18;  // eyeball a point out on intersection, get b/a then adjust
real f3(real x) {
  return sqrt(HYPERBOLA_B^2*((x^2/HYPERBOLA_A^2)-1));
}
real f3_minus(real x) {
  return -1*f3(x);
}

picture pic;
int picnum = 3;
size(pic,0,5cm);
size3(pic,0,5cm,0,keepAspect=true);

// cone
surface c_lower = shift(0,0,-1)*unitcone;
surface c_upper = rotate(180, X)*shift(0,0,-1)*unitcone;
draw(pic, c_lower, surfacepen=figure_material);
draw(pic, c_upper, surfacepen=figure_material);

// plane
triple v1 = 2*X;
triple v2 = 2*Y;
real ang = 80;  // degrees
path3 p = shift(-1,-1,0)*plane(v1, v2);  
// triple p_normal = cross(v1, v2);
// real p_normal_angle_with_z = colatitude(p_normal);
surface p_surface = surface(p);
transform3 t = shift(0,0.5,0)*rotate(ang, X);
draw(pic, t*p_surface, surfacepen=slice_material);

// to eyeball the parameters
// real a_top = 0.61;
// real a_bot = 0.43;
// triple intersect_top = (0,a_top,0); 
// triple intersect_bot = (0,-1*a_bot,0); 
// dot(pic, t*intersect_top, green);
// dot(pic, t*intersect_bot, green);
// real b_bot = 0.58;  // to find b/a
// real x_val = 0.85;
// triple ba_bot = (x_val,-1*b_bot/HYPERBOLA_A*x_val,0); 
// dot(pic, t*ba_bot, blue);

// hyperbola
real hyperbola_start = HYPERBOLA_A;
real hyperbola_end = 0.90;
path f_top_after = graph(pic, f3, hyperbola_start, hyperbola_end);
path f_top_before = graph(pic, f3_minus, hyperbola_start, hyperbola_end);
path f_top = reverse(f_top_before)&f_top_after;
path3 f_top_3d = rotate(90, Z)*path3(f_top);
path3 e_top = shift(0,0.52,0.1)*rotate(ang,X)*scale(1, 1,1)*f_top_3d;
draw(pic, e_top, highlight_color);
real hyperbola_end = 1.075;
path f_bot_after = graph(pic, f3, -hyperbola_start, -hyperbola_end);
path f_bot_before = graph(pic, f3_minus, -hyperbola_start, -hyperbola_end);
path f_bot = reverse(f_bot_before)&f_bot_after;
path3 f_bot_3d = rotate(90, Z)*path3(f_bot);
path3 e_bot = shift(0,0.515,0.085)*rotate(ang,X)*scale(1,1,1)*f_bot_3d;
draw(pic, e_bot, highlight_color);

xaxis3(pic,Label("$x$"),
       0,1.5, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,1.15, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),
        0,1.15, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


