// quadric_3d.asy

// compile with asy -inlineimage stub_3d
// In the .tex file put this in the preamble ... 
// % For 3D PRC asymptote
// % compile with asy -inlineimage stub_3d
// \def\asydir{asy/}
// \graphicspath{{asy/}}
// \input asy/stub_3d.pre
// \usepackage[bigfiles]{media9}
// \RequirePackage{asymptote}
// ... and then include the 3D material with something like this.
// \begin{center}
//   \uncover<2->{\input asy/stub_3d001.tex }% need the space following .tex   
// \end{center}


cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = true;
settings.embed = true;
settings.render = 0;
settings.tex="lualatex";  // for compiling in-pic text
settings.outformat="pdf";

import embed;

import fontsize;

import graph3; import solids;
currentprojection = orthographic(1.2,3,2,up=Z);
currentlight=White; defaultrender.merge=true;
// unitsize(1cm);

string OUTPUT_FN = "quadric_3d%03d";

material figure_material = material(diffusepen=bold_color+white+opacity(0.5),
				 emissivepen=bold_color+white,
				 specularpen=bold_color+white);
material slice_material = material(diffusepen=gray_color+opacity(0.25),
				   emissivepen=gray_color,
				   specularpen=gray_color);
pen boundary_pen = gray(0.2)+opacity(0.5);

currentlight = nolight;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// ===== Gabriel's horn =======
currentprojection = orthographic(4,1,10,up=Y);

real f6(real x) {return 1/x;}

picture pic;
int picnum = 6;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

path f = graph(pic, f6, 1, 5);
path3 f_3d = path3(f);

// region that was rotated
path r = ( (1,0)--relpoint(f,0)&f&relpoint(f,1)--(5,0)--(1,0) )--cycle;
path3 r_3d = path3(r);
surface base = surface(r_3d);
draw(pic, base, surfacepen=gray(0.8)+opacity(0.5));
draw(pic, r_3d);

// horn
surface horn = surface(O, f_3d, X);
// draw end circles
draw(pic, shift(1,0,0)*rotate(90,Y)*unitcircle3, grayed);
draw(pic, shift(5,0,0)*rotate(90,Y)*scale(f6(5),f6(5),1)*unitcircle3, grayed);
draw(pic, horn, surfacepen=figure_material);

// Add slice
// real x = 1+0.618*(5-1);
// real rad = f6(x);
// transform3 slice_t = shift((x,0,0))*rotate(90,Y)*scale(rad,rad,1); 
// surface slice = slice_t*unitdisk;
// draw(pic, slice, surfacepen=slice_material);
// // add a circular edge
// draw(pic, slice_t*unitcircle3, highlight_color);

xaxis3(pic,Label("$x$"),
       0,6, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,1, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
        0,1, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



