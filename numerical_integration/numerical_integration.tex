\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 7.7\hspace{1em}Numerical integration}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Numerical integration}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}
  \frametitle{When antidifferentiation is a problem}
    
We are trying to find $\integral{a}{b}{f(x)}{x}$ for various $f$'s.
The Fundamental Theorem says that if we can antidifferentiate then we
have a solution.

\begin{enumerate}
\item Sometimes finding an antiderivative is hard.
Even when we can find an antiderivative symbolically, 
it may be easier to compute a numerical approximation than to work with
the antiderivative (such as when it uses a special function that 
is not available, or slow to compute).

\item
Sometimes antiderivatives don't exist\Dash there simply is 
no formula for the antiderivative of $f(x)=e^{-x^2}\!\!$, which is a 
very important function in statistics.

\item
It can be that~$f$ doesn't even have a formula so there is nothing 
to antidifferentiate.
For instance, we may be getting values by sampling from a
measuring device.
\end{enumerate}

So we we will take a route other than antidifferentiation, 
calculating a numerical approximation.
This is \alert{numerical integration} or 
\alert{numerical quadrature}
(the term refers to the geometrical problem of finding a square with 
the same area as a given plane figure).

\pause
We will see a number of approximation methods directed at situation~(2).
For each we will see an \alert{error function} giving the 
method's accuracy in the worst case.
The slogan is `$\text{answer}=\text{approximation}+\text{error}$'. 
\end{frame}


\begin{frame}{Endpoint approximation}
To approximate this area we can divide into subintervals $\closed{x_i}{x_{i+1}}$.
\begin{center}
  \only<1>{\vcenteredhbox{\includegraphics{asy/numerical_integration000.pdf}}}%
  \only<2->{\vcenteredhbox{\includegraphics{asy/numerical_integration001.pdf}}}%
\end{center}
\pause
Over each we erect a rectangle of height $f(x_i)$ 
for the \alert{left-hand approximation} as shown, or $f(x_{i+1})$
for the \alert{right-hand approximation}.

\Ex Where $f(x)=x^2$ and $a=1$ and $b=3$
the correct answer is $\bigl[x^3/3\bigr]_{1}^{3}\approx 8.67$.
With four subintervals we get this
left-hand approximation.
\begin{equation*}
  \integral{1}{3}{x^2}{x}\approx
  1^2\cdot(0.5)+1.5^2(0.5)+2^2(0.5)+2.5^2(0.5)=6.75
\end{equation*}
\end{frame}


\begin{frame}[fragile]{Code for \textit{Sage}}
\lstinputlisting[firstline=1,lastline=14]{src/numerical_integration.sage}

Here is what happens when we run it.
  
\begin{lstlisting}
jim@millstone:~/teach/calc_ii/numerical_integration$ sage
┌────────────────────────────────────────────────────────────────────┐
│ SageMath version 9.5, Release Date: 2022-01-30                     │
│ Using Python 3.10.12. Type "help()" for help.                      │
└────────────────────────────────────────────────────────────────────┘
sage: load("src/endpoint.sage")
sage: def f(x):
....:     return(x^2)
....: 
sage: left_hand(f, 1, 3, 4)
i=0 anchor=1.00 area=0.50
i=1 anchor=1.50 area=1.12
i=2 anchor=2.00 area=2.00
i=3 anchor=2.50 area=3.12
total=6.75
\end{lstlisting}
\end{frame}

% $

\begin{frame}
This table contains the numbers from that computation.
\begin{center}
  \begin{tabular}{r|*{2}{r}}
    \multicolumn{1}{c}{\tabulartext{Box}}
      &\multicolumn{1}{c}{\tabulartext{$x_i$}}
      &\multicolumn{1}{c}{\tabulartext{Area}}  \\ \hline
    $0$ &$1.00$ &$0.50$ \\
    $1$ &$1.50$ &$1.12$ \\
    $2$ &$2.00$ &$2.00$ \\
    $3$ &$2.50$ &$3.12$ \\  \hline
    \multicolumn{2}{r}{\tabulartext{Total:\ }}        &$6.75$

  \end{tabular}
\end{center}
It computes the sizes of these boxes; note that it underestimates the $8.67$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/numerical_integration002.pdf}}%
\end{center}
\end{frame}

\begin{frame}[fragile]
\lstinputlisting[firstline=4,lastline=26]{src/numerical_integration.sage}

\pause
\begin{lstlisting}
sage: left_hand(f, 1, 3, 4)
i=0 anchor=1.00 area=0.50
i=1 anchor=1.50 area=1.12
i=2 anchor=2.00 area=2.00
i=3 anchor=2.50 area=3.12
total=6.75
sage: right_hand(f, 1, 3, 4)
i=0 anchor=1.50 area=1.12
i=1 anchor=2.00 area=2.00
i=2 anchor=2.50 area=3.12
i=3 anchor=3.00 area=4.50
total=10.75
\end{lstlisting}
\end{frame}





\begin{frame}{Midpoint rule}
Instead of anchoring on the left point~$x_i$ or the right point~$x_{i+1}$
we can use the midpoint $m_i=(x_i+x_{i+1})/2$.   
\begin{center}
  \vcenteredhbox{\includegraphics{asy/numerical_integration003.pdf}}%
\end{center}
We hope that the overcounts are somewhat balanced by the undercounts,
to better approximate the $8.67$.
\end{frame}


\begin{frame}[fragile]
\lstinputlisting[firstline=28,lastline=39]{src/numerical_integration.sage}

It is more accurate than the other two.
\begin{lstlisting}
sage: left_hand(f, 1, 3, 4)
i=0 anchor=1.00 area=0.50
i=1 anchor=1.50 area=1.12
i=2 anchor=2.00 area=2.00
i=3 anchor=2.50 area=3.12
total=6.75
sage: right_hand(f, 1, 3, 4)
i=0 anchor=1.50 area=1.12
i=1 anchor=2.00 area=2.00
i=2 anchor=2.50 area=3.12
i=3 anchor=3.00 area=4.50
total=10.75
sage: midpoint(f, 1, 3, 4)
i=0 anchor=1.25 area=0.78
i=1 anchor=1.75 area=1.53
i=2 anchor=2.25 area=2.53
i=3 anchor=2.75 area=3.78
total=8.62
\end{lstlisting}
\end{frame}


\begin{frame}{Trapezoid method}
Instead of making a rectangle, we can make a line segment
connecting $f(x_i)$ to~$f(x_{i+1})$ 
\begin{center}
  \vcenteredhbox{\includegraphics{asy/numerical_integration005.pdf}}%
\end{center}
The area of a trapeziod is the base width times the average of the two
heights, $(f(x_i)+f(x_{i+1}))/2$.
\end{frame}


\begin{frame}
On the problem of approximating $\integral{1}{3}{x^2}{x}=8.67$
the picture hardly shows any overcount or undercount.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/numerical_integration004.pdf}}%
\end{center}
\end{frame}


\begin{frame}[fragile]
\lstinputlisting[firstline=41,lastline=51]{src/numerical_integration.sage}

It is pretty accurate.
\begin{lstlisting}
sage: midpoint(f, 1, 3, 4)
i=0 anchor=1.25 area=0.78
i=1 anchor=1.75 area=1.53
i=2 anchor=2.25 area=2.53
i=3 anchor=2.75 area=3.78
total=8.62
sage: trapezoid(f, 1, 3, 4)
i=0 anchor=1.00 area=0.81
i=1 anchor=1.50 area=1.56
i=2 anchor=2.00 area=2.56
i=3 anchor=2.50 area=3.81
total=8.75
\end{lstlisting}

However, note that the Midpoint is more accurate than the Trapezoid.
So the picture fools your eye.
\end{frame}


\begin{frame}{Simpson's rule}
If approximating with a line is good, perhaps approximating with a
curve is better?
\alert{Simpson's method} uses a quadratic.

A parabola is determined by three points.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/numerical_integration006.pdf}}%
\end{center}  
So we divide the $\closed{x_i}{x_{i+1}}$ interval into two halves.

We will work with the picture centered at zero, because 
the development equations below are cleaner.
But shifting does not change the area under the curve so it doesn't matter.
\end{frame}

\begin{frame}{Simpson's rule}
We want the area under this quadratic.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/numerical_integration006.pdf}}%
\end{center}  
\begin{equation*}
  \integral{x=-h}{h}{ Ax^2+Bx+C }{x}
  =\Bigl[ A\frac{x^3}{3}+B\frac{x^2}{2}+Cx \Bigr]_{x=-h}^h
  =\frac{h}{3}(2Ah^2+6C)
\end{equation*}
We also know the function passes through the first, second, and third point.
\begin{equation*}
\begin{linsys}{3}
  Ah^2 &- &Bh &+ &C &= &y_0  \\    
       &  &   &  &C &= &y_1  \\    
  Ah^2 &+ &Bh &+ &C &= &y_2  \\    
\end{linsys}
\grstep{\rho_i\leftrightarrow\rho_2}
\begin{linsys}{3}
  Ah^2 &- &Bh &+ &C &= &y_0  \\    
  Ah^2 &+ &Bh &+ &C &= &y_2  \\    
       &  &   &  &C &= &y_1  \\    
\end{linsys}
\end{equation*}
Adding the first two rows gives $2Ah^2+2C=y_0+y_2$ and then we have
$2Ah^2+6C=y_0+y_2+4y_1$.
So the area under that curve in terms of the values of the function
is $(h/3)\cdot(y_0+4y_1+y_2)$.
\end{frame}




\begin{frame}[fragile]
\lstinputlisting[firstline=53,lastline=65]{src/numerical_integration.sage}

\begin{lstlisting}
sage: midpoint(f, 1, 3, 4)
i=0 anchor=1.25 area=0.78
i=1 anchor=1.75 area=1.53
i=2 anchor=2.25 area=2.53
i=3 anchor=2.75 area=3.78
total=8.62
sage: trapezoid(f, 1, 3, 4)
i=0 anchor=1.00 area=0.81
i=1 anchor=1.50 area=1.56
i=2 anchor=2.00 area=2.56
i=3 anchor=2.50 area=3.81
total=8.75
sage: simpson(f, 1, 3, 4)
i=0 anchor=1.00 area=0.79
i=1 anchor=1.50 area=1.54
i=2 anchor=2.00 area=2.54
i=3 anchor=2.50 area=3.79
total=8.67
\end{lstlisting}
\end{frame}








\section{Judging the methods}

\begin{frame}{Engineering judgements}
As algorithms, we can evaluate these  approximation methods
by how quickly they run or how much
memory they use.
As programs, we can look at how prone they are to numerical error or 
coding error (this last is mitigated by the presence of widely-tested
standard libraries).

But we will focus here on more directly mathematical concerns. 
A common way to rank approximation methods is to express the error.
While using a method, to improve the approximation you'd think to increase
the number of
boxes~$n$, that is, to shrink the interval width~$\Delta x$ and ask
how quickly the total error falls.


% \pause
% One way to rank the methods is by the highest order of these that
% it gets right.
% \begin{center}\small
% \begin{tabular}{r|*{4}{c}}
%     \tabulartext{Order $p$}
%        &$1$ &$2$ &$3$ &$4$                 \\  \cline{2-5}
%     \tabulartext{Integral}
%        &\rule{0pt}{12pt} $\displaystyle\integral{}{}{1}{x}$ 
%        &$\displaystyle\integral{}{}{x}{x}$ 
%        &$\displaystyle\integral{}{}{x^2}{x}$ 
%        &$\displaystyle\integral{}{}{x^3}{x}$ 
% \end{tabular}
% \end{center}
% The left and right endpoint methods are order~$1$.
% The midpoint and trapezoid methods are order~$2$.
% Simpson's method is order~$3$.  
\end{frame}



\begin{frame}{Error example}
Here is the left-hand endpoint method approximating a line.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/numerical_integration007.pdf}}%
\end{center}  
The per-box error is the area of the triangle: $(1/2)(\Delta x)(y_{i+1}-y_i)$.
The total error depends on the slope~$K$ of the line and the entire interval
$b-a$ of integration.

While working on a given problem, what we can change is the mesh.
The error falls with the first power of $\Delta x$, that is with $1/n$.
To halve the error we need to do twice as many iterations.
\end{frame}


\begin{frame}
The other methods do better.

\Tm
When approximating a function~$f$ on $\closed{a}{b}$ these are the 
worst case errors,
\begin{center}
\begin{tabular}{r|c}
  \multicolumn{1}{c}{\tabulartext{Method}} 
    &\multicolumn{1}{c}{\tabulartext{Error $\epsilon$}}   \\ \hline
   Trapezoid\rule{0pt}{15pt} &$\displaystyle \frac{K\cdot (b-a)^3}{12n^2}$   \\[2ex]
   Midpoint  &$\displaystyle \frac{K\cdot (b-a)^3}{24n^2}$   \\[2ex]
   Simpson   &$\displaystyle \frac{K\cdot (b-a)^5}{180n^4}$   \\
\end{tabular}
\end{center}
where for the first two $K\geq |f''(x)|$ and for the last $K\geq |f^{(4)}(x)|$.
(Think of $K$ as a constant.
To get a $K$, for instance for Trapeziod, find the second derivative of the 
function and maximize over the interval.)

\medskip
Simpson's rule gives a very accurate answer quickly\Dash increasing
the number of intervals by a factor of ten will decrease the error 
by a factor of ten thousand.
Also, Midpoint has half the error of Trapezoid.
(However, Trapezoid works on a data set where there is no formula for
midpoints.)
\end{frame}



\begin{frame}{Practice}
Use a calculator.

\begin{enumerate}
\item Use the Midpoint method and Simpson's method to approximate
the area under $f(x)=\sin\sqrt{x}$ on $\closed{1}{4}$ with $n=4$ boxes.

\item Do the same for $f(x)=\cos(x^2)$ on $\closed{0}{1}$.
How large should we choose~$n$ to ensure the error is less than $0.001$? 
\end{enumerate}
\end{frame}



\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
