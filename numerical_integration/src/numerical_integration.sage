BOX_FMT = "i={i} anchor={anchor:0.2f} area={area:0.2f}"
TOT_FMT = "total={total:0.2f}"

# Compute left-hand approximation to the area between fcn and the x axis
def left_hand(fcn, start, end, num_boxes):
    box_width = (end-start)/num_boxes
    accumulated_area = 0
    for i in range(num_boxes):
        xi = start+i*box_width
        xiplus = start+(i+1)*box_width
        box_area = f(xi)*box_width
        accumulated_area = accumulated_area+box_area
        print(BOX_FMT.format( i=i, anchor=float(xi), area=float(box_area)))
    print(TOT_FMT.format( total=float(accumulated_area) ))

# Compute right-hand approximation to the area between fcn and the x axis
def right_hand(fcn, start, end, num_boxes):
    box_width = (end-start)/num_boxes
    accumulated_area = 0
    for i in range(num_boxes):
        xi = start+i*box_width
        xiplus = start+(i+1)*box_width
        box_area = f(xiplus)*box_width
        accumulated_area = accumulated_area+box_area
        print(BOX_FMT.format( i=i, anchor=float(xiplus), area=float(box_area)))
    print(TOT_FMT.format( total=float(accumulated_area) ))

# Compute midpoint approximation to the area between fcn and the x axis
def midpoint(fcn, start, end, num_boxes):
    box_width = (end-start)/num_boxes
    accumulated_area = 0
    for i in range(num_boxes):
        xi = start+i*box_width
        xiplus = start+(i+1)*box_width
        mi = (xi+xiplus)/2
        box_area = f(mi)*box_width
        accumulated_area = accumulated_area+box_area
        print(BOX_FMT.format( i=i, anchor=float(mi), area=float(box_area)))
    print(TOT_FMT.format( total=float(accumulated_area) ))

# Compute trapezoid approximation to the area between fcn and the x axis
def trapezoid(fcn, start, end, num_boxes):
    box_width = (end-start)/num_boxes
    accumulated_area = 0
    for i in range(num_boxes):
        xi = start+i*box_width
        xiplus = start+(i+1)*box_width
        box_area = (f(xi)+f(xiplus))*box_width/2
        accumulated_area = accumulated_area+box_area
        print(BOX_FMT.format( i=i, anchor=float(xi), area=float(box_area)))
    print(TOT_FMT.format( total=float(accumulated_area) ))
    
# Compute simpson approximation to the area between fcn and the x axis
def simpson(fcn, start, end, num_boxes):
    double_num_boxes = 2*num_boxes
    box_width = (end-start)/double_num_boxes
    accumulated_area = 0
    for i in range(num_boxes):
        xi = start+2*i*box_width
        xiplus = start+(2*i+1)*box_width
        xiplusplus = start+(2*i+2)*box_width
        box_area = (f(xi)+4*f(xiplus)+f(xiplusplus))*box_width/3
        accumulated_area = accumulated_area+box_area
        print(BOX_FMT.format( i=i, anchor=float(xi), area=float(box_area)))
    print(TOT_FMT.format( total=float(accumulated_area) ))
    