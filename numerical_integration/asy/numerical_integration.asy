// numerical_integration.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "numerical_integration%03d";

import graph;

// ==== general function ====

path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);
real a = 1; real b = 4;


// ===== usual integration picture ======
picture pic;
int picnum = 0;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, (a,0)--point(f,f_a_time));
draw(pic, (b,0)--point(f,f_b_time));

// fcn
draw(pic, f, FCNPEN);

real[] T0 = {a, b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic);




// ===== Left-hand endpoint boxes ======
picture pic;
int picnum = 1;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, f, FCNPEN);

int num_boxes = 4;
// Reimann boxes
for(int i=0; i < num_boxes; ++i) {
  real x_i = a+(b-a)*(i/num_boxes);
  real x_iplus = a+(b-a)*((i+1)/num_boxes);
  real f_x_i_time = times(f, x_i)[0];
  real f_x_iplus_time = times(f, x_iplus)[0];
  path reimann_box = ((x_i,ybot)--point(f,f_x_i_time)--(x_iplus,point(f,f_x_i_time).y)--(x_iplus,ybot)--(x_i,ybot))--cycle;
  filldraw(pic, reimann_box, light_color+opacity(0.35), highlight_color);
}

real[] T1 = {a,b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T1,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ===== Left-hand endpoint boxes for y=x^2 ======
real f2(real x) { return x^2; }

picture pic;
int picnum = 2;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=9;

real a=1;
real b=3;
path f = graph(pic, f2, a-0.2, b+0.05);

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, f, FCNPEN);

int num_boxes = 4;
// Reimann boxes
for(int i=0; i < num_boxes; ++i) {
  real x_i = a+(b-a)*(i/num_boxes);
  real x_iplus = a+(b-a)*((i+1)/num_boxes);
  real f_x_i_time = times(f, x_i)[0];
  real f_x_iplus_time = times(f, x_iplus)[0];
  path reimann_box = ((x_i,ybot)--point(f,f_x_i_time)--(x_iplus,point(f,f_x_i_time).y)--(x_iplus,ybot)--(x_i,ybot))--cycle;
  filldraw(pic, reimann_box, light_color+opacity(0.35), highlight_color);
}

xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ===== Midpoint boxes for y=x^2 ======
real f3(real x) { return x^2; }

picture pic;
int picnum = 3;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=9;

real a=1;
real b=3;
path f = graph(pic, f3, a-0.2, b+0.05);

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, f, FCNPEN);

int num_boxes = 4;
// Reimann boxes
for(int i=0; i < num_boxes; ++i) {
  real x_i = a+(b-a)*(i/num_boxes);
  real x_iplus = a+(b-a)*((i+1)/num_boxes);
  real mid_i = (x_i+x_iplus)/2;
  real f_x_i_time = times(f, x_i)[0];
  real f_x_iplus_time = times(f, x_iplus)[0];
  path reimann_box = ((x_i,ybot)--(x_i,f3(mid_i))--(x_iplus,f3(mid_i))--(x_iplus,ybot)--(x_i,ybot))--cycle;
  filldraw(pic, reimann_box, light_color+opacity(0.35), highlight_color);
}

xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ===== Trapezoid boxes for y=x^2 ======
real f4(real x) { return x^2; }

picture pic;
int picnum = 4;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=9;

real a=1;
real b=3;
path f = graph(pic, f4, a-0.2, b+0.05);

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, f, FCNPEN);

int num_boxes = 4;
// Reimann boxes
for(int i=0; i < num_boxes; ++i) {
  real x_i = a+(b-a)*(i/num_boxes);
  real x_iplus = a+(b-a)*((i+1)/num_boxes);
  real f_x_i_time = times(f, x_i)[0];
  real f_x_iplus_time = times(f, x_iplus)[0];
  path reimann_box = ((x_i,ybot)--point(f,f_x_i_time)--point(f,f_x_iplus_time)--(x_iplus,ybot)--(x_i,ybot))--cycle;
  filldraw(pic, reimann_box, light_color+opacity(0.35), highlight_color);
}

xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ..... Trapezoid boxes for general function ......
picture pic;
int picnum = 5;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

real a=1;
real b=4;
path f = shift(0,2)*general_fcn;

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, f, FCNPEN);

int num_boxes = 4;
// Reimann boxes
for(int i=0; i < num_boxes; ++i) {
  real x_i = a+(b-a)*(i/num_boxes);
  real x_iplus = a+(b-a)*((i+1)/num_boxes);
  real f_x_i_time = times(f, x_i)[0];
  real f_x_iplus_time = times(f, x_iplus)[0];
  path reimann_box = ((x_i,ybot)--point(f,f_x_i_time)--point(f,f_x_iplus_time)--(x_iplus,ybot)--(x_i,ybot))--cycle;
  filldraw(pic, reimann_box, light_color+opacity(0.35), highlight_color);
}

xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ======== Simpson's method ============
picture pic;
int picnum = 6;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=0; ytop=2.2;

real a=-2;
real b=2;
path f = shift(-2.5,1.5)*general_fcn;

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, f, FCNPEN);

// Reimann boxes
real a=-1;
real b=1;
int num_boxes = 3;
real[] x;
real[] f_time;
for(int i=0; i < num_boxes; ++i) {
  x[i] = a+(b-a)*(i/(num_boxes-1));
  f_time[i] = times(f, x[i])[0];
}
path reimann_boxes = ( (x[0],ybot)
		       --point(f,f_time[0])
		       ..point(f,f_time[1])
		       ..point(f,f_time[2])
		       --(x[2],ybot)
		       --(x[0],ybot) )--cycle;
filldraw(pic, reimann_boxes, light_color+opacity(0.35), highlight_color);
draw(pic, (0,ybot)--point(f,f_time[1]), highlight_color);

// label the three points
dotfactor = 4;
dot(pic, "$(-h,y_0)$", point(f,f_time[0]), 1.5*N);
dot(pic, Label("$(0,y_1)$", filltype=Fill(white)), point(f,f_time[1]), 2.25*N);
dot(pic, "$(h,y_2)$", point(f,f_time[2]), 1.5*N);

real[] T6 = {a, b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%", T6, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x_i$", a);
labelx(pic, "$x_{i+1}$", b);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ======== Error in left hand endpoint ============
picture pic;
int picnum = 7;
size(pic,0,3cm,keepAspect=true);

path box = (0,0)--(0,2)--(1,2)--(1,0)--cycle;
path line = (-0.5,1.5)--(1.5,3.5);

draw(pic, (-0.5,0)--(1.5,0)); // axis

path region = (0,0)--(0,2)--(1,3)--(1,0)--cycle;
fill(pic, region, FILLCOLOR);

draw(pic, line, FCNPEN);
filldraw(pic, box, light_color+opacity(0.35), highlight_color);

// label the three points
dotfactor = 4;
dot(pic, "$(x_{i},y_i)$", (0,2), 1.5*NW);
dot(pic, "$(x_{i+1},y_{i+1})$", (1,3), 1.5*NW);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// =========== ===============

// picture pic;
// int picnum = 0;
// size(pic,0,3cm,keepAspect=true);
// path f = shift(0,1)*panel_fcn;

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=5;
// ybot=-1; ytop=3;

// real a = 0; real b = 4.6;

// // Draw graph paper
// for(real i=ceil(xleft); i <= floor(xright); ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ceil(ybot); j <= floor(ytop); ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, f, FCNPEN);

// real[] T0 = {b};
// xaxis(pic, L="\scriptsize $x$",  
//       axis=YZero,
//       xmin=xleft-0.25, xmax=xright+.25,
//       p=currentpen,
//       ticks=RightTicks("%", T0, Size=2pt),
//       arrow=Arrows(TeXHead));
// labelx(pic, "$b$", b);

// Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// yaxis(pic, L=L,  
//       axis=XZero,
//       ymin=ybot-0.25, ymax=ytop+0.25,
//       p=currentpen,
//       ticks=NoTicks,
//       arrow=Arrows(TeXHead));


// shipout(format(OUTPUT_FN,picnum),pic); // ,format="pdf"




