\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 11.10\hspace{1em} Taylor series}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Taylor series}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage stub_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
% \input asy/arc_length_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}


% From https://tex.stackexchange.com/a/131138/339
\newcommand\jhlongdivision[2]{%
\strut#1\kern.25em\smash{\raise.4ex\hbox{$\big)$}}\mkern-8mu
        \overline{\enspace\strut#2}}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



% \section{}

\begin{frame}{The prototypes}
We will end having derived each of our prototypes. 
\begin{center}
\begin{tabular}{c|l}
   \multicolumn{1}{c}{\tabulartext{Function}}
     &\multicolumn{1}{c}{\tabulartext{Taylor series}}  \\ \hline
   $\displaystyle \frac{1}{1-x}$ &$1+x+x^2+x^3+\cdots$ \rule{0pt}{14pt} \\[1ex]
   $\displaystyle e^x$           &$1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\cdots$  \\[1ex]
   $\displaystyle \cos x$        &$1-\frac{x^2}{2!}+\frac{x^4}{4!}-\frac{x^6}{6!}+\cdots$  \\[1ex]
   $\displaystyle \sin x$        &$x-\frac{x^3}{3!}+\frac{x^5}{5!}-\frac{x^7}{7!}+\cdots$  \\[1ex]
   $\displaystyle \ln(1+x)$      &$x-\frac{x^2}{2}+\frac{x^3}{3}-\frac{x^4}{4}+\cdots$  \\
\end{tabular}
\end{center}
\end{frame}


\begin{frame}
  \frametitle{Functions have at most one power series representation}
\Lm (Uniqueness of the power series) 
If the function $\map{f}{\R}{\R}$ has a power series representation
$f(x)=\sum_n c_n(x-a)^n$ on the interval where $|x-a|<\rho$ for 
real numbers $a$ and~$\rho$, then the coefficients of that power series
must be those from Taylor polynomials, $c_n=f^{(n)}(a)/n!\,$.
\begin{equation*}
  f(x) =f(a)+f'(a)\cdot(x-a)
       +\frac{f''(a)}{2!}\cdot(x-a)^2
       +\frac{f'''(a)}{3!}\cdot(c-a)^3+\cdots  
       =\sum_{0\leq n}\frac{f^{(n)}(a)}{n!}\cdot (x-a)^n
\end{equation*}

\Pf
Let the function have this representation
\begin{equation*}
  f(x) =c_0
       +c_1\cdot(x-a)
       +c_2\cdot(x-a)^2
       +c_3\cdot(c-a)^3
       +\cdots   
\end{equation*}
and differentiate term-by-term.
\begin{align*}
  f(x)&=c_0
       +c_1\cdot(x-a)
       +c_2\cdot(x-a)^2
       +c_3\cdot(c-a)^3
       +\cdots  \\
  f'(x)&=
       c_1
       +2c_2\cdot(x-a)
       +3c_3\cdot(c-a)
       +\cdots  \\
  f''(x)&=
       2c_2
       +6c_3\cdot(c-a)
       +\cdots  \\
       &\vdotswithin{=}
\end{align*}
Substituting $x=a$ into the first equation gives $c_0=f(a)$.
In the second it gives $c_1=f'(a)$, and in the third it gives
$c_2=f''(a)/2!$, etc. 
\end{frame}




\begin{frame}{Taylor series}
\Df Assume that 
the function~$\map{f}{\R}{\R}$ has derivatives of every order at the
anchor point~$a$, that is,
$f^{(n)}(a)$ is defined for all~$n$.
Then the \alert{Taylor series} of~$f$ about that anchor point is this.
\begin{equation*}
  T(x) =f(a)+f'(a)\cdot(x-a)
       +\frac{f''(a)}{2!}\cdot(x-a)^2
       +\cdots
       =\sum_{0\leq n}\frac{f^{(n)}(a)}{n!}\cdot (x-a)^n
\end{equation*}
When $a=0$ this is sometimes called the \alert{Maclaurin series}.
\end{frame}










\section{Verifying convergence}
\begin{frame} %{Does the Taylor series represent the function?}
The Uniqueness result says that if $f$ has a power series representation 
$f(x)=\sum_n c_n(x-a)^n$ then 
that representation must be its Taylor series.
But it does not say that every function must have a 
representation.
A given function may fail to have a Taylor series because it does not
have derivatives of all orders.

\Ex
Consider the function $f(x)=x^{2/3}$.
Its derivative $f'(x)=(2/3)x^{-1/3}$ is undefined at $x=0$. 
If we look at the anchor point $a=0$ then $f$ does not have even a first
derivative, so it does not have derivatives 
of all orders.
There is no power series about~$a$ whose function is~$f$.

\pause
Even if there is a Taylor series for~$f$
the uniqueness result does not imply that $T(x)$ must converge,
or that it must converge to~$f(x)$.

\Ex
Consider the absolute value function $f(x)=|x|$
at the anchor point $a=1$.
Here, $f(a)=1$, and $f'(1)=1$, and then
all higher derivatives are zero.
So the Taylor polynomial for $f$ at $a$ is 
$T(x)=1+1\cdot(x-1)=x$, which converges everywhere 
but does not equal~$f(x)$.

% \Ex
% Another case is the function $f(x)=e^{-1/x^2}$
% at the anchor point $a=0$.
% As $x\to 0$ we have $1/x^2\to\infty$ and so $e^{-1/x^2}\to 0$.
% Hence, we can set $f(0)=0$ and the function will be continuous.
% This function has the property that all derivatives at $a=0$ are 
% $f^{(n)}(0)=0$ (verifying this is outside of our scope).
% So the Taylor series has all terms with $c_n=0$, and 
% represents the zero function, not~$f$.

\medskip
\textit{Comment:}~there are results sufficient to imply that the 
Taylor series converges,
and converges to~$f(x)$.
Below is one
(recall that the remainder function is defined by $f(z)=T_n(z)+R_n(z)$).

\Tm If $\lim_{n\to\infty}R_n(z)=0$ 
for all $z$ in the interval where $|x-a|<\rho$  
then $f$ equals the sum of its Taylor series on that interval.
\end{frame}


% \begin{frame}{Optional: proof and examples}
% The result below is sufficient to imply that the Taylor series converges,
% and converges to~$f(x)$.
% Recall that the remainder function is defined by $f(z)=T_n(z)+R_n(z)$.

% \Tm If $\lim_{n\to\infty}R_n(z)=0$ 
% for all $z$ in the interval where $|x-a|<\rho$  
% then $f$ equals the sum of its Taylor series on that interval.

% \Pf 
% The value of the Taylor series $T(z)$ is the limit of its partial sums,
% that is, the limit of the Taylor polynomials.
% Since $R_n(z)=f(z)-T_n(z)$, 
% the Taylor series converges to $f$ if and only if the limit of the 
% remainders is zero.
  
% \pause
% \Co
% If there is a constant~$K$ where
% $|f^{(i)}(z)|\leq K$ for all orders~$i$ of derivatives, and
% for all $z$ in the interval $|x-a|<\rho$, then
% $f$ equals the sum of its Taylor series in that interval. 

% \Pf
% Recall from the Taylor polynomials section that
% the error is the absolute value of the remainder, $\varepsilon(x)=|R_n(x)|$,
% and that we have a bound.  
% \begin{equation*}
%   \varepsilon(x)\leq \frac{K}{(n+1)!}\cdot|x-a|^{n+1}
% \end{equation*}
% Because $z$ is in the interval, $|z-a|<\rho$ and so we have
% \begin{equation*}
%   |R_n(z)|\leq \frac{K}{(n+1)!}\cdot\rho^{n+1}
% \end{equation*}
% which goes to zero as $n$ goes to infinity.
% \end{frame}



% \begin{frame}
% \Ex For $f(x)=e^x$ find the Maclaurin series, 
% the Taylor series anchored at $a=0$, and also 
% find the interval of convergence.

% \pause
% We have
% \begin{equation*}
%   f(0)=e^0=1
%   \quad
%   f'(0)=1
%   \quad
%   \frac{f''(0)}{2!}=\frac{1}{2!}
%   \quad
%   \frac{f'''(0)}{3!}=\frac{1}{3!}
%   \quad
%   \ldots
% \end{equation*}
% The power series is this.
% \begin{equation*}
%   e^x
%   =1+x+\frac{1}{2!}\cdot x^2+\frac{1}{3!}\cdot x^3+\cdots
%   \;=\sum_{0\leq n} \frac{1}{n!}\cdot x^n
% \end{equation*}

% To establish convergence, 
% note that $e^x$ is increasing and so
% for any $\rho$ we have for all numbers in the
% intervl $\open{a-\rho}{a+\rho}$ the bound is $|f^{(n)}(x)|\leq e^{a+\rho}$.
% The above result gives that the Taylor series equals $f(x)$.
% Since we gave no limit for~$\rho$, we have that for all real
% numbers~$x$ the function $f(x)$ equals its Taylor series $T(x)$.
% \end{frame}



% \begin{frame}
% \Ex Find the Taylor series anchored at $a=0$ for $f(x)=\sin x$, and also 
% find its interval of convergence.

% \pause
% We have
% \begin{equation*}
%   \begin{array}{r|*{7}{c}}
%     \multicolumn{1}{r}{\ } 
%     i               &0       &1     &2       &3       &4       &5     &\ldots \\
%     \cline{2-8}
%     f^{(i)}(x)       &\sin x &\cos x &-\sin x &-\cos x &\sin x &\cos x &\ldots  \\
%     f^{(i)}(a)       &0      &1      &0       &-1      &0      &1      &\ldots \\
%   \end{array}
% \end{equation*}
% The power series is this.
% \begin{equation*}
%   \sin(x)
%   =x-\frac{1}{3!}\cdot x^3+\frac{1}{5!}\cdot x^5-\frac{1}{7!}x^7+\cdots
%   \;=\sum_{0\leq n} (-1)^n\frac{1}{(2n+1)!}\cdot x^{2n+1}
% \end{equation*}

% To establish convergence, 
% for any $\rho$ we have for all numbers in the
% interval $\open{a-\rho}{a+\rho}$ the bound is $|f^{(n)}(x)|\leq 1$.
% The above result gives that the Taylor series equals $f(x)$.
% % Again we needed no restriction on~$\rho$ and so for all real
% % numbers~$x$ the function $f(x)$ equals its Taylor series $T(x)$.
% \end{frame}




\section{Computing Taylor series}

\begin{frame}
For the functions that we work with, the Taylor series converges to the
given function.
For these, the computations are straightforward.
%\end{frame}



%\begin{frame}
\Ex Find the Taylor series for $f(x)=e^x$ at the anchor point $a=0$,
and find its interval of convergence.
\pause
\begin{center}
  \begin{tabular}{r|*{6}{c}}
    \multicolumn{1}{c}{\ }&$0$ &$1$ &$2$ &$3$  &\ldots &$n$ \\ 
    \cline{2-7}
    $f^{(n)}(x)$ &$e^x$ &$e^x$ &$e^x$ &$e^x$ &\ldots &$e^x$ \\
    $f^{(n)}(a)$ &$1$   &$1$  &$1$   &$1$   &\ldots &$1$ 
  \end{tabular}
\end{center}  
Here is the Taylor series.
\begin{align*}
  T(x) &= f(a)+f'(a)(x-a)+\frac{f''(a)}{2!}(x-a)^2+\frac{f'''(a)}{3!}(x-a)^3
          +\cdots  \\
       &=1+x+\frac{1}{2!}x^2+\frac{1}{3!}x^3+\frac{1}{4!}x^4+\cdots \\
       &=\sum_{0\leq n}\frac{1}{n!}\cdot x^n
\end{align*}
For its radius of convergence~$\rho$ take the limit of the ratio of coefficients.
\begin{equation*}
  \frac{1}{\rho}
  =\lim_{n\to\infty}\biggl| \frac{c_{n+1}}{c_n} \biggr|
  =\lim_{n\to\infty}\biggl| \frac{1/(n+1)!}{1/n!} \biggr|
  =\lim_{n\to\infty}\biggl| \frac{1}{n+1} \biggr|
  =0
\end{equation*}
The interval of convergence is all of the real line,
$\open{-\infty}{\infty}$.
\end{frame}



\begin{frame}
\Ex Find the Taylor series for $f(x)=\ln(x)$ at the anchor point $a=1$
(recall that $\ln(0)$ is undefined).
Also find its interval of convergence.
\pause
\begin{center}
  \begin{tabular}{r|*{7}{c}}
    \multicolumn{1}{c}{\ }&$0$ &$1$ &$2$ &$3$ &$4$ &\ldots &$n$ \\ 
    \cline{2-8}
    $f^{(n)}(x)$ &$\ln(x)$ &$x^{-1}$ &$-x^{-2}$ &$2x^{-3}$ &$-6x^{-4}$  &\ldots &$(-1)^{n-1}(n-1)!x^{-n}$ \\
    $f^{(n)}(1)$ &$0$      &$1$     &$-1$     &$2$      &$-6$        &\ldots &$(-1)^{n-1}(n-1)!$ 
  \end{tabular}
\end{center}
Here is the Taylor series.
\begin{align*}
  T(x) &= f(1)+f'(1)(x-1)+\frac{f''(1)}{2!}(x-1)^2+\frac{f'''(1)}{3!}(x-1)^3
          +\cdots  \\
       &=0+1\cdot(x-1)-\frac{1}{2!}\cdot(x-1)^2+\frac{2}{3!}\cdot(x-1)^3-\frac{3!}{4!}\cdot(x-1)^4+\cdots \\
       &=\sum_{1\leq n}(-1)^{n+1}\frac{1}{n}\cdot (x-1)^n
  \tag{$*$}
\end{align*}
This gives the radius of convergence~$\rho$.
\begin{equation*}
  \frac{1}{\rho}
  =\lim_{n\to\infty}\biggl| \frac{c_{n+1}}{c_n} \biggr|
  =\lim_{n\to\infty}\biggl| \frac{1/(n+1)}{1/n} \biggr|
  =\lim_{n\to\infty}\biggl| \frac{n}{n+1} \biggr|
  =1
\end{equation*}
As to the boundary, at $x=0$ the series ($*$) 
is the negative of the harmonic series and so diverges.
At $x=2$ it is the alternating harmonic series and so converges.
The interval of convergence is $\rightclosed{0}{2}$.
\end{frame}





\begin{frame}
\Ex Find the Taylor series anchored at $a=0$ for $f(x)=\sin x$ and also 
find its interval of convergence.
\pause
\begin{equation*}
  \begin{array}{r|*{7}{c}}
    \multicolumn{1}{r}{$i$} 
                    &0       &1     &2       &3       &4       &5     &\ldots \\
    \cline{2-8}
    f^{(i)}(x)       &\sin x &\cos x &-\sin x &-\cos x &\sin x &\cos x &\ldots  \\
    f^{(i)}(a)       &0      &1      &0       &-1      &0      &1      &\ldots \\
  \end{array}
\end{equation*}
The Taylor series is this.
\begin{align*}
  \sin(x)
  &=x-\frac{1}{3!}\cdot x^3+\frac{1}{5!}\cdot x^5-\frac{1}{7!}x^7+\cdots  \\
  &=\sum_{0\leq n} (-1)^{n}\frac{1}{(2n+1)!}\cdot x^{2n+1}
\end{align*}
Here is the radius of convergence.
\begin{align*}
  \frac{1}{\rho}
  =\lim_{n\to\infty}\biggl| \frac{c_{n+1}}{c_n} \biggr|
  &=\lim_{n\to\infty}\biggl| \frac{(-1)^{n+1}/(2n+3)!}{(-1)^n/(2n+1)!} \biggr|  \\
  &=\lim_{n\to\infty}\frac{1/(2n+3)!}{1/(2n+1)!}
  =\lim_{n\to\infty}\frac{1\cdot 2\cdots (2n+1)}{1\cdot 2\cdots (2n+1)\cdot (2n+2)\cdot(2n+3)} \\
  &=\lim_{n\to\infty}\frac{1}{(2n+2)\cdot(2n+3)} 
  =0
\end{align*}
The interval of convergence is the real line, $\open{-\infty}{\infty}$.
\end{frame}



\begin{frame}
\Ex Find the Taylor series anchored at $a=0$ for $f(x)=\cos x$ and also 
find its interval of convergence.
\pause
\begin{equation*}
  \begin{array}{r|*{7}{c}}
    \multicolumn{1}{r}{\ } 
    i               &0       &1     &2       &3       &4       &5     &\ldots \\
    \cline{2-8}
    f^{(i)}(x)       &\cos x &-\sin x &-\cos x &\sin x &\cos x &-\sin x &\ldots  \\
    f^{(i)}(a)       &1      &0      &-1       &0      &1      &0      &\ldots \\
  \end{array}
\end{equation*}
The Taylor series is this.
\begin{align*}
  \cos(x)
  &=1-\frac{1}{2!}\cdot x^2+\frac{1}{4!}\cdot x^4-\frac{1}{6!}\cdot x^6+\cdots  \\
  &=\sum_{0\leq n} (-1)^{n}\frac{1}{(2n)!}\cdot x^{2n}
\end{align*}
Here is the radius of convergence.
\begin{equation*}
  \frac{1}{\rho}
  =\lim_{n\to\infty}\biggl| \frac{c_{n+1}}{c_n} \biggr|
  =\lim_{n\to\infty}\biggl| \frac{(-1)^{n+1}/(2n+2)!}{(-1)^n/(2n)!} \biggr|
  =\lim_{n\to\infty}\frac{1}{(2n+1)\cdot(2n+2)} 
  =0
\end{equation*}
The interval of convergence is the real line, $\open{-\infty}{\infty}$.
\end{frame}



\begin{frame}
\Ex Find the Taylor series for the polynomial 
$f(x)=3x^2+2x+1$ about $a=0$ and also 
find its interval of convergence.
\pause
\begin{equation*}
  \begin{array}{r|*{6}{c}}
    \multicolumn{1}{r}{$i$} 
                    &0         &1     &2       &3       &4        &\ldots \\
    \cline{2-7}
    f^{(i)}(x)       &3x^2+2x+1 &6x+2  &6      &0       &0         &\ldots  \\
    f^{(i)}(a)       &1        &2      &6      &0      &0          &\ldots \\
  \end{array}
\end{equation*}
The Taylor series is this.
\begin{align*}
  T(x) 
  &= f(a)+f'(a)\cdot(x-a)+\frac{f''(a)}{2!}\cdot(x-a)^2+\frac{f'''(a)}{3!}\cdot(x-a)^3
          +\cdots  \\
  &=1 +2\cdot x+\frac{6}{2!}\cdot x^2+\frac{0}{3!}\cdot x^3+\frac{0}{4!}\cdot x^4+\cdots  
\end{align*}
Because there are finitely many summands, this converges for all~$x$.
The polynomial is the Taylor series of itself.
\end{frame}


% sage: def T(x):
% ....:     return (1/2)+(1/4)*(x+1)+(1/16)*(x+1)^2+(1/(16*6))*(x+1)^3
% ....: 
% sage: plot(T, -3,2)
% Launched png viewer for Graphics object consisting of 1 graphics primitive
% sage: def f(x):
% ....:     return 1/(1-x)
% ....: 
% sage: plot([T,f], -3,2)



\begin{frame}{Practice: new series from old}
Find the Taylor series for each function.

\begin{enumerate}
\item For $f(x)=x\cdot\ln(1+x)$ centered at $a=0$.

\pause\smallskip
Here is the Taylor series for $\ln(1+x)$ centered at $a=0$,
and then the same multiplied by~$x$.  
\begin{align*}
  \ln(1+x)
  &=x
  -\frac{1}{2}\cdot x^2
  +\frac{1}{3}\cdot x^3
  -\frac{1}{4}\cdot x^4
  +\cdots               \\
  x\cdot\ln(1+x)
  &=x^2
  -\frac{1}{2}\cdot x^3
  +\frac{1}{3}\cdot x^4
  -\frac{1}{4}\cdot x^5
  +\cdots
   =\sum_{1\leq n}(-1)^{n+1}\frac{1}{n}\cdot x^{n+1}
\end{align*}

\item For $f(x)=e^{2x}$ centered at $a=0$.

\pause\smallskip
Here is the Taylor series for $e^x$ centered at $a=0$, and below it 
we have substituted $2x$.  
\begin{align*} 
   1+x+\frac{1}{2!}\cdot x^2+\frac{1}{3!}\cdot x^3+\frac{1}{4!}\cdot x^4+\cdots 
   &=\sum_{0\leq n}\frac{1}{n!}\cdot x^n   \\
   1+2x+\frac{1}{2!}\cdot (2x)^2+\frac{1}{3!}\cdot (2x)^3+\frac{1}{4!}\cdot (2x)^4+\cdots 
   &=\sum_{0\leq n}\frac{2^n}{n!}\cdot x^n
\end{align*}
\end{enumerate}
\end{frame}



% \begin{frame}
% \Ex Find the Taylor series for $f(x)=x\cdot\ln(1+x)$ centered at $a=0$.

% \pause
% The Taylor series for $\ln(1+x)$ centered at $a=0$ is this.  
% \begin{equation*}
%   \ln(1+x)
%   =x
%   -\frac{1}{2}\cdot x^2
%   +\frac{1}{3}\cdot x^3
%   -\frac{1}{4}\cdot x^4
%   +\cdots
% \end{equation*}
% Multiplying through by $x$ gives this.
% \begin{equation*}
%   x\cdot\ln(1+x)
%   =x^2
%   -\frac{1}{2}\cdot x^3
%   +\frac{1}{3}\cdot x^4
%   -\frac{1}{4}\cdot x^5
%   +\cdots
%    =\sum_{1\leq n}(-1)^{n+1}\frac{1}{n}\cdot x^{n+1}
% \end{equation*}

% \pause
% \Ex Find the Taylor polynomial for $f(x)=e^{2x}$ centered at $a=0$.

% \pause
% The Taylor series for $e^x$ centered at $a=0$ is this.  
% \begin{equation*} 
%    1+x+\frac{1}{2!}\cdot x^2+\frac{1}{3!}\cdot x^3+\frac{1}{4!}\cdot x^4+\cdots 
%    =\sum_{0\leq n}\frac{1}{n!}\cdot x^n
% \end{equation*}
% Substituting $2x$ gives this.
% \begin{equation*}
%    1+2x+\frac{1}{2!}\cdot (2x)^2+\frac{1}{3!}\cdot (2x)^3+\frac{1}{4!}\cdot (2x)^4+\cdots 
%    =\sum_{0\leq n}\frac{2^n}{n!}\cdot x^n
% \end{equation*}
% \end{frame}

% \begin{frame}
% \Ex Find the Taylor polynomial for $f(x)=\cos\sqrt{x}$ centered at $a=0$.

% \pause
% The Taylor series for $\cos x$ centered at $a=0$ is this.  
% \begin{equation*}  
%   1-\frac{1}{2!}\cdot x^2+\frac{1}{4!}\cdot x^4-\frac{1}{6!}x^6+\cdots  
%   =\sum_{0\leq n} (-1)^{n}\frac{1}{(2n)!}\cdot x^{2n}
% \end{equation*}
% Substituting $\sqrt{x}$ gives this.
% \begin{equation*}
%   1-\frac{1}{2!}\cdot x+\frac{1}{4!}\cdot x^2-\frac{1}{6!}x^3+\cdots  
%   =\sum_{0\leq n} (-1)^{n}\frac{1}{(2n)!}\cdot x^{n}
% \end{equation*}
% \end{frame}


\section{Extra: multiplication and division}
\begin{frame}
Taylor series are in some ways like polynomials, only of infinite degree.
We can multiply and divide them as we do with polynomials.

\Ex
We illustrate multiplication by finding the Taylor series for 
$e^x\cdot \sin x$.
\begin{equation*}
  \biggl(1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\cdots \biggr)
    \biggl(x-\frac{x^3}{3!}+\frac{x^5}{5!}+\cdots \biggr)
\end{equation*}
In the series resulting from the product, what is the constant term?
There is no combinations of a term on the left with one on the right
that gives a constant.

The linear term?
There is only one way to get a linear term, taking $1$ from the left
with $x$ from the right to get $1\cdot x$.

\pause
The quadratic term?
There is one way to get a quadratic, $x\cdot x$.

Cubic?
There are two ways to get a cubic, 
$1\cdot (-x^3/3!)$ and $(x^2/2!)\cdot x$.
Continuing in this way we get the Taylor series.
\begin{equation*}
  e^x\sin x=x+x^2+\frac{1}{3}x^3+\cdots
\end{equation*}
\end{frame}


\begin{frame}
\Ex To illustrate division, we derive that $\tan x=\sin x/\cos x$.
\begin{equation*}
  \tan x =\frac{x-\frac{1}{3!}x^3+\frac{1}{5!}x^5-\cdots}{1-\frac{1}{2!}x^2+\frac{1}{4!}x^4+\cdots}
\end{equation*}
Here is the synthetic division.
\begin{equation*}
  \begin{array}{r@{}l}
          &\quad x+\frac{1}{3}x^3+\frac{2}{15}x^5+\cdots            \\
    1-\frac{1}{2}x^2+\frac{1}{24}x^4-\cdots   
          &\jhlongdivision{}{x-\frac{1}{6}x^3+\frac{1}{120}x^5-\cdots}   \\
          &\hspace*{-3pt}\underline{-(x-\frac{1}{2}x^3+\frac{1}{24}x^5-\cdots)}           \\
          &\hspace*{25pt}\frac{1}{3}x^3-\frac{1}{30}x^5+\cdots            \\
          &\hspace*{14pt}\underline{-(\frac{1}{3}x^3-\frac{1}{6}x^5+\cdots)}           \\
          &\hspace*{46pt}\frac{2}{15}x^5+\cdots                          \\
          &\hspace*{56pt}\vdots          
  \end{array}
\end{equation*}

\end{frame}





\section{Extra: extending the Binomial Theorem to any real power}
\begin{frame}
Recall the Binomial Theorem.
Here are the first few natural number powers of $(1+x)^r\!$.
\begin{gather*}
  (1+x)^0 
  = 1 \\
  (1+x)^1 
  = 1+x \\
  (1+x)^2 
  = 1+2x+x^2 \\
  (1+x)^3 
  = 1+3x+3x^2+x^3 \\
  (1+x)^4 
  = 1+4x+6x^2+4x^3+x^4 \\
\end{gather*}
On each line the powers of $x$ go from $k=0$ to~$k=r$. 
The coefficients form \alert{Pascal's triangle}.
\begin{gather*}
  1 \\
  1\quad 1 \\
  1\quad 2\quad 1 \\
  1\quad 3\quad 3\quad 1 \\
  1\quad 4\quad 6\quad 4\quad 1 
\end{gather*}
On line~$r+1$, get entry~$k$ by adding the two above it:~line~$r$'s 
entry~$k-1$ and entry~$k$.
\end{frame}


\begin{frame}
Here is a copy of Pascal's triangle.
\begin{gather*}
  1 \\
  1\quad 1 \\
  1\quad 2\quad 1 \\
  1\quad 3\quad 3\quad 1 \\
  1\quad 4\quad 6\quad 4\quad 1 
\end{gather*}
This is the formula for  entry~$k$ of row~$r$, that is, for the
coefficient of $x^k$ in the expansion of $(1+x)^k$
\begin{equation*}
  \binom{r}{k}=\frac{r\cdot (r-1)\cdot(r-2)\cdots (r-k+1)}{k!}
\end{equation*}
(sometimes written $r!/(k!(r-k)!$).
Read the symbol on the left aloud as ``\alert{$r$ choose $k$}.''
Thus the $6$ in the bottom row of the Pascal's triangle above is 
$\binom{4}{2}=4\cdot 3/2\cdot 1$.

\Tm (Binomial Theorem)
For natural number powers~$r$, this holds.
\begin{equation*}
  (1+x)^r=\sum_{0\leq i\leq r}\binom{r}{k}x^k
\end{equation*}
\end{frame}

\begin{frame}{Powers~$r$ that are not natural numbers}
Newton generalized the Binomial Theorem to non-natural number~$r$'s.
Take $r$ to be any real number, not just a natural.
Expand $f(x)=(1+x)^r$ about $a=0$.
\begin{multline*}\arraycolsep=3pt
  \begin{array}{r|*{4}{c}}
    \multicolumn{1}{r}{i} 
                   &0         
                   &1     &2       &\ldots                       \\
    \cline{2-5}
    f^{(i)}(x)       &(1+x)^r  
                   &r\cdot(1+x)^{r-1}  &r(r-1)\cdot(1+x)^{r-2}  &\ldots     \\
    f^{(i)}(0)       &1        
                    &r                &r(r-1)                 &\ldots     \\
  \end{array}                \\  
  \begin{array}{r|*{2}{c}}
    \multicolumn{1}{r}{\ } 
                   &\ldots &k     \\
    \cline{2-3}
    f^{(i)}(x)      &\ldots  &r(r-1)(r-2)\cdots(r-k+1)\cdot(1+x)^{r-k}   \\
    f^{(i)}(0)      &\ldots  &r(r-1)(r-2)\cdots(r-k+1)   \\
  \end{array}
\end{multline*}
The Taylor series centered at $a=0$ is this.
\begin{align*}
  T(x) 
  &= f(a)+f'(a)(x-a)+\frac{f''(a)}{2!}(x-a)^2+\frac{f'''(a)}{3!}(x-a)^3
          +\cdots  \\
  &=1 +r\cdot x+\frac{r\cdot(r-1)}{2!}\cdot x^2+\frac{r(r-1)(r-2)}{3!}\cdot x^3+\cdots   \\
  &=\sum_{0\leq k} \binom{r}{k}\cdot x^k  
\end{align*}
The radius of convergence is $\rho=1$.
\end{frame}

\begin{frame}
In the special case that
$r$ is a natural number, when we take the sequence
\begin{equation*}
  r, \quad r(r-1),\quad r(r-1)(r-2)\quad \cdots
\end{equation*}
at some point we have a multiplicand that is zero.
From then on every $\binom{r}{k}$ is zero and so every term is zero.
But if $r$ is not a natural number then the coefficients $\binom{r}{k}$ are
never zero, so the Taylor series is infinite.

\Ex
Here is the expansion of the one-half power, $f(x)=\sqrt{1+x}$.
\begin{align*}
  &1+\frac{1}{2}\cdot x+\frac{(1/2)(-1/2)}{2!}\cdot x^2+\frac{(1/2)(-1/2)(-3/2)}{3!}x^3+\cdots  \\
  &=1+\frac{1}{2}\cdot x-\frac{1}{8}\cdot x^2+\frac{1}{16}x^3-\frac{5}{128}\cdot x^4+\frac{7}{256}\cdot x^5+\cdots  \\
\end{align*}
\end{frame}

\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
