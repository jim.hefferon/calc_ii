// trig_subst.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "trig_subst%03d";

import graph;



// =========== quarter circle ===============
real f0(real x) {
  return(sqrt(1-x^2));
}

picture pic;
int picnum = 0;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1;
ybot=0; ytop=1;

real a = 0; real b = 1;
path f = graph(pic, f0, a-0.1, b);

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.1)--(i,ytop+0.1),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.2,j)--(xright+0.2,j),GRAPHPAPERPEN);
  }
}

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ( point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(0,0)--point(f,f_a_time) )--cycle;
fill(pic, region, FILLCOLOR);

// slice
real x = a+ 0.618*(b-a);
real f_x_time = times(f, x)[0];
draw(pic, (x,0)--point(f, f_x_time), highlight_color);

draw(pic, f, FCNPEN);
  
real[] T0 = {x};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%", T0, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", x);

// Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.1, ymax=ytop+0.1,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ......... quarter ellipse ............
picture pic;
int picnum = 1;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

real ellipse_semimajor = 1.5;
real ellipse_semiminor = 1.25;
real a = 0; real b = ellipse_semimajor;

path f = scale(ellipse_semimajor,ellipse_semiminor)*graph(pic, f0, a, 1);

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.1)--(i,ytop+0.1),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.2,j)--(xright+0.2,j),GRAPHPAPERPEN);
  }
}

// region to be integrated  
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ( point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(0,0)--point(f,f_a_time) )--cycle;
fill(pic, region, FILLCOLOR);

// slice
real x = a+ 0.6*(b-a);
real f_x_time = times(f, x)[0];
draw(pic, (x,0)--point(f, f_x_time), highlight_color);

draw(pic, f, FCNPEN);
  
real[] T1 = {x, ellipse_semimajor};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%", T1, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", x);
labelx(pic, "$(a,0)$", ellipse_semimajor);

real[] T1a = {ellipse_semiminor};
// Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.1,
      p=currentpen,
      ticks=LeftTicks("%", T1a, Size=2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$(0,b)$", ellipse_semiminor);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");






// ========= trig substitution right triangles ====

// ....... sqrt(a^2-x^2) ..........
picture pic;
int picnum = 2;
size(pic,0,1.75cm,keepAspect=true);

real theta = radians(25);
path triangle = (0,0)--(cos(theta),0)--(cos(theta),sin(theta))--cycle;

label(pic, "$a$", midpoint(subpath(triangle,2,3)), NW);
label(pic, "$x$", midpoint(subpath(triangle,1,2)), E);
label(pic, "$\sqrt{a^2-x^2}$", midpoint(subpath(triangle,0,1)), S);

// right angle mark
path right_angle_mark = scale(0.06)*shift(-1,0)*subpath(unitsquare,2,4);
draw(pic, shift(cos(theta),0)*right_angle_mark, gray_color);

// theta arc
path theta_arc = arc((0,0), 0.25, 0, degrees(theta), CCW);
draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

// Overwrite other lines
draw(pic, triangle, FCNPEN+miterjoin);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... sqrt(a^2+x^2) ..........
picture pic;
int picnum = 3;
size(pic,0,1.75cm,keepAspect=true);

real theta = radians(25);
path triangle = (0,0)--(cos(theta),0)--(cos(theta),sin(theta))--cycle;

label(pic, "$\sqrt{a^2+x^2}$", midpoint(subpath(triangle,2,3)), NW);
label(pic, "$x$", midpoint(subpath(triangle,1,2)), E);
label(pic, "$a$", midpoint(subpath(triangle,0,1)), S);

// right angle mark
path right_angle_mark = scale(0.06)*shift(-1,0)*subpath(unitsquare,2,4);
draw(pic, shift(cos(theta),0)*right_angle_mark, gray_color);

// theta arc
path theta_arc = arc((0,0), 0.25, 0, degrees(theta), CCW);
draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

// Overwrite other lines
draw(pic, triangle, FCNPEN+miterjoin);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... sqrt(x^2-a^2) ..........
picture pic;
int picnum = 4;
size(pic,0,1.75cm,keepAspect=true);

real theta = radians(25);
path triangle = (0,0)--(cos(theta),0)--(cos(theta),sin(theta))--cycle;

label(pic, "$x$", midpoint(subpath(triangle,2,3)), NW);
label(pic, "$\sqrt{x^2-a^2}$", midpoint(subpath(triangle,1,2)), E);
label(pic, "$a$", midpoint(subpath(triangle,0,1)), S);

// right angle mark
path right_angle_mark = scale(0.06)*shift(-1,0)*subpath(unitsquare,2,4);
draw(pic, shift(cos(theta),0)*right_angle_mark, gray_color);

// theta arc
path theta_arc = arc((0,0), 0.25, 0, degrees(theta), CCW);
draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

// Overwrite other lines
draw(pic, triangle, FCNPEN+miterjoin);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ========= trig substitution examples right triangles ====

// ....... sqrt(16-x^2) ..........
picture pic;
int picnum = 5;
size(pic,0,2cm,keepAspect=true);

real theta = radians(25);
path triangle = (0,0)--(cos(theta),0)--(cos(theta),sin(theta))--cycle;

label(pic, "$4$", midpoint(subpath(triangle,2,3)), NW);
label(pic, "$x$", midpoint(subpath(triangle,1,2)), E);
label(pic, "$\sqrt{16-x^2}$", midpoint(subpath(triangle,0,1)), S);

// right angle mark
path right_angle_mark = scale(0.06)*shift(-1,0)*subpath(unitsquare,2,4);
draw(pic, shift(cos(theta),0)*right_angle_mark, gray_color);

// theta arc
path theta_arc = arc((0,0), 0.25, 0, degrees(theta), CCW);
draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

// Overwrite other lines
draw(pic, triangle, FCNPEN+miterjoin);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... sqrt( 4x^2+36x ) ..........
picture pic;
int picnum = 6;
size(pic,0,2cm,keepAspect=true);

real theta = radians(25);
path triangle = (0,0)--(cos(theta),0)--(cos(theta),sin(theta))--cycle;

label(pic, "$\sqrt{x^2+9}$", midpoint(subpath(triangle,2,3)), NW);
label(pic, "$x$", midpoint(subpath(triangle,1,2)), E);
label(pic, "$3$", midpoint(subpath(triangle,0,1)), S);

// right angle mark
path right_angle_mark = scale(0.06)*shift(-1,0)*subpath(unitsquare,2,4);
draw(pic, shift(cos(theta),0)*right_angle_mark, gray_color);

// theta arc
path theta_arc = arc((0,0), 0.25, 0, degrees(theta), CCW);
draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

// Overwrite other lines
draw(pic, triangle, FCNPEN+miterjoin);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... sqrt( 4x^2+36x ) ..........
picture pic;
int picnum = 7;
size(pic,0,2cm,keepAspect=true);

real theta = radians(25);
path triangle = (0,0)--(cos(theta),0)--(cos(theta),sin(theta))--cycle;

label(pic, "$\sqrt{(x-3)^2+4}$", midpoint(subpath(triangle,2,3)), NW);
label(pic, "$x-3$", midpoint(subpath(triangle,1,2)), E);
label(pic, "$2$", midpoint(subpath(triangle,0,1)), S);

// right angle mark
path right_angle_mark = scale(0.06)*shift(-1,0)*subpath(unitsquare,2,4);
draw(pic, shift(cos(theta),0)*right_angle_mark, gray_color);

// theta arc
path theta_arc = arc((0,0), 0.25, 0, degrees(theta), CCW);
draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

// Overwrite other lines
draw(pic, triangle, FCNPEN+miterjoin);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... 1/sqrt( 4-x^2 ) ..........
picture pic;
int picnum = 8;
size(pic,0,2cm,keepAspect=true);

real theta = radians(25);
path triangle = (0,0)--(cos(theta),0)--(cos(theta),sin(theta))--cycle;

label(pic, "$2$", midpoint(subpath(triangle,2,3)), NW);
label(pic, "$x$", midpoint(subpath(triangle,1,2)), E);
label(pic, "$\sqrt{4-x^2}$", midpoint(subpath(triangle,0,1)), S);

// right angle mark
path right_angle_mark = scale(0.06)*shift(-1,0)*subpath(unitsquare,2,4);
draw(pic, shift(cos(theta),0)*right_angle_mark, gray_color);

// theta arc
path theta_arc = arc((0,0), 0.25, 0, degrees(theta), CCW);
draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

// Overwrite other lines
draw(pic, triangle, FCNPEN+miterjoin);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... sqrt( x^2-4 )/x ..........
picture pic;
int picnum = 9;
size(pic,0,2cm,keepAspect=true);

real theta = radians(25);
path triangle = (0,0)--(cos(theta),0)--(cos(theta),sin(theta))--cycle;

label(pic, "$x$", midpoint(subpath(triangle,2,3)), NW);
label(pic, "$\sqrt{x^2-4}$", midpoint(subpath(triangle,1,2)), E);
label(pic, "$2$", midpoint(subpath(triangle,0,1)), S);

// right angle mark
path right_angle_mark = scale(0.06)*shift(-1,0)*subpath(unitsquare,2,4);
draw(pic, shift(cos(theta),0)*right_angle_mark, gray_color);

// theta arc
path theta_arc = arc((0,0), 0.25, 0, degrees(theta), CCW);
draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

// Overwrite other lines
draw(pic, triangle, FCNPEN+miterjoin);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... x^3 sqrt(5+x^2) ..........
picture pic;
int picnum = 10;
size(pic,0,2cm,keepAspect=true);

real theta = radians(25);
path triangle = (0,0)--(cos(theta),0)--(cos(theta),sin(theta))--cycle;

label(pic, "$\sqrt{x^2+5}$", midpoint(subpath(triangle,2,3)), NW);
label(pic, "$x$", midpoint(subpath(triangle,1,2)), E);
label(pic, "$\sqrt{5}$", midpoint(subpath(triangle,0,1)), S);

// right angle mark
path right_angle_mark = scale(0.06)*shift(-1,0)*subpath(unitsquare,2,4);
draw(pic, shift(cos(theta),0)*right_angle_mark, gray_color);

// theta arc
path theta_arc = arc((0,0), 0.25, 0, degrees(theta), CCW);
draw(pic, "$\theta$", theta_arc, Arrow(ARROW_SIZE));

// Overwrite other lines
draw(pic, triangle, FCNPEN+miterjoin);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





