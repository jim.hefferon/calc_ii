// Ellipse with axes
include "../../asy/jh.asy";
// picture size
real height; height=2.25cm; size(0,height);
// the ellipse
pair A=(0,0);
real r=1;
real majoraxis=2;
real minoraxis=1.5;
pair B=scale(majoraxis,minoraxis)*(r,0);
pair XULIMIT=xscale(1.4)*B;
pair XLLIMIT=(-.4*r,0);
path quartercircle=arc(A,r,0,90);
path quarterellipse=scale(majoraxis,minoraxis)*quartercircle;
path area=quarterellipse--A--B--cycle;
fill(area,evenodd+FILLCOLOR);
draw(quarterellipse--A,FCNPEN);
// the x axis
draw(XULIMIT--XLLIMIT,AXISPEN,Arrows);
// the dx-wide box
real X=0.4;
pair b=scale(majoraxis,minoraxis)*(X,0);
pair t=scale(majoraxis,minoraxis)*(X,sin(acos(X)));
draw(b--t,DXPEN);
label("$x$",b,S);
