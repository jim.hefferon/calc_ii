// Half circle with axes
import geometry;
include "../../asy/jh.asy";
// texpreamble("\usepackage{conc}");
real height; height=2.25cm; size(0,height);

point A=(0,0);
real r; r=1;
point B=(r,0);
point XULIMIT=(1.4*r,0);
point XLLIMIT=(-.4*r,0);
circle C=circle(A,r);
arc AC=arc(C,0,180);
arc ACH=arc(C,0,90);
// label("\scriptsize $(1,0)$",B,S);
//  label("\scriptsize $(-1,0)$",-1*B,S);
path halfcircle=AC..-1*B--B--cycle;
path quartercircle=ACH--A--B--cycle;
fill(quartercircle,evenodd+FILLCOLOR);
draw(XULIMIT--XLLIMIT,AXISPEN,Arrows);
draw(ACH--A,FCNPEN);
real X=0.4;
pair b=(X,0);
pair t=(X,sin(acos(X)));
draw(b--t,DXPEN);
label("$x$",b,S);
