\makeatletter\let\ifGm@compatii\relax\makeatother 
\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
% \usepackage{present}
% \usepackage{../calcii}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 7.3: Trigonometric Substitution}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}


\usepackage{siunitx}

\subject{Trigonometric substitution}
% This is only inserted into the PDF information catalog. Can be left
% out. 

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}
\frametitle{Integrals with radicals}  
To find the area of a unit circle it is natural to do this.
\begin{equation*}
  4\cdot\integral{x=0}{1}{\sqrt{1-x^2}}{x}
  \qquad
  \vcenteredhbox{\includegraphics{asy/trig_subst000.pdf}}
\end{equation*}
\pause
To find the area of an ellipse $x^2/a^2+y^2/b^2=1$ we can 
rewrite the relation as a function, $y=b\sqrt{1-(x^2/a^2)}$, and 
do this.
\begin{equation*}
  4\cdot\integral{x=0}{a}{(b/a)\sqrt{a^2-x^2}}{x}
  \qquad
  \vcenteredhbox{\includegraphics{asy/trig_subst001.pdf}}
\end{equation*}
\end{frame}



\begin{frame}
How to deal with radicals?
\begin{center}
  \includegraphics[height=0.2\textheight]{pix/che.jpg}
\end{center}
\pause
That's not what we mean.
We mean that 
antidifferentiating is complicated by the radical.
\begin{equation*}
  \integral{}{}{\sqrt{1-x^2}}{x}
\end{equation*}
\begin{itemize}
\item Everyone knows that $\sqrt{1-x^2}\neq \sqrt{1}-\sqrt{x^2}$, for instance 
when $x=1/2$.
\pause
\item Trying $u=x^2$ gives $\d{u}=2x\;d{x}$.
  This does not help because we don't have an $x$ outside the radical.
\end{itemize}
\end{frame}



\begin{frame}
For help we could look to 
derivatives we have already seen with  
radicals.
\begin{center}
  \begin{tabular}{l@{\hspace{3em}}l}
    $\displaystyle \frac{d\,\sin^{-1}x}{dx}=\frac{1}{\sqrt{1-x^2}} $  
      &$\displaystyle \frac{d\,\cos^{-1}x}{dx}=-\frac{1}{\sqrt{1-x^2}} $  
   \\[2.5ex]
    $\displaystyle \frac{d\,\tan^{-1}x}{dx}=\frac{1}{1+x^2} $  
      &$\displaystyle \frac{d\,\cot^{-1}x}{dx}=-\frac{1}{1+x^2} $  
   \\[2.5ex]
    $\displaystyle \frac{d\,\sec^{-1}x}{dx}=\frac{1}{x\,\sqrt{x^2-1}} $  
      &$\displaystyle \frac{d\,\csc^{-1}x}{dx}=-\frac{1}{x\,\sqrt{x^2-1}} $  
  \end{tabular}
\end{center}

\pause
\Ex
To deal with the radical in this integral
\begin{equation*}
  \integral{}{}{\frac{1}{\sqrt{1-x^2}}}{x}
\end{equation*}
do the substitution $x=\sin\theta$ so that
$\d{x}=\cos\theta\;d{\theta}$. 
\begin{multline*}
  \integral{}{}{\frac{1}{\sqrt{1-x^2}}}{x}
  =\integral{}{}{\frac{1}{\sqrt{1-(\sin\theta)^2}}\cdot\cos\theta}{\theta} 
  =\integral{}{}{\frac{\cos\theta}{\sqrt{(\cos\theta)^2}}}{\theta}   \\
  =\integral{}{}{1}{\theta} 
  =\theta+C \
  =\sin^{-1} x+C
\end{multline*}
\textit{Remark.}
The square roots and trig functions mean that we should think about the domain.
We wrote $\sqrt{(\cos\theta)^2}=\cos\theta$, which holds when 
$\cos\theta \geq 0$.
That implies $-\pi/2\leq \theta\leq\pi/2$.
That implies $-1\leq x\leq 1$, which is true in the starting integral.
\end{frame}


\begin{frame}
\frametitle{Leveraging substituting a trig function}
Here again is the area of a quarter circle integral.
\begin{equation*}
  \integral{x=0}{x=1}{\sqrt{1-x^2}}{x}
  \qquad
  \vcenteredhbox{\includegraphics{asy/trig_subst000.pdf}}
\end{equation*}
Take $x=\sin\theta$ so $\d{x}=\cos\theta\,\d{\theta}$.
\pause
\begin{align*}
  \integral{x=0}{1}{\sqrt{1-x^2}}{x}
  &=\integral{\theta=0}{\pi/2}{\sqrt{1-(\sin\theta)^2}\cdot\cos\theta}{\theta} \\
  &=\integral{0}{\pi/2}{\sqrt{(\cos\theta)^2}\cdot\cos\theta}{\theta} 
  =\integral{0}{\pi/2}{\cos^2\theta}{\theta}    \\ 
  &=\integral{0}{\pi/2}{ \frac{1+\cos 2\theta}{2} }{\theta}         
  =\frac{1}{2}\integral{0}{\pi/2}{ 1+\cos 2\theta }{\theta}         \\
  &=\frac{1}{2}\Bigl[\theta+\frac{\sin2\theta}{2}\Bigr]_{0}^{\pi/2} 
  =\frac{1}{2}\Bigl[(\frac{\pi}{2}+\frac{\sin \pi}{2})-(0+\frac{\sin 0}{2})\Bigr] 
  =\frac{\pi}{4}
\end{align*}
\end{frame}



\begin{frame}
\frametitle{The quarter ellipse}
This is the area for a quarter of the ellipse  $x^2/a^2+y^2/b^2=1$.
\begin{equation*}
  \integral{x=0}{x=a}{(b/a)\sqrt{a^2-x^2}}{x}
  \qquad
  \vcenteredhbox{\includegraphics{asy/trig_subst001.pdf}}
\end{equation*}
Take $x=a\sin\theta$ so $\d{x}=a\cos\theta\,\d{\theta}$.
\pause
\begin{equation*}\begin{split}
  \frac{b}{a}\cdot\integral{x=0}{a}{\sqrt{a^2-x^2}}{x}
  &=\frac{b}{a}\cdot\integral{\theta=0}{\pi/2}{\sqrt{a^2-(a\sin\theta)^2}\cdot a\cos\theta}{\theta} \\
  &=\frac{b}{a}\cdot\integral{0}{\pi/2}{\sqrt{a^2(1-\sin^2\theta)}\cdot a\cos\theta}{\theta} 
  =b\integral{0}{\pi/2}{\sqrt{a^2\cos^2\theta}\cdot\cos\theta}{\theta} \\
  &=b\cdot\integral{0}{\pi/2}{a\cos^2\theta}{\theta} 
  =ba\integral{0}{\pi/2}{\frac{1+\cos 2\theta}{2}}{\theta} \\
  &=\frac{ba}{2}\Big[\theta+(1/2)\sin 2\theta\Big]_{0}^{\pi/2} 
  =\frac{ba}{2}\Bigl[(\frac{\pi}{2}+\frac{\sin\pi}{2})-(0+\frac{\sin 0}{2})\Bigr] 
  =\frac{ba\cdot\pi}{4}
\end{split}\end{equation*}
\end{frame}



\begin{frame}
\frametitle{Suggested substitutions}
\noindent
  \begin{tabular}{cc@{\hspace*{3em}}l}
    \multicolumn{1}{c}{\tabulartext{If the integrand has \ldots}}
      &\multicolumn{1}{l}{\tabulartext{\ldots{} then try}}    
      &\multicolumn{1}{c}{\tabulartext{Associated triangle}}    \\ \hline
    \rule{0pt}{30pt}$\sqrt{a^2-x^2}$ &$x=a\sin\theta$ &\vcenteredhbox{\includegraphics{asy/trig_subst002.pdf}} \\
    \rule{0pt}{30pt}$\sqrt{a^2+x^2}$ &$x=a\tan\theta$  &\vcenteredhbox{\includegraphics{asy/trig_subst003.pdf}}\\
    \rule{0pt}{30pt}$\sqrt{x^2-a^2}$ &$x=a\sec\theta$  &\vcenteredhbox{\includegraphics{asy/trig_subst004.pdf}}\\
  \end{tabular}

Useful identities:
\begin{align*}
  &\sin^2x+\cos^2x=1         &&1+\tan^2x=\sec^2x         \\
  &\sin^2x=(1/2)(1-\cos 2x)  &&\cos^2x=(1/2)(1+\cos 2x)  \\ 
  &\sin 2x=2\sin x\cos x     &&\cos 2x=\cos^2x-\sin^2x
\end{align*}
\end{frame}


\begin{frame}\vspace*{-1ex}
\Ex $\displaystyle \integral{}{}{ \frac{\sqrt{x^2-4}}{x} }{x}$  
\quad
Substitute $x=2\sec\theta$ so that $dx=2\sec\theta\tan\theta\;d\theta$.
\pause
\begin{align*}
  \integral{}{}{ \frac{ \sqrt{(2\sec\theta)^2-4} }{ 2\sec\theta }\cdot 2\sec\theta\tan\theta }{\theta}
  &=\integral{}{}{ \frac{ \sqrt{4(\sec^2\theta-1)} }{ 2\sec\theta }\cdot 2\sec\theta\tan\theta }{\theta}    \\
  &=\integral{}{}{ \frac{ \sqrt{4\tan^2\theta} }{ 2\sec\theta }\cdot 2\sec\theta\tan\theta }{\theta}  \\
  &=\integral{}{}{ \frac{ 2\tan\theta }{ 2\sec\theta }\cdot 2\sec\theta\tan\theta }{\theta}            \\
  &=2\integral{}{}{ \tan^2\theta }{\theta}
   =2\integral{}{}{ \sec^2\theta-1 }{\theta}
   =2\tan\theta-2\theta+C
\end{align*}
\pause The question was stated in terms of~$x$ so we convert back.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/trig_subst009.pdf}}
  \quad
  \begin{minipage}{0.3\linewidth}%
     \vspace*{-4ex}
    \begin{align*}
      \tan\theta  &= \frac{\sqrt{x^2-4}}{2} 
    \end{align*}
  \end{minipage}
\end{center}
The antiderivative is this.
\begin{equation*}
  \sqrt{x^2-4}-2\sec^{-1}\bigl(\frac{x}{2}\bigr)+C
\end{equation*}
\end{frame}




\begin{frame}
\Ex
$\displaystyle \integral{}{}{\frac{1}{(16-x^2)^{3/2}}}{x}$  
\quad Try $x=4\sin\theta$, so that $dx=4\cos\theta\;d\theta$.

\pause
\begin{align*}
  %\integral{}{}{\frac{1}{(16-x^2)^{3/2}}}{x}
  \integral{}{}{\frac{4\cos\theta}{(16-(4\sin\theta)^2)^{3/2}}}{\theta}
  &=\integral{}{}{\frac{4\cos\theta}{\bigl(16(1-\sin^2\theta)\bigr)^{3/2}}}{\theta}  
  =\integral{}{}{ \frac{4\cos\theta}{(16\cos^2\theta)^{3/2}} }{\theta} \\  
   &=\integral{}{}{\frac{4\cos\theta}{64\cos^3\theta}}{\theta}  
   =\frac{1}{16}\integral{}{}{\frac{1}{\cos^2\theta}}{\theta} \\  
   &=\frac{1}{16}\integral{}{}{\sec^2\theta}{\theta}  
   =\frac{1}{16}\tan\theta+C  
\end{align*}
The question was asked in terms of $x$'s.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/trig_subst005.pdf}}
  \qquad
  \begin{minipage}{0.3\linewidth}
    \begin{equation*}
      \tan\theta=\frac{x}{\sqrt{16-x^2}}
    \end{equation*}
  \end{minipage}
\end{center}
The antiderivative is this.
\begin{equation*}
  \frac{1}{16}\cdot\frac{x}{\sqrt{16-x^2}}+C
\end{equation*}
\end{frame}


\begin{frame}
\Ex $\displaystyle \integral{}{}{ x^3\sqrt{5+x^2} }{x}$ 
\quad Try $x=\sqrt{5}\tan\theta$ so that $dx=\sqrt{5}\sec^2\theta\;d\theta$
\begin{align*}
  \integral{}{}{ (\sqrt{5}\tan\theta)^3\sqrt{5+(\sqrt{5}\tan\theta)^2}\cdot \sqrt{5}\sec^2\theta }{\theta}
  &=
  \integral{}{}{ 5^{3/2}\tan^3\theta\sqrt{5+5\tan^2\theta}\cdot \sqrt{5}\sec^2\theta }{\theta}     \\
  &=
  5^{4/2}\cdot\integral{}{}{ \tan^3\theta\sqrt{5(1+\tan^2\theta)}\cdot\sec^2\theta }{\theta}  \\
  &=
  5^{4/2}\integral{}{}{ \tan^3\theta\sqrt{5\sec^2\theta}\cdot\sec^2\theta }{\theta} \\ 
  &=
  5^{5/2}\integral{}{}{ \tan^3\theta\sec\theta\cdot\sec^2\theta }{\theta}  \\  
  &=
  5^{5/2}\integral{}{}{ \tan^3\theta\sec^3\theta }{\theta}   
\end{align*}
\pause
The power of $\tan\theta$ is odd so the Trig Integrals sections
says to pair one tangent with a secant and turn the rest into secants. 
\begin{align*}
  5^{5/2}\integral{}{}{ \tan^2\theta\sec^2\theta \cdot\sec\theta\tan\theta}{\theta}
  &=5^{5/2}\integral{}{}{ (\sec^2\theta-1)\sec^2\theta \cdot\sec\theta\tan\theta}{\theta}  \\
  &=5^{5/2}\integral{}{}{ (\sec^4\theta-\sec^2\theta) \cdot\sec\theta\tan\theta}{\theta}  
\end{align*}
\end{frame}

\begin{frame}
This is the integral from the prior slide.
\begin{equation*}
  5^{5/2}\integral{}{}{ (\sec^4\theta-\sec^2\theta) \cdot\sec\theta\tan\theta}{\theta}  
\end{equation*}
The substitution $u=\sec\theta$ gives $du=\sec\theta\tan\theta\;d\theta$.
\begin{equation*}
  5^{5/2}\integral{}{}{ u^4-u^2}{u}=5^{5/2}\biggl( \frac{u^5}{5}-\frac{u^3}{3}\biggr)+C  
\end{equation*}
Replacing the $u$'s gives this.
\begin{equation*}
  5^{5/2}\biggl( \frac{\sec^5\theta}{5}-\frac{\sec^3\theta}{3}\biggr)+C
\end{equation*}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/trig_subst010.pdf}}
  \qquad
  \begin{minipage}{0.3\linewidth}%
     \vspace*{-4ex}
    \begin{align*}
      \sec\theta  &= \frac{\sqrt{x^2+5}}{\sqrt{5}}
    \end{align*}
  \end{minipage}
\end{center}
This is the antiderivative.
\begin{equation*}
  5\biggl( \frac{(x^2+5)^{5/2}}{25}-\frac{(x^2+5)^{3/2}}{3}\biggr)+C  
\end{equation*}
\end{frame}


% \begin{frame}
% \Ex $\displaystyle \integral{}{}{ \frac{1}{\sqrt{4-x^2}} }{x}$

% Substitute $x=2\sin\theta$, so that $dx=2\cos\theta\,d\theta$.
% \pause
% \begin{align*}
%   \integral{}{}{ \frac{2\cos\theta}{\sqrt{4-(2\sin\theta)^2}} }{\theta}
%   &=\integral{}{}{ \frac{2\cos\theta}{\sqrt{4-4\sin^2\theta}} }{\theta}
%   =\integral{}{}{ \frac{2\cos\theta}{\sqrt{4(1-\sin^2\theta)}} }{\theta}  \\
%   &=\integral{}{}{ \frac{2\cos\theta}{\sqrt{4\cos^2\theta}} }{\theta}  
%   =\integral{}{}{ \frac{2\cos\theta}{2\cos\theta} }{\theta}  
%   =\integral{}{}{ 1 }{\theta}  \\
%   &=\theta+C
% \end{align*}
% To convert back to $x$'s we don't need the triangle.
% % \begin{center}
% %   \vcenteredhbox{\includegraphics{asy/trig_subst008.pdf}}
% %   \qquad
% %   \begin{minipage}{0.3\linewidth}%
% %      \vspace*{-4ex}
% %     \begin{align*}
% %       \sec\theta  &= \frac{2}{\sqrt{4-x^2}}  \\
% %       \tan\theta &=  \frac{x}{\sqrt{4-x^2}}
% %     \end{align*}
% %   \end{minipage}
% % \end{center}
% This is the antiderivative.
% \begin{equation*}
%   \arcsin(x/2)+C
% \end{equation*}  
% \end{frame}


\begin{frame}
\Ex $\displaystyle \integral{}{}{ \sqrt{4x^2+36x} }{x}$

\pause
Factor out $\sqrt{4x^2+36x}=\sqrt{4(x^2+9)}=2\sqrt{x^2+9}$,
and then  set $x=3\tan\theta$ so that $dx=3\sec^2\theta\;d\theta$.
\begin{align*}
  2\integral{}{}{ \sqrt{9\tan^2\theta+9}\cdot 3\sec^2\theta }{\theta}
  &=6\integral{}{}{ 3\sqrt{\tan^2\theta+1}\cdot\sec^2\theta }{\theta}  \\
  &=12\integral{}{}{ \sec\theta\cdot\sec^2\theta }{\theta}   
  =12\integral{}{}{ \sec^3\theta }{\theta}    \\
   &=12\cdot\frac{1}{2}\bigl( \sec\theta\tan\theta+\ln|\sec\theta+\tan\theta| \bigr)   
\end{align*}
(The final antiderivative we did in the prior section.)
Convert to $x$'s.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/trig_subst006.pdf}}
  \qquad
  \begin{minipage}{0.3\linewidth}%
    \begin{align*}
      &\tan\theta=\frac{x}{3}  \\
      &\sec\theta=\frac{\sqrt{x^2+9}}{3}
    \end{align*}
  \end{minipage}
\end{center}
This is the antiderivative.
\begin{equation*}
  6\cdot\biggl(\frac{x\sqrt{x^2+9}}{9}+\ln\bigl| \frac{\sqrt{x^2+9}+x}{3} \bigr|\biggr)+C
\end{equation*}
\end{frame}


\begin{frame}
\Ex $\displaystyle \integral{}{}{ \frac{1}{(x^2-6x+13)^2} }{x}$

\pause
Complete the square $(x^2-6x+13)^2=((x^2-6x+9)+4)^2=((x-3)^2+4)^2\!$.
Substitute $x-3=2\tan\theta$ so that $dx=2\sec^2\theta\;d\theta$.
\begin{align*}
  \integral{}{}{ \frac{1}{\bigl( (2\tan\theta)^2+4 \bigr)^2}\cdot 2\sec^2\theta }{\theta}  
  &=
  \integral{}{}{ \frac{1}{( 4\tan^2\theta+4 )^2}\cdot 2\sec^2\theta }{\theta}  \\
  &=
  \integral{}{}{ \frac{1}{16( \tan^2\theta+1 )^2}2\sec^2\theta }{\theta}  
  =\frac{1}{8}\integral{}{}{ \frac{1}{(\sec^2\theta )^2}\sec^2\theta }{\theta}  \\
  &=\frac{1}{8}\integral{}{}{ \frac{1}{ \sec^2\theta }}{\theta}  
  =\frac{1}{8}\integral{}{}{ \cos^2\theta }{\theta}   \\
  &=\frac{1}{8}\integral{}{}{ \frac{1}{2}(1+\cos2\theta) }{\theta}  
  =\frac{1}{16}\bigl( \theta+\frac{1}{2}\sin2\theta \bigr) +C 
\end{align*}
\pause
\begin{center}
  \vcenteredhbox{\includegraphics{asy/trig_subst007.pdf}}
  \quad
  \begin{minipage}{0.3\linewidth}%
     \vspace*{-4ex}
    \begin{align*}
      (1/2)\sin2\theta &=(1/2)\cdot 2\sin\theta\cos\theta  \\
                 &=\frac{x-3}{\sqrt{(x-3)^2+4}}\frac{2}{\sqrt{(x-3)^2+4}}  
    \end{align*}
  \end{minipage}
\end{center}
This is the antiderivative.
\begin{equation*}
  \frac{1}{16}\biggl(\tan^{-1}(\frac{x-3}{2})+  \frac{2(x-3)}{x^2-6x+13}\biggr)+C
\end{equation*}
\end{frame}





% \begin{frame}
% % \frametitle{Examples}
% \medskip
% \noindent{\large Examples}
% \begin{enumerate}
% \item $\integral{}{}{\sqrt{4x^2+20}}{x}$
% \pause
% \item $\integral{}{}{1/(4+x^2)^{3/2}}{x}$
% \pause
% \item $\integral{}{}{1/(x^2+8x+25)^2}{x}$
% \end{enumerate}

% \end{frame}



% \begin{frame}
% \frametitle{}
% 
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
