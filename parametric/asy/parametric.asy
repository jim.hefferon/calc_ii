// parametric.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "parametric%03d";

import graph;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// =========== line defined parametrically ===============
real f0(real x) { return 2*x; }

picture pic;
int picnum = 0;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=8;

path f = graph(pic, f0, 0, 4);
draw(pic, f, FCNPEN);

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YEquals(ytop),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XEquals(xright),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ........... simplest parametrization ......
real f1 (real t) {return 2*t;}
pair F1(real t) { return (t, f1(t));}

picture pic;
int picnum = 1;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=8;

path f = graph(pic, F1, 0, 4);
draw(pic, f, FCNPEN);

dotfactor = 4;
dot(pic, F1(0), highlight_color);
for (int t=1; t<=4; ++t) {
  dot(pic, Label(format("$t=%d$", t), filltype=Fill(white)),
      F1(t), 1.5*E, highlight_color); 
}

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YEquals(ytop),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XEquals(xright),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// .............. (2t, 4t) ................
real f2(real t) {return 4*t;}
pair F2(real t) { return (2*t, f2(t));}

picture pic;
int picnum = 2;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=8;

path f = graph(pic, F2, 0, 2);
draw(pic, f, FCNPEN);

dotfactor = 4;
dot(pic, F2(0), highlight_color);
for (int t=1; t<=2; ++t) {
  dot(pic, Label(format("$t=%d$", t), filltype=Fill(white)),
      F2(t), E, highlight_color); 
}

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YEquals(ytop),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XEquals(xright),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// .............. In reverse ................
real g3(real t) {return 4-t; }
real f3(real t) {return 2*t; }
pair F3(real t) { return (g3(t), f3(g3(t)));}

picture pic;
int picnum = 3;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=8;

path f = graph(pic, F3, 0, 4);
draw(pic, f, FCNPEN);

dotfactor = 5;
for (int t=0; t<=4; ++t) {
  dot(pic, Label(format("$t=%d$", t), filltype=Fill(white)),
      F3(t), 1.5*E, highlight_color); 
}

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YEquals(ytop),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XEquals(xright),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// .............. (t^2, 2t^2) ................
real f4(real t) {return 2*t^2;}
pair F4(real t) { return (t^2, f4(t));}

picture pic;
int picnum = 4;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=8;

path f = graph(pic, F4, 0, 2);
draw(pic, f, FCNPEN);

dotfactor = 5;
for (int t=0; t<=2; ++t) {
  dot(pic, Label(format("$t=%d$", t),filltype=Fill(white)),
      F4(t), E, highlight_color); 
}

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YEquals(ytop),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XEquals(xright),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ============= parametrize y=x^2 ============
real f5(real x) { return x^2; }
pair F5(real t) { return (t, f5(t)); }
			  
picture pic;
int picnum = 5;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=9;

path f = graph(pic, F5, xleft-0.2, xright+0.05);
draw(pic, f, FCNPEN);

dotfactor = 5;
for (int t=0; t<=3; ++t) {
  dot(pic, Label(format("$t=%d$", t),filltype=Fill(white)),
      F5(t), E, highlight_color); 
}

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YEquals(ytop),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XEquals(xright),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ........... add backtracking ...........
real g6(real t) { return 3*4/25*(-t^2+5*t); }

picture pic;
int picnum = 6;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = graph(pic, g6, xleft-0.2, xright+0.2);
draw(pic, f, FCNPEN);

// dotfactor = 5;
// dot(pic, F4(0), highlight_color);
// for (int t=1; t<=3; ++t) {
//   dot(pic, format("$t=%d$", t), F4(t), E, highlight_color); 
// }

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $t$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YEquals(ytop),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $\hat{g}(t)$",EndPoint,W);
yaxis(pic, L="",  
      axis=XEquals(xright),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
Label L = rotate(0)*Label("",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks("%", Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ........... compose with backtracking ...........
real f7(real x) { return x^2; }
pair F7(real t) { return (g6(t), f5(g6(t))); }

picture pic;
int picnum = 7;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=9;

path f = graph(pic, F7, xleft-0.2, xright+0.05);
draw(pic, f, FCNPEN);

dotfactor = 5;
for (int t=0; t<=2; ++t) {
  dot(pic, Label(format("$t=%d$", t), filltype=Fill(white)),
      F7(t), E, highlight_color); 
}
for (int t=3; t<=5; ++t) {
  dot(pic, Label(format("$t=%d$", t), filltype=Fill(white)),
      F7(t), W, highlight_color); 
}
dot(pic, Label(format("$t=%f$", 2.5), filltype=Fill(white)),
    F7(2.5), E, highlight_color); 

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YEquals(ytop),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XEquals(xright),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// =========== circle =============
pair F8(real t) { return (cos(t), sin(t)); }

picture pic;
int picnum = 8;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-1; ytop=1;

path f = graph(pic, F8, 0, 2*pi);
draw(pic, f, FCNPEN);

dotfactor = 5;
for (int t=0; t<=2*pi; ++t) {
  dot(pic, Label(format("$t=%d$", t), filltype=Fill(white)),
      F8(t), E, highlight_color); 
}
dot(pic, Label(format("$t=2\pi$", 2*pi), filltype=Fill(white)),
    F8(2*pi), W, highlight_color); 

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ........ circle at an uneven rate .........
real g9(real t) {return t^2;}
pair F9(real t) { return (cos(g9(t)), sin(g9(t))); }

picture pic;
int picnum = 9;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-1; ytop=1;

path f = graph(pic, F9, 0, sqrt(2*pi));
draw(pic, f, FCNPEN);

dotfactor = 5;
for (int t=0; t<=sqrt(2*pi); ++t) {
  dot(pic, Label(format("$t=%d$", t), filltype=Fill(white)),
      F9(t), E, highlight_color); 
}
dot(pic, Label(format("$t=\sqrt{2\pi}$", sqrt(2*pi)), filltype=Fill(white)),
    F9(sqrt(2*pi)), W, highlight_color); 

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ======= parametrize a line =========
real f10(real t) {return 3*t-1;}
pair F10(real t) { return (t, f10(t)); }

picture pic;
int picnum = 10;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=4;
ybot=f10(xleft); ytop=f10(xright);

path f = graph(pic, F10, xleft-0.1, xright+0.1);
draw(pic, f, FCNPEN);

dotfactor = 4;
for (int t=-1; t<=4; ++t) {
  dot(pic, Label(format("$t=%d$", t), filltype=Fill(white)),
      F10(t), 1.5*E, highlight_color); 
}

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ....... adjust the start .............
real g11(real t) {return t+2;}
pair F11(real t) { return ( g11(t), f10(g11(t))); }

picture pic;
int picnum = 11;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=0; ytop=17;

path f = graph(pic, F11, -2-0.01, 4+0.1);
draw(pic, f, FCNPEN);

dotfactor = 4;
for (int t=-1; t<=4; ++t) {
  dot(pic, Label(format("$t=%d$", t), filltype=Fill(white)),
      F11(t), 1.5*E, highlight_color); 
}

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=5, step=0, OmitTick(0), Size=2pt, size=0pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);





// ======= from prametrizationto curve, parabola =========
pair F12(real t) { return (t^2-5, 2*t); }

picture pic;
int picnum = 12;
size(pic,0,3.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=0;
ybot=-2; ytop=4;

path f = graph(pic, F12, -1, 2);
draw(pic, f, FCNPEN);

dotfactor = 4;
for (int t=-1; t<=2; ++t) {
  dot(pic, Label(format("$t=%d$", t), filltype=Fill(white)),
      F12(t), 2*E, highlight_color); 
}

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);





// ======= cycloid =========
pair Cycloid(real t) { return (t-sin(t), 1-cos(t)); }

picture pic;
int picnum = 13;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2*pi;
ybot=0; ytop=2;

path f = graph(pic, Cycloid, 0-1, 2*pi+1);
draw(pic, f, FCNPEN);

dotfactor = 4;
for (int t=0; t<=6; ++t) {
  // dot(pic, Label(format("$t=%d$", t), filltype=Fill(white)),
  //     Cycloid(t), 2*N, highlight_color); 
  dot(pic,
      Cycloid(t), highlight_color); 
}

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ........ describe the triangle ..........
picture pic;
int picnum = 14;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=2;

path f = graph(pic, Cycloid, 0, 3.25);
draw(pic, f, FCNPEN+(0.5)*white);

real t = 0.85;
pair circle_center = (t,1);
real circle_rad = 1; 
path C = circle(circle_center, circle_rad);
draw(pic, C, highlight_color);
pair P = Cycloid(t);
pair Q = (circle_center.x, 0);
pair R = (circle_center.x, P.y);
draw(pic, Q--R);
draw(pic, R--P--circle_center--cycle);

dotfactor = 3;
dot(pic, Label("$(x,y)=P$",filltype=Fill(white)), P, 1.5*W);
dot(pic, "$Q$", Q, S);
dot(pic, "$R$", R, E);
dot(pic, "$C$", circle_center, E);

Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ........... cycloid derivation .........
picture pic;
int picnum = 15;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=2;

path f = graph(pic, Cycloid, 0, 3.25);
draw(pic, f, FCNPEN+(0.5)*white);

real t = 0.85;
pair circle_center = (t,1);
real circle_rad = 1; 
// path C = circle(circle_center, circle_rad);
draw(pic, arc(circle_center, circle_rad, degrees(1.5*pi), degrees(1.5*pi-t), CCW), black);
draw(pic, arc(circle_center, circle_rad, degrees(1.5*pi-t), degrees(1.5*pi), CCW), highlight_color);

pair P = Cycloid(t);
pair Q = (circle_center.x, 0);
pair R = (circle_center.x, P.y);
draw(pic, Q--R);
draw(pic, R--P--circle_center--cycle);
dotfactor = 4;
dot(pic, Label("$(x,y)=P$",filltype=Fill(white)), P, 1.5*W);
dot(pic, "$Q$", Q, S);
dot(pic, "$R$", R, E);
dot(pic, "$C$", circle_center, E);

// line at the bottom indicating the circle has rolled t units
real T_LINE_HGT = -0.35;
draw(pic, "$t$", (0,T_LINE_HGT)--(t,T_LINE_HGT), S, highlight_color,
     Bars(ARROW_SIZE), Arrows(ARROW_SIZE));

// arc in center indicating angle is t radians 
draw(pic, "$t$", arc(circle_center, 0.25, degrees(1.5*pi-t), degrees(1.5*pi), CCW), highlight_color, Arrow(ARROW_SIZE));



Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


