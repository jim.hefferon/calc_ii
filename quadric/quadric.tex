\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 12.6\hspace{1em}Quadric surfaces}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Quadric surfaces}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage stub_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
% uncomment for 3D graphics: \input asy/arc_length_3d.pre
\input asy/quadric_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................


\section{Cylinders}

\begin{frame}
If an equation with three unknowns has a missing letter then we get
a \alert{cylinder}.

\Ex In $\R^3$ graph $x^2+y^2=1$.  
\begin{center}
   \vcenteredhbox{\input asy/quadric_3d010.tex }%
\end{center}
\end{frame}

\begin{frame}
\Ex In $\R^3$ graph $y=x^2$.  
\pause
\begin{center}
   \vcenteredhbox{\input asy/quadric_3d009.tex }%
\end{center}
\end{frame}

\begin{frame}
\Ex In $\R^3$ graph $z=2y+1$.  
\pause
\begin{center}
   \vcenteredhbox{\input asy/quadric_3d011.tex }%
\end{center}
\end{frame}



\section{Quadric Surfaces}

\begin{frame}{Recall: conic sections}
A conic section is the graph in the plane of a general equation 
of degree two with two variables,  
\begin{equation*}
  Ax^2+By^2+Cxy+Dx+Ey+F=0
\end{equation*}
where $A,B,\ldots$ are constants.
By rotating and translating to center it on the origin, 
there prove to be three nondegenerate cases: ellipse, hyperbola, or
parabola. 
(This follows from the Principal Axis Theorem of Linear Algebra.)
\end{frame}


\begin{frame}{Quadric surfaces}\vspace*{-2ex}
\Df A \alert{quadric surface} is the graph in $\R^3$ of an
equation of degree two with three variables.
\begin{equation*}
  Ax^2+By^2+Cz^2+Dxy+Eyz+Fxz+Gx+Hy+Iz+J=0
\end{equation*}
Here also by translation and rotation we get to a small number of cases, 
six of them.

Those six fall into two categories: (1)~all three variables 
are to the second power and (2)~one variable is to the first power.
\begin{center}
  \begin{tabular}{lp{0.6\textwidth}}
    \tabulartext{Category}
      &\tabulartext{Quadric surface cases}  \\
    \hline  
    $Ax^2+By^2+Cz^2+J=0$ 
    &Ellipsoid, 
    Cone, 
    Hyperboloid of one sheet,
    Hyperboloid of two sheets  \\
    $Ax^2+By^2+Iz=0$  \
    &Elliptic paraboloid, 
    Hyperbolic paraboloid
  \end{tabular}
\end{center}
\end{frame}


\begin{frame}{Ellipsoid}
The graph of
\begin{equation*}
  \frac{x^2}{a^2} 
  +\frac{y^2}{b^2}
  +\frac{z^2}{c^2}
  =1
\end{equation*}
is an \alert{ellipsoid}.
\begin{center}
    \input asy/quadric_3d000.tex %
\end{center}
Its furthest points on the $x$~axis are $(\pm a,0,0)$,
on the $y$~axis $(0, \pm b,0)$, and
on the $z$~axis $(0,0,\pm c)$.
\end{frame}


\begin{frame}{Sketching quadric surfaces: traces}
A \alert{trace} is the curve that you get from the intersecting the 
surface with a plane parallel to a coordinate planes.
These are traces from intersecting with $z=0$, $z=1$, and $z=2$.
\begin{center}
 \begin{minipage}{2cm}
 \begin{equation*}
  \frac{x^2}{5^2} 
  +\frac{y^2}{7^2}
  +\frac{z^2}{9^2}
  =1
  \end{equation*}
  \end{minipage}
  \qquad
    \only<1>{\vcenteredhbox{\input asy/quadric_3d001.tex }}%
    \only<2>{\vcenteredhbox{\input asy/quadric_3d002.tex }}%
    \only<3->{\vcenteredhbox{\input asy/quadric_3d003.tex }}%
\end{center}
The equation of the first is this ellipse in the $xy$~plane.
\begin{equation*}
  \frac{x^2}{5^2} 
  +\frac{y^2}{7^2}
  =1
\end{equation*}
Each height $\hat{z}$ 
with $-9\leq \hat{z}\leq 9$ gives an ellipse in the plane $z=\hat{z}$.
\begin{equation*}
  \frac{x^2}{5^2} 
  +\frac{y^2}{7^2}
  =1-\frac{\hat{z}^2}{81}
  \quad\Longrightarrow\quad
  \frac{x^2}{5^2\cdot (1-\frac{\hat{z}^2}{81})} 
  +\frac{y^2}{7^2\cdot (1-\frac{\hat{z}^2}{81})}
  =1
\end{equation*}
\end{frame}



\begin{frame}{Second case: Cone}
The graph of
\begin{equation*}
  \frac{x^2}{a^2} 
  +\frac{y^2}{b^2}
  =\frac{z^2}{c^2}
\end{equation*}
is a \alert{cone}.
\begin{center}
    \input asy/quadric_3d007.tex %
\end{center}

The horizontal $z=k$ traces are ellipses or circles, 
or a point if $k=0$.

Vertical traces in the plane $x=k$ and $y=k$ are hyperbolas if $k\neq 0$. 
The $k=0$ coordinate planes give lines.
\end{frame}




\begin{frame}{Third case: Hyperboloid of one sheet}
The graph of
\begin{equation*}
  \frac{x^2}{a^2} 
  +\frac{y^2}{b^2}
  -\frac{z^2}{c^2}
  =1
\end{equation*}
is an \alert{hyperboloid of one sheet}.
\begin{center}
    \input asy/quadric_3d004.tex %
\end{center}
The axis of symmetry is associated with the variable having the minus.
Horizontal traces are ellipses, while vertical traces are hyperbolas.
\end{frame}




\begin{frame}{Fourth: Hyperboloid of two sheets}
The graph of
\begin{equation*}
  -\frac{x^2}{a^2} 
  -\frac{y^2}{b^2}
  +\frac{z^2}{c^2}
  =1
\end{equation*}
is an \alert{hyperboloid of two sheets}.
\begin{center}
    \input asy/quadric_3d005.tex %
\end{center}
Horizontal traces are ellipses
(when they exist), while vertical traces are hyperbolas.
\end{frame}




\begin{frame}{Fifth case: elliptical paraboloid}
The graph of
\begin{equation*}
  \frac{x^2}{a^2} 
  +\frac{y^2}{b^2}
  =\frac{z}{c}
\end{equation*}
is an \alert{elliptical paraboloid}.
\begin{center}
    \input asy/quadric_3d006.tex %
\end{center}
The linear variable gives the axis of the paraboloid.
Traces perpendicular to it are ellipses while
traces parallel to it (here the vertical traces) are parabolas. 
\end{frame}



\begin{frame}{Sixth: Hyperbolic paraboloid}
The graph of
\begin{equation*}
  \frac{x^2}{a^2} 
  -\frac{y^2}{b^2}
  =\frac{z}{c}
\end{equation*}
is an \alert{hyperbolic paraboloid} or saddle.
\begin{center}
    \input asy/quadric_3d008.tex %
\end{center}
Horizontal traces are hyperbolas while vertical traces are parabolas.
\end{frame}



\begin{frame}{The zoo}
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{\tabulartext{Equation}}
      &\multicolumn{1}{c}{\tabulartext{Surface}}  \\ \hline
    $\displaystyle \frac{x^2}{a^2}+\frac{y^2}{b^2}+\frac{z^2}{c^2}=1$
        &Ellipsoid\rule{0pt}{15pt} \\
    $\displaystyle \frac{x^2}{a^2}+\frac{y^2}{b^2}=\frac{z^2}{c^2}$
        &Cone\rule{0pt}{18pt} \\
    $\displaystyle \frac{x^2}{a^2}+\frac{y^2}{b^2}-\frac{z^2}{c^2}=1$
        &Hyperboloid of one sheet\rule{0pt}{18pt} \\
    $\displaystyle -\frac{x^2}{a^2}-\frac{y^2}{b^2}+\frac{z^2}{c^2}=1$
        &Hyperboloid of two sheets\rule{0pt}{18pt} \\
    $\displaystyle \frac{x^2}{a^2}+\frac{y^2}{b^2}=\frac{z}{c}$
        &Elliptical paraboloid\rule{0pt}{18pt} \\
    $\displaystyle \frac{x^2}{a^2}-\frac{y^2}{b^2}=\frac{z}{c}$
        &Hyperbolic paraboloid\rule{0pt}{18pt} 
  \end{tabular}
\end{center}
\end{frame}

\begin{frame}{Practice}

Identify and sketch
\begin{enumerate}
\item $\displaystyle 4x^2-y^2+2z^2+4=0$
\pause
\quad\textit{Ans.} Hyperboloid of two sheets with axis the $y$~axis.

\pause
\item $\displaystyle 4x^2+y^2+z^2=4$
\pause
\quad\textit{Ans.} Ellipsoid.

\pause
\item $\displaystyle 4x^2+4y^2-z^2=4$
\pause
\quad\textit{Ans.} Hyperboloid of one sheet with axis the $z$~axis.

\pause
\item $\displaystyle x^2+y^2-z^2=0$
\pause
\quad\textit{Ans.} Cone with axis the $z$~axis.
\end{enumerate}
  
\end{frame}


\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
