// quadric_3d.asy

// compile with asy -inlineimage stub_3d
// In the .tex file put this in the preamble ... 
// % For 3D PRC asymptote
// % compile with asy -inlineimage stub_3d
// \def\asydir{asy/}
// \graphicspath{{asy/}}
// \input asy/stub_3d.pre
// \usepackage[bigfiles]{media9}
// \RequirePackage{asymptote}
// ... and then include the 3D material with something like this.
// \begin{center}
//   \uncover<2->{\input asy/stub_3d001.tex }% need the space following .tex   
// \end{center}


cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = true;
settings.embed = true;
settings.render = 0;
settings.tex="lualatex";  // for compiling in-pic text
settings.outformat="pdf";

import embed;

import fontsize;

import graph3; import solids;
currentprojection = orthographic(1.2,3,2,up=Z);
currentlight=White; defaultrender.merge=true;
// unitsize(1cm);

string OUTPUT_FN = "quadric_3d%03d";

material figure_material = material(diffusepen=bold_color+opacity(0.5), // omitted white
				 emissivepen=bold_color+white,
				 specularpen=bold_color+white);
material slice_material = material(diffusepen=gray_color+opacity(0.25),
				   emissivepen=gray_color,
				   specularpen=gray_color);
pen boundary_pen = gray(0.2)+opacity(0.5);

currentlight = nolight;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// ===== ellipsoid =======
currentprojection = orthographic(4,4,1,up=Z);

picture pic;
int picnum = 0;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

real a = 5;
real b = 7;
real c = 9;

transform3 sphere_t = scale(a,b,c);
surface e = sphere_t*unitsphere;

draw(pic, e, surfacepen=figure_material);

dotfactor = 4;
dot(pic, "$a$", (a,0,0));
dot(pic, "$b$", (0,b,0));
dot(pic, "$c$", (0,0,c));

xaxis3(pic,Label("$x$"),
       0,10, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,10, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),
        0,12, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


// .......... with traces parallel to xy plane .............
picture pic;
int picnum = 1;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

real x(real t) {return a*cos(2pi*t);}
real y(real t) {return b*sin(2pi*t);}
real z(real t) {return 0;}

path3 hgt0 = graph(x, y, z, 0, 2*pi);
draw(pic, hgt0, gray(0.8)); 
draw(pic, e, surfacepen=figure_material);
draw(pic, hgt0, boundary_pen);

dotfactor = 4;
dot(pic, "$5$", (a,0,0));
dot(pic, "$7$", (0,b,0));
dot(pic, "$9$", (0,0,c));

xaxis3(pic,Label("$x$"),
       0,10, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,10, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),
        0,12, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


// .......... trace z=2 .............
picture pic;
int picnum = 2;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

real hgt = 1;
real x(real t) {return (a*sqrt(1-(hgt^2/81)))*cos(2pi*t);}
real y(real t) {return (b*sqrt(1-(hgt^2/81)))*sin(2pi*t);}
real z(real t) {return hgt;}

path3 hgt_pth = graph(x, y, z, 0, 2*pi);
draw(pic, e, surfacepen=figure_material);
draw(pic, hgt_pth, boundary_pen);

dotfactor = 4;
dot(pic, "$5$", (a,0,0));
dot(pic, "$7$", (0,b,0));
dot(pic, "$9$", (0,0,c));

xaxis3(pic,Label("$x$"),
       0,10, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,10, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),
        0,12, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// .......... trace z=3 .............
picture pic;
int picnum = 3;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

real hgt = 3;
real x(real t) {return (a*sqrt(1-(hgt^2/81)))*cos(2pi*t);}
real y(real t) {return (b*sqrt(1-(hgt^2/81)))*sin(2pi*t);}
real z(real t) {return hgt;}

path3 hgt_pth = graph(x, y, z, 0, 2*pi);
draw(pic, e, surfacepen=figure_material);
draw(pic, hgt_pth, boundary_pen);

dotfactor = 4;
dot(pic, "$5$", (a,0,0));
dot(pic, "$7$", (0,b,0));
dot(pic, "$9$", (0,0,c));

xaxis3(pic,Label("$x$"),
       0,10, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,10, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),
        0,12, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ============ Hyperboloid ==============

// One sheet
// https://mathworld.wolfram.com/One-SheetedHyperboloid.html
// (x/a)^2+(y/a)^2-(z/c)^2=1
//  x=a cosh(u)cos(v)
//  y=a cosh(u)sin(v)
//  z=c sinh(u)
triple f4(pair t){
  return (cosh(t.y)*cos(t.x)/2,cosh(t.y)*sin(t.x)/2, sinh(t.y));
}

picture pic;
int picnum = 4;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

surface s=surface(f4,(0,-2) ,(2*pi,2),nu=40,nv=40,Spline);
draw(pic, s,surfacepen=figure_material);

// dotfactor = 4;
// dot(pic, "$a$", (a,0,0));
// dot(pic, "$b$", (0,b,0));
// dot(pic, "$c$", (0,0,c));

xaxis3(pic,Label("$x$"),
       0,4, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,4, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),
        0,4, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


// ............... Two sheets ...................
// https://mathworld.wolfram.com/Two-SheetedHyperboloid.html
// (x/a)^2+(y/a)^2-(z/c)^2=-1
//  x=a sinh(u)cos(v)
//  y=a sinh(u)sin(v)
//  z=c cosh(u)

triple f5(pair t){
  return (sinh(t.y)*cos(t.x)/2,sinh(t.y)*sin(t.x)/2, cosh(t.y));
}

picture pic;
int picnum = 5;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);


surface s=surface(f5,(0,-2) ,(2*pi,2),nu=40,nv=40,Spline);
draw(pic, s,surfacepen=figure_material);
draw(pic, reflect((0,0,0),(1,0,0),(0,1,0))*s,surfacepen=figure_material);

// dotfactor = 4;
// dot(pic, "$a$", (a,0,0));
// dot(pic, "$b$", (0,b,0));
// dot(pic, "$c$", (0,0,c));

xaxis3(pic,Label("$x$"),
       0,4, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,4, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),
        -4,4, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ............... Elliptical paraboloid ...................
// https://mathworld.wolfram.com/EllipticParaboloid.html
// Height h, semimajor axis a, and semiminor axis b 
// x = a*sqrt(u)*cos v	
// y = b*sqrt(u)*sin v	
// z = u	
// for or v in [0,2pi) and u in [0,h]. 
 
triple f6(pair t){
  return (t.x, t.y, t.x^2/(1/9)+t.y^2);
}
triple f6_polars(pair t){
  real r = t.x;
  real theta = t.y;
  return (r*cos(theta), r*sin(theta), (r*cos(theta))^2/(1/9) + (r*sin(theta))^2);
}
triple f6_param(pair t){
  real r = t.x;
  real theta = t.y;
  return (2*sqrt(r)*cos(theta), sqrt(r)*sin(theta), r);
}

picture pic;
int picnum = 6;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

surface s=surface(f6_param,(0,0) ,(1,2*pi),nu=40,nv=40,Spline);
draw(pic, s,surfacepen=figure_material);

xaxis3(pic,Label("$x$"),
       0,2, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,1, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),
       0,1.5, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


// ............... Cone ...................
// https://mathworld.wolfram.com/Cone.html
// a general (infinite, double-napped) cone
// x = a* u *cos(v)	
// y = a* u* sin(v)	
// z = u

real a = 1;
triple f7(pair t){
  real u = t.x;
  real theta = t.y;
  return ( a*u*cos(theta), a*u*sin(theta), u);
}

picture pic;
int picnum = 7;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

surface s=surface(f7,(-1,0) ,(1,2*pi),nu=40,nv=40,Spline);
draw(pic, s,surfacepen=figure_material);

xaxis3(pic,Label("$x$"),
       0,1, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,1, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),
       -1.5,1.5, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ............... Hyperbolic paraboloid ...................
// https://mathworld.wolfram.com/HyperbolicParaboloid.html
// a general (infinite, double-napped) cone
// z=x*y

real a = 1;
triple f8(pair t) {
  return ( t.x, t.y, t.x^2-t.y^2);
}

picture pic;
int picnum = 8;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

surface s=surface(f8,(-1,-1) ,(1,1),nu=40,nv=40,Spline);
draw(pic, s,surfacepen=figure_material);

xaxis3(pic,Label("$x$"),
       0,1, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,1, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),
       -1.5,1.5, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ========== Cylinders =================
real f9(real x) {
  return x^2;
}

picture pic;
int picnum = 9;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

path f = graph(f9, -2, 2);
surface s=extrude(f);
draw(pic, shift(0,0,-1)*zscale3(2)*s,surfacepen=figure_material);

xaxis3(pic,Label("$x$"),
       -2,2, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,4, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),
       -1.5,1.5, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


// .................. cylinder ...............
picture pic;
int picnum = 10;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

draw(pic, shift(0,0,-1)*zscale3(2)*unitcylinder,surfacepen=figure_material);

xaxis3(pic,Label("$x$"),
       -1,1, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        -1,1, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),
       -1.5,1.5, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


// ............... Line in yz plane ..................
triple f11(pair t) {
  return ( t.x, t.y, 2*t.y+1);
}

picture pic;
int picnum = 11;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

surface s=surface(f11,(-1,0) ,(2,1),nu=40,nv=40,Spline);
draw(pic, s,surfacepen=figure_material);

xaxis3(pic,Label("$x$"),
       -1,2, black, Arrows3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,1, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),
       0,3, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



