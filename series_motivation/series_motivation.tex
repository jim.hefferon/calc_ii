\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}
\usepackage{animate}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Chapter 11.0\hspace{1em}Our interest in series}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Motivation for infinite series}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage stub_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
% \input asy/arc_length_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Higher-order polynomials are refinements of lower-order ones}
Consider $f(x)=e^x$.  
Here are the derivatives.
\begin{center}
  \begin{tabular}{r|*{5}{c}}
    \tabulartext{$i$}         &$0$   &$1$   &$2$  &$3$   &$4$ \\
    \cline{2-6}
    \tabulartext{$f^{(i)}(x)$} &$e^x$ &$e^x$ &$e^x$ &$e^x$ &$e^x$ \\
    \tabulartext{$f^{(i)}(0)$} &$1$   &$1$   &$1$  &$1$   &$1$ \\
  \end{tabular}
\end{center}
So these are the Taylor polynomials.
\begin{align*}
  T_1(x) &=1+x  \\
  T_2(x) &=1+x+\frac{1}{2!}x^2  \\
  T_3(x) &=1+x+\frac{1}{2!}x^2+\frac{1}{3!}x^3  \\
  &\vdotswithin{=} \\
  T_n(x) &=1+x+\frac{1}{2!}x^2+\frac{1}{3!}x^3+\cdots +\frac{1}{n!}x^n
\end{align*}
\end{frame}

\begin{frame}
  \frametitle{If longer polynomials are better then \ldots{}}
We can have the idea 
that since the $T_n(x)$'s are increasingly accurate,
to get equality we can do this.
\begin{equation*}
  e^x 
  =1+x+\frac{1}{2!}x^2+\frac{1}{3!}x^3+\cdots 
  =\sum_n\frac{1}{n!}x^n
\end{equation*}
In a sense, this is a polynomial of infinite degree.

Here are the approximations to $f(x)=e^x$ given as Taylor polynomials
anchored at $a=0$ of various orders.
\begin{center}
  \begin{tabular}{r|l}
    \multicolumn{1}{r}{\tabulartext{Order} $n$}
      &\multicolumn{1}{l}{$T_n(0.25)$}  \\ \hline
    $0$ &$1$  \\
    $1$ &$1.25000000000000$  \\
    $2$ &$1.28125000000000$  \\
    $3$ &$1.28385416666667$  \\
    $4$ &$1.28401692708333$  \\
    $5$ &$1.28402506510417$  \\
    $6$ &$1.28402540418837$  \\
    $7$ &$1.28402541629852$  \\
    $8$ &$1.28402541667696$  \\
    $9$ &$1.28402541668747$  \\
    $\infty$ &$e^{0.25}=1.28402541668774$
  \end{tabular}
\end{center}
\end{frame}
% sage: for deg in range(10):
% ....:     f = taylor(e^x, x, 0, deg)
% ....:     print("taylor=",f(x=0.25))
% ....: 
% taylor= 1
% taylor= 1.25000000000000
% taylor= 1.28125000000000
% taylor= 1.28385416666667
% taylor= 1.28401692708333
% taylor= 1.28402506510417
% taylor= 1.28402540418837
% taylor= 1.28402541629852
% taylor= 1.28402541667696
% taylor= 1.28402541668747
% sage: e^(0.25)
% 1.28402541668774

\begin{frame}{This raises mathematical issues}
How to make precise sense of this infinitely-long sum?
\begin{equation*}
  1+x+\frac{1}{2!}x^2+\frac{1}{3!}x^3+\cdots 
  =\sum_{0\leq n<\infty}\frac{1}{n!}\cdot x^n
\end{equation*}
The Calculus way to avoid the ``$\cdots$'' is to take a limit. 
So we will define it to be this. 
\begin{equation*}
  =\lim_{k\to\infty}\biggl(\, \sum_{0\leq n<k}\frac{1}{n!}\cdot x^n \biggr)
  \tag{$*$}
\end{equation*}
We will develop how to express many functions as 
these infinitely long polynomials, called \alert{Taylor series}.
Beyond being mathematically interesting, 
they are extremely important in applications.

\pause\medskip
This chapter explains how to work with these.
In our development there will be two things to watch out for. 

First, in equation~($*$) the $k$ is not what we are used to.
Taylor polynomials come as $T_1$, $T_2$, $T_3$, \ldots{}, in 
discrete steps.  
So we are not taking the limit of real numbers: $k$ is not a real number, 
it is an integer.
We need to define how to take discrete limits.

Second, infinitely long sums come with several third rails that
we must avoid.
The next slide illustrates.
\end{frame}

\begin{frame}
\Ex
These are the Taylor polynomials for $f(x)=1/(1-x)$ about $a=0$.
\begin{center}
  \begin{tabular}{r|*{5}{c}}
    $i$
      &\multicolumn{1}{c}{$0$}
      &\multicolumn{1}{c}{$1$}
      &\multicolumn{1}{c}{$2$}
      &\multicolumn{1}{c}{$3$}
      &\multicolumn{1}{c}{$4$}    
    \\ \cline{2-6}
    $f^{(i)}(x)$  &$(1-x)^{-1}$ &$(1-x)^{-2}$ &$2(1-x)^{-3}$ &$6(1-x)^{-4}$ &$24(1-x)^{-5}$ \\
    $f^{(i)}(0)$  &$1$       &$1$       &$2$        &$6$       &$24$     
  \end{tabular}
\end{center}
\begin{align*}
  T_1(x) &= 1+1\cdot x  &&=1+x \\
  T_2(x) &= 1+1\cdot x+\frac{2}{2!} \cdot x^2  &&=1+x+x^2 \\
  T_3(x) &= 1+1\cdot x+\frac{2}{2!} \cdot x^2+\frac{6}{3!}\cdot x^3  &&=1+x+x^2+x^3\\ 
  T_4(x) &= 1+1\cdot x+\frac{2}{2!} \cdot x^2+\frac{6}{3!}\cdot x^3 +\frac{24}{4!}\cdot x^4   &&=1+x+x^2+x^3+x^4
\end{align*}
We expect that the infinite sum is this.
\begin{equation*}
  S(x)=\frac{1}{1-x}=1+x+x^2+x^3+x^4+x^5+\cdots
\end{equation*}
\pause
If we plug in $x=2$ then we get $S=1+2+4+8+16+\cdots\,$.
Notice that $2S=2+4+8+16+\cdots$ 
and also that $S-1=2+4+8+16+\cdots\,$.
We seem to have $2S=S-1$, which gives $S=-1$, but that says
$1+2+4+8+16+\cdots=-1$.

\medskip
So there must be rules and infinite sums can give 
crazy results unless you follow the rules.
\end{frame}

\begin{frame}{Preview}
Here is a taste of what we will do in this chapter.
These are a few Taylor series for familiar functions.
\begin{center}
\begin{tabular}{c|l}
   \multicolumn{1}{c}{\tabulartext{Function}}
     &\multicolumn{1}{c}{\tabulartext{Taylor series}}  \\ \hline
   $\displaystyle \frac{1}{1-x}$ &$1+x+x^2+x^3+\cdots$ \rule{0pt}{14pt} \\[1ex]
   $\displaystyle e^x$           &$1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\cdots$  \\[1ex]
   $\displaystyle \cos x$        &$1-\frac{x^2}{2!}+\frac{x^4}{4!}-\frac{x^6}{6!}+\cdots$  \\[1ex]
   $\displaystyle \sin x$        &$x-\frac{x^3}{3!}+\frac{x^5}{5!}-\frac{x^7}{7!}+\cdots$  \\[1ex]
   $\displaystyle \ln(1+x)$      &$x-\frac{x^2}{2}+\frac{x^3}{3}-\frac{x^4}{4}+\cdots$  \\
\end{tabular}
\end{center}
For each there are some $x$'s where there is equality.
For instance, we will find that for the first series equality holds inside
the interval $\open{-1}{1}$ and not outside that interval.
(We will say that the series is centered at $a=0$ 
and the radius of convergence is $1$.)

We will learn how to generate expressions like the above.
And, we will develop the rules for when they make sense,
for example, the rules for where equality holds, or when you can
integrate a function by integrating its series.
We will give a number of ways to calculate those.
\end{frame}



\section{Deep magic}

\begin{frame}
% We are in the process of making sense of these.
% Why?
% \begin{center}
% \begin{tabular}{c|l}
%    \multicolumn{1}{c}{\tabulartext{Function}}
%      &\multicolumn{1}{c}{\tabulartext{Taylor series}}  \\ \hline
%    $\displaystyle \frac{1}{1-x}$ &$1+x+x^2+x^3+\cdots$ \rule{0pt}{14pt} \\[1ex]
%    $\displaystyle e^x$           &$1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\cdots$  \\[1ex]
%    $\displaystyle \cos x$        &$1-\frac{x^2}{2!}+\frac{x^4}{4!}-\frac{x^6}{6!}+\cdots$  \\[1ex]
%    $\displaystyle \sin x$        &$x-\frac{x^3}{3!}+\frac{x^5}{5!}-\frac{x^7}{7!}+\cdots$  \\[1ex]
%    $\displaystyle \ln(1+x)$      &$x-\frac{x^2}{2}+\frac{x^3}{3}-\frac{x^4}{4}+\cdots$  \\
% \end{tabular}
% \end{center}

We be able to do, and understand, things that we did not have before.
One example is the ability to take the derivative or integral of
a series.



\Ex Take the derivative of $\sin x$.
\pause
\begin{align*}
  \frac{d\,\sin x}{dx}
    &=\frac{d\,\bigl( x-\frac{x^3}{3!}+\frac{x^5}{5!}-\frac{x^7}{7!}+\cdots \bigr)}{dx}  \\
    &=1-\frac{x^2}{2!}+\frac{x^4}{4!}-\frac{x^6}{6!}+\cdots
\end{align*}
\pause
Now take the antiderivative.
\pause
\begin{align*}
  \integral{}{}{ \sin x }{x}
    &=\integral{}{}{ \bigl( x-\frac{x^3}{3!}+\frac{x^5}{5!}-\frac{x^7}{7!}+\cdots \bigr) }{x}  \\
    &=\frac{x^2}{2!}-\frac{x^4}{4!}+\frac{x^6}{6!}+\cdots+C  \\
    &=-1+\frac{x^2}{2!}-\frac{x^4}{4!}+\frac{x^6}{6!}+\cdots
\end{align*}
(We get $C=-1$ by setting $x$ to zero on both sides of the equation.)
\end{frame}


\begin{frame}
\Ex Do the same for $e^x\!$. 
\pause 
\begin{align*}
  \frac{d\,e^x}{dx}
    &=\frac{d\,\bigl( 1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\cdots \bigr)}{dx}  \\
    &=1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\cdots
\end{align*}
Now the antiderivative.
\begin{align*}
  \integral{}{}{ e^x }{x}
    &=\integral{}{}{ \bigl( 1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\cdots \bigr) }{x}  \\
    &=x+\frac{x^2}{2!}+\frac{x^3}{3!}+\cdots+C  \\
    &=1+x+\frac{x^2}{2!}+\frac{x^3}{3!}+\cdots  
\end{align*}
(We get $C=1$ by setting $x$ to zero on both sides of the equation.)
\end{frame}

\begin{frame}{They just keep coming}
When you take the term-by-term derivatives, in from the right come
new ones exactly like the old ones that you just handled.
For every term~$1$ whose derivative is zero, there comes in an~$x$
whose derivative replaces the~$1$.
It is like an assembly line.
\begin{center}
\animategraphics[loop,width=4cm]{10}{pix/lucy-}{0}{53}
\end{center}
\end{frame}


\begin{frame}[fragile]{This is powerful magic.}  
\Ex
Recall that $(1/\sqrt{2\pi})\cdot e^{-x^2/2}$ is the probability density 
function for the Normal curve from statistics.
There is no elementary formula for its antiderivative.
\pause
\begin{equation*}
  e^{-x^2/2}=1+\frac{-x^2}{2}+\frac{(-x^2/2)^2}{2!}+\frac{(-x^2/2)^3}{3!}+\cdots
         =1+-\frac{x^2}{2}+\frac{x^4}{4\cdot 2!}-\frac{-x^6}{8\cdot 3!}+\cdots
\end{equation*}
So we get this.
\begin{equation*}
  \integral{}{}{ e^{-x^2} }{x}
    =x-\frac{1}{3}\frac{x^3}{2}
      +\frac{1}{5}\frac{x^5}{4\cdot 2!}-\frac{1}{7}\frac{x^7}{8\cdot 3!}+\cdots  
\end{equation*}
\pause
\begin{lstlisting}
sage: def int_f(x):
....:     return (1/sqrt(2*pi))*(x-(x^3/6)+(x^5/40)-(x^7/366))
....: 
sage: round(int_f(1)-int_f(0), ndigits=3)
0.341
\end{lstlisting}
% sage: int_f(1)-int_f(0)
% 6263/14640*sqrt(2)/sqrt(pi)
\pause
Here is the Normal curve picture from Wikipedia.
\begin{center}
  \includegraphics[height=0.25\textheight]{pix/std_normal_curve.png}
\end{center}
\end{frame}

\begin{frame}
Now, there is tons to do.
We have to understand under what circumstances these manipulations succeed,
and when they fail.
We have to prove that stuff.
And we have to practice lots of examples, etc.
But ...  

\pause\bigskip
\begin{center}
  \includegraphics[height=0.5\textheight]{pix/wizard.png}
\end{center}
\end{frame}



\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
