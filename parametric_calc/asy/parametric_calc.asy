// parametric_calc.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "parametric_calc%03d";

import graph;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// =========== Lissajous ===============
pair F0(real t) { return (cos(t), sin(2*t) ); }

picture pic;
int picnum = 0;
size(pic,0,3.5cm,keepAspect=true);
path f = graph(pic, F0, 0, 2*pi);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-1; ytop=1;

draw(pic, f, FCNPEN);

dotfactor = 4;
dot(pic, Label(format("$t=%d$", 0), filltype=Fill(white)), F0(0), 2*NE, highlight_color); 
dot(pic, Label(format("$t=%d$", 1), filltype=Fill(white)), F0(1), 2*N, highlight_color); 
dot(pic, Label(format("$t=%d$", 2), filltype=Fill(white)), F0(2), 2*SE, highlight_color);
dot(pic, Label(format("$t=%d$", 3), filltype=Fill(white)), F0(3), 2*NW, highlight_color); 
dot(pic, Label(format("$t=%d$", 4), filltype=Fill(white)), F0(4), 2N, highlight_color); 
dot(pic, Label(format("$t=%d$", 5), filltype=Fill(white)), F0(5), NE, highlight_color); 
dot(pic, Label(format("$t=%d$", 6), filltype=Fill(white)), F0(6), E, highlight_color); 

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ....... include tangent line ...........
picture pic;
int picnum = 1;
size(pic,0,3.5cm,keepAspect=true);
path f = graph(pic, F0, 0, 2*pi);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-1; ytop=1;

draw(pic, f, FCNPEN);

// draw tangent line
real t = 0.7;
// can't get dir to work
// pair m = dir(f, t, normalize=true);
// pair m = dir(f, t, normalize=true);
// write(format(" m.x=%f", m.x));
// write(format(" m.y=%f", m.y));
pair m = (1, (2*cos(2*t))/(-sin(t)));
pair ell(real s) { return F0(t)+(s*m.x,s*m.y); };
draw(pic, graph(pic, ell, -0.5, 0.5), highlight_color);
dot(pic, F0(t), highlight_color);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ======== proof of derivative ============
picture pic;
int picnum = 2;
size(pic,0,2.5cm,keepAspect=true);

real t = 0.7;
path f_above = graph(pic, F0, t-0.2, t+0.2);
real t_below = 2*pi-0.7;
path f_below = graph(pic, F0, t_below-0.2, t_below+0.2);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-1; ytop=1;

draw(pic, f_above, FCNPEN);
draw(pic, f_below, FCNPEN);
dotfactor = 4;
dot(pic, F0(t), highlight_color);
dot(pic, F0(t_below), highlight_color);

// draw tangent line
// can't get dir to work
// pair m = dir(f, t, normalize=true);
// pair m = dir(f, t, normalize=true);
// write(format(" m.x=%f", m.x));
// write(format(" m.y=%f", m.y));
// pair m = (1, (2*cos(2*t))/(-sin(t)));
// pair ell(real s) { return F0(t)+(s*m.x,s*m.y); };
// draw(pic, graph(pic, ell, -0.5, 0.5), highlight_color);
// dot(pic, F0(t), highlight_color);

// x and y axes

real[] T2 = {cos(t)};
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks("%", T2, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$\cos(0.7)$", cos(t));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// .............. nbd of t=0 no functional relationship ..........
picture pic;
int picnum = 3;
size(pic,0,2.5cm,keepAspect=true);

real t = 0;
path f = graph(pic, F0, t-0.3, t+0.3);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=-1; ytop=1;

draw(pic, f, FCNPEN);
dotfactor = 4;
dot(pic, F0(t), highlight_color);

// x and y axes

real[] T2 = {cos(t)};
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=NoTicks,   // RightTicks("%", T2, Size=2pt),
      arrow=Arrows(TeXHead));
// labelx(pic, "$\cos()$", cos(t));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// =========== parametric that crosses itself ===============
pair F4(real t) { return (t^3-4t, t^2 ); }

picture pic;
int picnum = 4;
size(pic,0,3.5cm,keepAspect=true);
path f = graph(pic, F4, -2.5, 2.5);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-6; xright=6;
ybot=0; ytop=7;

draw(pic, f, FCNPEN);

dotfactor = 5;
dot(pic, Label(format("$t=%f$", -2.5), filltype=Fill(white)), F4(-2.5), 2*NE, highlight_color); 
dot(pic, Label(format("$t=%d$", 0), filltype=Fill(white)), F4(0), 2*N, highlight_color); 
dot(pic, Label(format("$t=%f$", 2.5), filltype=Fill(white)), F4(2.5), 2*SE, highlight_color);
// dot(pic, Label(format("$t=%d$", 3), filltype=Fill(white)), F0(3), 2*NW, highlight_color); 
// dot(pic, Label(format("$t=%d$", 4), filltype=Fill(white)), F0(4), 2N, highlight_color); 
// dot(pic, Label(format("$t=%d$", 5), filltype=Fill(white)), F0(5), NE, highlight_color); 
// dot(pic, Label(format("$t=%d$", 6), filltype=Fill(white)), F0(6), E, highlight_color); 

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.25, xmax=xright+0.25,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// =========== arc length for a parametric ===============
path f = (0.5,1){E}..(1,0.75)..(3,0.65)..(3.5,1.5)..(3,2.5)..{S}(1.5,2.15);

for (int i=0; i<3; ++i) {
  picture pic;
  int picnum = 5+i;
  size(pic,0,3.5cm,keepAspect=true);

  real xleft, xright, ybot, ytop; // limits of graph
  xleft=0; xright=4;
  ybot=0; ytop=3;

  draw(pic, f, FCNPEN);

  pair[] pts;
  dotfactor = 5;
  int num_dots = i*6;
  for (int j=0; j<num_dots; ++j) {
    pts.push( relpoint(f, j/(num_dots-1)) );
  }
  for (int j=1; j<num_dots; ++j) {
    draw(pic, pts[j-1]--pts[j], highlight_color);
  }
  for (int j=0; j<num_dots; ++j) {
    dot(pic, pts[j], highlight_color);
  }

// x and y axes
  Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
  // Draw graph paper
  xaxis(pic, L="",  
	axis=YEquals(ytop+0.2),
	xmin=xleft-0.25, xmax=xright+0.25,
	p=nullpen,
	ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
	arrow=Arrows(TeXHead));
  xaxis(pic, L="",  
	axis=YEquals(ybot-0.2),
	xmin=xleft-0.25, xmax=xright+0.25,
	p=nullpen,
	ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
	arrow=Arrows(TeXHead));
  // The x axis
  xaxis(pic, L=L,  
	axis=YZero,
	xmin=xleft-0.25, xmax=xright+0.25,
	p=currentpen,
	ticks=NoTicks, // RightTicks(Step=1, OmitTick(0), Size=2pt),
	arrow=Arrows(TeXHead));
  
  // y axis
  Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
  // Graph paper
  yaxis(pic, L="",  
	axis=XEquals(xleft-0.2),
	ymin=ybot-0.25, ymax=ytop+0.25,
	p=nullpen,
	ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
	arrow=Arrows(TeXHead));
  yaxis(pic, L="",  
	axis=XEquals(xright+0.2),
	ymin=ybot-0.25, ymax=ytop+0.25,
	p=nullpen,
	ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
	arrow=Arrows(TeXHead));
  // y-axis
  yaxis(pic, L=L,  
	axis=XZero,
	ymin=ybot-0.25, ymax=ytop+0.25,
	p=currentpen,
	ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
	arrow=Arrows(TeXHead));
  
  shipout(format(OUTPUT_FN,picnum),pic);
}



