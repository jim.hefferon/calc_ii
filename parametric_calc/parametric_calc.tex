\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 10.2\hspace{1em}Calculus with parametrics}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Calculus with parametrics}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage arc_length_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
% \input asy/arc_length_3d.pre
% \usepackage[bigfiles]{media9}
% \RequirePackage{asymptote}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



% \section{}


\begin{frame}
  \frametitle{With parametrics we can describe non-functions}
This curve is described by $F(t)=(\cos t, \sin 2t)$ for 
$t\in\leftclosed{0}{2\pi}$.
Notice that $y$ is not a function of~$x$\Dash there are $x$'s for which
there are two associated~$y$'s.\begin{center}
  \vcenteredhbox{\includegraphics{parametric_calc000.pdf}}    
\end{center}
Another example of a curve where $y$ is not a function of~$x$ that we have
already seen is the parametrized unit circle.
While $x$ and~$y$ are functions of~$t$, that is, $x=x(t)$ and $y=y(t)$, 
we have that $y$ is not a function of~$x$.

Nonetheless, we will want to do Calculus.
In particular, we want to take the derivative, $dy/dx$, and find 
arc lengths.  

\pause
\textit{Remark}\hspace{0.5em} 
This is a \alert{Lissajous figure}, describing the superposition of 
two perpendicular oscillations of different angular frequencies,
here $t$ and~$2t$.
% It was  
% first investigated by Nathaniel Bowditch,
% whose his book \textit{The New American Practical Navigator}.,
% first published in 1802, is still on board every commissioned US Naval 
% vessel.)
\end{frame}

\begin{frame}{Derivatives}
We want to find the derivative, the rate of change.
\begin{center}
  \vcenteredhbox{\includegraphics{parametric_calc001.pdf}}    
\end{center}

\Tm Where $F(t)=(x(t),y(t))$, the rate of change is this.
\begin{equation*}
  \frac{dy}{dx}=\frac{y'(t)}{x'(t)}
               =\frac{dy/dt}{dx/dt}
  \qquad 
  \text{provided $x'(t)$ is not zero}
\end{equation*}

\Ex The figure above shows $F(t)=(\cos t, \sin 2t)$.
The slope of the tangent line is 
\begin{equation*}
  \frac{dy}{dx}=\frac{2\cos 2t}{-\sin t}
  \qquad\text{where $\sin t\neq 0$}
\end{equation*}
It picks out the point
$F(0.7)\approx (0.765,0.985)$ and at $t=0.7$ the slope is about $-0.528$.
\end{frame}
% sage: def F(t):
% ....:     return (cos(t), sin(2*t))
% ....: 
% sage: F(0.7)
% (0.764842187284488, 0.985449729988460)
% sage: def F_prime(t):
% ....:     return 2*cos(2*t)/(-sin(t))
% ....: 
% sage: F_prime(0.7)
% -0.527669904963444


\begin{frame} 
\Pf
In the neighborhood of the point of interest, think of $y$ as a function of~$x$,
which in turn is a function of~$t$.
A function of a function is a composition, so apply the Chain Rule.
\begin{equation*}
  \frac{dy}{dt}=\frac{dy}{dx}\cdot \frac{dx}{dt}
\end{equation*}
The theorem statement reorders this equation.
\end{frame}


\begin{frame}{Mathematical niceties}
In that proof the `think of $y$ as a function of~$x$' is doing a lot of work.
In the figure, directly below the point $F(0.7)$ the curve contains 
another point.
\begin{center}\small
  $F(t)=(\cos t,\sin 2t)$ near $x=\cos(0.7)$
  \quad
  \vcenteredhbox{\includegraphics{parametric_calc002.pdf}}
\end{center}
(On top $t=0.7$, while on the bottom $t=2\pi-0.7$.)
At the two points, just by eye, there are different $dy/dx$'s.

Stated another way, naively looking for the derivative at that $x$~coordinate 
using the limit of the difference quotient
\begin{equation*}
  \frac{dy}{dx}=\lim_{h\to 0}\frac{f(\cos(0.7)+h)-f(\cos(0.7))}{h}
\end{equation*}
is a problem:~the expressions $f(\cos(0.7)+h)$ and $f(\cos(0.7))$ are ambiguous.

Instead, we need to look at the curve only close to the point $F(0.7)$.
Around $F(0.7)$ we draw a circle of small radius and consider only what
is happening inside that circle.
Inside that circle $y$ is a function of~$x$ and we can proceed.
\end{frame}

\begin{frame}
The only place that we can't get a functional relationship between $x$ and~$y$
is where
$x'(t)$ is zero. 

This picture shows a point on the figure where $x'(t)=0$.
We cannot solve the problem of ambiguity with 
`around this point draw a circle of small radius'
because no matter how small a circle you draw around $(0,0)$,
on the curve there are $x$'s with two associated~$y$'s.
\begin{center}
  \vcenteredhbox{\includegraphics{parametric_calc003.pdf}}    
\end{center}
That's why the theorem statement excludes $x'(t)=0$.
\end{frame}

\begin{frame}
\Ex Find the slope of the tangent to the cycloid
at $\theta=\pi/4$.
\begin{equation*}
  F(\theta)=(\theta-\sin \theta,1-\cos \theta)
\end{equation*}

Taking the derivatives gives this.
\begin{equation*}
  \frac{dy}{dx}=\frac{\sin\theta}{1-\cos\theta}
\end{equation*}
and plugging in $\theta=\pi/4$ gives $\sqrt{2}/(2-\sqrt{2})$.
\end{frame}

\begin{frame}{Practice}
\begin{enumerate}
\item Let $x(t)=t^2$ and $y(t)=2t+1$.
Find $dy/dx$ when $t=1$.
Find the equation of the tangent line when~$t=1$.
Find all $t$ where the tangent line is horizontal. 

\pause\item Let $F(t)=(2t^2+1,3t-t^3)$.
Find the equation of the tangent line at $t=4$.

\pause\item Find $dy/dx$ for $x(t)=3\cos t$ and $y(t)=3\sin t$
with $0\leq t<2\pi$.

% \pause\item Find the derivative for $F(t)=(t^2-3, 2t-1)$ when 
% $-2\leq t\leq 2$.
% Check that it is defined at all $t$ in the domain.
\end{enumerate}
\end{frame}

\begin{frame}{Curves can have multiple derivatives}
\Ex
Let $x(t)=t^3-4t$ and $y(t)=t^2\!$.
Try some values for $t$ and sketch.
\pause
\begin{center}
  \vcenteredhbox{\includegraphics{parametric_calc004.pdf}}
\end{center}
Observe that the curve crosses itself at the point $(0,4)$.
So it has two tangent lines there.
Find their equations.

\pause
Set $t^3-4t=0$ and $t^2=4$.
The only $t$'s shared between the two equaions are $-2$ and~$2$.

We have $dx/dt=3t^2-4$ and $dy/dt=2t$.
So $dy/dx=(2t)/(3t^2-4)$.
When $t=-2$ the tangent line has the slope $-1/2$.
When $t=+2$ the tangent line has the slope $1/2$. 
\end{frame}


% \begin{frame}
% \Ex We fire a projectile at the angle~$\theta$ with an initial speed of~$v_0$.
% Under the influence of gravity alone
% (that is, assuming no air resistance and that the ground is flat) the object
% traverses a path given by this 
% parametric equation where $t\geq 0$.
% \begin{equation*}
%   x(t)=(v_0\cos \theta)\cdot t
%   \quad 
%   y(t)=-\frac{1}{2}g\cdot t^2+(v_0\sin \theta)\cdot t
% \end{equation*}
% Find $dy/dx$ as a function of time.

% \pause
% We have $x'(t)=v_0\cos \theta$ and 
% $y'(t)=-gt+v_0\sin\theta$.
% The slope of the tangent line is the quotient of the two.
% \begin{equation*}
%   \frac{dy}{dx}
%   =
%   \frac{-gt+v_0\sin\theta}{v_0\cos \theta}
%   =\tan\theta-\frac{gt}{v_0\cos\theta}
% \end{equation*}

% \pause
% At what time is the projectile at its high spot?

% \pause
% We have this.
% \begin{equation*}
%   0=\frac{v_0\sin\theta-gt}{v_0\cos\theta}
%   \quad\text{implies}\quad
%   v_0\sin\theta-gt=0
% \end{equation*}
% So $t=v_0\sin\theta/g$.
% \end{frame}






\section{Arc length and speed}

\begin{frame}{Background of arc length for parametric functions}
To find the arc length we start with the curve described by $x(t)$ and
$y(t)$.
\begin{center}
  \only<1>{\vcenteredhbox{\includegraphics{parametric_calc005.pdf}}}%    
  \only<2>{\vcenteredhbox{\includegraphics{parametric_calc006.pdf}}}%    
  \only<3->{\vcenteredhbox{\includegraphics{parametric_calc007.pdf}}}%    
\end{center}
\pause
We subdivide into line segments described with $\Delta t$ increments.
Each segment is 
$|P_iP_{i+1}|=\sqrt{(x(t_{i+1})-x(t_{i}))^2+(y(t_{i+1})-y(t_{i}))^2}$ long.
\pause
We intend to let $\Delta t$ go to zero and get an integral.
There are a few technicalities to overcome.

As with the arc length argument from a couple of sections ago, 
to get an integral expression we need to put the limit
in a form that matches the definition of the integral.
\begin{equation*}
  \integral{t=a}{b}{f(t)}{t}
  =\lim_{\Delta\theta\to 0}\,\Bigl[ \sum_{i} f(t_i^*)\,\Delta t\Bigr]
  \qquad\text{where $t_i^*$ in interval $i$}
\end{equation*}
\end{frame}


\begin{frame}
We need the Mean Value Theorem, twice.
In the interval $\closed{t_{i}}{t_{i+1}}$ we find $t_i^*$
and $t_i^{**}$ so that this holds
(where $\Delta t_i=t_{i+1}-t_{i}$).
\begin{equation*}
  \frac{x(t_{i+1})-x(t_{i})}{\Delta t_i}=x'(t_i^*)
  \qquad
  \frac{y(t_{i+1})-y(t_{i})}{\Delta t_i}=y'(t_i^{**})
\end{equation*}
Clear the fractions.
\begin{equation*}
  x(t_{i+1})-x(t_{i})=x'(t_i^*)\Delta t_i
  \qquad
  y(t_{i+1})-y(t_{i})=y'(t_i^{**})\Delta t_i
\end{equation*}
Then this is the length of line segments. 
\begin{align*}
  |P_iP_{i+1}|
    &=\sqrt{\bigl(x'(t_i^*)\,\Delta t_i\bigr)^2+(y'(t_i^{**})\,\Delta t_i)^2} \\
    &=\sqrt{x'(t_i^*)^2+y'(t_i^{**})^2}\,\Delta t_i
\end{align*}
% \end{frame}

% \begin{frame}
% With the line segments lengths at $\sqrt{x'(t_i^*)^2+y'(t_i^{**})^2}\,\Delta t$
We are close to fitting the definition of the definite integral.
% \begin{center}
%   \vcenteredhbox{\includegraphics{parametric_calc007.pdf}}%    
% \end{center}
The problem is that $t_i^*$ and~$t_i^{**}$ are not equal.
But for this class we will wave this away and just assert that
if the derivatives $x'$ and $y'$ are continuous then we can show that the
sum 
has the integral as the limit as $\Delta t_i$ goes to zero.
\begin{equation*}
  \lim_{\Delta t\to 0}\Bigl[\sum_i\sqrt{x'(t_i^*)^2+y'(t_i^{**})^2}\,\Delta t\Bigr]
  =\integral{t=a}{b}{ \sqrt{x'(t)^2+y'(t)^2} }{t}
\end{equation*}
\end{frame}


\begin{frame}{Definition of arc length for parametrically defined functions}
\Df 
Let $\map{F}{\R}{\R^2}$ have the coordinate functions $x$ and~$y$,
where the derivatives $x'$ and~$y'$ are continuous.
Then the \alert{arc length} for $a\leq t\leq b$ is this.
\begin{equation*}
  s=
  \integral{t=a}{b}{ \sqrt{x'(t)^2+y'(t)^2} }{t}
\end{equation*}
Further, the \alert{speed} as a function of $t$ is 
$\operatorname{Speed}(t)=\sqrt{x'(t)^2+y'(t)^2}$.

\medskip
\textit{Remark}\hspace{0.5em} This illustrates again the Net Change Theorem
because we find the distance~$s$ by accumulating the rate 
$\operatorname{Speed}$.
\end{frame}



\begin{frame}
\Ex Find the arc length for the curve parametrized by 
$F(t)=(3t^2,2t^3)$ where $1\leq t\leq 10$.

\pause
The formula gives this.  
\begin{equation*}
  \integral{t=1}{10}{ \sqrt{(6t)^2+(6t^2)^2} }{t}
  =
  \integral{t=1}{10}{ \sqrt{36t^2(1+t^2)} }{t}
  =
  6\cdot \integral{1}{10}{ t\cdot\sqrt{1+t^2} }{t}
\end{equation*}
For the antiderivative take $t=\tan\theta$, giving $dt=\sec^2\theta\,d\theta$.
\begin{equation*}
  \integral{}{}{ t\sqrt{1+t^2} }{t}
  =
  \integral{}{}{ \tan\theta\sqrt{\sec^2\theta}\sec^2\theta }{\theta}
  =
  \integral{}{}{ \sec^2\theta\cdot\sec\theta\tan\theta }{\theta}
\end{equation*}
With $u=\sec\theta$ and $du=\sec\theta\tan\theta$ we get
\begin{equation*}
  =\integral{}{}{u^2}{u}
  =\frac{u^3}{3}+C
  =\frac{\sec^3\theta}{3}+C
\end{equation*}
Converting back to $t$'s gives the answer.
\begin{equation*}
  6\cdot\netchange{t=1}{10}{ \frac{(\sqrt{1+t^2})^3}{3} }
  =2\cdot\bigl((101)^{3/2}-(2)^{3/2}\bigr)
\end{equation*}
\end{frame}


\begin{frame}
\Ex
Find the arc length for one arch of a cycloid.
\begin{equation*}
  x(t)=t-\sin t
  \quad
  y(t)=1-\cos t
  \quad
  0\leq t\leq 2\pi
\end{equation*}
\pause
Taking derivatives
$x'(t)=1-\cos t$
and
$y'(t)=\sin t$
and putting it into the formula gives this.
\begin{align*}
  s
  =\integral{t=0}{2\pi}{ \sqrt{(1-\cos t)^2+\sin^2 t} }{t}
  &=\integral{0}{2\pi}{ \sqrt{ 1-2\cos t+\cos^2t+\sin^2 t } }{t}  \\
  &=\integral{0}{2\pi}{ \sqrt{ 2(1-\cos t) } }{t}
\end{align*}
Recall the half-angle formula, $\sin^2 t=(1/2)(1-\cos 2t)$.
Restated it is $(1/2)(1-\cos t)=\sin^2(t/2)$.
\begin{align*}
  &=\integral{t=0}{2\pi}{ \sqrt{ 2(2\sin^2(t/2)) } }{t}  \\
  &=\integral{0}{2\pi}{ \sqrt{ 4\sin^2(t/2) } }{t}  \\
  &=\integral{0}{2\pi}{ 2\sin(t/2) }{t}  \\
  &=-4\cdot\netchange{0}{2\pi}{\cos(t/2)}=8
\end{align*}
\end{frame}



\begin{frame}{Practice}
% Find the arc length of each.
% \begin{enumerate}
% \item $x(t)=2t^{3/2}$, $y(t)=3t+1$, $0\leq t\leq 4$
% \pause
% \item $x(t)=t^3+2$, $y(t)=2t^{9/2}$, $1\leq t\leq 3$
% \end{enumerate}

\Ex For $F(t)=(3t^2+4,1-2t)$, find the expression for speed.
Find the speed at $t=3$.
When is the speed minimal?

\pause
The speed is $\sqrt{(6t)^2+(-2)^2}=\sqrt{36t^2+4}$.
At $t=3$ the speed is $\sqrt{328}\approx 18.11$.
The speed is minimal at $t=0$, where it equals $2$.
\end{frame}



  
\begin{frame}{Arc length may differ from curve length}
For example, if we traverse a circle twice then the arc length formula
will give twice the circumference.

\Ex Take $F(\theta)=(\cos\theta,\sin\theta)$ for $0\leq\theta\leq 4\pi$.
\begin{equation*}
  \integral{0}{4\pi}{ \sqrt{(-\sin\theta)^2+(\cos\theta)^2} }{\theta}
  =\integral{0}{4\pi}{ \sqrt{1} }{\theta}
  =\Bigl[ \theta \Bigr]_{0}^{4\pi}
  =4\pi
\end{equation*}

Likewise, if we traversed the curve in reverse, $4\pi\leq \theta\leq 0$,
then we get negative arc length.
\end{frame}


\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
