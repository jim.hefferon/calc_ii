# Makefile for calc_ii slides
#
# 2024-May-05  Jim Hefferon Public Domain

# Usage: This works for me.
# For initial compilation, from my Linux command line I run this.
#   $ make all LATEX_TWICE=yes
# I have TeX Live with Asymptote installed. I'm sorry, I can't help
# with other systems.

# Outline.
# Each section (that is, lecture) is in a separate directory, structured as here.
# This shows only the source files.
#  work/         
#      work.tex  Beamer file to make slides .pdf
#      asy/     
#          work.asy  For 2D graphics in .pdf
#          work_3d.asy  For 3D graphics
#      pix/   Holds .jpg, etc.
# To compile the .tex Beamer files the system must first compile the .asy files,
# some of which are 2D and some 3D.

# TODO
# 1) Way to take advantage of repeated structure of various dirs?

SHELL=/bin/bash

LATEX:=lualatex
ASYMPTOTE:=asy
ASYMPTOTE3D:=asy -inlineimage

ABSOLUTE_CONVERGENCE := absolute_convergence
ARC_LENGTH := arc_length
BY_PARTS := by_parts
COMPLEX_NUMBERS := complex_numbers
CONICS := conics
CROSS_PRODUCT := cross_product
DOT_PRODUCT := dot_product
GAUSS_METHOD := gauss_method
IMPROPER_INTEGRALS := improper_integrals
LINES := lines
NUMERICAL_INTEGRATION := numerical_integration
PARAMETRIC := parametric
PARAMETRIC_CALC := parametric_calc
PARTIAL_FRACTIONS := partial_fractions
POLARS := polars
POWER_SERIES := power_series
QUADRIC := quadric
REVIEW_INTEGRATION := review_integration
REVIEW_TRIG := review_trig
ROOT_TEST := root_test
SEQUENCES := sequences
SERIES := series
SERIES_MOTIVATION := series_motivation
SERIES_REVIEW := series_review
SERIES_TEST_GUIDELINES := series_test_guidelines
SERIES_WITH_POSITIVE_TERMS := series_with_positive_terms
TAYLOR_POLYS := taylor_polys
TAYLOR_SERIES := taylor_series
TRIG_INTEGRALS := trig_integrals
TRIG_SUBST := trig_subst
VECTORS := vectors


all: $(ARC_LENGTH)/$(ARC_LENGTH).pdf  \
    $(ABSOLUTE_CONVERGENCE)/$(ABSOLUTE_CONVERGENCE).pdf \
    $(BY_PARTS)/$(BY_PARTS).pdf \
    $(COMPLEX_NUMBERS)/$(COMPLEX_NUMBERS).pdf \
    $(CONICS)/$(CONICS).pdf \
    $(CROSS_PRODUCT)/$(CROSS_PRODUCT).pdf \
    $(DOT_PRODUCT)/$(DOT_PRODUCT).pdf \
    $(GAUSS_METHOD)/$(GAUSS_METHOD).pdf \
    $(IMPROPER_INTEGRALS)/$(IMPROPER_INTEGRALS).pdf \
    $(LINES)/$(LINES).pdf \
    $(NUMERICAL_INTEGRATION)/$(NUMERICAL_INTEGRATION).pdf \
    $(PARAMETRIC)/$(PARAMETRIC).pdf \
    $(PARAMETRIC_CALC)/$(PARAMETRIC_CALC).pdf \
    $(PARTIAL_FRACTIONS)/$(PARTIAL_FRACTIONS).pdf \
    $(POLARS)/$(POLARS).pdf \
    $(POWER_SERIES)/$(POWER_SERIES).pdf \
    $(QUADRIC)/$(QUADRIC).pdf \
    $(REVIEW_INTEGRATION)/$(REVIEW_INTEGRATION).pdf \
    $(REVIEW_TRIG)/$(REVIEW_TRIG).pdf \
    $(ROOT_TEST)/$(ROOT_TEST).pdf \
    $(SEQUENCES)/$(SEQUENCES).pdf \
    $(SERIES)/$(SERIES).pdf \
    $(SERIES_MOTIVATION)/$(SERIES_MOTIVATION).pdf \
    $(SERIES_REVIEW)/$(SERIES_REVIEW).pdf \
    $(SERIES_TEST_GUIDELINES)/$(SERIES_TEST_GUIDELINES).pdf \
    $(SERIES_WITH_POSITIVE_TERMS)/$(SERIES_WITH_POSITIVE_TERMS).pdf \
    $(TAYLOR_POLYS)/$(TAYLOR_POLYS).pdf \
    $(TAYLOR_SERIES)/$(TAYLOR_SERIES).pdf \
    $(TRIG_INTEGRALS)/$(TRIG_INTEGRALS).pdf \
    $(TRIG_SUBST)/$(TRIG_SUBST).pdf \
    $(VECTORS)/$(VECTORS).pdf \


# =============================
# Includes 2D asy and 3D
$(ARC_LENGTH)/$(ARC_LENGTH).pdf: $(ARC_LENGTH)/$(ARC_LENGTH).tex $(ARC_LENGTH)/asy/$(ARC_LENGTH)000.pdf
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(ARC_LENGTH)/asy/$(ARC_LENGTH)000.pdf: $(ARC_LENGTH)/asy/$(ARC_LENGTH).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))

$(ARC_LENGTH)/asy/$(ARC_LENGTH)_3d000.tex: $(ARC_LENGTH)/asy/$(ARC_LENGTH)_3d.asy
	cd $(@D); \
	$(ASYMPTOTE3D) $(subst 000,,$(basename $(notdir $(@))))


# =============================
# No 2D asy and no 3D
$(ABSOLUTE_CONVERGENCE)/$(ABSOLUTE_CONVERGENCE).pdf: $(ABSOLUTE_CONVERGENCE)/$(ABSOLUTE_CONVERGENCE).tex $(ABSOLUTE_CONVERGENCE)/pix/rtvva.png 
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif


# =============================
$(BY_PARTS)/$(BY_PARTS).pdf: $(BY_PARTS)/$(BY_PARTS).tex 
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif


# =============================
$(COMPLEX_NUMBERS)/$(COMPLEX_NUMBERS).pdf: $(COMPLEX_NUMBERS)/$(COMPLEX_NUMBERS).tex $(COMPLEX_NUMBERS)/asy/$(COMPLEX_NUMBERS)000.pdf
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(COMPLEX_NUMBERS)/asy/$(COMPLEX_NUMBERS)000.pdf: $(COMPLEX_NUMBERS)/asy/$(COMPLEX_NUMBERS).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(CONICS)/$(CONICS).pdf: $(CONICS)/$(CONICS).tex $(CONICS)/asy/$(CONICS)000.pdf $(CONICS)/asy/$(CONICS)_3d000.tex
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(CONICS)/asy/$(CONICS)000.pdf: $(CONICS)/asy/$(CONICS).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))

$(CONICS)/asy/$(CONICS)_3d000.tex: $(CONICS)/asy/$(CONICS)_3d.asy
	cd $(@D); \
	$(ASYMPTOTE3D) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(CROSS_PRODUCT)/$(CROSS_PRODUCT).pdf: $(CROSS_PRODUCT)/$(CROSS_PRODUCT).tex $(CROSS_PRODUCT)/asy/$(CROSS_PRODUCT)000.pdf $(CROSS_PRODUCT)/asy/$(CROSS_PRODUCT)_3d000.tex $(CROSS_PRODUCT)/pix/right.png
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(CROSS_PRODUCT)/asy/$(CROSS_PRODUCT)000.pdf: $(CROSS_PRODUCT)/asy/$(CROSS_PRODUCT).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))

$(CROSS_PRODUCT)/asy/$(CROSS_PRODUCT)_3d000.tex: $(CROSS_PRODUCT)/asy/$(CROSS_PRODUCT)_3d.asy
	cd $(@D); \
	$(ASYMPTOTE3D) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(DOT_PRODUCT)/$(DOT_PRODUCT).pdf: $(DOT_PRODUCT)/$(DOT_PRODUCT).tex $(DOT_PRODUCT)/asy/$(DOT_PRODUCT)000.pdf $(DOT_PRODUCT)/asy/$(DOT_PRODUCT)_3d000.tex $(DOT_PRODUCT)/pix/pulling.jpg $(DOT_PRODUCT)/pix/walking.jpg  
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(DOT_PRODUCT)/asy/$(DOT_PRODUCT)000.pdf: $(DOT_PRODUCT)/asy/$(DOT_PRODUCT).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))

$(DOT_PRODUCT)/asy/$(DOT_PRODUCT)_3d000.tex: $(DOT_PRODUCT)/asy/$(DOT_PRODUCT)_3d.asy
	cd $(@D); \
	$(ASYMPTOTE3D) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(GAUSS_METHOD)/$(GAUSS_METHOD).pdf: $(GAUSS_METHOD)/$(GAUSS_METHOD).tex
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif


# =============================
$(IMPROPER_INTEGRALS)/$(IMPROPER_INTEGRALS).pdf: $(IMPROPER_INTEGRALS)/$(IMPROPER_INTEGRALS).tex $(IMPROPER_INTEGRALS)/pix/baseball.png 
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(IMPROPER_INTEGRALS)/asy/$(IMPROPER_INTEGRALS)000.pdf: $(IMPROPER_INTEGRALS)/asy/$(IMPROPER_INTEGRALS).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
# lines_3d000.tex dne
$(LINES)/$(LINES).pdf: $(LINES)/$(LINES).tex $(LINES)/asy/$(LINES)000.pdf $(LINES)/asy/$(LINES)_3d001.tex
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(LINES)/asy/$(LINES)000.pdf: $(LINES)/asy/$(LINES).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))

$(LINES)/asy/$(LINES)_3d001.tex: $(LINES)/asy/$(LINES)_3d.asy
	cd $(@D); \
	$(ASYMPTOTE3D) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(NUMERICAL_INTEGRATION)/$(NUMERICAL_INTEGRATION).pdf: $(NUMERICAL_INTEGRATION)/$(NUMERICAL_INTEGRATION).tex $(NUMERICAL_INTEGRATION)/asy/$(NUMERICAL_INTEGRATION)000.pdf $(NUMERICAL_INTEGRATION)/src/$(NUMERICAL_INTEGRATION).sage
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(NUMERICAL_INTEGRATION)/asy/$(NUMERICAL_INTEGRATION)000.pdf: $(NUMERICAL_INTEGRATION)/asy/$(NUMERICAL_INTEGRATION).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(PARAMETRIC)/$(PARAMETRIC).pdf: $(PARAMETRIC)/$(PARAMETRIC).tex $(PARAMETRIC)/asy/$(PARAMETRIC)000.pdf
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(PARAMETRIC)/asy/$(PARAMETRIC)000.pdf: $(PARAMETRIC)/asy/$(PARAMETRIC).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(PARAMETRIC_CALC)/$(PARAMETRIC_CALC).pdf: $(PARAMETRIC_CALC)/$(PARAMETRIC_CALC).tex $(PARAMETRIC_CALC)/asy/$(PARAMETRIC_CALC)000.pdf 
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(PARAMETRIC_CALC)/asy/$(PARAMETRIC_CALC)000.pdf: $(PARAMETRIC_CALC)/asy/$(PARAMETRIC_CALC).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(PARTIAL_FRACTIONS)/$(PARTIAL_FRACTIONS).pdf: $(PARTIAL_FRACTIONS)/$(PARTIAL_FRACTIONS).tex
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif


# =============================
$(POLARS)/$(POLARS).pdf: $(POLARS)/$(POLARS).tex $(POLARS)/asy/$(POLARS)000.pdf
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(POLARS)/asy/$(POLARS)000.pdf: $(POLARS)/asy/$(POLARS).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(POWER_SERIES)/$(POWER_SERIES).pdf: $(POWER_SERIES)/$(POWER_SERIES).tex $(POWER_SERIES)/asy/$(POWER_SERIES)000.pdf
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(POWER_SERIES)/asy/$(POWER_SERIES)000.pdf: $(POWER_SERIES)/asy/$(POWER_SERIES).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(QUADRIC)/$(QUADRIC).pdf: $(QUADRIC)/$(QUADRIC).tex $(QUADRIC)/asy/$(QUADRIC)000.pdf $(QUADRIC)/asy/$(QUADRIC)_3d000.tex
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(QUADRIC)/asy/$(QUADRIC)000.pdf: $(QUADRIC)/asy/$(QUADRIC).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))

$(QUADRIC)/asy/$(QUADRIC)_3d000.tex: $(QUADRIC)/asy/$(QUADRIC)_3d.asy
	cd $(@D); \
	$(ASYMPTOTE3D) $(subst 000,,$(basename $(notdir $(@))))


# =============================
# Includes 2D asy but no 3D
$(REVIEW_INTEGRATION)/$(REVIEW_INTEGRATION).pdf: $(REVIEW_INTEGRATION)/$(REVIEW_INTEGRATION).tex $(REVIEW_INTEGRATION)/asy/$(REVIEW_INTEGRATION)000.pdf $(REVIEW_INTEGRATION)/pix/rooftop-solar-panels.jpg 
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(REVIEW_INTEGRATION)/asy/$(REVIEW_INTEGRATION)000.pdf: $(REVIEW_INTEGRATION)/asy/$(REVIEW_INTEGRATION).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(REVIEW_TRIG)/$(REVIEW_TRIG).pdf: $(REVIEW_TRIG)/$(REVIEW_TRIG).tex $(REVIEW_TRIG)/asy/$(REVIEW_TRIG)000.pdf $(REVIEW_TRIG)/pix/tree.png
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(REVIEW_TRIG)/asy/$(REVIEW_TRIG)000.pdf: $(REVIEW_TRIG)/asy/$(REVIEW_TRIG).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(ROOT_TEST)/$(ROOT_TEST).pdf: $(ROOT_TEST)/$(ROOT_TEST).tex
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif


# =============================
$(SEQUENCES)/$(SEQUENCES).pdf: $(SEQUENCES)/$(SEQUENCES).tex $()/asy/$()000.pdf $()/asy/$()_3d000.tex
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(SEQUENCES)/asy/$(SEQUENCES)000.pdf: $(SEQUENCES)/asy/$(SEQUENCES).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(SERIES)/$(SERIES).pdf: $(SERIES)/$(SERIES).tex $(SERIES)/asy/$(SERIES)000.pdf
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(SERIES)/asy/$(SERIES)000.pdf: $(SERIES)/asy/$(SERIES).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(SERIES_MOTIVATION)/$(SERIES_MOTIVATION).pdf: $(SERIES_MOTIVATION)/$(SERIES_MOTIVATION).tex $(SERIES_MOTIVATION)/pix/lucy-0.png $(SERIES_MOTIVATION)/pix/std_normal_curve.png $(SERIES_MOTIVATION)/pix/wizard.png
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif


# =============================
$(SERIES_REVIEW)/$(SERIES_REVIEW).pdf: $(SERIES_REVIEW)/$(SERIES_REVIEW).tex
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif


# =============================
$(SERIES_TEST_GUIDELINES)/$(SERIES_TEST_GUIDELINES).pdf: $(SERIES_TEST_GUIDELINES)/$(SERIES_TEST_GUIDELINES).tex
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif


# =============================
$(SERIES_WITH_POSITIVE_TERMS)/$(SERIES_WITH_POSITIVE_TERMS).pdf: $(SERIES_WITH_POSITIVE_TERMS)/$(SERIES_WITH_POSITIVE_TERMS).tex $()/asy/$()000.pdf $()/asy/$()_3d000.tex
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(SERIES_WITH_POSITIVE_TERMS)/asy/$(SERIES_WITH_POSITIVE_TERMS)000.pdf: $(SERIES_WITH_POSITIVE_TERMS)/asy/$(SERIES_WITH_POSITIVE_TERMS).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(TAYLOR_POLYS)/$(TAYLOR_POLYS).pdf: $(TAYLOR_POLYS)/$(TAYLOR_POLYS).tex $(TAYLOR_POLYS)/asy/$(TAYLOR_POLYS)000.pdf
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(TAYLOR_POLYS)/asy/$(TAYLOR_POLYS)000.pdf: $(TAYLOR_POLYS)/asy/$(TAYLOR_POLYS).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(TAYLOR_SERIES)/$(TAYLOR_SERIES).pdf: $(TAYLOR_SERIES)/$(TAYLOR_SERIES).tex
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif


# =============================
$(TRIG_INTEGRALS)/$(TRIG_INTEGRALS).pdf: $(TRIG_INTEGRALS)/$(TRIG_INTEGRALS).tex
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif


# =============================
$(TRIG_SUBST)/$(TRIG_SUBST).pdf: $(TRIG_SUBST)/$(TRIG_SUBST).tex $(TRIG_SUBST)/asy/$(TRIG_SUBST)000.pdf $(TRIG_SUBST)/pix/che.jpg
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(TRIG_SUBST)/asy/$(TRIG_SUBST)000.pdf: $()/asy/$().asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))


# =============================
$(VECTORS)/$(VECTORS).pdf: $(VECTORS)/$(VECTORS).tex $(VECTORS)/asy/$(VECTORS)000.pdf $(VECTORS)/asy/$(VECTORS)_3d000.tex
ifdef LATEX_TWICE
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@))); \
	$(LATEX) $(basename $(notdir $(@)));
else
	cd $(@D); \
	$(LATEX) $(basename $(notdir $(@)));
endif

$(VECTORS)/asy/$(VECTORS)000.pdf: $(VECTORS)/asy/$(VECTORS).asy
	cd $(@D); \
	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))

$(VECTORS)/asy/$(VECTORS)_3d000.tex: $(VECTORS)/asy/$(VECTORS)_3d.asy
	cd $(@D); \
	$(ASYMPTOTE3D) $(subst 000,,$(basename $(notdir $(@))))



# Stub of section
# =============================
# $()/$().pdf: $()/$().tex $()/asy/$()000.pdf $()/asy/$()_3d000.tex
# ifdef LATEX_TWICE
# 	cd $(@D); \
# 	$(LATEX) $(basename $(notdir $(@))); \
# 	$(LATEX) $(basename $(notdir $(@)));
# else
# 	cd $(@D); \
# 	$(LATEX) $(basename $(notdir $(@)));
# endif

# $()/asy/$()000.pdf: $()/asy/$().asy
# 	cd $(@D); \
# 	$(ASYMPTOTE) $(subst 000,,$(basename $(notdir $(@))))

# $()/asy/$()_3d000.tex: $()/asy/$()_3d.asy
# 	cd $(@D); \
# 	$(ASYMPTOTE3D) $(subst 000,,$(basename $(notdir $(@))))

