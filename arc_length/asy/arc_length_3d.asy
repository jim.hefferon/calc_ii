// arc_length_3d.asy

// compile with asy -inlineimage arc_length_3d
// cd("../../asy");
// import settexpreamble;
// cd("");
// settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = true;
settings.embed = true;
settings.render = 0;
settings.tex="lualatex";  // for compiling in-pic text
settings.outformat="pdf";
// settings.render=16;

import embed;

import fontsize;

import graph3; import solids;
currentprojection = orthographic(1.2,3,2,up=Z);
currentlight=White; defaultrender.merge=true;
// unitsize(10cm,0);

string OUTPUT_FN = "arc_length_3d%03d";

material figure_material = material(diffusepen=bold_color+white+opacity(0.5),
				 emissivepen=bold_color+white,
				 specularpen=bold_color+white);
material slice_material = material(diffusepen=gray_color+opacity(0.25),
				   emissivepen=gray_color,
				   specularpen=gray_color);
pen boundary_pen = gray(0.2)+opacity(0.5);

currentlight = nolight;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);



// ===== Gabriel's horn =======
real f0(real x) {return 1/x;}

picture pic;
int picnum = 0;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

path f = graph(pic, f0, 1, 5);
path3 f_3d = path3(f);

// region that was rotated
path r = ( (1,0)--relpoint(f,0)&f&relpoint(f,1)--(5,0)--(1,0) )--cycle;
path3 r_3d = path3(r);
surface base = surface(r_3d);
draw(pic, base, surfacepen=gray(0.8)+opacity(0.5));
draw(pic, r_3d);

// horn
surface horn = surface(O, f_3d, X);
// draw end circles
draw(pic, shift(1,0,0)*rotate(90,Y)*unitcircle3, grayed);
draw(pic, shift(5,0,0)*rotate(90,Y)*scale(f0(5),f0(5),1)*unitcircle3, grayed);
draw(pic, horn, surfacepen=figure_material);

// Add slice
real x = 1+0.618*(5-1);
real rad = f0(x);
transform3 slice_t = shift((x,0,0))*rotate(90,Y)*scale(rad,rad,1); 
surface slice = slice_t*unitdisk;
draw(pic, slice, surfacepen=slice_material);
// add a circular edge
draw(pic, slice_t*unitcircle3, highlight_color);

// draw(pic, region, surfacepen=figure_material);
// draw(pic, region_t*unitcircle3, gray(0.85));
// draw(pic, shift(len,0,0)*region_t*unitcircle3, gray(0.75));


xaxis3(pic,Label("$x$"),
       0,6, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,1, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
        0,1, black, Arrow3(TeXHead2));

  
shipout(format(OUTPUT_FN,picnum),pic);


// ======== general function ==========
currentprojection = orthographic(6,0,10,up=Y);

picture pic;
int picnum = 1;
size(pic,5cm);
size3(pic,5cm,0,keepAspect=true);

path f = shift(0,2)*subpath(general_fcn,1,5);
path3 f_3d = path3(f);
surface f_rotated = surface(O, f_3d, X);
// Add circle ends
real left_rad = relpoint(f, 0).y;
real right_rad = relpoint(f, 1).y;
real left_shift = relpoint(f, 0).x;
real right_shift = relpoint(f, 1).x;
draw(pic, shift(left_shift,0,0)*rotate(90,Y)*scale(left_rad,left_rad,1)*unitcircle3, gray_color+opacity(0.5));
// draw the figure
draw(pic, f_rotated, surfacepen=figure_material);
draw(pic, shift(right_shift,0,0)*rotate(90,Y)*scale(right_rad,right_rad,1)*unitcircle3, gray_color+opacity(0.5));

// real rad = 0.5;
// real len  = 0.15;

// transform3 slice_t = rotate(90,Y)*scale(rad,rad,len); 
// surface slice = slice_t*unitcylinder;
// surface slice_front = shift(len,0,0)*slice_t*unitdisk;
// draw(pic, slice, surfacepen=figure_material);
// draw(pic, slice_t*unitcircle3, highlight_color+opacity(0.5));
// // surface in front
// draw(pic, slice_front, surfacepen=slice_material);
// draw(pic, shift(len,0,0)*slice_t*unitcircle3, highlight_color+opacity(0.5));

// Label the slice
// draw(pic, "$r$", (len,0,0)--(len,rad,0));
// path3 t = (0,0,0)--(len,0,0);
// draw(pic, "$dx$", shift(0,1.3*rad,0)*t, 1.25*S+0.5*E, Bars3(2));

xaxis3(pic,Label("$x$"),
       0,7, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,3, black, Arrow3(TeXHead2));
// zaxis3(pic,Label(""),\
//         0,1, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


// ....... include the slice ............
picture pic;
int picnum = 2;
size(pic,5cm);
size3(pic,5cm,0,keepAspect=true);

path f = shift(0,1.5)*subpath(general_fcn,1,5);
path3 f_3d = path3(f);
surface f_rotated = surface(O, f_3d, X);
// Add circle ends
real left_rad = relpoint(f, 0).y;
real right_rad = relpoint(f, 1).y;
real left_shift = relpoint(f, 0).x;
real right_shift = relpoint(f, 1).x;
draw(pic, shift(left_shift,0,0)*rotate(90,Y)*scale(left_rad,left_rad,1)*unitcircle3, gray_color+opacity(0.5));
// draw the figure
draw(pic, f_rotated, surfacepen=figure_material);
draw(pic, shift(right_shift,0,0)*rotate(90,Y)*scale(right_rad,right_rad,1)*unitcircle3, gray_color+opacity(0.5));

// Draw a slice; couldn't figure out unit frustrum
real slice_left = 0.3;
real slice_right = slice_left+0.05;
path slice_line_segment = relpoint(f, slice_left)--relpoint(f, slice_right);
path3 slice_line_segment_3d = path3(shift(0.02,0.02)*slice_line_segment); // shift is here so slice does not lay on surface, where Asy is not sure whether to show surface or slice.
surface slice = surface(O, slice_line_segment_3d, X);
draw(pic, slice, surfacepen=slice_material);

// Label the slice
// draw(pic, "$r$", (len,0,0)--(len,rad,0));
// path3 t = (0,0,0)--(len,0,0);
// draw(pic, "$dx$", shift(0,1.3*rad,0)*t, 1.25*S+0.5*E, Bars3(2));

xaxis3(pic,Label("$x$"),
       0,7, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,3, black, Arrow3(TeXHead2));
// zaxis3(pic,Label(""),\
//         0,1, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ======== surface area of a cone ========
currentprojection = orthographic(1.5,0.5,0.25,up=Z);

picture pic;
int picnum = 3;
size(pic,3cm);
size3(pic,3cm,0,keepAspect=true);

surface c = scale(1,1,2)*unitcone;
draw(pic, c, surfacepen=figure_material);
// base circle
path3 base_circle = unitcircle3;
draw(pic, base_circle, gray_color+opacity(0.25));

// slant length
path3 slant_length_line = shift(0,0.2,0.1)*( (0,1,0)--(0,0,2) );
draw(pic, "$\ell$", slant_length_line, Bars3(ARROW_SIZE), Arrows3(ARROW_SIZE));
// radius
path3 rad_line = shift(0,0,-0.30)*( (0,0,0)--(0,1,0) );
draw(pic, "$r$", rad_line, Bars3(ARROW_SIZE), Arrows3(ARROW_SIZE));

xaxis3(pic,Label(""),
       0,2, black, Arrow3(TeXHead2));
yaxis3(pic,Label(""),
        0,1.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
         0,2.35, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


// .............. frustrum ...........
picture pic;
int picnum = 4;
size(pic,3cm);
size3(pic,3cm,0,keepAspect=true);

surface c = shift(0,0,2)*scale(1,1,2)*unitfrustum(1,2);
draw(pic, c, surfacepen=figure_material);
// base circle
path3 top_circle = shift(0,0,2)*unitcircle3;
draw(pic, top_circle, gray_color+opacity(0.25));
path3 base_circle = scale(2,2,1)*unitcircle3;
draw(pic, base_circle, gray_color+opacity(0.25));

xaxis3(pic,Label(""),
       0,3.5, black, Arrow3(TeXHead2));
yaxis3(pic,Label(""),
        0,2.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
         0,2.5, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);




// ======== surface area of truncated sphere ========
currentprojection = orthographic(1.25,0.15,10,up=Y);

real f5(real x) { return sqrt(4-x^2) ; }
picture pic;
int picnum = 5;
size(pic,0,4cm);
size3(pic,0,4cm,keepAspect=true);

path f = graph(pic, f5, -1, 1);
path3 f_3d = path3(f);
surface region = surface(O, f_3d, X);
// boundary circles
path3 left_boundary = shift(-1,0,0)*rotate(90, Y)*scale3(sqrt(3))*unitcircle3;
draw(pic, left_boundary, boundary_pen);
path3 right_boundary = shift(1,0,0)*rotate(90, Y)*scale3(sqrt(3))*unitcircle3;
draw(pic, right_boundary, boundary_pen);

// Draw the region
draw(pic, region, surfacepen=figure_material);

real[] T5 = {1};
xaxis3(pic,Label("\scriptsize $x$"),
       0,2, black,
       // ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
       Arrow3(TeXHead2));
yaxis3(pic,Label("\scriptsize $y$"),
        0,2.5, black, Arrow3(TeXHead2));
// zaxis3(pic,Label("\scriptsize $z$"),\
//          0,2.35, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);




// ===== Gabriel's horn =======
currentprojection = orthographic(4,1,10,up=Y);

real f6(real x) {return 1/x;}

picture pic;
int picnum = 6;
size(pic,0,4cm);
size3(pic,0,4cm,0,keepAspect=true);

path f = graph(pic, f6, 1, 5);
path3 f_3d = path3(f);

// region that was rotated
path r = ( (1,0)--relpoint(f,0)&f&relpoint(f,1)--(5,0)--(1,0) )--cycle;
path3 r_3d = path3(r);
surface base = surface(r_3d);
draw(pic, base, surfacepen=gray(0.8)+opacity(0.5));
draw(pic, r_3d);

// horn
surface horn = surface(O, f_3d, X);
// draw end circles
draw(pic, shift(1,0,0)*rotate(90,Y)*unitcircle3, grayed);
draw(pic, shift(5,0,0)*rotate(90,Y)*scale(f6(5),f6(5),1)*unitcircle3, grayed);
draw(pic, horn, surfacepen=figure_material);

// Add slice
// real x = 1+0.618*(5-1);
// real rad = f6(x);
// transform3 slice_t = shift((x,0,0))*rotate(90,Y)*scale(rad,rad,1); 
// surface slice = slice_t*unitdisk;
// draw(pic, slice, surfacepen=slice_material);
// // add a circular edge
// draw(pic, slice_t*unitcircle3, highlight_color);

xaxis3(pic,Label("$x$"),
       0,6, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,1, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
        0,1, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


