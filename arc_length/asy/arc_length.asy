// arc_length.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "arc_length%03d";

import graph;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// ==== polygonal path ====
for(int pn=0; pn<3; ++pn) { 
  picture pic;
  int picnum = 0+pn;
  size(pic,0,4cm,keepAspect=true);

  real xleft, xright, ybot, ytop; // limits of graph
  xleft=0; xright=5;
  ybot=0; ytop=2.5;

  real a = 0.25;  real b = 4.75;
  
  path f = shift(0,1.5)*general_fcn;
  draw(pic, f, FCNPEN);

  real[] x;  // points on path  
  real[] f_x_time;    
  // make path
  real f_a_time = times(f, a)[0];
  guide g = point(f, f_a_time);
  
  int num_points = 5*pn; // number of line segments is one less
  real[] T0 = {a, b};
  dotfactor = 5;
  for(int i=0; i < num_points; ++i) {
    x[i] = a+(b-a)*(i/(num_points-1));
    f_x_time[i] = times(f, x[i])[0];
    g = g--point(f, f_x_time[i]);
    dot(pic, point(f, f_x_time[i]));
    if (i !=0 ) {
      T0.push(x[i]);
    }
  }
  draw(pic, g, highlight_color);
  
  xaxis(pic, L="{\scriptsize $x$}",  
	axis=YZero,
	xmin=xleft-0.75, xmax=xright+.75,
	p=currentpen,
	ticks=RightTicks("%", T0, Size=2pt),
	arrow=Arrows(TeXHead));
  labelx(pic, "$a$", a);
  labelx(pic, "$b$", b);
  
  yaxis(pic, L="{\scriptsize $y$}",  
	axis=XZero,
	ymin=ybot-0.25, ymax=ytop+0.25,
	p=currentpen,
	ticks=NoTicks,
	arrow=Arrows(TeXHead));
  
  shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
}



// ==== length of one line segment ====
picture pic;
int picnum = 3;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=2.5;

real xi = 1.5;  real xiplus = 4;

path f = (1,1)..(1.5,1.2)..(1.75,1.5)..(4,2)..(5,2.25);
draw(pic, f, FCNPEN);

real f_xi_time = times(f, xi)[0];
real f_xiplus_time = times(f, xiplus)[0];

path slope_triangle = point(f, f_xi_time)
     --point(f, f_xiplus_time)
     --(point(f, f_xiplus_time).x, point(f, f_xi_time).y)
  --cycle;
draw(pic, slope_triangle, highlight_color);
dotfactor = 4;
dot(pic, "$(x_i,f(x_i))$", point(f, f_xi_time), NW);
dot(pic, "$(x_{i+1},f(x_{i+1}))$", point(f, f_xiplus_time), N);
label(pic,
      "$f(x_{i+1})-f(x_i)=\Delta y_i$",
      midpoint(subpath(slope_triangle,1,2)),
      E);
label(pic,
      "$x_{i+1}-x_i=\Delta x_i$",
      midpoint(subpath(slope_triangle,2,3)),
      S);

real[] T3 = {xi, xiplus};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks("%", T3, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x_i$", xi);
labelx(pic, "$x_{i+1}$", xiplus);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ==== y=2x^(3/2) ====
real f4(real x) {return 2*x^(3/2);}

picture pic;
int picnum = 4;
scale(pic, Linear(1.5), Linear);
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft = 0; xright = 4;
ybot = 0; ytop = 16;

real a = 1;  real b = 4;

path f = graph(pic, f4, a, b);
draw(pic, f, FCNPEN);

dotfactor = 4;
dot(pic, Scale(pic, (1,f4(1))));
dot(pic, Scale(pic, (4,f4(4))));

real[] T4 = {a, b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ==== y=x^2 ====
real f5(real x) {return x^2;}

picture pic;
int picnum = 5;
scale(pic, Linear, Linear);
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft = 0; xright = 1;
ybot = 0; ytop = 1;

real a = 0;  real b = 1;

path f = graph(pic, f5, xleft-0.2, xright+0.1);
draw(pic, f, FCNPEN);

dotfactor = 4;
dot(pic, Scale(pic, (a,f5(a))));
dot(pic, Scale(pic, (b,f5(b))));

xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ==== surface area of a cone ====
picture pic;
int picnum = 6;
size(pic,4cm,keepAspect=true);

real rad = 2;
real slant_length = 0.35;
real theta = 2*pi*(rad/slant_length);
path bottom_arc = arc( (0,0), slant_length, 0, theta);
path region = ( (0,0)--(slant_length,0)
  &bottom_arc&relpoint(bottom_arc,1)--(0,0) )--cycle;
filldraw(pic, region, FILLCOLOR, FCNPEN+miterjoin);

// label the sizes
path theta_arc = arc( (0,0), 0.1, 0, theta);
draw(pic, Label("$\theta$"), theta_arc, 2*E+0.5*N, MidArrow(ARROW_SIZE));
label(pic, "$\ell$", midpoint(subpath(region, 0, 1)), S);
path base_arc = arc( (0,0), slant_length+0.05, 0, theta);
draw(pic, Label("$\ell\theta=2\pi r$"), base_arc, 2*E, Bars(ARROW_SIZE), Arrows(ARROW_SIZE));

shipout(format(OUTPUT_FN,picnum),pic);





// ==== y=sqrt(4-x^2)  x in [-1..1] ====
real f7(real x) {return sqrt(4-x^2);}

picture pic;
int picnum = 7;
// scale(pic, Linear, Linear);
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft = -1; xright = 1;
ybot = 0; ytop = 2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

real a = -1;  real b = 1;

path f_left = graph(pic, f7, a, b);
draw(pic, f_left, FCNPEN);

real[] T7 = {a, b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks("%", T7, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$-1$", a);
labelx(pic, "$1$", b);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ==== y=t^{3/2} ====
real f8(real x) {return x^(3/2);}

picture pic;
int picnum = 8;
scale(pic, Linear, Linear);
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft = 0; xright = 4;
ybot = 0; ytop = 8;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

real a = 0;  real b = 4;

path f = graph(pic, f8, a+0.02, b+0.2);
draw(pic, f, FCNPEN);
draw(pic, subpath(f,0,50), FCNPEN_NOCOLOR+highlight_color);
dotfactor = 4;
dot(pic, Label("$(x,x^{3/2})$", black,filltype=Fill(white)), point(f, 50), SE, highlight_color);

xaxis(pic, L="\scriptsize $t$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ==== y= 1/x ====
real f9(real x) {return 1/x;}

picture pic;
int picnum = 9;
size(pic,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft = 0; xright = 8;
ybot = 0; ytop = 1;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

real a = 1;  real b = xright;

path f = graph(pic, f9, a-0.1, b+0.2);

// region
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ( (a,0)--point(f, f_a_time)
		&subpath(f, f_a_time, f_b_time)
		&point(f, f_b_time)--(b,0)--(a,0) )--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, f, FCNPEN);

xaxis(pic,  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic,  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



