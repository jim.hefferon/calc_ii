% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}
\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 8.1\hspace{1em}Arc length}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Inverse trig functions}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% compile with asy -inlineimage arc_length_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
\input asy/arc_length_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



% \section{}


\begin{frame}
  \frametitle{Development}
To find the length of a curve, divide the $x$~axis into
intervals $\closed{x_i}{x_{i+1}}$, each $\Delta x$~wide.
Connect the points $(x_i,f(x_i))$ with line segments.
The total length of the segments approximates the length of the 
curve.
\begin{center}
  \only<1>{\vcenteredhbox{\includegraphics{asy/arc_length000.pdf}}}%    
  \only<2>{\vcenteredhbox{\includegraphics{asy/arc_length001.pdf}}}%    
  \only<3->{\vcenteredhbox{\includegraphics{asy/arc_length002.pdf}}}%    
\end{center}
\only<3->{We take it that the approximation gets better when the number of segments
gets larger, that is, when $\Delta x\to 0$.
\begin{equation*}
  \text{Arc length} = \lim_{\Delta x\to 0}\biggl( \sum_i \;\text{length of line segment}_i \biggr)
\end{equation*}
}
\end{frame}


\begin{frame}
The length of that line segment
is $\sqrt{(\Delta x_i)^2+(\Delta y_i)^2}$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/arc_length003.pdf}}%    
\end{center}
We want to integrate~$dx$ so we look to
fit the form given in the definition of a definite integral.
\begin{equation*}
  \integral{a}{b}{ g(x) }{x}=
  \lim_{\Delta x\to 0}\Bigl( \sum_{i} g(x_i^*)\Delta x \Bigr)
  \quad 
  \text{where $x_i^*$ is in the $i$th interval $\closed{x_i}{x_{i+1}}$}
\end{equation*}
We need to change the 
$\Delta y_i$ expression to one involving 
$x_i$ and~$\Delta x_i$.
\end{frame}

\begin{frame}
The Mean Value Theorem comes to the rescue here, saying that 
in the interval $\closed{x_i}{x_{i+1}}$ there is a number $x_i^*$
with $\Delta y_i/\Delta x_i=f'(x_i^*)$,
and so $\Delta y_i=f'(x_i^*)\Delta x_i=f'(x_i^*)\Delta x$. 
\begin{align*}
  \text{Arc length} 
   &= \lim_{\Delta x\to 0}\biggl( \sum_i \sqrt{(\Delta x)^2+(\Delta y_i)^2} \biggr) \\
   &= \lim_{\Delta x\to 0}\biggl( \sum_i \sqrt{(\Delta x)^2+(\,f'(x_i^*)\Delta x\,)^2} \biggr)  \\ 
   &= \lim_{\Delta x\to 0}\biggl( \sum_i \sqrt{ (1+f'(x_i^*)^2) \cdot  (\Delta x)^2} \biggr)  \\ 
   &= \lim_{\Delta x\to 0}\biggl( \sum_i \sqrt{1+f'(x_i^*)^2} \cdot \Delta x\biggr) 
\end{align*}
Those technical manipulations explain the perhaps unexpected form
of the definition's integral.
\end{frame}

\begin{frame}{Definition of arc length}
\Df
Where $f'$ is continuous on $\closed{a}{b}$, this is the \alert{arc length} 
of the graph of~$f(x)$ from $x=a$ to~$b$.
\begin{equation*}
  \text{Arc length}
  =\integral{x=a}{b}{ \sqrt{1+(f'(x))^2} }{x}
  =\integral{x=a}{b}{ \sqrt{1+\biggl(\frac{dy}{dx}\biggr)^2} }{x}
\end{equation*}
\end{frame}



\begin{frame}
\Ex Find the length of the graph of $f(x)=2x^{3/2}$ between $x=1$
and $x=4$.  
\begin{center}
  \vcenteredhbox{\includegraphics{asy/arc_length004.pdf}}%    
\end{center}
\pause
The formula gives this.
\begin{equation*}
  \integral{x=1}{4}{ \sqrt{1+(3x^{1/2})^2} }{x}
  =
  \integral{1}{4}{ \sqrt{1+9x} }{x}
\end{equation*}
Use $u=1+9x$ so that $du=9\,dx$.
\begin{equation*}
  \integral{u=10}{37}{ \sqrt{u}\cdot (1/9)}{u}
  =
  \frac{1}{9}\Bigl[ \frac{2}{3}u^{3/2} \Bigr]_{10}^{37}
  % =\frac{2}{27}(6^3-3^3)
  % =14
\end{equation*}
\end{frame}




\begin{frame}
\Ex Find the length of the graph of $f(x)=x^2$ between $x=0$
and $x=1$.  
\begin{center}
  \vcenteredhbox{\includegraphics{asy/arc_length005.pdf}}%    
\end{center}
\pause
The formula gives this.
\begin{equation*}
  \integral{x=0}{1}{ \sqrt{1+(2x)^2} }{x}
  =
  \integral{0}{1}{ \sqrt{1+4x^2} }{x}
\end{equation*}
Use $x=(1/2)\tan\theta$ so that $dx=(1/2)\sec^2\theta\,d\theta$.
The limits of integration change from $x$ between $0$ and~$1$
to $\theta$ between $0$ and $\arctan(2)$. % $\approx 1.11$.
\begin{align*}
  \integral{\theta=0}{\tan^{-1}(2)}{ \sec\theta\cdot(1/2)\sec^2\theta }{\theta}
  &=
  \frac{1}{2}\cdot\integral{0}{\tan^{-1}(2)}{ \sec^3\theta }{\theta}  \\
  &=
  \frac{1}{4} \Bigl[ \sec\theta\tan\theta+\ln|\sec\theta+\tan\theta| \Bigr]_0^{\tan^{-1} (2)}
\end{align*}
The upper limit has $\tan\theta=2$, and by
$\sec^2\theta=\tan^2\theta+1=2$ we get this length.
\begin{equation*}
  \frac{\sqrt{5}}{2}+\frac{\ln(\sqrt{5}+2)}{4}
\end{equation*}
\end{frame}



\begin{frame}{Arc length function}
Fix an anchor point on the curve, $(a,f(a))$.
Considering another point $(x,f(x))$ that varies 
gives the \alert{arc length function}.
\begin{equation*}
  s_a(x)=\integral{t=a}{x}{ \sqrt{1+(f'(t))^2} }{t}
\end{equation*}
Note that $s_a$'s input variable~$x$ is the upper limit of integration.
The $t$ is just a dummy variable.

The radical means that finding the antiderivative can be hard, and 
often we use a numerical approximation.
\end{frame}

\begin{frame}
\Ex Find the arc length function for $f(t)=t\sqrt{t}$ and $a=0$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/arc_length008.pdf}}
\end{center}
We have $f(t)=t^{3/2}$ and $f'(t)=(3/2)t^{1/2}\!$.
\begin{equation*}
  s_0(x)
  =\integral{t=0}{x}{ \sqrt{1+((3/2)t^{1/2})^2} }{t}
  =\integral{0}{x}{ \sqrt{1+(9t/4)} }{t}
\end{equation*}
Take $u=1+9t/4$ so that $du=(9/4)\,dt$ and $(4/9)\,du=dt$.
\begin{equation*}
  =\integral{u=1}{1+(9x/4)}{ u^{1/2}\cdot \frac{4}{9} }{u}
  =\frac{4}{9}\cdot\Bigl[ \frac{2}{3}u^{3/2} \Bigr]_{1}^{1+(9x/4)}
  =\frac{8}{27}\cdot\biggl( (1+(9x/4))^{3/2}-1 \biggr)
\end{equation*}
\end{frame}


\begin{frame}
Observe that the Fundamental Theorem 
\begin{equation*}
  s_a(x)=\integral{t=a}{x}{ \sqrt{1+(f'(t))^2} }{t}
  \quad\Longrightarrow\quad
  \frac{d\,s_a}{dx}=\sqrt{1+(f'(x))^2}
\end{equation*}
gives that the rate of change of the arc length function, 
$d\,s_a/dx$, is at least~$1$.

This is a version of ``the shortest distance between two points is
in a straight line.''
A curve that bends and twists will have an arc length that grows at a
rate greater than~$1$, but the slowest that the arc length can grow
is the rate of~$1$ that you get when the curve is a straight line.
\end{frame}




\section{Surface area}

\begin{frame}
In much the same way that we found the arc length 
we can find the area of a surface.  
Here we will just illustrate and so we stick to a surface of revolution.
\begin{center}
  % \vcenteredhbox{\includegraphics{asy/arc_length_3d001.pdf}}
  \input asy/arc_length_3d001.tex
\end{center}
\end{frame}

\begin{frame}
We break the underlying curve into line segments and rotate those.
Each line segment becomes the frustum of a cone.
% \begin{center}
%   \vcenteredhbox{\includemedia{\includegraphics{asy/arc_length_3d002.pdf}}{}}
% \end{center}
\begin{center}
  \input asy/arc_length_3d002.tex
\end{center}
We will define the surface area to be this.
\begin{equation*}
  \lim_{\Delta x\to 0}\biggl( \sum_i \text{surface area of frustum}_i\biggr)
\end{equation*}
\end{frame}

\begin{frame}{Surface area of a frustum}\vspace*{-1ex}
Start with a cone with base radius~$r$ and slant length~$\ell$.
Cut the surface from bottom to top and lay it flat.
As the right picture shows, $\theta=2\pi r/\ell$.
\begin{center}
  %\vcenteredhbox{\includegraphics{asy/arc_length_3d003.pdf}}
  \vcenteredhbox{\input asy/arc_length_3d003.tex }
  \qquad\vcenteredhbox{\includegraphics{asy/arc_length006.pdf}}
\end{center}
The cone's area is $\ell^2\cdot (1/2)\theta$ because $\theta$ is in radians.
That's $\ell^2\cdot(1/2)(2\pi r/\ell)=\pi r\ell$.
\begin{center}
  \vcenteredhbox{\input asy/arc_length_3d004.tex }
\end{center}
For the frustum, take away a top cone to get the surface area formula,
$2\pi r\ell$,
where $r$ is the average of the radius of the top and that of the bottom.  
\end{frame}

\begin{frame}
With that formula, the same work we did for arc length gives this for the  
surface area.
\begin{equation*}
  \lim_{\Delta x\to 0}\biggl( \sum_i 2\pi r_i\ell_i \biggr)
  =\lim_{\Delta x\to 0}\biggl( \sum_i 2\pi f(x_i^*)\sqrt{1+(f'(x_i^*))^2} \biggr) 
\end{equation*}

\Df The surface area for the surface of solid of revolution 
that comes from rotating the graph of $f(x)$ about the $x$~axis
is this.
\begin{equation*}
    \integral{a}{b}{ 2\pi f(x) \sqrt{1+(f'(x))^2} }{x}
\end{equation*}
\pause
\Ex Consider the graph of $f(x)=2\sqrt{x}$ between $a=1$ and $b=3$.
Rotate it about the $x$~axis.
What is that figure's surface area?

\pause
\begin{align*}
  \integral{x=1}{3}{ 2\pi\cdot 2\sqrt{x}\cdot\sqrt{1+(1/x)} }{x}
  &=4\pi\cdot\integral{1}{3}{ \sqrt{x}\cdot\sqrt{1+(1/x)} }{x}  \\
  &=4\pi\cdot\integral{1}{3}{ \sqrt{x+1} }{x}    \\
  &=\frac{8\pi}{3}\Bigl[ (x+1)^{3/2} \Bigr]_1^3    \\
  &=\frac{16\pi}{3}\bigl( 4-\sqrt{2} \bigr)  
\end{align*}
\end{frame}


\begin{frame}
\Ex
The function $f(x)=\sqrt{4-x^2}$ gives the upper half of the
circle of radius~$2$.
Consider the arc between $x=-1$ and~$1$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/arc_length007.pdf}}
\end{center}
Rotate it about the $x$~axis.
Find the surface area.
\end{frame}

\begin{frame}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/arc_length_3d005.pdf}}
\end{center}
\begin{align*}
    \integral{-1}{1}{ 2\pi \sqrt{4-x^2}\cdot \sqrt{1+(\frac{-x}{\sqrt{4-x^2}})^2} }{x}
    &=2\pi\integral{-1}{1}{ \sqrt{4-x^2}\cdot\sqrt{1+\frac{x^2}{4-x^2}} }{x}  \\
    &=2\pi\integral{-1}{1}{ \sqrt{4-x^2}\cdot\sqrt{\frac{(4-x^2)+x^2}{4-x^2}} }{x}  \\
    &=2\pi\integral{-1}{1}{ \sqrt{4-x^2}\cdot\sqrt{\frac{4}{4-x^2}} }{x}  \\
    &=2\pi\integral{-1}{1}{ \sqrt{4-x^2}\cdot\frac{2}{\sqrt{4-x^2}} }{x}  \\
    &=4\pi\integral{-1}{1}{ 1 }{x}=8\pi
\end{align*}
\end{frame}


\begin{frame}{Gabriel's Horn}
\Ex In the Improper Integrals section we considered the graph of $f(x)=1/x$ from 
$x=1$ to~$\infty$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/arc_length009.pdf}}
\end{center}
\pause
Rotating about the $x$~axis gives \alert{Gabriel's horn}.
By the method of discs we found it has volume~$\pi$.
\begin{center}
  \vcenteredhbox{\input asy/arc_length_3d006.tex }
\end{center}
\end{frame}




% \begin{frame}
% \begin{center}
%   \vcenteredhbox{\input asy/arc_length_3d006.tex }
% \end{center}  
% We got the volume by the method of discs.
% \begin{equation*}
%   \integral{x=1}{\infty}{ \pi(1/x)^2 }{x}
%   =\pi\cdot \lim_{R\to\infty} \bigl( \integral{x=1}{\infty}{ \frac{1}{x^2} }{x} \bigr)           
%   =\pi\cdot \lim_{R\to\infty} \bigl( \Bigl[ -\frac{1}{x} \Bigr]_{x=1}^{R}\bigr)  
%   % &=\pi\cdot \lim_{R\to\infty} \bigl( 1-\frac{1}{R} \bigr)  \\
%   =\pi
% \end{equation*}
% \end{frame}

\begin{frame}
This is the horn's surface area.
\begin{equation*}
  \lim_{R\to\infty}\biggl( 2\pi\integral{1}{R}{ \frac{1}{x}\sqrt{1+\bigl(\frac{-1}{x^2}\bigr)^2} }{x} \biggr)    
  =\lim_{R\to\infty}\biggl( 2\pi\integral{1}{R}{ \frac{1}{x}\sqrt{1+\frac{1}{x^4}} }{x} \biggr)    
\end{equation*}
\quad
That antiderivative is hard.
But there is a point that we want to make about the horn and
we can make it by noting this.
\begin{equation*}
  \sqrt{1+\frac{1}{x^4}}\;>1
  \quad\text{so}\quad 
  2\pi\integral{1}{R}{ \frac{1}{x}\cdot\sqrt{1+\frac{1}{x^4}} }{x}
  \geq
  2\pi\integral{1}{R}{ \frac{1}{x} }{x}
\end{equation*}
The comparison gives this.
\begin{equation*}
  \lim_{R\to\infty}\biggl( 2\pi\integral{1}{R}{ \frac{1}{x} }{x} \biggr)      
  =2\pi \lim_{R\to\infty}\Bigl[ \ln|x| \Bigr]_1^R      
  =2\pi \lim_{R\to\infty}\bigl( \ln R-0 \bigr)
  =\infty      
\end{equation*}
So the surface area is infinite, despite that the volume is finite.

\pause
This is a paradox.
Imagine you filled the horn with paint, using $\pi$ cubic units.
That's not enough paint to cover the outside.
Further, imagine you let the paint dry and then 
took the paint-filled horn and pulled away the sides.
The paint on the outside is not sufficient to paint the side of the horn.
\end{frame}



\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
