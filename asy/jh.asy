// jh.asy  Asymptote common definitions

import fontsize;
defaultpen(fontsize(9pt));
// import texcolors;

// colors from kgross  https://color.adobe.com/search?q=Primary%20colors&t=term
// pen lightbrown_color=rgb("E0D4BE");  // Particular Mint,
// pen lightbrown_color=rgb("E6D1A8");  // 
// pen black_color=rgb("595241");  // Daintree
// pen white_color=rgb("FFFFFF");
// pen lightblue_color=rgb("56B1BF");  // Aqua Verde
// pen teal_color=rgb("08708A");  // Teal Tune  
// pen red_color=rgb("D73A31");        // Red Clown

// pen highlight_color=red_color;
// pen background_color=lightbrown_color;
// pen bold_color=teal_color;
// pen light_color=lightblue_color;
// pen verylight_color=white_color;
// pen verydark_color=black_color;

// for places where the highlight color is too bold or dark, as with a colored node
// pen highlight_light = rgb(248/255, 145/255, 157/255); // from: http://hslpicker.com/#f67987
// pen highlight_fcn = rgb("CB0022");
// pen bold_light = rgb(165/255, 155/255, 131/255);

// grayed-out such as the edge of a graph
pen grayed=gray(0.35);

// https://color.adobe.com/ Fisher French Tomato, James Morris
// Original doesn't show on the beamer.  I tried some colors online and
// a threshold of 80% lightness in HSL seemed to be needed.  So I dialed
// the original Golen Impression from HSL(43.02, 100.00, 89.61)
// to HSL(43.02, 100.00, 80), which is HEX=FFE299.
pen highlight_color = rgb("8D001A");  // Burgundy
// pen background_color = rgb("FFF0CA"); // Golden Impression original.
pen background_color = rgb("FFE299"); // Golden Impression.
// pen background_color = rgb("E5CB89"); // Golden Impression.
pen bold_color = rgb("00394F");       // Maniac Mansion
pen light_color = rgb("B29596");      // Hazy Rose
pen neutral_color = rgb("8A897A");    // Kosher Khaki
pen gray_color = gray(0.1);

pen FILLCOLOR = background_color+opacity(0.60);
pen FCNCOLOR = bold_color; // gray(0.15); // rgb("E4E4E4");

pen MAINPEN=linecap(0)
             +linewidth(0.4pt);
pen THINPEN=linecap(0)
             +linewidth(0.25pt);
pen DASHPEN=linecap(0)
             +linewidth(0.4pt)
             +linetype(new real[] {8,8});
pen FCNPEN_NOCOLOR=linecap(0)
             +linewidth(1.25pt);
pen FCNPEN=FCNPEN_NOCOLOR
  //+linecap(0)
             +FCNCOLOR
  //+linewidth(1.25pt)
             +opacity(.5,"Normal");
pen AXISPEN=linecap(0)
             +gray(0.3)
             +linewidth(0.4pt)
             +opacity(.5,"Normal");
pen DXPEN=linecap(0)
             +red
             +linewidth(1pt);

pen TICLABELPEN=fontsize(7pt);
pen GRAPHPAPERPEN=(0.25*rgb("00BFFF")+0.75*white)
  +linewidth(0.4pt)
  +squarecap;

pen LIGHTPEN=linewidth(0.4pt)+squarecap; // matches mpost line_width_light
real DARKPEN_SIZE=0.8pt;             // line_width_dark
pen DARKPEN=linewidth(DARKPEN_SIZE); // 
real WHITEPEN_SIZE=1.8pt;             // put a white outline around a curve
pen WHITEPEN=white+linewidth(WHITEPEN_SIZE); // 

// some parameters
real BAR_SIZE = 2.5pt;
real ARROW_SIZE = 4pt;
real AXIS_ARROW_SIZE = 0.35mm;
real AXIS_TICK_SIZE = 0.75mm;


import graph;

pen GRAPHPAPERPEN=(0.4*rgb("00BFFF")+0.6*white)
  +linewidth(0.3pt)
  +squarecap;

void draw_graphpaper(picture pic, real xleft, real xright, real ytop, real ybot, real EqualsTopExtra=0.2, real EqualsBotExtra=0.2, real EqualsLeftExtra=0.2, real EqualsRightExtra=0.2, real TopExtra=0.5, real BotExtra=0.5, real LeftExtra=0.5, real RightExtra=0.5, real xStep=1, real yStep=1) {
  xaxis(pic,axis=YEquals(ytop+EqualsTopExtra),
	xmin=xleft-LeftExtra, xmax=xright+RightExtra,
	p=nullpen,
	ticks=RightTicks("%", Step=xStep, NoZero, extend=true, pTick=GRAPHPAPERPEN));
  xaxis(pic,axis=YEquals(ybot-EqualsBotExtra),
	xmin=xleft-LeftExtra, xmax=xright+RightExtra,
	p=nullpen,
	ticks=RightTicks("%", Step=xStep, NoZero, extend=true, pTick=GRAPHPAPERPEN));
  yaxis(pic,axis=XEquals(xleft-EqualsLeftExtra),
	ymin=ybot-BotExtra, ymax=ytop+TopExtra,
	p=nullpen,
	ticks=LeftTicks("%", Step=yStep, NoZero, extend=true, pTick=GRAPHPAPERPEN));
  yaxis(pic,axis=XEquals(xright+EqualsRightExtra),
	ymin=ybot-BotExtra, ymax=ytop+TopExtra,
	p=nullpen,
	ticks=LeftTicks("%", Step=yStep, NoZero, extend=true, pTick=GRAPHPAPERPEN));  
}

// texpreamble("\usepackage{conc}");

// 3D colors
// import graph3; import solids;
// material figure_material = material(diffusepen=bold_color+white+opacity(0.5),
// 				 emissivepen=bold_color+white,
// 				 specularpen=bold_color+white);
// material slice_material = material(diffusepen=gray_color+opacity(0.25),
// 				   emissivepen=gray_color,
// 				   specularpen=gray_color);
// pen boundary_pen = gray(0.2)+opacity(0.5);
// currentlight = nolight;
