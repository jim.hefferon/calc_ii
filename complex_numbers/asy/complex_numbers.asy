// complex_numbers.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "complex_numbers%03d";

import graph;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// =========== Bombelli's equation x^3=15x+4 ===============
real f0(real x) { return x^3; }
real f0a(real x) { return 15*x+4; }

picture pic;
int picnum = 0;
scale(pic, Linear(10), Linear);
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-4; xright=4;
ybot=-60; ytop=60;

real a = -4; real b = 4; 

path f = graph(pic, f0, a, b);
path fa = graph(pic, f0a, a, b);

draw(pic, f, FCNPEN);
draw(pic, fa, FCNPEN);


real[] T0 = {-4,4};
// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks(T0, Size=2pt),
      arrow=Arrows(TeXHead));

real[] T0y = {-60,60};
// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=10,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=10,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-5, ymax=ytop+5,
      p=currentpen,
      ticks=LeftTicks(T0y, Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ========== polar form =============
picture pic;
int picnum = 1;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=2.5;

dotfactor = 4;
pair z = (3,2);

path triangle = (0,0)--(z.x,0)--z--cycle;
draw(pic, triangle, FCNPEN+extendcap);
label(pic, "$|z|$", midpoint((0,0)--z), NW, highlight_color);
path ang_circle = circle( (0,0), 0.5);
real ang_arc_t = intersect( ang_circle, (0,0)--z)[0];
path ang_arc = subpath(ang_circle, 0, ang_arc_t);
draw(pic, "$\theta=\arg(z)$", ang_arc, highlight_color, Arrow(ARROW_SIZE));
dot(pic, "$z$", z);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
// xaxis(pic, L="",  
//       axis=YEquals(ytop+0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// xaxis(pic, L="",  
//       axis=YEquals(ybot-0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
// yaxis(pic, L="",  
//       axis=XEquals(xleft-0.2),
//       ymin=ybot-0.5, ymax=ytop+0.5,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// yaxis(pic, L="",  
//       axis=XEquals(xright+0.2),
//       ymin=ybot-0.5, ymax=ytop+0.5,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ========== complex plane =============
picture pic;
int picnum = 2;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-4; xright=4;
ybot=-2; ytop=4;

pair z0 = (3,2);
pair z1 = (-1,3);
pair z0_conj = (3,-2);

dotfactor = 4;
dot(pic, "$z_0$", z0, highlight_color);
dot(pic, "$z_1$", z1, highlight_color);
dot(pic, "$\overline{z_0}$", z0_conj, highlight_color);

// x and y axes
Label L = rotate(0)*Label("\scriptsize $\operatorname{Re}$",EndPoint,2*S, filltype=Fill(white));
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $\operatorname{Im}$",EndPoint,W, filltype=Fill(white));
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ........ addition ..........
picture pic;
int picnum = 3;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

pair z0 = (3,1);
pair z1 = (2,2);
pair z = (5,3);

draw(pic, (0,0)--z0, highlight_color);
draw(pic, (0,0)--z1, highlight_color);
draw(pic, z0--z, highlight_color);
draw(pic, z1--z, highlight_color);

dotfactor = 4;
dot(pic, "$z_0$", z0, highlight_color);
dot(pic, "$z_1$", z1, N, highlight_color);
dot(pic, "$z_0+z_1$", z, N, highlight_color);

// x and y axes
Label L = rotate(0)*Label("\raisebox{-10pt}{\scriptsize $x$}",EndPoint,2*E);
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ================ homeworks ===============

// ....... sixth roots of 5 ..........
picture pic;
int picnum = 4;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-2; ytop=2;

draw(pic, circle( (0,0), 5^(1/6)));
real rad = 5^(1/6);
for (int k; k<=6; ++k) {
  // dot(pic, format("$w_{%d}$", k), (rad*cos(2*k*pi/6), rad*sin(2*k*pi/6)), highlight_color);
  dot(pic, (rad*cos(2*k*pi/6), rad*sin(2*k*pi/6)), highlight_color);
}
label(pic, "$w_0$", (rad*cos(2*0*pi/6), rad*sin(2*0*pi/6)), NE, highlight_color);
label(pic, "$w_1$", (rad*cos(2*1*pi/6), rad*sin(2*1*pi/6)), E, highlight_color);
label(pic, "$w_2$", (rad*cos(2*2*pi/6), rad*sin(2*2*pi/6)), W, highlight_color);
label(pic, "$w_3$", (rad*cos(2*3*pi/6), rad*sin(2*3*pi/6)), NW, highlight_color);
label(pic, "$w_4$", (rad*cos(2*4*pi/6), rad*sin(2*4*pi/6)), W, highlight_color);
label(pic, "$w_5$", (rad*cos(2*5*pi/6), rad*sin(2*5*pi/6)), E, highlight_color);

// x and y axes
Label L = rotate(0)*Label("\tiny $\operatorname{Re}$",EndPoint,SE, filltype=Fill(white));
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\tiny $\operatorname{Im}$",EndPoint,W, filltype=Fill(white));
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ........ \pm (3/2)i
picture pic;
int picnum = 5;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-2; ytop=2;

dotfactor = 4;
dot(pic, "$3/2$", (0,3/2), E, highlight_color);
dot(pic, "$-3/2$", (0,-3/2), E, highlight_color);

// x and y axes
Label L = rotate(0)*Label("\tiny $\operatorname{Re}$",EndPoint,S+SE, filltype=Fill(white));
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\tiny $\operatorname{Im}$",EndPoint,W, filltype=Fill(white));
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// =============== complex cube roots of 1 ==========
picture pic;
int picnum = 6;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1.5; xright=1.5;
ybot=-1.5; ytop=1.5;

path c = circle( (0,0), 1); 
draw(pic, c, FCNPEN);

pair r0 = (1,0);
pair r1 = (cos(2*pi/3), sin(2*pi/3));
pair r2 = (cos(4*pi/3), sin(4*pi/3));

dotfactor = 4;
dot(pic, "$z_0$", r0, NE, highlight_color);
dot(pic, "$z_1$", r1, NW, highlight_color);
dot(pic, "$z_2$", r2, SW, highlight_color);

// x and y axes
Label L = rotate(0)*Label("\tiny $\operatorname{Re}$",EndPoint,S+SE, filltype=Fill(white));
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\tiny $\operatorname{Im}$",EndPoint,W, filltype=Fill(white));
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




