// Ellipse with axes
include "../../asy/jh.asy";
// picture size
real height; height=2.25cm; size(0,height);
// the earth
pair earth_center=(-1,-1);
real earth_radius=2;
path earth_surface=arc(earth_center,earth_radius,30,60);  
// earth
pair origin=(xpart(point(earth_surface,1)),ypart(point(earth_surface,0)));
fill(earth_surface--origin--cycle,MAINPEN+rgb(110/225,156/255,206/255));
draw(earth_surface,MAINPEN);
// ball path
pair end_point=point(arc(earth_center,3.5*earth_radius,30,60),0.5);
pair surface_point=intersectionpoint(earth_center--end_point,earth_surface);
path ball_path=surface_point--end_point;
path ball=(scale(.075)*unitcircle);
path big_ball=scale(1.4)*ball;
draw(ball_path,MAINPEN+linetype(new real[] {2,2}));
fill(shift(point(ball_path,.91))*big_ball,THINPEN+rgb(1,1,1));
filldraw(shift(point(ball_path,.91))*ball,THINPEN,fillpen=THINPEN+rgb(.85,.85,.85));
// fill(ball,MAINPEN+rgb(1,0,0));
// draw(ball,MAINPEN);
