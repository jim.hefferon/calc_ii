// Ellipse with axes
include "../../asy/jh.asy";
// picture size

import graph;

// size(150,0);
real height; height=2.25cm; size(0,height);

real f(real x) {return exp(-x);}
pair F(real x) {return (x,f(x));}

path g=graph(f,-0.1,5.1,operator ..);

real xlim=5.0;
real ylim=1.0;
path c=buildcycle(g,(xlim,50)--(xlim,-1),(xlim,0)--(-1,0),(0,-1)--(0,ylim),(0,ylim)--(50,ylim));
fill(c,THINPEN+FILLCOLOR);

xaxis("\tiny $x$",-.2);
yaxis("\raisebox{-2ex}{\tiny $y$}",-.2);

draw(g,FCNPEN);

// labely(1,W);
// label("$\log x$",F(7),SE);
