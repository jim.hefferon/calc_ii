// Improper integral with 1/x
include "../../asy/jh.asy";
// picture size

import graph;

// size(150,0);
real height; height=2.25cm;
real width; width=5cm;
size(width,height);

real f(real x) {return x^(-1);}
pair F(real x) {return (x,f(x));}

path g=graph(f,0.2,5.1,operator ..);

real xlim=5.0;
real ylim=2.0;
// draw((xlim,50)--(xlim,-1),THINPEN+FILLCOLOR); 
path c=buildcycle(g,(xlim,50)--(xlim,-1),(xlim,0)--(1,0),(1,-1)--(1,ylim));
fill(c,THINPEN+FILLCOLOR);

xaxis("\tiny $x$",-0.1);
yaxis("\raisebox{-2ex}{\tiny $y$}",-0.1);

draw(g,FCNPEN);

label("\makebox[0em][l]{\tiny $y=1/x$}",(0.25,4),E);
label("\tiny $1$",(1,0),S);
