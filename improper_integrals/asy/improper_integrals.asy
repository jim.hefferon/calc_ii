// improper_integrals.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "improper_integrals%03d";

import graph;



// =========== ball leaving earth ===============

picture pic;
int picnum = 0;
size(pic,0,4cm,keepAspect=true);

pair earth_center=(-1,-1);
real earth_radius=2;
path earth_surface=arc(earth_center,earth_radius,0,90);  

pair ball_center = (3,3);
path ball_path = (0,0)--ball_center;
draw(pic, subpath(ball_path,0,0.97), gray(0.85), Arrow(ARROW_SIZE));

real earth_surface_0_horiz_time = times(earth_surface, (0,0))[0];
real earth_surface_0_vert_time = times(earth_surface, 0)[0];
path earth_shown = ( (0,0)--point(earth_surface,earth_surface_0_horiz_time)
		     &subpath(earth_surface,earth_surface_0_horiz_time,earth_surface_0_vert_time)
		     &point(earth_surface,earth_surface_0_vert_time)--(0,0) )--cycle;
fill(pic, earth_shown, rgb(110/225,156/255,206/255));
draw(pic, subpath(earth_surface,earth_surface_0_horiz_time,earth_surface_0_vert_time), FCNPEN);

path ball = circle(ball_center, 0.05);
// filldraw(pic, ball, background_color, FCNCOLOR);
label(pic, graphic("../pix/baseball.png","width=0.2cm"),ball_center);

shipout(format(OUTPUT_FN,picnum),pic); // ,format="pdf"



// ========== e^{-x} ============
real f1(real x) {return exp(-x);}

picture pic;
int picnum = 1;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=2;

real a = 0; real b = 5;
path f = graph(pic, f1, a-0.1, b+0.1);

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region= ( (a,0)--point(f,f_a_time)
	       &subpath(f,f_a_time,f_b_time)
	       &point(f,f_b_time)--(b,0)--(a,0) )--cycle;
fill(pic, region, FILLCOLOR+opacity(0.6));
draw(pic, f, FCNPEN);

xaxis(pic, L="\scriptsize $x$",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T1, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", a);

real[] T1a = {1};
Label L = rotate(0)*Label("\scriptsize $y$",EndPoint,W);
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=LeftTicks("%", T1a, Size=2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$1$", 1);

shipout(format(OUTPUT_FN,picnum),pic);





// ========== 1/x ============
real f2(real x) {return 1/x;}

picture pic;
int picnum = 2;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=2;

real a = 1; real b = 5;
path f = graph(pic, f2, a-0.5, b+0.1);

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region= ( (a,0)--point(f,f_a_time)
	       &subpath(f,f_a_time,f_b_time)
	       &point(f,f_b_time)--(b,0)--(a,0) )--cycle;
fill(pic, region, FILLCOLOR+opacity(0.6));
draw(pic, f, FCNPEN);

label(pic, "$f(x)=1/x$", (3,1), filltype=Fill(white));

real[] T1 = {a};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T1, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", a);

// real[] T1a = {1};
Label L = rotate(0)*Label("",EndPoint,W);
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=LeftTicks("%", T1a, Size=2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$1$", 1);

shipout(format(OUTPUT_FN,picnum),pic);





// ========== 1/x^2 ============
real f3(real x) {return 1/(x^2);}

picture pic;
int picnum = 3;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=2;

real a = 1; real b = 5;
path f = graph(pic, f3, a-0.25, b+0.1);

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region= ( (a,0)--point(f,f_a_time)
	       &subpath(f,f_a_time,f_b_time)
	       &point(f,f_b_time)--(b,0)--(a,0) )--cycle;
fill(pic, region, FILLCOLOR+opacity(0.6));
draw(pic, f, FCNPEN);

label(pic, "$f(x)=1/x^2$", (3,1), filltype=Fill(white));

xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T1, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", a);

Label L = rotate(0)*Label("",EndPoint,W);
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks("%", T1a, Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ========== 1/x^(1/2) ============
real f4(real x) {return 1/(sqrt(x));}

picture pic;
int picnum = 4;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=2;

real a = 1; real b = 5;
path f = graph(pic, f4, a-0.25, b+0.1);

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region= ( (a,0)--point(f,f_a_time)
	       &subpath(f,f_a_time,f_b_time)
	       &point(f,f_b_time)--(b,0)--(a,0) )--cycle;
fill(pic, region, FILLCOLOR+opacity(0.6));
draw(pic, f, FCNPEN);

real[] T1 = {a,b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T1, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);

// real[] T1a = {1};
Label L = rotate(0)*Label("",EndPoint,W);
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks("%", T1a, Size=2pt),
      arrow=Arrows(TeXHead));
//  labely(pic, "$1$", 1);

shipout(format(OUTPUT_FN,picnum),pic);





// ========== x vs x^2 vs sqrt(x) ============
real f5(real x) {return x;}
real f5a(real x) {return x^2;}
real f5b(real x) {return sqrt(x);}

picture pic;
int picnum = 5;
scale(pic, Linear(5), Linear);
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=25;

real a = 0+.01; real b = 5;
path f = graph(pic, f5, a, b+0.1);
path fa = graph(pic, f5a, a, b+0.1);
path fb = graph(pic, f5b, a, b+0.1);

draw(pic, f, FCNPEN);
draw(pic, fa, FCNPEN);
draw(pic, fb, FCNPEN);

real c = 5.1;
label(pic, "\scriptsize $x^2$", Scale(pic, (c,f5a(c))), W);
label(pic, "\scriptsize $x$", Scale(pic, (c,f5(c))), NW);
label(pic, "\scriptsize $x^{1/2}$", Scale(pic, (c,f5b(c))), E);

xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

Label L = rotate(0)*Label("",EndPoint,W);
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-1.5, ymax=ytop+1.5,
      p=currentpen,
      ticks=LeftTicks(Step=10, step=1, OmitTick(0,-1,26), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);





// ========== discontinuity at 0 of 1/x^2 ============
real f6(real x) {return 1/x^2;}

picture pic;
int picnum = 6;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=4;
ybot=0; ytop=6;

real a = -1; real b = 3;

real slop = 0.4;  // how close we come to 0 in graph of f
path f_neg = graph(pic, f6, xleft-0.2, 0-slop);
path f_pos = graph(pic, f6, 0+slop, xright+0.2);

// Draw graph paper
for(real i=ceil(xleft); i <= floor(xright); ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ceil(ybot); j <= floor(ytop); ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

real f_a_time = times(f_neg, a)[0];
real f_neg_slop_time = times(f_neg, 0-slop)[0];
real f_pos_slop_time = times(f_pos, 0+slop)[0];
real f_b_time = times(f_pos, b)[0];
path region= ( (a,0)--point(f_neg,f_a_time)
	       &subpath(f_neg,f_a_time,f_neg_slop_time)
	       &point(f_neg,f_neg_slop_time)
	       --point(f_pos,f_pos_slop_time)
	       &subpath(f_pos,f_pos_slop_time,f_b_time)
	       &point(f_pos,f_b_time)
	       --(b,0)--(a,0) )--cycle;
fill(pic, region, FILLCOLOR+opacity(0.6));
draw(pic, f_neg, FCNPEN);
draw(pic, f_pos, FCNPEN);

real[] T1 = {a,b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", T1, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$-1$", a);
labelx(pic, "$3$", b);

// real[] T1a = {1};
Label L = rotate(0)*Label("",EndPoint,W);
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks("%", T1a, Size=2pt),
      arrow=Arrows(TeXHead));
//  labely(pic, "$1$", 1);

shipout(format(OUTPUT_FN,picnum),pic);





// ========== exponential distribution ============
real LAMBDA = 0.5;
real f7(real x) {return LAMBDA*exp(-1*LAMBDA*x);}

picture pic;
int picnum = 7;
size(pic,0,1.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=0.5;

real a = 0.3; real b = 3;
path f = graph(pic, f7, 0, xright+0.2);

real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region= ( (a,0)--point(f,f_a_time)
	       &subpath(f,f_a_time,f_b_time)
	       &point(f,f_b_time)
	       --(b,0)--(a,0) )--cycle;
fill(pic, region, FILLCOLOR+opacity(0.6));
draw(pic, f, FCNPEN);

real[] T7 = {a, b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks("%", T7, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", a);
labelx(pic, "$10$", b);

Label L = rotate(0)*Label("",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks("%", T1a, Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ............... to infinity .............
picture pic;
int picnum = 8;
size(pic,0,1.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=0.5;

real a = 3; real b = xright;
path f = graph(pic, f7, 0, xright+0.2);

real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region= ( (a,0)--point(f,f_a_time)
	       &subpath(f,f_a_time,f_b_time)
	       &point(f,f_b_time)
	       --(b,0)--(a,0) )--cycle;
fill(pic, region, FILLCOLOR+opacity(0.6));
draw(pic, f, FCNPEN);

real[] T7 = {a};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks("%", T7, Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$10$", a);
// labelx(pic, "$b$", b);

Label L = rotate(0)*Label("",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks("%", T1a, Size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ================ arctan =============
real f9(real x) { return atan(x); }

picture pic;
int picnum = 9;
size(pic,6cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-0.5; xright=10;
ybot=-0.5; ytop=pi/2;

draw(pic, (0,pi/2)--(xright+0.2,pi/2), dashed);
path f = graph(pic, f9, xleft-0.2, xright+0.2);
draw(pic, f, FCNPEN);

xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks(Step=5, step=1, OmitTick(0), Size=2pt, size=2pt),
      arrow=Arrows(TeXHead));

real[] T9a = {1,pi/2};
Label L = rotate(0)*Label("",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks("%", T9a, Size=2pt),
      arrow=Arrows(TeXHead));
labely(pic, "$\pi/2$", pi/2);

shipout(format(OUTPUT_FN,picnum),pic);




// ================ normal curve =============
real MEAN = 0;
real STDDEV = 1;
real f10(real x) { return (1/(STDDEV*sqrt(2*pi)))*exp(-1*((x-MEAN)^2)/((2*STDDEV)^2)); }

picture pic;
int picnum = 10;
size(pic,6cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3.5; xright=3.5;
ybot=0; ytop=0.5;

path f = graph(pic, f10, xleft-0.2, xright+0.2);

// region
real a= xleft; real b = 1.618;
real f_a_time = times(f, a)[0];
real f_b_time = times(f, b)[0];
path region = ( (a,0)--point(f,f_a_time)
		&subpath(f,f_a_time,f_b_time)
                &point(f,f_b_time)
                --(b,0)--(a,0) )--cycle;
fill(pic, region, FILLCOLOR);
draw(pic, (b,0)--point(f,f_b_time));

draw(pic, f, FCNPEN);

real[] T10 = {-3,-2,-1,1,2,3};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.5,
      p=currentpen,
      ticks=RightTicks("%", T10, Size=2pt, size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", b);

Label L = rotate(0)*Label("",EndPoint,W);
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




