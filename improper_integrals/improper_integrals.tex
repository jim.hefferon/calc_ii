\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

% \setbeamerfont{frametitle}{size=\LARGE}
% \setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
% \setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 7.8\hspace{1em} Improper integrals}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Improper integrals}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage arc_length_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
\input asy/improper_integrals_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................


\section{Integrals to infinity}

\begin{frame}
\frametitle{Escape velocity} 
\Ex
Toss a ball in the air and it gradually slows and then falls.
Tossing it harder makes it go higher but gravity keeps
pulling and eventually the ball slows, stops, and falls back.
Can we throw the ball so hard that it will never come back:~is there 
an amount of work that we can do on the ball to make it 
go up forever? 
\begin{center}
  \vcenteredhbox{\includegraphics{asy/improper_integrals000.pdf}}
\end{center}
\end{frame}

\begin{frame}
We need to counter the work done on the ball by the earth's gravity.
Work is the integral of force.
The earth's radius is $6371$~km.
\begin{align*}
  \text{Work}
  &=\integral{r=6371}{\infty}{\operatorname{Force}(r)}{r}  \\
  &=\integral{6371}{\infty}{\frac{GmM}{r^2}}{r}  \\ 
  &=
  \lim_{R\to\infty}\bigl(\integral{r=6371}{R}{\frac{GmM}{r^2}}{r}\bigr)  \\
  &=
  \lim_{R\to\infty}\bigl(GmM\integral{r=6371}{R}{r^{-2}}{r}\bigr)  \\
  &=
  GmM\cdot\lim_{R\to\infty}\bigl(\Bigl[-r^{-1}\Bigr]_{r=6371}^{R}\bigr)  \\
  &=
  GmM\cdot\lim_{R\to\infty}\bigl(\frac{1}{6371}-\frac{1}{R}\bigr) 
  =
  \frac{GmM}{6371}  
\end{align*}
Tha answer is yes\Dash if we impart a kinetic energy to the ball
of at least $GmM/6371$~Joules then it will not return.
\end{frame}

\begin{frame}
\Df Where $a\in\R$, suppose that the function $f$ is integrable on 
$\closed{a}{b}$ for $b\geq a$.
The \alert{improper integral} of $f$ over $\leftclosed{a}{\infty}$ is
\begin{equation*}
  \integral{x=a}{\infty}{f(x)}{x}
  =\lim_{R\to\infty}\bigl(\integral{x=a}{R}{f(x)}{x}\bigr)
\end{equation*}
if that limit exists.
Similarly, 
\begin{equation*}
  \integral{-\infty}{a}{f(x)}{x}
  =\lim_{R\to-\infty}\bigl(\integral{R}{a}{f(x)}{x}\bigr)
\end{equation*}
and 
\begin{equation*}
  \integral{-\infty}{\infty}{f(x)}{x}
  =\integral{-\infty}{0}{f(x)}{x}+\integral{0}{\infty}{f(x)}{x}
\end{equation*}
provided that those limits exist.

When the limit exists we say that the integral \alert{converges},
otherwise it \alert{diverges}.  
\end{frame}


\begin{frame}
\Ex
$\displaystyle  \integral{x=0}{\infty}{e^{-x}}{x} $
\pause
\begin{align*}
  \lim_{R\to\infty}\bigl( \integral{x=0}{R}{e^{-x}}{x} \bigr)    
  &=
  \lim_{R\to\infty}\bigl( \Bigl[ -e^{-x} \Bigr]_{x=0}^{R} \bigr)    \\
  &=
  \lim_{R\to\infty}\bigl( 1-\frac{1}{e^{R}} \bigr)  \\
  &=1
\end{align*}
\begin{center}
  \includegraphics{asy/improper_integrals001.pdf}
\end{center}

\medskip
So a planar region can be infinite in extent, and yet have finite area.
\end{frame}



\begin{frame}
\Ex $\displaystyle \integral{x=1}{\infty}{1/x^{2}}{x} $
\pause
\begin{align*}
  \lim_{R\to\infty}\bigl( \integral{x=1}{R}{x^{-2}}{x} \bigr)   
  &=
  \lim_{R\to\infty}\bigl( \Bigl[ -x^{-1} \Bigr]_{x=1}^{R} \bigr)    \\
  &=
  \lim_{R\to\infty}\bigl( 1-\frac{1}{R} \bigr)    \\
  &=1
\end{align*}
\begin{center}
  \includegraphics{asy/improper_integrals003.pdf}
\end{center}
\end{frame}


\begin{frame}{Practice}
\begin{enumerate}
\item $\displaystyle \integral{-1}{\infty}{ e^{-x} }{x}$
\pause
\item $\displaystyle \integral{0}{\infty}{ \cos x }{x}$
% \pause
% \item $\displaystyle \integral{-\infty}{0}{ xe^x }{x}$
\end{enumerate}
  
\end{frame}



% \begin{frame}{Application: probability density functions}
% \Ex
% We will perform this experiment on specialty florescent bulbs from a vendor:~we 
% select one at random and run it until it fails.
% What is the chance that the bulb lasts between $1$ and $10$ hours?

% \pause
% Here the area under the curve gives the chance the bulb 
% lasts the specified period.
% \begin{center}
%   \vcenteredhbox{\includegraphics{asy/improper_integrals007.pdf}}
% \end{center}
% Florescent bulb life is known to follow a certain pattern, where the function
% in the graph is $f(x)=\lambda e^{-\lambda x}\!$.
% The bulb manufacturing process results $\lambda=0.5$.
  
% We want this.
% \begin{equation*}
%   \integral{x=1}{10}{ 0.5e^{-0.5 x} }{x}
%   =0.5\cdot\integral{1}{10}{ e^{-0.5 x} }{x}
%   =0.5\cdot\netchange{1}{10}{ \frac{1}{-0.5}e^{-0.5 x} }
%   =(-e^{-5})-(-e^{-0.5})
% \end{equation*}
% The answer is approximately $0.600$.
% \end{frame}

% \begin{frame}
% What is the chance that the bulb lasts more than $10$ hours?  
% \begin{center}
%   \vcenteredhbox{\includegraphics{asy/improper_integrals008.pdf}}
% \end{center}
% \begin{equation*}
%   \integral{x=10}{\infty}{ 0.5\cdot e^{-0.5 x} }{x}
%   =\lim_{R\to\infty}\;\integral{10}{R}{ 0.5\cdot e^{-0.5 x} }{x}
%   =0.5\cdot\lim_{R\to\infty}\; \Bigl[ \frac{1}{-0.5}e^{-0.5 x} \Bigr]_{10}^{R}
% \end{equation*}
% Cancel the $0.5$'s and plug in $R$ and~$10$ to get this.
% \begin{equation*}
%   \lim_{R\to\infty} \bigl( -e^{-R}-(-e^{-5})\bigr)
%   =\lim_{R\to\infty} \bigl( e^{-5}-e^{-R}\bigr)
%   =\lim_{R\to\infty} \bigl( \frac{1}{e^5}-\frac{1}{e^R}\bigr)
%   =\frac{1}{e^5}\approx 0.007
% \end{equation*}
% \end{frame}
% % sage: def f(x):
% % ....:     return  -exp(-0.5*x)
% % ....: 
% % sage: f(300)-f(100)
% % 1.92874984796392e-22
% % sage: 1-f(1000)
% % 1.00000000000000
% % sage: f(100)-f(50)
% % 1.38879438647711e-11
% % sage: f(10)-f(1)
% % 0.599792712713548
% % sage: n(exp(-5))
% % 0.00673794699908547


% \begin{frame}{The role of the integrand function~$f$}
% The Net Change Theorem says we can view the integrand as a rate.
% With probability density functions, 
% as we sweep left to right, we acumulate probability.
% The height of the function~$f$ is the rate at which we are
% currently accumulating probability. 

% For example, here is a standard Normal curve.
% \begin{center}
%   \vcenteredhbox{\includegraphics{improper_integrals010.pdf}}%
%   \qquad
%   \vcenteredhbox{$\displaystyle f(t)=\frac{1}{\sqrt{2\pi}}\cdot e^{-t^2/2}$}
% \end{center}
% The area shown is this.
% \begin{equation*}
%   \operatorname{Prob}(t\leq x)
%   =
%   \integral{t=-\infty}{x}{ \frac{1}{\sqrt{2\pi}}\cdot e^{-t^2/2} }{t}
% \end{equation*}
% The $1/\sqrt{2\pi}$ is a normalizing factor, ensuring that
% the total area from $t=-\infty$ to $t=+\infty$ is $1.00$, as 
% a total probablility should be.
% \end{frame}


% \begin{frame}
% \Df A \alert{probability density function} on $\closed{0}{\infty}$
% is a function~$f$ such that
% $f(x)\geq 0$ for all $x$ in its domain and such that
% $\integral{x=0}{\infty}{ f(x) }{x}=1$.

% \Ex Find the constant~$c$ so that $c\cdot 1/(1+x^2)$ is a probability
% density function.

% \pause
% We need this.
% \begin{equation*}
%   1
%   =\integral{x=0}{\infty}{ \frac{c}{1+x^2} }{x}
%   =\lim_{R\to\infty}\;\integral{x=0}{R}{ \frac{c}{1+x^2} }{x}
%   =c\cdot\lim_{R\to\infty}\;\integral{x=0}{R}{ \frac{1}{1+x^2} }{x}
% \end{equation*}
% Use Trig Substitution with $x=\tan\theta$.
% \begin{equation*}
%   =c\cdot\lim_{R\to\infty}\;\netchange{x=0}{R}{ \tan^{-1}x }  
% \end{equation*}
% As a reminder, this is the graph of $\tan^{-1}(x)$.
% \begin{center}
%   \vcenteredhbox{\includegraphics{improper_integrals009.pdf}}
% \end{center}
% We have this
% \begin{equation*}
%   1=c\cdot \lim_{R\to\infty}\bigl( \tan^{-1}R-\tan^{-1}0\bigr)
%    =c\cdot\frac{\pi}{2}
% \end{equation*}
% so $c=2/\pi$.
% \end{frame}





% \begin{frame}{Application: money}
% \Ex
% People click on your YouTube channel around the world, 
% twenty four hours per day. 
% We can model this as a continuous stream of income.

% You get $\$10\,000$ per year.
% The current market interest rate is $5\%$ per year, meaning that money
% next year is worth less than money todayand so we must discount future
% payments to their \alert{present value}.
% Find how much your channel is worth today.

% \pause
% Accumulate the stream of income, discounted at the market rate,
% using continuous interest.
% \begin{align*}
%   \text{Present value} 
%    &= \integral{t=0}{\infty}{ 10000e^{-0.05t} }{t}      \\
%    &= \lim_{R\to\infty}\bigl( \integral{0}{R}{ 10000e^{-0.05t} }{t} \bigr)  \\
%    &= \lim_{R\to\infty}\bigl( \Bigl[ \frac{10000e^{-0.05t}}{-0.05} \Bigr]_0^R \bigr)  \\
%   &=200\,000
% \end{align*}
% \end{frame}




\begin{frame}
\Ex $\displaystyle \integral{x=1}{\infty}{\frac{1}{x}}{x}$
\pause
\begin{align*}
  \lim_{R\to\infty}\bigl( \integral{x=1}{R}{x^{-1}}{x} \bigr)    
  &=
  \lim_{R\to\infty}\bigl( \Bigl[ \ln(x) \Bigr]_{x=1}^{R} \bigr)    \\
  &=
  \lim_{R\to\infty}\bigl( \ln(R) \bigr)                         \\
  &=\infty
\end{align*}
\begin{center}
  \includegraphics{asy/improper_integrals002.pdf}
\end{center}
\end{frame}



\begin{frame}
\Tm
Let $p$ be a real number power.
\begin{equation*}
  \integral{x=1}{\infty}{\frac{1}{x^p}}{x}
  =
  \begin{cases}
    \text{converges to $1/(p-1)$}     &\text{--if $p>1$}  \\
    \text{diverges}  &\text{--otherwise}
  \end{cases}
\end{equation*}

\pause\medskip
\Pf
We have this.
\begin{align*}
  \lim_{R\to\infty}\bigl( \integral{x=1}{R}{x^{-p}}{x} \bigr)
  &=
  \lim_{R\to\infty}\;\bigl( \left[ \frac{x^{-p+1}}{-p+1} \right]_{x=1}^{R} \bigr)   \\
  &=\frac{1}{1-p}\cdot
  \lim_{R\to\infty}\bigl( R^{1-p}-1 \bigr)            
\end{align*}
If $p>1$ then $1-p$ is negative, so $R^{1-p}\to 0$, 
and the limit converges to $1/p-1$.
If $p=1$ then the prior example shows that the integral diverges.
If $p<1$ then $1-p$ is positive and $R^{1-p}\to\infty$, so the integral diverges.
\end{frame}


\begin{frame}{Intuition: convergence is about the rate at which $f$ falls to zero}\vspace*{-1ex}
Convergence or divergence of the integral
has to do with the order of growth of the functions $x^p\!$.
These power functions that all have $\lim_{x\to\infty}1/f(x)=0$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/improper_integrals005.pdf}}%
\end{center}
The $x^2$ goes to infinity quickly and so $1/x^2$ goes to zero quickly.
The $x$ goes to infinity relatively slowly
and so $1/x$ goes to zero relatively slowly.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/improper_integrals003.pdf}}%
  \quad
  \vcenteredhbox{\includegraphics{asy/improper_integrals002.pdf}}%
\end{center}
Consequently, the total area can build fast enough to escape to infinity.

The integral converges if the function goes to zero sufficiently fast.
\end{frame}




\begin{frame}{Gabriel's horn}
\Ex
Consider the region under the curve $f(x)=1/x$ for $x\geq 1$.
Rotate that region about the $x$~axis and find the volume.
\begin{center}
  \vcenteredhbox{\input asy/improper_integrals_3d000.tex }
\end{center}
\pause
\begin{align*}
  \integral{x=1}{\infty}{ \pi\biggl(\frac{1}{x}\biggr)^2 }{x}
  &=\pi\cdot \lim_{R\to\infty} \bigl( \integral{x=1}{R}{ \frac{1}{x^2} }{x} \bigr)            \\
  &=\pi\cdot \lim_{R\to\infty} \bigl( \Bigl[ -\frac{1}{x} \Bigr]_{x=1}^{R}\bigr)  \\
  &=\pi\cdot \lim_{R\to\infty} \bigl( 1-\frac{1}{R} \bigr)  
  =\pi
\end{align*}
\end{frame}



% \begin{frame}{Comparison test}
% \Tm
% Let $f(x)\geq g(x)\geq 0$ for $x\geq a$.
% \begin{itemize}
% \item
% If $\integral{a}{\infty}{ f(x) }{x}$ converges then so does
% $\integral{a}{\infty}{ g(x) }{x}$.
% \item
% If $\integral{a}{\infty}{ g(x) }{x}$ diverges then so does
% $\integral{a}{\infty}{ f(x) }{x}$.
% \end{itemize}

% % \Ex We have remarked that we cannot find this by finding an antiderivative.
% % \begin{equation*}
% %   \integral{x=1}{\infty}{ e^{-x^2} }{x}
% % \end{equation*}
% % But we can show that it converges\Dash it has a finite value\Dash
% % by comparing it with $\integral{1}{\infty}{ e^{-x}}{x}$, which converges by 
% % a calculation that we've done earlier.
% % Because $-x^2\leq -x$ when $x$ is greater than one, the integrands compare
% % as $e^{-x^2}\leq e^{-x}\!$. 

% % \pause
% % \Ex Consider $\integral{1}{\infty}{1/\sqrt{x^2+1}}{x}$.
% % We suspect that it diverges
% % because $1/\sqrt{x^2+1}$ is like $1/x$.
% % But these two don't fit immediately the comparison 
% % test because $1/\sqrt{x^2+1}$ is less than $1/x$.

% % We instead take $x^2+1\leq x^2+x^2=2x^2$ for $x\geq 1$, and so
% % \begin{equation*}
% %   \frac{1}{\sqrt{x^2+1}}\geq \frac{1}{\sqrt2\cdot x}  
% % \end{equation*}
% % for $x\geq 1$.
% % The latter diverges because the integral is $1/\sqrt{2}$ times the 
% % integral of $1/x$.
% \end{frame}






\section{Integrals with infinite discontinuities}

\begin{frame}
Consider this problem.
\begin{equation*}
  \integral{-1}{3}{ \frac{1}{x^2} }{x}
\end{equation*}
A naive approach is this.
\begin{equation*}
  =\Bigl[ -x^{-1} \Bigr]_{-1}^{3}
  =\bigl(-\frac{1}{3}\bigr) - \bigl( -\frac{1}{-1} \bigr)
  =-\frac{4}{3}
\end{equation*}
Remember that $1/x^2$ is always positive, so how
can the area be negative?
Obviously we did something wrong. 
\pause
The problem is the discontinuity at zero.
\begin{center}
  \includegraphics{asy/improper_integrals006.pdf}
\end{center}
\end{frame}


\begin{frame}
We need this.
\begin{equation*}
  \integral{-1}{3}{ \frac{1}{x^2} }{x}
  =
  \integral{-1}{0}{ \frac{1}{x^2} }{x}
  +\integral{0}{3}{ \frac{1}{x^2} }{x}
\end{equation*}
On the right, take limits.
Here is the first.
\begin{multline*}
  \integral{-1}{0}{ \frac{1}{x^2} }{x}
  =\lim_{R\to 0^{-}}\bigl( \integral{-1}{R}{ \frac{1}{x^2}  }{x} \bigr)
  =\lim_{R\to 0^{-}}\bigl( \Bigl[ -x^{-1} \Bigr]_{-1}^{R} \bigr)    \\
  =\lim_{R\to 0^{-}} \bigl( -\frac{1}{R} \bigr)-\bigl( -\frac{1}{-1} \bigr)
  =\lim_{R\to 0^{-}} \bigl( -\frac{1}{R}-1 \bigr)
\end{multline*}
This limit is infinite.
The area under the curve to the left of zero is infinite.
Because one of the two integrals does not give a finite value, we cannot
add the two to get the single integral at the top of this page. 
\end{frame}



\begin{frame}
\Df
Where $f$ is discontinuous at~$b$ but continuous on 
its left $\leftclosed{a}{b}$,
\begin{equation*}
  \integral{a}{b}{ f(x) }{x}
  =\lim_{R\to b^{-}} \bigl( \integral{a}{R}{ f(x) }{x} \bigr)
\end{equation*}
and similarly on the right $\rightclosed{b}{a}$,
\begin{equation*}
  \integral{b}{a}{ f(x) }{x}
  =\lim_{R\to b^{+}} \bigl( \integral{R}{a}{ f(x) }{x} \bigr)
\end{equation*}
provided that the limits exist.
If $f$ has an infinite discontinuity at some $c$ in the interval $\closed{a}{b}$
but is continuous elsewhere in that interval then 
\begin{equation*}
  \integral{a}{b}{ f(x) }{x}
  =
  \integral{a}{c}{ f(x) }{x}
  +\integral{c}{b}{ f(x) }{x}
\end{equation*}
as long as both right-hand integrals converge.
\end{frame}

\begin{frame}
\Ex $\displaystyle \integral{0}{3}{ \frac{1}{\sqrt{x}} }{x}$

\pause
\begin{align*}
  \lim_{R\to 0^{+}}\bigl( \integral{R}{3}{ x^{-1/2} }{x} \bigr)
  &=
  \lim_{R\to 0^{+}}\bigl( \Bigl[ 2x^{1/2} \Bigr]_{R}^{3} \bigr)  \\
  &=
  \lim_{R\to 0^{+}}\bigl( 2\sqrt{3}-2\sqrt{R} \bigr)  \\
  &=2\sqrt{3}
\end{align*}
\end{frame}


\begin{frame}{Practice}

\begin{enumerate}
  \item $\displaystyle \integral{0}{3}{ \frac{1}{x^2} }{x}$
  \item $\displaystyle \integral{-2}{0}{ \frac{1}{x} }{x}$
\end{enumerate}

\end{frame}




% \begin{frame}
% \frametitle{}
% 
% \end{frame}
\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
