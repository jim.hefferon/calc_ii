\documentclass[9pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../sty/present}
\usepackage{concmath-otf}
% \usepackage{unicode-math}
% \setmathfont{Concrete Math}
% \setmathfont{Concrete-Math-Regular.otf}[BoldFont=Concrete-Math-Bold.otf,CharacterVariant={10}]
\usepackage{../sty/calcii}

% Colors
% From https://color.adobe.com/ Fisher French Tomato, James Morris
\definecolor{Burgundy}{HTML}{8D001A}
\definecolor{GoldenImpression}{HTML}{FFF0CA}
\definecolor{ManiacMansion}{HTML}{00394F}
\definecolor{HazyRose}{HTML}{B29596}
\definecolor{KosherKhaki}{HTML}{8A897A}
\definecolor{UsualGray}{gray}{0.1}  % matches GoldenImpression
\setbeamercolor{frametitle}{fg=ManiacMansion,bg=white}
\setbeamercolor{part title}{fg=ManiacMansion,bg=white}
\setbeamercolor{itemize item}{fg=ManiacMansion}
\setbeamercolor{alerted text}{fg=Burgundy}

\setbeamerfont{frametitle}{size=\LARGE}
\setbeamertemplate{itemize item}{\color{ManiacMansion}$\blacktriangleright$}
\setbeamertemplate{description item}{\color{ManiacMansion}}
% \setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
% \addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 12.5\hspace{1em}Lines and planes}
% This is only inserted into the PDF information catalog. Can be left
% out. 
\subject{Lines and planes}

\author{J Hef{}feron}
\institute{
  Department of Mathematics and Statistics\\
  University of Vermont  %\\[1ex]
  %\texttt{James.Hefferon@uvm.edu}
}
\date{}

% For 3D PRC asymptote
% compile with asy -inlineimage stub_3d
\def\asydir{asy/}
\graphicspath{{asy/}}
% uncomment for 3D graphics: 
\input asy/lines_3d.pre
\usepackage[bigfiles]{media9}
\RequirePackage{asymptote}

\usepackage{siunitx}
\usepackage[f]{esvect}
\renewcommand{\vec}[1]{\vv{#1}}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



% \section{}



\section{Lines in 2D}
\begin{frame}{Describing lines with vectors}
In the plane, the line through \( (1,2) \) and \( (3,1) \)
is comprised of (the endpoints of) the vectors in this set.
\begin{equation*}
   \ell=\set{ \colvec{1 \\ 2}+t\cdot\colvec{2 \\ -1}\suchthat t\in\R}
   \qquad
   \colvec{2 \\ -1}=\colvec{3 \\ 1}-\colvec{1 \\ 2}
\end{equation*}
That \alert{vector form} description expresses this picture.
\begin{center}
  \vcenteredhbox{\includegraphics{lines000.pdf}}
  \quad
  \vcenteredhbox{
    \begin{tabular}{cc}
      \tabulartext{value of $t$} &\tabulartext{$(x,y)$} \\
      \hline
      $-1$ &$(-1,3)$  \\
      $0$  &$(1,2)$   \\
      $1$  &$(3,1)$   \\
      $2$  &$(5,0)$   \\
    \end{tabular}%
  }
\end{center}
The vector that is associated with the parameter~\( t \)
\begin{equation*}
  \vec{v}_2-\vec{v}_1=\colvec{2 \\ -1}
\end{equation*}
is the one highlighted in the picture as having its whole body in the 
line. 
It
is a \alert{direction vector} for the line.
Note that points on the line to the left of \( x=1 \) are described
using negative values of \( t \).
\end{frame}




\begin{frame}{Connection with $y=mx+b$}
\begin{center}
  \vcenteredhbox{\includegraphics{lines002.pdf}}
  \quad
  $\displaystyle \colvec{x \\ y}=\colvec{0 \\ b}+\colvec{1 \\ m}\cdot t$
\end{center}
The vector equation gives $x=0+t=t$ and $y=b+m\cdot t=b+mx$.
\end{frame}



\begin{frame}{The particular vector}
The line description 
\begin{equation*}
   \ell=\set{ \vec{p}+t\cdot\vec{d} \suchthat t\in\R}
   =\set{ \colvec{1 \\ 2}+t\colvec{2 \\ -1}\suchthat t\in\R}
\end{equation*}
involves two vectors, $\vec{d}$ and~$\vec{p}$.
To understand the role of the second, here   
\begin{center}
  \includegraphics{lines001.pdf}
\end{center}
is the line whose points are multiples of the 
direction vector~$\vec{d}$.
\begin{equation*}
   \set{t\cdot\vec{d}\suchthat t\in\R}
   =\set{ t\colvec{2 \\ -1}\suchthat t\in\R}
\end{equation*}
The vector $\vec{p}$ is a \alert{particular vector} in the line~$\ell$,
which shifts the line away from the origin.
\end{frame}


\begin{frame}{Any of the line's particular vectors would serve}
Recall the line described in this way.
\begin{equation*}
   \ell
   =\set{\vec{p}+t\cdot\vec{d}\suchthat t\in\R}
   =\set{ \colvec{1 \\ 2}+t\colvec{2 \\ -1}\suchthat t\in\R}
\end{equation*}
We can give a different description of the same line
by substituting any particular vector at all.
\begin{center}
    \begin{tabular}{ccc}
      \tabulartext{$t$} &\tabulartext{associated vector}  &\tabulartext{line description} \\
      \hline
      $-1$ &$\colvec{-1 \\ 3}$ 
           &$\set{ \colvec{-1 \\ 3}+s\colvec{2 \\ -1}\suchthat s\in\R}$ \\[2ex]
      $0$  &$\colvec{1 \\ 2}$   
           &$\set{ \colvec{1 \\ 2}+t\colvec{2 \\ -1}\suchthat t\in\R}$ \\[2ex]
      $1$  &$\colvec{ 3 \\ 1}$   
           &$\set{ \colvec{3 \\ 1}+u\colvec{2 \\ -1}\suchthat u\in\R}$ \\
    \end{tabular}%
\end{center}
For instance, the first has parametric equations $x=-1+2s$ and $y=3-1s$.
Replacing $s$ with $t+1$ gives $x=-1+2(t+1)=-1+2t+2=1+2t$
and $y=3-1(t+1)=3-t-1=2-t$.
Those are the equations from the second description.  
\end{frame}



\begin{frame}{Reminder: the of-the-way function}
\Lm The line segment from $\vec{r}_0$ to $\vec{r}_1$ is this set of points.
\begin{equation*}
  \vec{r}(t)=(1-t)\cdot \vec{r}_0+t\cdot \vec{r}_1
  \qquad\text{where $t\in\closed{0}{1}$}
\end{equation*}
In this picture $t=0.62$.
The resulting $\vec{r}$ is $62\%$ of the way
from $\vec{r}_0$ to $\vec{r}_1$.
\begin{center}
  \vcenteredhbox{\includegraphics{lines003.pdf}}
\end{center}
The formula holds in any number of dimensions, including in 2D and 3D.
\end{frame}



\section{Lines in 3D}

\begin{frame}
Given two points $(u_1,u_2,u_3)$ and $(v_1,v_2,v_3)$, the line through them
has this \alert{vector equation}.
\begin{equation*}
  \vec{r}
    =\colvec{r_1 \\ r_2 \\ r_3}
    =\colvec{u_1 \\ u_2 \\ u_3}+t\cdot\colvec{v_1-u_1 \\ v_2-u_2 \\ v_3-u_3}
    =\vec{p}+t\cdot \vec{d}
\end{equation*}
The vector $\vec{d}$ is a \alert{direction vector} and
$\vec{p}$ is a \alert{particular vector}.  
The real number~$t$ is the \alert{parameter}.

Where the components of~$\vec{d}$ are $d_1$, 
$d_2$, and
$d_3$, these are the line's \alert{parametric equations}.
\begin{equation*}
  r_1=u_1+t\cdot d_1
  \quad
  r_2=u_2+t\cdot d_2
  \quad
  r_3=u_3+t\cdot d_3
\end{equation*}

Solving those three for the parameter gives
$t=(r_1-u_1)/d_1$ 
and 
$t=(r_2-u_2)/d_2$ 
and 
$t=(r_3-u_3)/d_3$
(assuming the $d$'s are nonzero).
Since the three are equal to~$t$ they are equal to each other.
These are the \alert{symmetric equations} of the line.
\begin{equation*}
  \frac{r_1-u_1}{d_1}
  =
  \frac{r_2-u_2}{d_2}
  =
  \frac{r_3-u_3}{d_3}
\end{equation*}
Lines are \alert{parallel} if they have the same direction vector.
They are \alert{skew} if they are not parallel and do not intersect.
\end{frame}


\begin{frame}
\Ex 
Find the vector equation for the line through $P=(3,2,1)$ and $Q=(-2,1,-4)$.
Name its direction vector and its particular vector.
Name three points on the line and three not on the line.
Use that equation to give the three parametric equations.
Then give the symmetric equations.
  
\pause
We have
\begin{equation*}
  \colvec{3 \\ 2 \\ 1}-\colvec{-2 \\ 1 \\ -4}
         =\colvec{5 \\ 1 \\ 5}
  \qquad
  \colvec{x \\ y \\ z}=\colvec{-2 \\ 1 \\ -4}+t\cdot\colvec{5 \\ 1 \\ 5}
\end{equation*}
so this is the direction vector and a particular vector.
\begin{equation*}
  \vec{d}
         =\colvec{5 \\ 1 \\ 5}
  \qquad
  \vec{p}
  =\colvec{-2 \\ 1 \\ -4}
\end{equation*}
Three points on the line, associated with $t=-1, 0, 1$, are 
$(-7,0,-9)$, $Q=(-2,1,4)$, and $P=(3,2,1)$.
Three points not on the line are 
$(-6,0,-9)$, $(-3,1,4)$, and $(2,2,1)$.
Here are the parametric equations and the symmetric equations.
\begin{gather*}
  x=2+5t\quad y=1+t\quad z=-4+5t  \\
  \frac{x-2}{5}=y-1=\frac{z+4}{5}
\end{gather*}
\end{frame}

\begin{frame}{Practice}
Find the vector equation of the line through (the endpoints of)
these two.
\begin{equation*}
  \colvec{2 \\ 2 \\ -1}
  \quad
  \colvec{-3 \\ 0 \\ 4}
\end{equation*}
Give the parametric equations.
Does it pass though the point $(-2,4,-3)$?

\pause
Find the direction vector and then use any particular vector.
\begin{equation*}
  \colvec{2 \\ 2 \\ -1}
  -
  \colvec{-3 \\ 0 \\ 4}
  =
  \colvec{-5 \\ 2 \\ -5}
  \qquad
  \colvec{x \\ y \\ z}
  =
  \colvec{-3 \\ 0 \\ 4}
  +t\cdot\colvec{-5 \\ 2 \\ -5}
\end{equation*}
\pause
The components give the parametric equations.
\begin{equation*}
  x=-3-5t\quad y=2t\quad z=4-5t
\end{equation*}
To see whether the point lies on the line, we check for a~$t$ satisfying 
these three. 
\begin{equation*}
  -2=-3-5t\quad 4=2t\quad -3=4-5t
\end{equation*}
There is no such~$t$ (by the second equation, $t=2$ but that does not work
in the first equation), so the point does not lie on the line.
\end{frame}

\begin{frame}
\Ex Do these lines intersect?  
If so, find the point of intersection.
\begin{equation*}
  \colvec{x \\ y \\ z}=
  \colvec{1 \\ 2 \\ 3}+t\cdot\colvec{4 \\ 5 \\ 6}
  \qquad  
  \colvec{x \\ y \\ z}=
  \colvec{0 \\ 3 \\ -1}+s\cdot\colvec{5 \\ -2 \\ -3}
\end{equation*}

\pause
Set them equal.
\begin{align*}
  \colvec{1 \\ 2 \\ 3}+t\cdot\colvec{4 \\ 5 \\ 6}
  &=
  \colvec{0 \\ 3 \\ -1}+s\cdot\colvec{5 \\ -2 \\ -3}  \\
  \colvec{4t \\ 5t \\ 6t}-\colvec{5s \\ -2s \\ -3s}
  &=
  \colvec{-1 \\ 1 \\ -4}
\end{align*}
Gauss's method shows that they do not intersect.
\begin{equation*}
  \begin{linsys}{2}
    4t  &-  &5s  &=  &-1  \\
    5t  &+  &2s  &=  &1 \\
    6t  &+  &3s  &=  &-4  \\
  \end{linsys}
  \grstep[-(3/2)\rho_1+\rho_3]{-(5/4)\rho_1+\rho_2}
  \begin{linsys}{2}
    4t  &-  &5s       &=  &-1  \\
        &   &(33/4)s  &=  &9/4 \\
        &   &(21/2)s  &=  &-5/2  \\
  \end{linsys}
  \grstep{-(14/11)\rho_2+\rho_3}
  \begin{linsys}{2}
    4t  &-  &5s       &=  &-1  \\
        &   &(33/4)s  &=  &9/4 \\
        &   &0        &=  &-59/11  \\
  \end{linsys}
\end{equation*}
\end{frame}
% sage: load("gauss_method.sage")
% sage: P = matrix(QQ, [[4,-5], [5,2], [6,3]])
% sage: w = vector(QQ, [1,-1,4])
% sage: P_prime = P.augment(w, subdivide=True)
% sage: gauss_method(P_prime)
% [ 4 -5| 1]
% [ 5  2|-1]
% [ 6  3| 4]
%  take -5/4 times row 1 plus row 2
%  take -3/2 times row 1 plus row 3
% [   4   -5|   1]
% [   0 33/4|-9/4]
% [   0 21/2| 5/2]
%  take -14/11 times row 2 plus row 3
% [    4    -5|    1]
% [    0  33/4| -9/4]
% [    0     0|59/11]



\section{Planes}

\begin{frame}{Motivation: moving from two dimensions to three}
A line  gives the freedom to
move back and fourth in one dimension.
The equation $\vec{r}=\vec{p}+t\cdot\vec{d}$ reflects that, 
allowing us to vary the parameter~$t$.  
\begin{center}
  \vcenteredhbox{\includegraphics{lines004.pdf}}%  
\end{center}
\end{frame}


\begin{frame}
A plane gives the freedom to move in two directions. 
\begin{center}
  % \vcenteredhbox{\includegraphics{lines_3d001.pdf}}%    
  \input asy/lines_3d001.tex %
\end{center}
That is reflected by its \alert{vector equation} 
$\vec{r}=\vec{p}+t\cdot\vec{d}_1+s\cdot\vec{d_2}$
(where neither of the direction vectors is a multiple of the other).
\begin{center}
  %\vcenteredhbox{\includegraphics{lines_3d002.pdf}}% 
  \vcenteredhbox{\input asy/lines_3d002.tex }%
  \qquad
  \begin{tabular}{rl}
    \multicolumn{1}{c}{\tabulartext{Zone}} 
      &\multicolumn{1}{c}{\tabulartext{Parameters}}  \\ \hline
    $A$ &both $t$ and $s$ positive \\
    $B$ &$t$ negative, $s$ positive  \\
    $C$ &both $t,s$ negative  \\
    $D$ &$t$ positive, $s$ negative  
  \end{tabular}
\end{center}
\end{frame}


\begin{frame}{Normal vector}
A vector is \alert{normal} to the plane if it is orthogonal to
every vector in that plane.
For
$\vec{r}=\vec{p}+t\cdot\vec{d}_1+s\cdot\vec{d_2}$
one such vector is $\vec{d}_1\times\vec{d}_2$.

\Lm (\alert{Linear equation} of a plane)
Let a plane have the normal vector~$\vec{n}$.
If
$\vec{p}$ is a particular vector in that plane
then all of the vectors $\vec{r}$ in the plane  
satisfy the dot product formula $\vec{n}\dotprod(\vec{r}-\vec{p})=0$,
\begin{equation*}
  n_1\cdot(x-p_1)+n_2\cdot(y-p_2)+n_3\cdot(z-p_3)=0
\end{equation*}
where $\vec{n}$'s endpoint is $(n_1,n_2,n_3)$, 
$\vec{p}$'s endpoint is $(p_1,p_2,p_3)$,
and $\vec{r}$'s is $(x,y,z)$.
Restated,
$n_1x+n_2y+n_3z=d$, where the constant $d$ is $n_1p_1+n_2p_2+n_3p_3$.
\begin{center}
  % \vcenteredhbox{\includegraphics{lines_3d003.pdf}}%    
  \vcenteredhbox{\input asy/lines_3d003.tex }%
\end{center}
\end{frame}


\begin{frame}
\Ex 
Give a linear equation of this plane.
\begin{equation*}
  \colvec{1 \\ 1 \\ 2}+t\cdot\colvec{3 \\ 1 \\ -1}+s\cdot\colvec{1 \\ 4 \\ 2}
  =\vec{p}+t\cdot\vec{d}_1+s\cdot\vec{d}_2
\end{equation*}

\pause
Find a vector normal to the plane.
\begin{equation*}
  \vec{n}=\vec{d}_1\times\vec{d}_2=\colvec{6 \\ -7 \\ 11}
\end{equation*}
Use that vector's components to make the plane's linear equation.
\begin{equation*}
  6\cdot(x-1)-7\cdot(y-1)+11\cdot(z-2)
  =0                  
\end{equation*}
You can also multiply out to get the form
$6x-7y+11z=21$.
A slight variant is $6x-7y+11z-21=0$.
\end{frame}
% sage: d1 = vector([3,1,-1])
% sage: d2 = vector([1,4,2])
% sage: n = d1.cross_product(d2)
% sage: n
% (6, -7, 11)



\begin{frame}{Practice}
Give a linear equation for this plane.  
\begin{equation*}
  \colvec{-4 \\ 2 \\ 0}+t\cdot\colvec{1 \\ 2 \\ 1}+s\cdot\colvec{0 \\ 3 \\ -1}
\end{equation*}
Does it contain the point $(-1,2,5)$?

\pause
The normal is 
\begin{equation*}
  \vec{n}=\vec{d}_1\times\vec{d}_2=\colvec{-5 \\ 1 \\ 3}
\end{equation*}
and so this is an equation of the plane.
\begin{align*}
  -5\cdot(x+4)+1\cdot(y-2)+3\cdot z
  &=0                  \\
  -5x+1y+3z
  &=22
\end{align*}
As to the point, $-5\cdot(-1)+1\cdot 2+3\cdot 5=22$, so it is a member of the
plane.
\end{frame}
% sage: d1 = vector(QQ, [1,2,1])
% sage: d2 =  vector(QQ, [0,3,-1])
% sage: d1.cross_product(d2)
% (-5, 1, 3)




\begin{frame}
\Lm Every plane has a linear equation $n_1x+n_2y+n_3z=d$, 
and for any linear equation 
(involving a nonzero normal vector) 
the set of points $(x,y,z)$ satisfying that
equation is a plane.

\pause
We will make this argument with examples.
We have already seen how to get from a plane vector equation 
$\vec{p}+t\cdot\vec{d}_1+s\cdot\vec{d_2}$ to a linear equation. 

Here is an example going in the other direction.
Assume that a plane has $2\cdot x+3\cdot y+4\cdot z=5$. 
Solve to get
$x=(5-3y-4z)/2$. 
This is a vector equation for that plane.
\begin{equation*}
  \colvec{x \\ y \\ z}
  =\colvec{5/2 \\ 0 \\ 0}
   +y\cdot\colvec{-3/2 \\ 1 \\ 0}
   +z\cdot\colvec{-2 \\ 0 \\ 1}
\end{equation*}
\end{frame}

\begin{frame}{Practice}
\Ex Give both the vector and linear equations 
for the plane through the points $P=(1,0,-1)$,
$Q=(2,2,1)$, and $R=(4,1,2)$.

\pause
Two directions vectors for the plane are
\begin{equation*}
  \vec{d}_1=\colvec{2 \\ 2 \\ 1}-\colvec{1 \\ 0 \\ -1}=\colvec{1 \\ 2 \\ 2}
  \qquad
  \vec{d}_2=\colvec{4 \\ 1 \\ 2}-\colvec{1 \\ 0 \\ -1}=\colvec{3 \\ 1 \\ 3}
\end{equation*}
This is a vector equation.
\begin{equation*}
  \colvec{x \\ y \\ z}
  =\colvec{1 \\ 0 \\ -1}
  +t\cdot\colvec{1 \\ 2 \\ 2}
  +s\cdot\colvec{3 \\ 1 \\ 3}
\end{equation*}
The normal is this.
\begin{equation*}
  \colvec{1 \\ 2 \\ 2}\times\colvec{3 \\ 1 \\ 3}=\colvec{4 \\ 3 \\ -5}
\end{equation*}
Here is a linear equation.
\begin{equation*}
  4\cdot(x-1)+3\cdot(y)-5\cdot(z+1)
  =0
  \quad\Longrightarrow\quad
  4x+3y-5z=9
\end{equation*}
(We could have used any of the three points for the particular vector.)
\end{frame}

\begin{frame}{Practice}
\Ex Does the line lie in the plane?
\begin{equation*}
  \colvec{3 \\ 1 \\ 2}+t\cdot\colvec{1 \\ -1 \\ 0}
  \qquad
  3x+y-2z=4
\end{equation*}
\pause
Find two different points on the line.
If they are both in the plane then the whole line is in the plane.
If at least one is not in the plane then the line does not lie in the
plane.

Take $t=0$ to get this member of the line.
\begin{equation*}
  \colvec{3 \\ 1 \\ 2}
\end{equation*}
It does not satisfy the equation of the plane
\begin{equation*}
  3\cdot 3+1-2\cdot 2=6\neq 4
\end{equation*}
so the line does not lie in the plane.
\end{frame}


\begin{frame}
\Df
Two planes are \alert{parallel}
if they do not intersect.

\Lm
Two planes are parallel if and only if their normal vectors 
$\vec{n}_1$ and $\vec{n}_2$ are parallel.
If they are not parallel then they intersect in a line with direction vector
$\vec{n}_1\times\vec{n}_2$, and
the angle between them is the same as the
angle between their normal vectors.
\begin{center}
  % \vcenteredhbox{\includegraphics{lines_3d004.pdf}}%
  \vcenteredhbox{\input asy/lines_3d004.tex }%
\end{center}
\pause\vspace*{-3ex}
\begin{center}
  % \vcenteredhbox{\includegraphics{lines_3d005.pdf}}%    
  \vcenteredhbox{\input asy/lines_3d005.tex }%
\end{center}
\end{frame}


\begin{frame}{Intersecting planes}
\Ex Find the line that is the intersection of these two planes.
\begin{equation*}
  x+3y-2z=4
  \qquad
  2x-y+z=5
\end{equation*}

\pause
Use Gauss's Method on the linear system.
\begin{equation*}
  \begin{linsys}{3}
    x  &+  &3y  &-  &2z  &=  &4  \\
   2x  &-  &y   &+  &z   &=  &5  
  \end{linsys}
  \grstep{-2\rho_1+\rho_2}
  \begin{linsys}{3}
    x  &+  &3y  &-  &2z  &=  &4  \\
       &   &-7y &+  &5z  &=  &-3  
  \end{linsys}
\end{equation*}
The first row has a \alert{leading variable} of~$x$,
while in the second row the leading variable is~$y$.
A variable that does not lead a row is a \alert{free variable}. 
We can always express the leading variables in terms of those that
are free.
Work from the bottom up
\begin{equation*}
  -7y+5z=-3
  \quad\Longrightarrow\quad
  y=\frac{-3-5z}{-7}=\frac{3}{7}+\frac{5}{7}z
\end{equation*}
and back substitute.
\begin{equation*}
  x+3(\frac{3}{7}+\frac{5}{7}z)-2z=4
  \quad\Longrightarrow\quad
  x=\frac{19}{7}-\frac{1}{7}z
\end{equation*}
We have this line.
\begin{equation*}
  \colvec{x \\ y \\ z}=\colvec{19/7 \\ 3/7 \\ 0}
                      +z\cdot\colvec{-1/7 \\ 5/7 \\ 1}
\end{equation*}
\end{frame}


\begin{frame}{Practice}
\begin{enumerate}
\item Find the angle between these two planes.
  \begin{equation*}
    2x-y-3z=5 
    \qquad
    x+z=3  
  \end{equation*}

  \pause
  These are the two normals.
  \begin{equation*}
    \vec{n}_0=\colvec{2 \\ -1 \\ -3}
    \quad
    \vec{n}_1=\colvec{1 \\ 0 \\ 1}
  \end{equation*}
  We have a formula for the angle between them.
  \begin{equation*}
    \theta
    =\cos^{-1}\bigl( \frac{2\cdot 1-1\cdot 0-3\cdot 1}{\sqrt{14}\cdot\sqrt{2}} \bigr)
    =\cos^{-1}\bigl( \frac{-1}{\sqrt{28}} \bigr)
    % \approx 0.333\text{~rad}
  \end{equation*}

\pause\item
  What is the angle between parallel planes?

  \pause Parallel planes have the same normal vector, 
   so the angle is~$0$~rad.
\end{enumerate}
\end{frame}






\section{Distance from a point to a line or a point to a plane}
\begin{frame}
\Lm
The distance from a point $Q=(q_1,q_2,q_3)$ to the line 
with equation $\vec{r}=\vec{p}+t\cdot\vec{d}$ is
\begin{equation*}
  \frac{|\vec{PQ}\times\vec{d}|}{|\vec{d}|}
\end{equation*}
where $P=(p_1,p_2,p_3)$ is the endpoint of~$\vec{p}\!$.

This picture is in the plane determined by the line and the point.
\begin{center}
  \vcenteredhbox{\includegraphics{lines005.pdf}}%
\end{center}
\Pf The area of the parallelogram is $|\vec{PQ}\times \vec{d}|$.
It is also base times height, $|\vec{d}|\cdot \text{dist}$.
\end{frame}

\begin{frame}
\Lm
The distance from a point $Q=(q_1,q_2,q_3)$ 
to the plane with equation $n_1x+n_2y+n_3z+d=0$ is this.
\begin{equation*}
  \frac{|n_1q_1+n_2q_2+n_3q_3+d|}{\sqrt{n_1^2+n_2^2+n_3^2}}
\end{equation*}
We want the perpendicular distance.
\begin{center}
  % \only<1>{\vcenteredhbox{\includegraphics{lines_3d006.pdf}}}%
  % \only<2->{\vcenteredhbox{\includegraphics{lines_3d007.pdf}}}%
  \only<1>{\vcenteredhbox{\input asy/lines_3d006.tex }}%
  \only<2->{\vcenteredhbox{\input asy/lines_3d007.tex }}%
\end{center}
\only<2->{%
Fix any point~$P=(p_1,p_2,p_3)$ in the plane.
Consider the vector from~$P$ to~$Q$.}
\end{frame}

% \begin{frame}
% Fix any point~$P=(p_1,p_2,p_3)$ in the plane.
% Consider the vector that when starting at~$P$ will end at~$Q$.  
% \begin{center}
%   \vcenteredhbox{\includegraphics{lines_3d007.pdf}}%
% \end{center}
% \end{frame}

\begin{frame}
Project $\vec{PQ}$ onto a vector~$\vec{n}$ that is normal to the plane.
The desired distance is the length of that projection.
\begin{center}
  % \vcenteredhbox{\includegraphics{lines_3d008.pdf}}%
  \vcenteredhbox{\input asy/lines_3d008.tex }
\end{center}
\begin{equation*}
  \left| \operatorname{proj}_{\,\vec{n}}(\vec{PQ}) \right|
  % =\left| \frac{\vec{PQ}\dotprod\vec{n}}{\vec{n}\dotprod\vec{n}}\cdot\vec{n} \right|
  =\frac{|\vec{PQ}\dotprod\vec{n}|}{|\vec{n}|} \cdot\frac{\vec{n}}{|\vec{n}|}
\end{equation*}
The scaling factor on the unit normal is this.
\begin{align*}
  \frac{|\vec{PQ}\dotprod\vec{n}|}{|\vec{n}|}
  &=\frac{|(q_1-p_1)\cdot n_1+(q_2-p_2)\cdot n_2+(q_3-p_3)\cdot n_3|}{|\vec{n}|} \\
  &=\frac{|(q_1n_1+q_2n_2+q_3n_3) -(p_1n_1+p_2n_2+p_3n_3)|}{\sqrt{n_1^2+n_2^2+n_3^2}}
\end{align*}
Because $P$ is in the plane, $p_1n_1+p_2n_2+p_3n_3=-d$, giving the required expression.
\end{frame}


\begin{frame}{Practice}
\begin{enumerate}
\item Find the distance from the point $(2,1,3)$
to the plane $4x+3y-z=5$.

\pause\smallskip 
Rewrite the plane's equation as $4x+3y-z-5=0$ to fit the form in the 
lemma.
The normal vector is this.
\begin{equation*}
  \vec{n}=\colvec{4 \\ 3  \\-1}
\end{equation*}
The formula gives this.
\begin{equation*}
  \frac{|n_1q_1+n_2q_2+n_3q_3+d|}{\sqrt{n_1^2+n_2^2+n_3^2}}
  =\frac{|4\cdot 2+3\cdot 1-1\cdot 3-5|}{\sqrt{16+9+1}}
  =\frac{3}{\sqrt{26}}
  \approx 0.588
\end{equation*}
\end{enumerate}
\end{frame}
% sage: round( 3/sqrt(26) , ndigits=3)
% 0.588

\end{document}
%
% These lines tells gnu-emacs to typeset with the luatex engine
% which requires Unicode encoding only (utf-8)
% ^c^t^s for toggling synctex. 
% ^-Shift-Click to move from pdf to source, Command-Shift-Click on OSX
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-source-correlate-method-active: synctex
%%% coding: utf-8
%%% End:
