// lines.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for compiling in-pic text
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "lines%03d";

import graph;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// ============= Lines and planes ==============

// ............. Line ...........
real f0(real x) { return( (-1/2)*x+(5/2) ); }
picture pic;
int picnum = 0;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=3;

pair v0 = (1,2);
pair v1 = (3,1);
draw(pic, "$\vec{v}_1$", subpath((0,0)--v0,0,0.975), Arrow(ARROW_SIZE));
draw(pic, "$\vec{v}_2$", subpath((0,0)--v1,0,0.975), Arrow(ARROW_SIZE));
path f_left = graph(pic, f0, xleft-0.2, v0.x-0.05);
draw(pic, f_left, FCNPEN);
path f_right = graph(pic, f0, v1.x+0.05, xright+0.2);
draw(pic, f_right, FCNPEN);
draw(pic, "$\vec{v}_2-\vec{v}_1$", v0--v1, 3*N, FCNPEN_NOCOLOR+highlight_color, Arrow(ARROW_SIZE) );

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);


// ............. Line thru origin, same direction ...........
real f1(real x) { return( (-1/2)*x ); }
picture pic;
int picnum = 1;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=4;
ybot=-2; ytop=1;

pair v0 = (1,2);
pair v1 = (3,1);
pair d = (2,-1);
// draw(pic, "$\vec{v}_1$", subpath((0,0)--v0,0,0.975), Arrow(ARROW_SIZE));
// draw(pic, "$\vec{v}_2$", subpath((0,0)--v1,0,0.975), Arrow(ARROW_SIZE));
path f_left = graph(pic, f1, xleft-0.2, d.x-0.05);
draw(pic, f_left, FCNPEN);
path f_right = graph(pic, f1, d.x+0.05, xright+0.2);
draw(pic, f_right, FCNPEN);
draw(pic, "$\vec{d}$", (0,0)--d, S, FCNPEN_NOCOLOR+highlight_color, Arrow(ARROW_SIZE) );

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
xaxis(pic, L="",  
      axis=YEquals(ytop+0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
xaxis(pic, L="",  
      axis=YEquals(ybot-0.2),
      xmin=xleft-0.5, xmax=xright+0.5,
      p=nullpen,
      ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.2, xmax=xright+0.2,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
yaxis(pic, L="",  
      axis=XEquals(xleft-0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
yaxis(pic, L="",  
      axis=XEquals(xright+0.2),
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=nullpen,
      ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
      arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.2, ymax=ytop+0.2,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ............. Line thru origin, same direction ...........
real f2(real x) { return( (0.6)*x+0.75 ); }
picture pic;
int picnum = 2;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=f2(xright);

path f = graph(pic, f2, xleft-0.2, xright+0.2);
draw(pic, f, FCNPEN);
draw(pic, "$1$", (0,0.75)--(2.5,0.75), S);
draw(pic, "$m$", (2.5,0.75)--(2.5,f2(2.5)), E);
draw(pic, "$\vec{d}$", (0,0.75)--(2.5,f2(2.5)), 2*N, FCNPEN_NOCOLOR+highlight_color, Arrow(ARROW_SIZE) );

dotfactor = 5;
dot(pic, "$(0,b)$", (0,0.75), NW, highlight_color);

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
// xaxis(pic, L="",  
//       axis=YEquals(ytop+0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// xaxis(pic, L="",  
//       axis=YEquals(ybot-0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
// yaxis(pic, L="",  
//       axis=XEquals(xleft-0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// yaxis(pic, L="",  
//       axis=XEquals(xright+0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);




// ............. of the way function ...........
picture pic;
int picnum = 3;
size(pic,0,2.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=2.5;

pair r0 = (0.75,2);
pair r1 = (3.75,1);
draw(pic, r0--r1, FCNPEN);
real t = 0.62;
pair r = (1-t)*r0+t*r1;
dotfactor = 5;
dot(pic, "$\vec{r}_0$", r0, N);
dot(pic, "$\vec{r}_1$", r1, N);
dot(pic, "$\vec{r}$", r, N, highlight_color);

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
// xaxis(pic, L="",  
//       axis=YEquals(ytop+0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// xaxis(pic, L="",  
//       axis=YEquals(ybot-0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
// yaxis(pic, L="",  
//       axis=XEquals(xleft-0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// yaxis(pic, L="",  
//       axis=XEquals(xright+0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);






// ============== line with pos t's and neg t's ===========
real f4(real x) { return 0.5*x+1.5; }
picture pic;
int picnum = 4;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=f4(xright);

path f_left = graph(pic, f4, xleft-0.5, 1-0.1);
path f_right = graph(pic, f4, 3+0.1, xright+0.2);
draw(pic, f_left, FCNPEN);
draw(pic, f_right, FCNPEN);
draw(pic, "$\vec{d}$", (1,f4(1))--(3,f4(3)), S, FCNPEN_NOCOLOR+black, Arrow(ARROW_SIZE) );
draw(pic, "$\vec{p}$", subpath((0,0)--(1,f4(1)), 0, 0.95), SE, Arrow(ARROW_SIZE));

Label l_less = Label("$\overbrace{\rule{1cm}{0pt}}^{\text{$t$ negative}}$", highlight_color);
Label l_more = Label("$\overbrace{\rule{2.15cm}{0pt}}^{\text{$t$ positive}}$", highlight_color);
label(pic, rotate(degrees(atan(0.5)))*l_less, (-0.25,f4(-0.25)), N);
label(pic, rotate(degrees(atan(0.5)))*l_more, (1.75,f4(1.75)), N);

// x and y axes
Label L = rotate(0)*Label("{\scriptsize $x$}");
// Draw graph paper
// xaxis(pic, L="",  
//       axis=YEquals(ytop+0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// xaxis(pic, L="",  
//       axis=YEquals(ybot-0.2),
//       xmin=xleft-0.5, xmax=xright+0.5,
//       p=nullpen,
//       ticks=RightTicks("%", Step=1, OmitTick(0), extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// The x axis
xaxis(pic, L=L,  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+0.25,
      p=currentpen,
      ticks=NoTicks, // RightTicks("%", Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// y axis
Label L = rotate(0)*Label("\scriptsize $y$");
// Graph paper
// yaxis(pic, L="",  
//       axis=XEquals(xleft-0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// yaxis(pic, L="",  
//       axis=XEquals(xright+0.2),
//       ymin=ybot-0.2, ymax=ytop+0.2,
//       p=nullpen,
//       ticks=LeftTicks("%", Step=1,  extend=true, pTick=GRAPHPAPERPEN),
//       arrow=Arrows(TeXHead));
// y-axis
yaxis(pic, L=L,  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks, // LeftTicks(Step=5, step=1, OmitTick(0), Size=3pt, size=2pt),
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic);



// ============== distance from a point to a line ===========
picture pic;
int picnum = 5;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=f4(xright);

pair p = (1,0);  // particular vector
pair d = (2,0); // length of direction vector
pair q = (2,3);  // location of pt off line, Q 
path ell_left = p-(0.1,0)--(0,0);
path ell_right = (p+d+(0.1,0))--(4,0);

// draw parallelogram
filldraw(pic, p--q--(q+d)--(p+d)--cycle, gray(0.92), gray(0.5)); 

// drop a perp
draw(pic, "dist", q--(q.x,0), E, dotted);
draw(pic, p--q, highlight_color, Arrow(ARROW_SIZE));

// draw line
draw(pic, ell_left, black);
draw(pic, ell_right, black);
draw(pic, "$\vec{d}$", p--(p+d), S, highlight_color, Arrow(ARROW_SIZE));

// draw pt off line
dotfactor = 4;
dot(pic, "$Q$", q, W, highlight_color);


shipout(format(OUTPUT_FN,picnum),pic);






