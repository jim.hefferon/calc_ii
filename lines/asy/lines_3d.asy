// lines_3d.asy

// compile with asy -inlineimage stub_3d
// In the .tex file put this in the preamble ... 
// % For 3D PRC asymptote
// % compile with asy -inlineimage stub_3d
// \def\asydir{asy/}
// \graphicspath{{asy/}}
// \input asy/stub_3d.pre
// \usepackage[bigfiles]{media9}
// \RequirePackage{asymptote}
// ... and then include the 3D material with something like this.
// \begin{center}
//   \uncover<2->{\input asy/stub_3d001.tex }% need the space following .tex   
// \end{center}


cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.prc = true;
settings.embed = true;
settings.render = 0;
settings.tex="lualatex";  // for compiling in-pic text
settings.outformat="pdf";

import embed;

import fontsize;

import graph3; import solids;
currentprojection = orthographic(1.2,3,2,up=Z);
currentlight=White; defaultrender.merge=true;
// unitsize(1cm);

string OUTPUT_FN = "lines_3d%03d";

material figure_material = material(diffusepen=bold_color+white+opacity(0.5),
				 emissivepen=bold_color+white,
				 specularpen=bold_color+white);
material slice_material = material(diffusepen=gray_color+opacity(0.25),
				   emissivepen=gray_color,
				   specularpen=gray_color);
pen boundary_pen = gray(0.2)+opacity(0.5);

currentlight = nolight;

// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);


// ==== general function in 3D ====
real GENFCN_maxx=3;
real GENFCN_maxy=4;
triple GENFCN_front_left = (GENFCN_maxx,0,1.8);
triple GENFCN_front_right = (GENFCN_maxx,GENFCN_maxy,1.75);
triple GENFCN_back_right = (0,GENFCN_maxy,1.95);
triple GENFCN_back_left = (0,0,2);

path3 GENFCN_left_edge = GENFCN_back_left..(0.6*GENFCN_maxx,0,2.05)..GENFCN_front_left;
path3 GENFCN_front_edge = GENFCN_front_left..(GENFCN_maxx,0.2*GENFCN_maxy,1.7)..(GENFCN_maxx,0.3*GENFCN_maxy,1.85)..(GENFCN_maxx,0.45*GENFCN_maxy,1.72)..GENFCN_front_right;
path3 GENFCN_right_edge = GENFCN_front_right..(0.4*GENFCN_maxx,GENFCN_maxy,2.0)..GENFCN_back_right;
path3 GENFCN_back_edge = GENFCN_back_right..(0,0.65*GENFCN_maxy,2.05)..(0,0.5*GENFCN_maxy,1.85)..(0,0.3*GENFCN_maxy,1.95)..(0,0.15*GENFCN_maxy,2.1)..GENFCN_back_left;

path3 GENFCN_edge = ( GENFCN_left_edge & GENFCN_front_edge & GENFCN_right_edge & GENFCN_back_edge )--cycle;
surface GENFCN=surface(GENFCN_edge, new triple[]);


// ===== General surface in 3D  ============
// currentprojection = orthographic(8,1,0.9,up=Z);
// picture pic;
// int picnum = 0;
// size(pic,0,4cm);
// size3(pic,0,4cm,0,keepAspect=true);

// draw(pic, GENFCN_edge, boundary_pen);
// draw(pic, GENFCN, surfacepen=figure_material);
// draw(pic, GENFCN_front_edge, boundary_pen);
// draw(pic, GENFCN_right_edge, boundary_pen);

// xaxis3(pic,Label("\footnotesize $x$"),
//        0,GENFCN_maxx+0.5, black,
//        OutTicks("%",Step=1,Size=2pt),
//        Arrow3(TeXHead2));
// yaxis3(pic,Label("\footnotesize $y$"),
//        0, GENFCN_maxy+0.5, black,
//        OutTicks("%",Step=1,Size=2pt), 
//        Arrow3(TeXHead2));
// zaxis3(pic,Label("\footnotesize $z$"),
//        0,2.5, black,
//        OutTicks("%",Step=1,Size=2pt),
//        Arrow3(TeXHead2));

// shipout(format(OUTPUT_FN,picnum),pic);




// ===== equation of plane =======
currentprojection = orthographic(4,1,1,up=Z);

triple d1 = (3,1,-1);
triple d2 = (1,4,2);
triple p = (0.5,0.5,1);
triple n = cross(d1,d2);
real f1(pair z) {return (n.x*(z.x-p.x)+n.y*(z.y-p.y)-n.z*p.z)/-n.z; }
  
picture pic;
int picnum = 1;
size(pic,0,3.5cm);
size3(pic,0,3.5cm,0,keepAspect=true);

surface s=surface(f1, (-1,-1), (4,5), nx=5, Spline);
draw(pic,s, surfacepen=figure_material);
// boundary
draw(pic, (4,-1,f1((4,-1)))--(4,5,f1((4,5)))
     --(-1,5,f1((-1,5)))
     --(-1,-1,f1((-1,-1)))
     --cycle, boundary_pen );
// vectors
draw(pic, O--p, black, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{d}_1$",Relative(0.9)), p--(p+d1), highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{d}_2$",Relative(0.75)), p--(p+d2), highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

// path pln_2d = (0,0)--(4,0)--(4,4)--(0,4)--cycle;
// pair d1_2d = (3,1);
// pair d2_2d = (1,3);
// path3 pln_edge = path3(pln_2d);
// path3 d1 = path3(d1_2d);
// path3 d2 = path3(d2_2d);

// surface s = surface(pln_edge);
// draw(pic,s, surfacepen=figure_material);

// draw(pic, O--d1, Arrow3(DefaultHead2,ARROW_SIZE));
// draw(pic, O--d2, Arrow3(DefaultHead2,ARROW_SIZE));

// path f = graph(pic, f6, 1, 5);
// path3 f_3d = path3(f);

// // region that was rotated
// path r = ( (1,0)--relpoint(f,0)&f&relpoint(f,1)--(5,0)--(1,0) )--cycle;
// path3 r_3d = path3(r);
// surface base = surface(r_3d);
// draw(pic, base, surfacepen=gray(0.8)+opacity(0.5));
// draw(pic, r_3d);

// // horn
// surface horn = surface(O, f_3d, X);
// // draw end circles
// draw(pic, shift(1,0,0)*rotate(90,Y)*unitcircle3, grayed);
// draw(pic, shift(5,0,0)*rotate(90,Y)*scale(f6(5),f6(5),1)*unitcircle3, grayed);
// draw(pic, horn, surfacepen=figure_material);

// Add slice
// real x = 1+0.618*(5-1);
// real rad = f6(x);
// transform3 slice_t = shift((x,0,0))*rotate(90,Y)*scale(rad,rad,1); 
// surface slice = slice_t*unitdisk;
// draw(pic, slice, surfacepen=slice_material);
// // add a circular edge
// draw(pic, slice_t*unitcircle3, highlight_color);

xaxis3(pic,Label("$x$"),
       0,9, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,5.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
        0,4, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


// ....... include areas for plus and minus ........
picture pic;
int picnum = 2;
size(pic,0,3.5cm);
size3(pic,0,3.5cm,0,keepAspect=true);

surface s=surface(f1, (-1,-1), (4,5), nx=5, Spline);
draw(pic,s, surfacepen=figure_material);
// boundary
draw(pic, (4,-1,f1((4,-1)))--(4,5,f1((4,5)))
     --(-1,5,f1((-1,5)))
     --(-1,-1,f1((-1,-1)))
     --cycle, boundary_pen );

// vectors
draw(pic, O--p, black, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, p--(p+d1), black, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, p--(p+d2), black, Arrow3(DefaultHead2,ARROW_SIZE));

// dashed lines
draw(pic, p--(p-0.45*d1), dotted);
draw(pic, p--(p-0.35*d2), dotted);

// label the four sectors
label(pic, "$A$", (2,2,f1((2,2))), highlight_color);
label(pic, "$B$", (-0.5,1,f1((-0.5,1))), highlight_color);
label(pic, "$C$", (-0.5,-0.5,f1((-0.5,-0.5))), highlight_color);
label(pic, "$D$", (2.2,0,f1((2.5,0))), highlight_color);

xaxis3(pic,Label("$x$"),
       0,9, black, Arrow3(TeXHead2));
yaxis3(pic,Label("$y$"),
        0,5.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label("$z$"),\
        0,4, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);



// ...... Equation of a plane ..........
picture pic;
int picnum = 3;
size(pic,0,3.5cm);
size3(pic,0,3.5cm,0,keepAspect=true);

// arbitrary vector in plane
draw(pic, Label("$\vec{p}$",align=1.5*W,Relative(0.75)), O--p, black, Arrow3(DefaultHead2,ARROW_SIZE));
triple p0 = (2,3,f1((2,3)));
draw(pic, Label("$\vec{r}$",Relative(0.85)), O--p0, black, Arrow3(DefaultHead2,ARROW_SIZE));

// surface over those
surface s=surface(f1, (-1,-1), (4,5), nx=5, Spline);
draw(pic,s, surfacepen=figure_material);
// boundary
draw(pic, (4,-1,f1((4,-1)))--(4,5,f1((4,5)))
     --(-1,5,f1((-1,5)))
     --(-1,-1,f1((-1,-1)))
     --cycle, boundary_pen );

// vectors
// draw(pic, O--p, black, Arrow3(DefaultHead2,ARROW_SIZE));
// draw(pic, p--(p+d1), black, Arrow3(DefaultHead2,ARROW_SIZE));
// draw(pic, p--(p+d2), black, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, Label("$\vec{n}$",align=1.5*W,Relative(0.85)), p--(p+2*unit(n)), highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
// arbitrary vector in plane
draw(pic, p--p0, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));


xaxis3(pic,Label(""),
       0,7, black, Arrow3(TeXHead2));
yaxis3(pic,Label(""),
        0,5.5, black, Arrow3(TeXHead2));
zaxis3(pic,Label(""),\
        0,4, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);





// ====== planes intersect in a line ===========
picture pic;
int picnum = 4;
size(pic,0,3.5cm);
size3(pic,0,3.5cm,0,keepAspect=true);

// define surfaces
path3 xy_edge = (0,0,0)--(3,0,0)--(3,4,0)--(0,4,0)--cycle;
surface xy = surface(xy_edge);
transform3 p_t = shift((0,2,0))*rotate(30, X)*shift((0,-2,0));
path3 p_edge = p_t*xy_edge;
surface p = p_t*xy;

// draw surfaces
draw(pic, xy, surfacepen=figure_material);
draw(pic, xy_edge, boundary_pen);
draw(pic, p, surfacepen=figure_material);
draw(pic, p_edge, boundary_pen);

// draw line of intersection
draw(pic, (-1,2,0)--(4,2,0), Arrows3(DefaultHead2,ARROW_SIZE));

shipout(format(OUTPUT_FN,picnum),pic);

 


// ====== angle between planes ===========
picture pic;
int picnum = 5;
size(pic,0,3.5cm);
size3(pic,0,3.5cm,0,keepAspect=true);

// define surfaces
real rotation_degs = 30;
path3 xy_edge = (0,0,0)--(3,0,0)--(3,4,0)--(0,4,0)--cycle;
surface xy = surface(xy_edge);
transform3 p_t = shift((0,2,0))*rotate(rotation_degs, X)*shift((0,-2,0));
path3 p_edge = p_t*xy_edge;
surface p = p_t*xy;

// draw surfaces
draw(pic, xy, surfacepen=figure_material);
draw(pic, xy_edge, boundary_pen);
draw(pic, p, surfacepen=figure_material);
draw(pic, p_edge, boundary_pen);

// draw intersection
draw(pic, (0,2,0)--(3,2,0), boundary_pen);

// normals
path3 n = (0,0,0)--(0,0,1.5);
path3 n_xy = shift((3,2,0))*n;
path3 n_p = p_t*shift((3,2,0))*n;
draw(pic, n_xy, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, n_p, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

// angle arc
triple v1 = (0,0,0.4); // determine radius
triple v2 = rotate(rotation_degs,X)*(v1);
path3 n_angle_arc = arc(O, v1, v2);
draw(pic, "$\theta$", shift((3,2,0))*n_angle_arc, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE/2)); // angle between normal vectors
draw(pic, "$\theta$", shift((3,2,0))*rotate(-90,X)*n_angle_arc, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE/2)); // angle between planes

shipout(format(OUTPUT_FN,picnum),pic);


 


// ====== distance from a point to a plane ===========

// ....... point and the perp distance to the plane .........
picture pic;
int picnum = 6;
size(pic,0,3.5cm);
size3(pic,0,3.5cm,0,keepAspect=true);

// define plane
real rotation_x_degs = 15;
real rotation_y_degs = 20;
path3 xy_edge = (0,0,0)--(3,0,0)--(3,4,0)--(0,4,0)--cycle;
surface xy = surface(xy_edge);
transform3 pl_t = shift((0,1,0))*rotate(rotation_y_degs, Y)*rotate(rotation_x_degs, X)*shift((0,-1,0));
path3 pl_edge = pl_t*xy_edge;
surface pl = pl_t*xy;

// define vectors
transform3 vec_t = pl_t*shift((1,1,0));
triple q = vec_t*(1,2,3);
triple r0 = vec_t*(1,2,0);
triple p = vec_t*(0,0,0);

// draw surfaces
draw(pic, pl, surfacepen=figure_material);
draw(pic, pl_edge, boundary_pen);

// draw vectors
label(pic, "$Q$", q, W, highlight_color);
draw(pic, shift(q)*scale3(0.04)*unitsphere, highlight_color);
// label(pic, "$P$", p, NW, highlight_color);
// draw(pic, Label("$\vec{PQ}$",align=W,Relative(0.5)), p--q, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, q--r0, dotted);
real eps = 0.25;
path3 rt_angle = vec_t*((1-eps,2-eps,0)--(1-eps,2-eps,eps)--(1,2,eps));
draw(pic, rt_angle);

// xaxis3(pic,Label(""),
//        0,7, black, Arrow3(TeXHead2));
// yaxis3(pic,Label(""),
//         0,5.5, black, Arrow3(TeXHead2));
// zaxis3(pic,Label(""),
//         0,4, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);

// ....... vector and the perp distance to the plane .........
picture pic;
int picnum = 7;
size(pic,0,3.5cm);
size3(pic,0,3.5cm,0,keepAspect=true);

// define plane
real rotation_x_degs = 15;
real rotation_y_degs = 20;
path3 xy_edge = (0,0,0)--(3,0,0)--(3,4,0)--(0,4,0)--cycle;
surface xy = surface(xy_edge);
transform3 pl_t = shift((0,1,0))*rotate(rotation_y_degs, Y)*rotate(rotation_x_degs, X)*shift((0,-1,0));
path3 pl_edge = pl_t*xy_edge;
surface pl = pl_t*xy;

// define vectors
transform3 vec_t = pl_t*shift((1,1,0));
triple q = vec_t*(1,2,3);
triple r0 = vec_t*(1,2,0);
triple p = vec_t*(0,0,0);

// draw surfaces
draw(pic, pl, surfacepen=figure_material);
draw(pic, pl_edge, boundary_pen);

// draw vectors
label(pic, "$Q$", q, W, highlight_color);
label(pic, "$P$", p, NW, highlight_color);
draw(pic, Label("$\vec{PQ}$",align=W,Relative(0.5)), p--q, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));
draw(pic, q--r0, dotted);
real eps = 0.25;
path3 rt_angle = vec_t*((1-eps,2-eps,0)--(1-eps,2-eps,eps)--(1,2,eps));
draw(pic, rt_angle);

// xaxis3(pic,Label(""),
//        0,7, black, Arrow3(TeXHead2));
// yaxis3(pic,Label(""),
//         0,5.5, black, Arrow3(TeXHead2));
// zaxis3(pic,Label(""),
//         0,4, black, Arrow3(TeXHead2));

shipout(format(OUTPUT_FN,picnum),pic);


// ....... add normal vector .........
picture pic;
int picnum = 8;
size(pic,0,3.5cm);
size3(pic,0,3.5cm,0,keepAspect=true);

// define plane
real rotation_x_degs = 15;
real rotation_y_degs = 20;
path3 xy_edge = (0,0,0)--(3,0,0)--(3,4,0)--(0,4,0)--cycle;
surface xy = surface(xy_edge);
transform3 pl_t = shift((0,1,0))*rotate(rotation_y_degs, Y)*rotate(rotation_x_degs, X)*shift((0,-1,0));
path3 pl_edge = pl_t*xy_edge;
surface pl = pl_t*xy;

// define vectors
transform3 vec_t = pl_t*shift((1,1,0));
triple q = vec_t*(1,2,3);
triple r0 = vec_t*(1,2,0);
triple p = vec_t*(0,0,0);

// draw surfaces
draw(pic, pl, surfacepen=figure_material);
draw(pic, pl_edge, boundary_pen);

// draw point
// label(pic, "$Q$", q, W, highlight_color);
// label(pic, "$P$", p, W, highlight_color);
draw(pic, Label("$\vec{PQ}$",align=W, Relative(0.75)), p--q, black, Arrow3(DefaultHead2,ARROW_SIZE));

// normal
path3 n_origin = (0,0,0)--(0,0,4);
path3 proj_origin = (0,0,0)--(0,0,q.z);
path3 n = vec_t*shift((1.2,2.5,0))*n_origin;
path3 proj_origin = (0,0,0)--(0,0,q.z);
path3 proj = vec_t*shift((1.2,2.5,0))*proj_origin;
draw(pic,  Label("$\vec{n}$",align=1.5*E,Relative(0.85)), n, black, Arrow3(DefaultHead2,ARROW_SIZE));

// right angle sign
real eps = 0.25;
path3 rt_angle = vec_t*shift((1.2,2.5,0))*((-eps,-eps,0)--(-eps,-eps,eps)--(0,0,eps));
draw(pic, rt_angle);
draw(pic,  Label("$\operatorname{proj}_{\vec{n}}(\vec{PQ})$",align=E,Relative(0.75)), proj, highlight_color, Arrow3(DefaultHead2,ARROW_SIZE));

shipout(format(OUTPUT_FN,picnum),pic);



